#!/bin/bash
####################################################################
# This script executes mpiLatency.o with multiple arguments
# the outputs are logged for further investigation
# created by: han
# date: 17 Feb 2014
####################################################################
VECT_SIZE=(1000 10000 100000 500000 1000000 5000000 10000000 20000000 100000000)
SAMPLE_SIZE=100

#NOW=$(date + "%d_%m_%Y")
#LOGFILE="log_$NOW.log"
LOGFILE="mpi_latency_bm.txt"

if [ ! -f $LOGFILE ]; then
	touch $LOGFILE
fi

for i in "${VECT_SIZE[@]}"
do
	mpirun -np 2 -hostfile hostfileSC ../mpiLatency.o $i $SAMPLE_SIZE >> $LOGFILE
done
