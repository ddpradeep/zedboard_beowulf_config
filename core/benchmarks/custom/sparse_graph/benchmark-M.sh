#!/bin/zsh

rm -f benchmark-M.csv
for NP in {1..14}; do
	for M in 100000 200000 400000 800000 1000000; do
		for N in 1000000; do
			make run NP=$NP REPEAT=10 M=$M N=$N | tail -n 1 >> benchmark-M.csv | cat
		done
	done
done
