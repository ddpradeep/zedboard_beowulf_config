#!/bin/bash

WORKLOAD_FILE="workload.txt"
OUTPUT_FILE="result/output.txt"
START_LINE_COUNT=1

# rm -rf $OUTPUT_FILE

TOTAL_LINES=0
while read -r line
do
	(( TOTAL_LINES+=1 ))
done < "$WORKLOAD_FILE"

for ((line=START_LINE_COUNT; line<TOTAL_LINES; line++))
do
	CMD=$(sed -n "${line}p" "$WORKLOAD_FILE")
    echo "LINE-$line: $CMD"
	CMD_OUTPUT=$($CMD)
	CMD_OUTPUT=$(echo "$CMD_OUTPUT" | tail -1)
	echo "$CMD_OUTPUT"
	echo $CMD_OUTPUT >> $OUTPUT_FILE
done