/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 

#define data_tt float
#define mpi_data_t MPI_FLOAT

// #define DEBUG

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iterator>
#include <vector>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include "graph_support.h"
#include "helper.h"
#include "build_mpi_structs.h"

#define TAG_SEND_T	1
#define TAG_RECV_T	2

#define SEND_TAG_OFFSET 1000
#define RECV_TAG_OFFSET 0

#define PARALLEL_ALL_MPI		0
#define PARALLEL_LOCAL_DIRECT	1
#define SEQUENTIAL_ALL_MPI		2
#define SEQUENTIAL_LOCAL_DIRECT	3

#define PROFILE_TOGETHER 		0
#define PROFILE_SEPARATELY 		1

using namespace std;

int main (int argc, char *argv[]) 
{
	// stick_this_thread_to_core(PRIMARY_CORE);
	init_rand();
	int PE;					// total number of processes	
	int rank;				// rank of process
	int num_nodes;			// number of graph nodes in this process
	int num_in_edges;		// number of incoming edges to this process
	int not_used_val;

	/******************************************************************************/
	//                           MPI Initialization
	/******************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;

	MPI_Init (&argc, &argv);		// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &PE);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// get current process rank
	
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %d running on %s\n", rank, processor_name);
	/******************************************************************************/

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	// 1. Get the parameters from input
	if (argc != 8)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<graph file>\n\t<repeat_count>\n\t<reset cache {1,0}>\n\t<{parallel all mpi(0), parallel local direct(1), sequential all mpi(2), sequential local direct(3)}>\n\t<local/remote separate profiling {0,1}>\n\t<fine grained scatter {1,0}>\n\t<overload single node {1,0}>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	data_tt *node_memory;
	data_tt *message_memory;	

	Graph g = get_graph_from_sparse_matrix(argv[1]);
	int repeat_count = atoi(argv[2]);
	int reset_cache = atoi(argv[3]);
	int scatter_type = atoi(argv[4]);
	int profile_local_rma_separately = atoi(argv[5]);
	int fine_grained_scatter = atoi(argv[6]);
	int overload_single_node = atoi(argv[7]);

	scatter_type = profile_local_rma_separately==PROFILE_SEPARATELY? PARALLEL_LOCAL_DIRECT : scatter_type; // if profiled separately, local scatter is always done using direct method

	if (scatter_type == SEQUENTIAL_ALL_MPI || scatter_type == SEQUENTIAL_LOCAL_DIRECT)
	{
		profile_local_rma_separately = PROFILE_TOGETHER;
		fine_grained_scatter = 0;
	}

	int pe_x = PE;
	int pe_y = 1;
	int partitions = pe_x*pe_y;
	assert(partitions==PE);

	float *scatterTime 	= new float[repeat_count];
	float *sepRMATime 	= new float[repeat_count];			// Only used when requested to measure rma timing separately

	/******************************************************************************/
	//                        		Build Graph
	/******************************************************************************/

	disp_memory_t disp_memory; // structure to contain the send and recv disp for all PEs

	if (rank == MASTER)
	{
		int* part = (int*)malloc(num_vertices(g)*sizeof(int));
		if(false)
		{
			if(true) //(hmetis_or_patoh==0)
			{
				debug ("Using hmetis\n");
				partition_graph_with_hmetis(g, partitions);
				read_hmetis_partition(partitions, part);
			}
			else
			{
				debug ("Using patoh\n");
				partition_graph_with_patoh(g, partitions);
				read_patoh_partition(partitions, part);
			}
		}
		else 
		{
			for(int i=0;i<num_vertices(g);i++)
			{
				int p = rand_between(0, PE);
				part[i] = p;
			}
		}
		annotate_graph_node_with_partition_id(g, part);
		debug ("Annotated graph\n");
		graph_memory_t* graph_memory = get_gmimage_from_graph(g, partitions, pe_x, pe_y, part);
		debug ("Graph parsed\n");
		build_mpi_structures(graph_memory, &disp_memory, partitions, pe_x, pe_y);

	}

	/******************************************************************************/
	//                        	Allocate Send/Recv Buffers
	/******************************************************************************/

	MPI_Scatter(disp_memory.num_nodes, 1, MPI_INT, &num_nodes, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	MPI_Scatter(disp_memory.num_in_edges, 1, MPI_INT, &num_in_edges, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	
	debug ("PE%d: Num Nodes: %d\tNum In Edges: %d\n", rank, num_nodes, num_in_edges);

	node_memory = new data_tt[num_nodes];
	message_memory = new data_tt[num_in_edges];

	for (int i=0; i<num_nodes; i++)
		node_memory[i] = rand_between(0, 100);	// assign random values
	memset (message_memory, 0, num_in_edges);

	/******************************************************************************/
	//						Average Outbound Traffic
	/******************************************************************************/

	long avg_send_count;

	if (rank == MASTER)
	{
		int *total_send_count = new int [PE];

		// for every node
		for (int i=0; i<PE; i++)
		{
			total_send_count[i] = 0;
			// for every other node (includeing self)
			for (int j=0; j<PE; j++)
				// count the number of outgoing edges
				total_send_count[i] += disp_memory.send_count[i][j];

			debug ("PE%d: Num Out Edges: %d\n", rank, total_send_count[i]);
		}

		// calculate average outbound traffic
		avg_send_count = 0;
		for (int i=0; i<PE; i++)
			avg_send_count += total_send_count[i];
		avg_send_count /= PE;

		delete [] total_send_count;

		debug ("Avg_Send_cnt: %d\n", avg_send_count);
	}

	/******************************************************************************/
	//                        Distribute Send/Recv Displacements
	/******************************************************************************/

	// initialize mpi_memory data structures
	mpi_memory_t mpi_memory;
	mpi_memory.send_addr = new int* [PE];
	mpi_memory.recv_addr = new int* [PE];
	mpi_memory.send_count = new int [PE];
	mpi_memory.recv_count = new int [PE];

	if (rank == MASTER)
	{
		// printf ("Preparing to send...\n");
		// for every node
		for (int src=0; src<PE; src++)
		{
			// for number of nodes
			for (int dst=0; dst<PE; dst++)
			{
				if (src==MASTER)
				{
					// copy send/recv pair: Node[src] -> Node[dst] to self
					mpi_memory.send_addr[dst]  = new int [disp_memory.send_count[src][dst]];
					mpi_memory.send_count[dst] = disp_memory.send_count[src][dst];
					memcpy(mpi_memory.send_addr[dst], disp_memory.send_addr[src][dst], disp_memory.send_count[src][dst]*sizeof(int));
					debug ("rank-%d> SEND[%d, %d] of size %d\n", rank, rank, dst, mpi_memory.send_count[dst]);
				}
				else
				{
					// debug ("Sending SEND_T to %d\n", src);
					// send WHAT_IS_TRANSFERRED info to slave
					// SEND_OFFSET + DST encoded in tag
					MPI_Send(disp_memory.send_addr[src][dst], disp_memory.send_count[src][dst], MPI_INT, src, SEND_TAG_OFFSET+dst, MPI_COMM_WORLD);
					// debug ("Sent SEND_T to %d\n", src);
				}
				if (dst==MASTER)
				{
					mpi_memory.recv_addr[src]  = new int [disp_memory.recv_count[src][dst]];
					mpi_memory.recv_count[src] = disp_memory.recv_count[src][dst];
					memcpy(mpi_memory.recv_addr[src], disp_memory.recv_addr[src][dst], disp_memory.recv_count[src][dst]*sizeof(int));
					debug ("rank-%d> RECV[%d, %d] of size %d\n", rank, rank, src, mpi_memory.recv_count[src]);
				}
				else
				{
					// transmit send/recv pair: Node[src] -> Node[dst] to Node[src]
					// debug ("Sending RECV_T to %d\n", dst);
					MPI_Send(disp_memory.recv_addr[src][dst], disp_memory.recv_count[src][dst], MPI_INT, dst, RECV_TAG_OFFSET+src, MPI_COMM_WORLD);
					// debug ("Sent RECV_T to %d\n", dst);
				}
			}
		}

		// free up disp_memory
		free_disp_memory_t(&disp_memory, PE);
	}
	else
	{
		// for number of nodes
		for (int count=0; count<(PE*2); count++)
		{
			MPI_Status status;
			int incoming_msg_size;
			// receive send/recv pair for Node[rank] -> Node[dst] from Master

			// i. probe incoming message
			// debug ("rank-%d> probing for something from %d\n", rank, MASTER);
			MPI_Probe(MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			// debug ("rank-%d> probing success\n", rank);
			MPI_Get_count(&status, MPI_INT, &incoming_msg_size);
			if (status.MPI_TAG >= SEND_TAG_OFFSET)
			{
				// debug ("rank-%d> SEND_T incoming", rank);
				int dst = status.MPI_TAG - SEND_TAG_OFFSET;
				// ii. allocate buffer of probed size
				mpi_memory.send_addr[dst]  = new int [incoming_msg_size];
				mpi_memory.send_count[dst] = incoming_msg_size;
				// iii. receive send type for Node[rank] -> Node[dst]
				MPI_Recv(mpi_memory.send_addr[dst], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d> SEND[%d, %d] of size %d\n", rank, rank, dst, mpi_memory.send_count[dst]);
			
			}
			else
			{
				// debug ("rank-%d> RECV_T incoming", rank);
				int src = status.MPI_TAG;
				// ii. allocate buffer of probed size
				mpi_memory.recv_addr[src]  = new int [incoming_msg_size];
				mpi_memory.recv_count[src] = incoming_msg_size;
				// iii. receive recv type for Node[rank] <- Node[src]
				MPI_Recv(mpi_memory.recv_addr[src], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d> RECV[%d, %d] of size %d\n", rank, rank, src, mpi_memory.recv_count[src]);
			}
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);

	/******************************************************************************/
	//      Optimize Send/Recv Displacements to Maximize Receive Block Length
	/******************************************************************************/

	mpi_memory.send_blklen 	= new int* [PE];
	mpi_memory.recv_blklen 	= new int* [PE];

	// debug ("rank-%d> pass\n", rank);

	for (int i=0; i<PE; i++)
	{
		mpi_memory.send_blklen[i] = new int [mpi_memory.send_count[i]];
		for (int j=0; j<mpi_memory.send_count[i]; j++)
			mpi_memory.send_blklen[i][j] = 1;
		mpi_memory.recv_blklen[i] = new int [mpi_memory.recv_count[i]];
		for (int j=0; j<mpi_memory.recv_count[i]; j++)
			mpi_memory.recv_blklen[i][j] = 1;

		// debug ("rank-%d> done %d/%d\n", rank, i, PE);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	// debug ("%d - done\n", rank);	

	/******************************************************************************/
	//                        Build Send/Recv MPI Datatypes
	/******************************************************************************/

	mpi_memory.send_t 		= new MPI_Datatype [PE];
	mpi_memory.recv_t 		= new MPI_Datatype [PE];

	// // allocate block length vector of largest disp length
	// int max_disp_length = 0;
	// for (int j=0; j<PE; j++)
	// {
	// 	if (mpi_memory.send_count[j]>max_disp_length)
	// 		max_disp_length = mpi_memory.send_count[j];
	// 	if (mpi_memory.recv_count[j]>max_disp_length)
	// 		max_disp_length = mpi_memory.recv_count[j];
	// }
	// mpi_memory.block_len = new int [max_disp_length];
	// for (int i=0; i<max_disp_length; i++)
	// 	mpi_memory.block_len[i] = 1;


	for (int j=0; j<PE; j++)
	{
		// build send type
		MPI_Type_indexed (
			mpi_memory.send_count[j],
			mpi_memory.send_blklen[j],
			mpi_memory.send_addr[j],
			mpi_data_t,
			&mpi_memory.send_t[j]);

		MPI_Type_commit(&mpi_memory.send_t[j]);

		// build recv type
		MPI_Type_indexed (
			mpi_memory.recv_count[j],
			mpi_memory.recv_blklen[j],
			mpi_memory.recv_addr[j],
			mpi_data_t,
			&mpi_memory.recv_t[j]);

		MPI_Type_commit(&mpi_memory.recv_t[j]);
	}

	/*******************************************************************************/
	//                          Initialize RMA Epoch
	/*******************************************************************************/

	// MPI_Win *win = new MPI_Win [PE];

	// // create the windows for remote and local access
	// for (int i=0; i<PE; i++)
	// {
	// 	if (rank == i)
	// 	{
	// 		MPI_Win_create(message_memory, num_in_edges*sizeof(data_tt), sizeof(data_tt), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
	// 	}
	// 	else
	// 	{
	// 		MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
	// 	}
	// }

	MPI_Barrier (MPI_COMM_WORLD);
	// debug ("%d - created data types\n", rank);	

	/*******************************************************************************/
	//                        		Scatter
	/*******************************************************************************/

	TIME_DATA_TYPE timer, sepTimer;

	switch (profile_local_rma_separately)
	{
		case PROFILE_TOGETHER:
			switch (scatter_type)
			{
				case PARALLEL_LOCAL_DIRECT:
					for (int i=0; i<repeat_count; i++)
				    {
				    	if (reset_cache)
				    		not_used_val += flush_cache(10);
				    	MPI_Barrier (MPI_COMM_WORLD);

				    	assert (mpi_memory.send_count[rank]==mpi_memory.recv_count[rank]); // Local scatter cant handle this case

				    	resetTime(&timer);
				    	// I. Perform local scatter //

				    	for (uint32_t disp=0; disp<mpi_memory.send_count[rank]; disp++)
						{
							message_memory[mpi_memory.recv_addr[rank][disp]] = node_memory[mpi_memory.send_addr[rank][disp]];
						}

						// II. Perform remote scatter //

						// // 1. Lock the remote windows
						// for (int j=0; j<PE; j++)
						// {
						// 	int k = overload_single_node==1? j:((j+rank) % PE);
						// 	if (k==rank) continue;
						// 	MPI_Win_lock(MPI_LOCK_SHARED,k,0,win[k]);
						// }

						// printf ("%d - local scatter done\n", rank);	

						// 2. Perform Scatter
						switch (fine_grained_scatter)
						{
							case 0:
								for (int base=PE-1, j=1; j<PE; j++)
								{
									int toNode = mod(rank+j, PE);
									int fromNode = mod(base+rank-(j-1), PE);
									debug ("%d:%d\t[%d]->[%d]->[%d]\n", rank, j, fromNode, rank, toNode);
									MPI_Sendrecv (node_memory, 1, mpi_memory.send_t[toNode], toNode, 100, message_memory, 1, mpi_memory.recv_t[fromNode], fromNode, 100, MPI_COMM_WORLD, &status);
									// MPI_Put(node_memory, 1, mpi_memory.send_t[toNode], toNode, 0, 1, mpi_memory.recv_t[toNode], win[toNode]);
								}
								break;
							case 1:
								break;
						}

					// 	// 3. Unlock the remote windows
					// 	for (int j=0; j<PE; j++)
					// 	{
					// 		int k = overload_single_node==1? j:((j+rank) % PE);
					// 		if (k==rank) continue;
					// 		MPI_Win_unlock(k, win[k]);
					// 	}

						scatterTime[i] = getTime(&timer);
					}
					break;
			}
			break;
		// case PROFILE_SEPARATELY:
		// 	for (int i=0; i<repeat_count; i++)
		//     {
		//     	if (reset_cache)
		//     		not_used_val += flush_cache(10);
		//     	MPI_Barrier (MPI_COMM_WORLD);

		//     	assert (mpi_memory.send_count[rank]==mpi_memory.recv_count[rank]); // Local scatter cant handle this case

		//     	resetTime(&timer);
		//     	// I. Perform local scatter //

		//     	for (uint32_t disp=0; disp<mpi_memory.send_count[rank]; disp++)
		// 		{
		// 			message_memory[mpi_memory.recv_addr[rank][disp]] = node_memory[mpi_memory.send_addr[rank][disp]];
		// 		}

		// 		resetTime(&sepTimer);
		// 		// II. Perform remote scatter //

		// 		// 1. Lock the remote windows
		// 		// for (int j=0; j<PE; j++)
		// 		// {
		// 		// 	int k = overload_single_node==1? j:((j+rank) % PE);
		// 		// 	if (k!=rank)
		// 		// 		MPI_Win_lock(MPI_LOCK_SHARED,k,0,win[k]);
		// 		// }

		// 		// 2. Perform Scatter
		// 		switch (fine_grained_scatter)
		// 		{
		// 			case 0:
		// 				for (int j=0; j<PE; j++)
		// 				{
		// 					int toNode = overload_single_node==1? j:((j+rank) % PE);
		// 					if (toNode==rank) continue;
		// 					MPI_Put(node_memory, 1, mpi_memory.send_t[toNode], toNode, 0, 1, mpi_memory.recv_t[toNode], win[toNode]);
		// 				}
		// 				break;
		// 			case 1:
		// 				break;
		// 		}

		// 		// // 3. Unlock the remote windows
		// 		// for (int j=0; j<PE; j++)
		// 		// {
		// 		// 	int k = overload_single_node==1? j:((j+rank) % PE);
		// 		// 	if (k!=rank)
		// 		// 		MPI_Win_unlock(k, win[k]);
		// 		// }

		// 		sepRMATime[i] = getTime(&sepTimer);
		// 		scatterTime[i] = getTime(&timer);
		// 	}
		// 	break;
	}
	
	MPI_Barrier(MPI_COMM_WORLD);

	/*******************************************************************************/
	//       Paranoid Code to Prevent Removal of Crucial Operations by O3 Opti
	/*******************************************************************************/
	for (int i=0; i<num_in_edges; i++)
		not_used_val += message_memory[i];

	/*******************************************************************************/
	//                      		Print Results
	/*******************************************************************************/

	if (rank == MASTER)
	{
		// 1. Initialize buffers for receving time data from all processes
		float *scatterTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * PE);
		float *sepRMATime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * PE);


		// 2. Receive time data from all processes
		for (int i=0; i<repeat_count; i++)
		{
			scatterTime_buffer[i] 	= scatterTime[i];
			sepRMATime_buffer[i]	= sepRMATime[i];
		}

		for (int i=1; i<PE; i++)
		{
			MPI_Recv (scatterTime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
			MPI_Recv (sepRMATime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
		}

		// 2. Sort the recorded timings
		sort_multiple (scatterTime, scatterTime_buffer, PE, repeat_count, SORT_MAX, 0);
		sort_multiple (sepRMATime, sepRMATime_buffer, PE, repeat_count, SORT_MAX, 0);

		// 3. Get avg of each time
		float scatterTimeMax	= scatterTime[repeat_count/2];
		float sepRMATimeMax		= sepRMATime[repeat_count/2];

		float scatterSpeedMin	= (float)(sizeof(data_tt)*avg_send_count*(PE))/(scatterTimeMax*1048576.0);

		printf ("%s, ignore{%d}, PE Count: %d, AvgOutBoundScatterPerPE: %ld, ResetCache: %s, ScatterType: %s, ProfileSeparately: %s, FineGrained: %s, Overload: %s, Local: %f ms, Total: %f ms, %.2f MB/s\n", 
			argv[1], 
			not_used_val, 
			PE, 
			avg_send_count,
			reset_cache==1?"YES":"NO", 
			scatter_type==PARALLEL_ALL_MPI?"PARALLEL_ALL_MPI":(scatter_type==PARALLEL_LOCAL_DIRECT?"PARALLEL_LOCAL_DIRECT":(scatter_type==SEQUENTIAL_ALL_MPI?"SEQUENTIAL_ALL_MPI":"SEQUENTIAL_LOCAL_DIRECT")), 
			profile_local_rma_separately==1?"YES":"NO", 
			fine_grained_scatter==1?"YES":"NO", 
			overload_single_node==1?"YES":"NO", 
			(scatterTimeMax - sepRMATimeMax)*1000.0,
			scatterTimeMax*1000.0,
			scatterSpeedMin);
	}
	else
	{
		MPI_Send (scatterTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
		MPI_Send (sepRMATime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	}


	/****************************************************************************/
	//                    				Finalize
	/****************************************************************************/
	// // free the windows
	// for (int i=0; i<PE; i++)
	// {
	// 	MPI_Win_free(&win[i]);
	// 	// MPI_Type_free (&mpi_memory.send_t[i]);
	// 	// MPI_Type_free (&mpi_memory.recv_t[i]);
	// }
	// MPI_Barrier(MPI_COMM_WORLD);

	// delete [] win;

	// // free data buffers
	// free_mpi_memory_t(&mpi_memory, PE);
	// delete [] node_memory;
	// delete [] message_memory;

	MPI_Finalize ();

	return 0;
}
