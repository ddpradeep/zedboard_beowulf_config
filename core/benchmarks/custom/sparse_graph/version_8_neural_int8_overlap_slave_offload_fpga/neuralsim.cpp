#include "mpiscatter.h"
#include "neuralsim.h"
#include "fpga_stream.h"
#include "omp.h"

#include <thread>
#include <mutex>

#define CORE_ID_MAIN	0
#define CORE_ID_COMPUTE	0
#define CORE_ID_SCATTER	1

// neural variables

BOOL* fired;
BOOL* fired_old;

REAL* fpga_stream;
REAL* fpga_stream_old;

// mpi variables
MPIScatter *mpiscatter;
int *col_index;

// fpga variables
FPGA *PL;

// thread variables
mutex loop_mutex;
int shared_loop_start_ctr = 0;
int shared_loop_end_ctr = 0;
int shared_loop_swap_ctr = 0;
void compute (int M, int repeat_count, double *time_compute_comms, double *time_compute_post_process);
void scatter (double repeat_count, double *time_scatter);


int main(int argc, char** argv)
{
	stick_this_thread_to_core (CORE_ID_MAIN);

	mpiscatter = new MPIScatter(&argc, &argv);	// MPI init

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                        Generate and Distribute Graph
	/******************************************************************************/

	mpiscatter->build_random_graph(M, N);
	mpiscatter->distribute_graph_structure();
	int total_nodes = M;
	int total_edges = N;
	M = mpiscatter->get_num_nodes();		// update M to reflect num nodes in this PE
	N = mpiscatter->get_num_in_edges();	// update N to reflect num incomming edges in this PE
	int transfer_size = max(M, N);

	/******************************************************************************/
	//                           Initialize Variables
	/******************************************************************************/

	fpga_stream 	= new REAL [transfer_size];
	fpga_stream_old = new REAL [transfer_size];

	fired = (BOOL*)malloc(M*sizeof(BOOL));
	fired_old = (BOOL*)malloc(M*sizeof(BOOL));

	// get edges to node mapping array (col_index)
	col_index = mpiscatter->get_edges_to_node_mapping_addr();

	// initialize all variables (excpet incoming edges)
	for(int m=0;m<M;m++)
		fired[m] = fired_old[m] = rand()%2;

	for(int m=0, edges_before_this_node=0;m<transfer_size;m++)
		fpga_stream [m] = rand()%2;

	// duplicate the array for overlapped processing
	memcpy (fpga_stream_old, fpga_stream, sizeof(REAL)*(transfer_size));

	//	set the node_memory buffer to point to 'fired (old)'
	mpiscatter->attach_node_memory (fired_old);
	mpiscatter->attach_message_memory (fpga_stream);

	/******************************************************************************/
	//                        		Neural Simulation
	/******************************************************************************/

	TIMER timer1, timer2;
	double *time_compute_comms = new double [repeat_count];
	double *time_compute_post_process = new double [repeat_count];
	double *time_scatter = new double [repeat_count];
	double *time_total = new double [repeat_count];

	thread compute_thread (compute, M, repeat_count, time_compute_comms, time_compute_post_process);
	thread scatter_thread (scatter, repeat_count, time_scatter);

	// record timings
	for (int i=0; i<repeat_count; i++)
	{
		resetTime(&timer1);

		// wait for all threads to reach this	
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		// resetTime(&timer2);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		time_total[i] = max ((double)(time_compute_comms[i]+time_compute_post_process[i]), time_scatter[i]);
		// time_total[i] = time_total[i] - (getTime(&timer1) - time_total[i]);

		swap (fired, fired_old);
		swap (fpga_stream, fpga_stream_old);
		mpiscatter->attach_node_memory (fired_old);
		mpiscatter->attach_message_memory (fpga_stream);
		if (mpiscatter->get_rank() == MASTER)
		{
			if (i%10==0)
				printf ("%d", i);
			else
				printf (".");
		}

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
		// printf ("%d rank - %d done\n", mpiscatter->get_rank(), i);
	}

	compute_thread.join();
	scatter_thread.join();

	// printf ("%d - here M = %d\n", mpiscatter->get_rank(), M);
	// paranoid code
	BOOL not_used_val = 0;
	for (int i=0; i<M; i++)
		not_used_val += fired[i];

	/******************************************************************************/
	//                        			Results
	/******************************************************************************/

	if (mpiscatter->get_rank() == MASTER)
	{
		delete [] time_compute_comms, time_compute_post_process, time_scatter, time_total;
		time_compute_comms 			= new double [mpiscatter->get_total_procs()];
		time_compute_post_process 	= new double [mpiscatter->get_total_procs()];
		time_scatter 				= new double [mpiscatter->get_total_procs()];
		time_total 					= new double [mpiscatter->get_total_procs()];
		double *time_total_min		= new double [mpiscatter->get_total_procs()];
		double *time_total_max		= new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_ub_MBps = new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_lb_MBps = new double [mpiscatter->get_total_procs()];

		// get compute time from nodes
		for (int pe=0; pe<mpiscatter->get_total_procs(); pe++)
		{
			MPI_Status status;
			MPI_Recv (time_compute_comms+pe, 1, MPI_DOUBLE, pe+1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv (time_compute_post_process+pe, 1, MPI_DOUBLE, pe+1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (time_scatter+pe, 1, MPI_DOUBLE, pe+1, 2, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total+pe, 1, MPI_DOUBLE, pe+1, 3, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total_min+pe, 1, MPI_DOUBLE, pe+1, 4, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total_max+pe, 1, MPI_DOUBLE, pe+1, 5, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_lb_MBps+pe, 1, MPI_DOUBLE, pe+1, 6, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_ub_MBps+pe, 1, MPI_DOUBLE, pe+1, 7, MPI_COMM_WORLD, &status);
		}

		sort (acp_bandwidth_lb_MBps, acp_bandwidth_lb_MBps+mpiscatter->get_total_procs());
		sort (acp_bandwidth_ub_MBps, acp_bandwidth_ub_MBps+mpiscatter->get_total_procs());
		sort (time_compute_comms, time_compute_comms+mpiscatter->get_total_procs());
		sort (time_compute_post_process, time_compute_post_process+mpiscatter->get_total_procs());
		sort (time_scatter, time_scatter+mpiscatter->get_total_procs());
		sort (time_total, time_total+mpiscatter->get_total_procs());
		sort (time_total_min, time_total_min+mpiscatter->get_total_procs());
		sort (time_total_max, time_total_max+mpiscatter->get_total_procs());

		double avg_acp_bandwidth_lb_MBps = acp_bandwidth_lb_MBps [mpiscatter->get_total_procs()/2];
		double avg_acp_bandwidth_ub_MBps = acp_bandwidth_ub_MBps [mpiscatter->get_total_procs()/2];
		double avg_time_compute_comms = time_compute_comms [mpiscatter->get_total_procs()/2];
		double avg_time_compute_post_process = time_compute_post_process [mpiscatter->get_total_procs()/2];
		double avg_time_compute = avg_time_compute_comms + avg_time_compute_post_process;
		double avg_time_scatter = time_scatter [mpiscatter->get_total_procs()/2];
		double avg_time_total 	= time_total [mpiscatter->get_total_procs()/2];
		double min_time_total 	= time_total_min [mpiscatter->get_total_procs()/2];
		double max_time_total 	= time_total_max [mpiscatter->get_total_procs()/2];

		printf ("%d\n", repeat_count);
		printf("{ignore %d}, %d, %d, %d, %d, %d, %.2f%%, < %.2f %.2f >, < %f, %f, %f >, %f, < %f, %f, %f >\n",
			(int) not_used_val,
			mpiscatter->get_total_procs(),
			omp_get_num_threads(),
			total_nodes,
			total_edges,
			mpiscatter->avg_outbound_count_per_node(),
			(float)mpiscatter->avg_outbound_count_per_node()*100.0/(float)mpiscatter->avg_send_count_per_node(),
			avg_acp_bandwidth_lb_MBps,
			avg_acp_bandwidth_ub_MBps,
			avg_time_compute_comms,
			avg_time_compute_post_process,
			avg_time_compute,
			avg_time_scatter,
			min_time_total, avg_time_total, max_time_total);
	}
	else
	{
		sort (time_compute_comms, time_compute_comms+repeat_count);
		sort (time_compute_post_process, time_compute_post_process+repeat_count);
		sort (time_scatter, time_scatter+repeat_count);
		sort (time_total, time_total+repeat_count);
		MPI_Send (time_compute_comms+repeat_count/2, 1, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
		MPI_Send (time_compute_post_process+repeat_count/2, 1, MPI_DOUBLE, MASTER, 1, MPI_COMM_WORLD);
		MPI_Send (time_scatter+repeat_count/2, 1, MPI_DOUBLE, MASTER, 2, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count/2, 1, MPI_DOUBLE, MASTER, 3, MPI_COMM_WORLD);
		MPI_Send (time_total+0, 1, MPI_DOUBLE, MASTER, 4, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count-1, 1, MPI_DOUBLE, MASTER, 5, MPI_COMM_WORLD);
		
		double avg_time_compute_comms = time_compute_comms[repeat_count/2];
		int ps_pl_traffic = transfer_size;
		int pl_ps_traffic = mpiscatter->get_num_nodes();
		int total_traffic_ub_bytes = (2*ps_pl_traffic)*sizeof(REAL);
		int total_traffic_lb_bytes = (2*pl_ps_traffic)*sizeof(REAL);

		double acp_bandwidth_ub_MBps = ((double)total_traffic_ub_bytes/(1000000.0))/avg_time_compute_comms;
		double acp_bandwidth_lb_MBps = ((double)total_traffic_lb_bytes/(1000000.0))/avg_time_compute_comms;

		MPI_Send (&acp_bandwidth_lb_MBps, 1, MPI_DOUBLE, MASTER, 6, MPI_COMM_WORLD);
		MPI_Send (&acp_bandwidth_ub_MBps, 1, MPI_DOUBLE, MASTER, 7, MPI_COMM_WORLD);
	}

	return 0;
}


void compute (int M, int repeat_count, double *time_compute_comms, double *time_compute_post_process)
{
	stick_this_thread_to_core (CORE_ID_COMPUTE);

	REAL *receive_stream;
	// init fpga stuff
	if (mpiscatter->get_rank() != MASTER)
	{
		receive_stream = new REAL [mpiscatter->get_num_nodes()];
		PL = new FPGA ("/dev/xillybus_read_32", "/dev/xillybus_write_32");
	}

	int transfer_size = max(mpiscatter->get_num_nodes(), FPGA_STREAM_NUM_EDGE_DESCRIPTOR*mpiscatter->get_num_in_edges());
	transfer_size -= transfer_size%4;

	// printf ("Sending %d bytes\n", transfer_size);
	// printf ("num nodes:%d num edges : %d t: %d\n", mpiscatter->get_num_nodes(), mpiscatter->get_num_in_edges(), transfer_size*4);
	
	// old recv buffer to new send buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{
		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		// PS <->PL
		if (mpiscatter->get_rank() != MASTER)
		{
			resetTime(&timer);
			PL->send (fpga_stream_old, transfer_size);
			PL->recv (receive_stream, mpiscatter->get_num_nodes()-mpiscatter->get_num_nodes()%4);
			time_compute_comms[i] = getTime(&timer);
			// delete PL;

			time_compute_post_process[i] = 0;
		}

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// if (mpiscatter->get_rank() != MASTER)
		// 	PL = new FPGA ("/dev/xillybus_read_32", "/dev/xillybus_write_32");

		if (mpiscatter->get_rank() != MASTER)
			PL->reset_pl();

		// wait for swap of buffers to be over
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
	}

	if (mpiscatter->get_rank() != MASTER)
	{
		delete PL;
		delete [] receive_stream;
	}
	debug ("%d - thread 1 exiting\n", mpiscatter->get_rank());
}

void scatter (double repeat_count, double *time_scatter)
{
	stick_this_thread_to_core (CORE_ID_SCATTER);

	// old send buffer to new recv buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{
		MPI_Barrier (MPI_COMM_WORLD);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		resetTime(&timer);
		mpiscatter->scatter();
		// MPI_Barrier (MPI_COMM_WORLD);
		time_scatter[i] = getTime(&timer);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// wait for swap of buffers to be over
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
	}
	debug ("%d - mpi exiting\n", mpiscatter->get_rank());
}