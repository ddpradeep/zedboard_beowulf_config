#include "neuralsim.h"
// 6th line of code in loop at http://www.izhikevich.org/publications/net.m
void synapse(REAL *I, REAL *weight, BOOL *fired, int* col_index, int m) {

	REAL sum=0;
	for(int n=0;n<col_index[m];n++) {
		sum += weight[n]*fired[n];
	}
	//*I=*I+sum;
	*I+=sum;

}				
