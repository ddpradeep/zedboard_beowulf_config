#include "mpi.h"
#include "helper.h"
#include <algorithm>
#include <iostream>
#include <assert.h>
using namespace std;

#define MASTER	0
#define SLAVE	11

typedef struct
{
	int pe_count;			// stores the number of PEs [N] *redundant?*
	int *num_nodes;			// number of nodes in each pe
	int *num_in_edges;		// number of total incomming edges for each pe
	int ***send_addr;		// 2D array of send disp_v vectors [N*N]
	int ***recv_addr;		// 2D array of recv disp_v vectors [N*N]
	int **edges_for_node;	// 1D array containing [in edges] to node mapping (size = num_in_edges) 
	int **send_count;		// 2D array containing length of each send vector [N*N]
	int **recv_count;		// 2D array containing length of each recv vector [N*N]
} disp_memory_t;

typedef struct
{
	int num_nodes;				// 1D array of number of graph nodes in each PE
	int num_in_edges;			// 1D array of number of incoming edges in each PE
	int *in_edges_for_node;	// 1D array containing [in edges] to node mapping (size = num_in_edges)
	int **send_addr;			// 1D array of send disp_v vectors
	int **recv_addr;			// 1D array of recv disp_v vectors
	int *send_count;			// 1D array containing length of each send vector
	int *recv_count;			// 1D array containing length of each recv vector
	int **send_blklen;				// 1D array containing block len (all ones)
	int **recv_blklen;				// 1D array containing block len (all ones) 
	MPI_Datatype *send_t;		// 1D array of corresponding MPI send data type
	MPI_Datatype *recv_t;		// 1D array of corresponding MPI recv data type
}mpi_memory_t;

void free_disp_memory_t(disp_memory_t *disp_memory, int partitions)
{
	for (int i=0; i<partitions; i++)
	{
		for (int j=0; j<partitions; j++)
		{
			delete [] disp_memory->send_addr[i][j];
			delete [] disp_memory->recv_addr[i][j];
		}
		delete [] disp_memory->send_addr[i];
		delete [] disp_memory->recv_addr[i];
		delete [] disp_memory->send_count[i];
		delete [] disp_memory->recv_count[i];
	}
	delete [] disp_memory->send_addr;
	delete [] disp_memory->recv_addr;
	delete [] disp_memory->send_count;
	delete [] disp_memory->recv_count;
	delete [] disp_memory->num_nodes;
	delete [] disp_memory->num_in_edges;
}

void free_mpi_memory_t(mpi_memory_t *mpi_memory, int partitions)
{
	for (int i=0; i<partitions; i++)
	{
		delete [] mpi_memory->send_addr[i];
		delete [] mpi_memory->recv_addr[i];
		delete [] mpi_memory->send_blklen[i];
		delete [] mpi_memory->recv_blklen[i];
	}
	delete [] mpi_memory->send_count;
	delete [] mpi_memory->recv_count;
	delete [] mpi_memory->send_blklen;
	delete [] mpi_memory->recv_blklen;
	// delete [] mpi_memory->num_nodes;
	// delete [] mpi_memory->num_in_edges;
	delete [] mpi_memory->send_t;
	delete [] mpi_memory->recv_t;
}


void build_mpi_structures(int* a_index, int* col_index, int* partition, int M, int N, int NP, disp_memory_t* disp_memory) 
{
	// initialize disp_memory_t
	disp_memory->pe_count 		= NP; // stores the number of PEs
	disp_memory->send_addr 		= new int** [NP]; // 2D array of send vectors
	disp_memory->recv_addr 		= new int** [NP]; // 2D array of recv vectors
	disp_memory->send_count 	= new int* [NP]; // 2D array of send vector length
	disp_memory->recv_count 	= new int* [NP]; // 2D array of recv vector length
	disp_memory->edges_for_node = new int* [NP];
	disp_memory->num_nodes 		= new int [NP];
	disp_memory->num_in_edges 	= new int [NP];
	for (int i=0; i<NP; i++)
	{
		disp_memory->send_addr[i] = new int* [NP];
		disp_memory->recv_addr[i] = new int* [NP];
		disp_memory->send_count[i] = new int [NP];
		disp_memory->recv_count[i] = new int [NP];
	}

	// set these to zero #bug fix when pe has no nodes
	memset(disp_memory->num_nodes, 0, NP*sizeof(int));
	memset(disp_memory->num_in_edges, 0, NP*sizeof(int));

	// 1. find out edge to node mapping
	vector<int> temp_edge_for_node(M);

	int sum_of_edges = 0;
	for (int src=0; src<M; src++)
	{
		for (int edge=a_index[src]; edge<a_index[src+1]; edge++)
		{
			temp_edge_for_node [col_index[edge]]++;
			sum_of_edges++;
		}
	}
	assert (sum_of_edges == N);

	// 2. partition the nodes according to pattern
	vector < vector<int> > nodes_collection;
	// iterate for every pe
	for (int pe=0; pe<NP; pe++)
	{
		vector<int> nodes_in_this_pe;
		// read the entire partition array
		for (int i=0; i<M; i++)
			if (partition[i] == pe)
				nodes_in_this_pe.push_back(i);

		nodes_collection.push_back(nodes_in_this_pe);
	}

	// 3.generate src/dst pairs <addr, pe> using global namespace of nodes
	vector< pair<int, int> > temp_src(N);
	vector< pair<int, int> > temp_dst(N);

	// keep track of added edges
	vector<int> temp_edge_ctr(M);

	// shortcut storing start address of each target node & do some disp_memory updates
	vector<int> temp_start_addr(M);
	for (int pe=0; pe<NP; pe++)
	{
		for (int i=0, sum=0; i<M; i++)
			if (partition[i] == pe)
			{
				temp_start_addr[i] = sum;
				sum += temp_edge_for_node[i];
				disp_memory->num_in_edges[pe] = sum;
			}
		// update num nodes local to pe
		disp_memory->num_nodes[pe] = nodes_collection[pe].size();
		
		// update edge to node mapping for this pe
		disp_memory->edges_for_node[pe] = new int [disp_memory->num_nodes[pe]];

		for (int node=0; node<nodes_collection[pe].size(); node++)
		{
			disp_memory->edges_for_node[pe][node] = temp_edge_for_node[nodes_collection[pe][node]];
		}
	}

	// for every node
	int itr = 0;
	for (int src_node=0; src_node<M; src_node++)
	{
		// for every outgoing edge
		for (int edge=a_index[src_node]; edge<a_index[src_node+1]; edge++)
		{
			// src is simply the node itself
			temp_src[itr].first 	= src_node;	// node is <src_node>
			temp_src[itr].second 	= partition[src_node];	// node is in pe <partition of src_node>

			// dst is complex --> get the starting address of dst node, then add offset (if neccessary)
			temp_dst[itr].first 	= temp_start_addr[col_index[edge]] + temp_edge_ctr[col_index[edge]];
			temp_dst[itr].second 	= partition[col_index[edge]];

			temp_edge_ctr[col_index[edge]]++;
			itr++;
		}
	}

	assert (itr == N);

	// 3.update src <addr, pe> using local namespace of nodes
	for (int i=0; i<N; i++)
	{
			// vector<int>::iterator it = find(nodes_collection[temp_src[i].second].begin(), nodes_collection[temp_src[i].second].end(), temp_src[i].first);
			vector<int>::iterator it_low = lower_bound(nodes_collection[temp_src[i].second].begin(), nodes_collection[temp_src[i].second].end(), temp_src[i].first);

			// is it in this pe
			if (it_low != nodes_collection[temp_src[i].second].end())
			{
				if (*it_low == temp_src[i].first)
				{
					temp_src[i].first = it_low - nodes_collection[temp_src[i].second].begin();
					// printf ("%d - done\n", i);
				}
				else
				{
					printf ("error..\n");
					while (true);
				}
			}
			else
			{
				printf ("error, cannot find\n");
				while (true);
			}
	}

	temp_start_addr.clear();
	temp_edge_ctr.clear();
	temp_edge_for_node.clear();
	
	// 4. KERNEL -> transform graph into disp_memory
	for(int pe_src=0;pe_src<NP;pe_src++) 
	{
		
		vector<int> send_addr_vect;
		vector<int> recv_addr_vect;

		for(int pe_dest=0;pe_dest<NP;pe_dest++) 
		{
			// iterate through all temp_src
			for (int i=0; i<N; i++)
			{
				if ( (temp_src[i].second == pe_src) && (temp_dst[i].second == pe_dest) )
				{
					send_addr_vect.push_back(temp_src[i].first);
					recv_addr_vect.push_back(temp_dst[i].first);
				}
			}

			int *send_addr_ptr = &send_addr_vect[0];
			int *recv_addr_ptr = &recv_addr_vect[0];

			// allocate memory for arrays
			disp_memory->send_addr[pe_src][pe_dest] = new int[send_addr_vect.size()];
			disp_memory->recv_addr[pe_src][pe_dest] = new int[recv_addr_vect.size()];

			// copy vector to array
			memcpy (disp_memory->send_addr[pe_src][pe_dest], send_addr_ptr, send_addr_vect.size()*sizeof(int));
			memcpy (disp_memory->recv_addr[pe_src][pe_dest], recv_addr_ptr, recv_addr_vect.size()*sizeof(int));

			// store the array lengths
			disp_memory->send_count[pe_src][pe_dest] = send_addr_vect.size();
			disp_memory->recv_count[pe_src][pe_dest] = recv_addr_vect.size();

			// free vector
			send_addr_vect.clear();
			recv_addr_vect.clear();
		}
	}

}


int compress_disp_blklen (int **disp_p, int *disp_len, int **blklen_p)
{
	vector <int> temp_disp, temp_blklen;

	int *disp_vect = *disp_p;

	int i = 0, old_disp = 0, blklen = 0;
	while (i<*disp_len)
	{
		temp_disp.push_back( disp_vect[i]);
		old_disp 	= disp_vect[i];
		blklen 		= 1;

		while ( (++i<*disp_len) && (disp_vect[i]==(old_disp+1)) )
		{
			old_disp 	= disp_vect[i];
			blklen++;
		}

		temp_blklen.push_back(blklen);
	}

	// printf ("here\n");

	assert (temp_disp.size() == temp_blklen.size());
	*disp_len = temp_disp.size();

	// printf ("here2\n");

	delete [] *disp_p;	// delete old space (as new might be smaller)
	*disp_p = new int [temp_disp.size()];
	copy(temp_disp.begin(), temp_disp.end(), *disp_p);
	temp_disp.clear();

	// printf ("here3\n");

	*blklen_p = new int [temp_blklen.size()];
	copy(temp_blklen.begin(), temp_blklen.end(), *blklen_p);
	temp_blklen.clear();

	// printf ("here4\n");

	// return total sum of blocklen
	int sum = 0;
	for (int i=0; i<*disp_len; i++)
		sum += (*blklen_p)[i];

	return sum;
}


void get_rand_array(int* array, int M, int N) {
	for(int i=0;i<M-1;i++) {
		array[i]=rand()%(N+1); // points on a line
	}
	array[M-1]=N;
	sort(array,array+M);
	for(int i=M-1;i>0;i--) {
		array[i] -= array[i-1];
		if(array[i]<0) {
			cout << "Error with negative values" << endl;
			exit(1);
		}
	}
}


void get_random_graph(int M, int N, int** a_index, int** col_index) {

	*a_index = (int*)malloc((M+1)*sizeof(int));
	*col_index = (int*)malloc(N*sizeof(int));
	if(*a_index==NULL || *col_index==NULL) {
		cout << "Not enough memory" << endl;
		exit(1);
	}

	int *edge_count_arr = (int*)malloc(M*sizeof(int));
	get_rand_array(edge_count_arr,M,N);
	(*a_index)[0]=0;
	for(int m=1; m<=M; m++) {
		int edges = edge_count_arr[m-1];
		(*a_index)[m]=(*a_index)[m-1]+edges;
		for(int k=0;k<edges;k++) {
			(*col_index)[(*a_index)[m-1]+k]=rand()%M; // any node
		}
	}
	free(edge_count_arr);
}
