#include <stdint.h>

#define REAL float
#define BOOL uint8_t

void derivative(REAL *V, REAL *u, REAL I, REAL a, REAL b);
void integrate(REAL c, REAL d, REAL *V, REAL *u, BOOL* fired);
void synapse2(REAL *I, REAL *weight, BOOL *fired, int* a_index, int* col_index, int m);

void neuron(REAL *V, REAL *u, BOOL *fired, REAL I, REAL a, REAL b, REAL c, REAL d);
void synapse(REAL *I, REAL *weight, BOOL *fired, int* col_index, int m);
void print_graph(int M, int N, int* a_index, int* col_index);
