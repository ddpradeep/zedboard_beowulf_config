#!/bin/bash

for M in 1000000 8000000 16000000 32000000
	do
	for N in 1000000 2000000 4000000 8000000 16000000 24000000 32000000
	do
		#OUTPUT=$(make operf NP=1 OMP=6 REPEAT=0 M=$M N=$N)
		## echo "$OUTPUT"
		#CACHE_M1=$(opreport 2>/dev/null | grep neuralsim.o | head -n 1 | tr -s ' ' | cut -d ' ' -f 2)
		#CACHE_H1=$(opreport 2>/dev/null | grep neuralsim.o | head -n 1 | tr -s ' ' | cut -d ' ' -f 4)
		#OUTPUT=$(make operf NP=1 OMP=6 REPEAT=50 M=$M N=$N)
		## echo "$OUTPUT"
		#CACHE_M2=$(opreport 2>/dev/null | grep neuralsim.o | head -n 1 | tr -s ' ' | cut -d ' ' -f 2)
		#CACHE_H2=$(opreport 2>/dev/null | grep neuralsim.o | head -n 1 | tr -s ' ' | cut -d ' ' -f 4)
		#echo $CACHE_M2
		#CACHE_MISS=$(( CACHE_M2 - CACHE_M1 ))
		#CACHE_HIT=$(( CACHE_H2 - CACHE_H1 ))
		#echo "$M, $N, 50, 100000, $CACHE_M1, $CACHE_M2, $CACHE_MISS, , $CACHE_H1, $CACHE_H2, $CACHE_HIT" >> $(hostname).cache_miss.csv

		OUTPUT=$(make run NP=1 OMP=4 REPEAT=24 M=$M N=$N)
		echo "$OUTPUT" | grep ignore >> results_launchpad.csv
	done
done
