#ifndef graph_memory_H
#define graph_memory_H

#include <stdint.h>

#define MAX_NODES_PER_PE 1024*16*8*8
#define MAX_EDGES_PER_PE 1024*16*8*8

typedef uint8_t address_t;

typedef struct {
        address_t X;
        address_t Y;
} pe_address_t;

typedef uint32_t graph_address_t;

typedef uint32_t node_address_t;
typedef uint32_t message_address_t;

typedef uint32_t edge_address_t;
typedef uint32_t message_address_t;

typedef int32_t node_state_t;
typedef int32_t edge_input_state_t;
typedef int32_t edge_output_state_t;

struct node_memory_t {
        uint32_t edge_input_count;
        uint32_t edge_output_count;
	edge_address_t edge_mem_input_offset;
        edge_address_t edge_mem_output_offset;
        node_state_t node_state;
};

struct edge_input_memory_t {
        message_address_t message_address;
        edge_input_state_t edge_state;
};

struct edge_output_memory_t {
        pe_address_t pe_address;
        graph_address_t graph_address;
        edge_output_state_t edge_state;
};

struct graph_memory_t {
	node_address_t max_nodes; // this stores the actual node count per pe..
        edge_address_t max_in_edges;
        edge_address_t max_out_edges;
	node_memory_t node_memory[MAX_NODES_PER_PE];
	edge_input_memory_t edge_input_memory[MAX_EDGES_PER_PE];
	edge_output_memory_t edge_output_memory[MAX_EDGES_PER_PE];
};

#endif
