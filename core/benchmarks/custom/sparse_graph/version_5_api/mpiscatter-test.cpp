#include "mpiscatter.h"

using namespace std;

int main (int argc, char *argv[]) 
{
	MPIScatter mpiscatter (&argc, &argv);

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                           Build Random Graph
	/******************************************************************************/

	mpiscatter.build_random_graph(M, N);


	/******************************************************************************/
	//                 		Distribute and Build Send/Recv Types
	/******************************************************************************/

	mpiscatter.prepare_sendrecv_buffer();
	mpiscatter.distribute_sendrecv_buffer();
	mpiscatter.build_data_type();


	/******************************************************************************/
	//                 					Scatter
	/******************************************************************************/

	TIME_DATA_TYPE timer;
	float *scatterTime 	= new float[repeat_count];

	for (int i=0; i<repeat_count; i++)
	{
		resetTime(&timer);
		mpiscatter.scatter();
		scatterTime[i] = getTime(&timer);
	}


	/******************************************************************************/
	//                 					Results
	/******************************************************************************/
	
	if (mpiscatter.get_rank() == MASTER)
	{
		float *scatterTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count);
		memcpy (scatterTime_buffer, scatterTime, repeat_count*sizeof(float));
		sort_multiple (scatterTime, scatterTime_buffer, 1, repeat_count, SORT_MAX, 0);

		float scatterTimeMax	= scatterTime[repeat_count/2];
		float scatterSpeedMin	= (float)(sizeof(data_tt)*mpiscatter.avg_send_count_per_node()*mpiscatter.get_total_procs())/(scatterTimeMax*1048576.0);

		printf ("%d, %d, %d, %f, %.2f\n", 
			mpiscatter.get_total_procs(), 
			M,
			N,
			scatterTimeMax*1000.0,
			scatterSpeedMin);
	}

	return 0;
}
