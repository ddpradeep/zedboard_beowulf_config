#include "mpiscatter.h"
#include <iostream>

using namespace std;


MPIScatter::MPIScatter(int *argc, char ***argv)
{
	/******************************************************************************/
	//                           MPI Initialization
	/******************************************************************************/
	MPI_Init (argc, argv);		// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &PE);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// get current process rank
	
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %d running on %s\n", rank, processor_name);
	/******************************************************************************/
}


int MPIScatter::get_rank()
{
	return rank;
}


int MPIScatter::get_total_procs()
{
	return PE;
}


int MPIScatter::avg_send_count_per_node()
{
	return avg_send_count;
}


void MPIScatter::build_random_graph(int M, int N)
{
	/******************************************************************************/
	//                        		Build Graph
	/******************************************************************************/

	int *a_index;
	int *col_index;

	int pe_x = PE;
	int pe_y = 1;
	int partitions = pe_x*pe_y;
	assert(partitions==PE);

	if (rank == MASTER)
	{
		get_random_graph(M, N, &a_index, &col_index);
		Graph g = build_graph(M, N, a_index, col_index);
		free (a_index);
		free (col_index);

		int* part = (int*)malloc(num_vertices(g)*sizeof(int));
		for(int i=0;i<num_vertices(g);i++)
		{
			int p = rand_between(0, PE);
			part[i] = p;
		}
		annotate_graph_node_with_partition_id(g, part);
		debug ("Annotated graph\n");
		graph_memory_t* graph_memory = get_gmimage_from_graph(g, partitions, pe_x, pe_y, part);
		debug ("Graph parsed\n");
		build_mpi_structures(graph_memory, &disp_memory, partitions, pe_x, pe_y);
	}
}


void MPIScatter::prepare_sendrecv_buffer()
{
	/******************************************************************************/
	//                        	Allocate Send/Recv Buffers
	/******************************************************************************/

	MPI_Scatter(disp_memory.num_nodes, 1, MPI_INT, &num_nodes, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	MPI_Scatter(disp_memory.num_in_edges, 1, MPI_INT, &num_in_edges, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	
	debug ("PE%d: Num Nodes: %d\tNum In Edges: %d\n", rank, num_nodes, num_in_edges);

	node_memory = new data_tt[num_nodes];
	message_memory = new data_tt[num_in_edges];

	for (int i=0; i<num_nodes; i++)
		node_memory[i] = rand_between(0, 100);	// assign random values
	memset (message_memory, 0, num_in_edges);

	/******************************************************************************/
	//						Average Outbound Traffic
	/******************************************************************************/

	if (rank == MASTER)
	{
		int *total_send_count = new int [PE];

		// for every node
		for (int i=0; i<PE; i++)
		{
			total_send_count[i] = 0;
			// for every other node (includeing self)
			for (int j=0; j<PE; j++)
				// count the number of outgoing edges
				total_send_count[i] += disp_memory.send_count[i][j];

			debug ("PE%d: Num Out Edges: %d\n", i, total_send_count[i]);
		}

		// calculate average outbound traffic
		avg_send_count = 0;
		for (int i=0; i<PE; i++)
			avg_send_count += total_send_count[i];
		avg_send_count /= PE;

		delete [] total_send_count;

		debug ("Avg_Send_cnt: %d\n", avg_send_count);
	}
}


void MPIScatter::distribute_sendrecv_buffer()
{
	/******************************************************************************/
	//                        Distribute Send/Recv Displacements
	/******************************************************************************/

	// initialize mpi_memory data structures
	mpi_memory.send_addr = new int* [PE];
	mpi_memory.recv_addr = new int* [PE];
	mpi_memory.send_count = new int [PE];
	mpi_memory.recv_count = new int [PE];

	if (rank == MASTER)
	{
		// printf ("Preparing to send...\n");
		// for every node
		for (int src=0; src<PE; src++)
		{
			// for number of nodes
			for (int dst=0; dst<PE; dst++)
			{
				if (src==MASTER)
				{
					// copy send/recv pair: Node[src] -> Node[dst] to self
					mpi_memory.send_addr[dst]  = new int [disp_memory.send_count[src][dst]];
					mpi_memory.send_count[dst] = disp_memory.send_count[src][dst];
					memcpy(mpi_memory.send_addr[dst], disp_memory.send_addr[src][dst], disp_memory.send_count[src][dst]*sizeof(int));
					debug ("rank-%d> SEND[%d, %d] of size %d\n", rank, rank, dst, mpi_memory.send_count[dst]);
				}
				else
				{
					// debug ("Sending SEND_T to %d\n", src);
					// send WHAT_IS_TRANSFERRED info to slave
					// SEND_OFFSET + DST encoded in tag
					MPI_Send(disp_memory.send_addr[src][dst], disp_memory.send_count[src][dst], MPI_INT, src, SEND_TAG_OFFSET+dst, MPI_COMM_WORLD);
					// debug ("Sent SEND_T to %d\n", src);
				}
				if (dst==MASTER)
				{
					mpi_memory.recv_addr[src]  = new int [disp_memory.recv_count[src][dst]];
					mpi_memory.recv_count[src] = disp_memory.recv_count[src][dst];
					memcpy(mpi_memory.recv_addr[src], disp_memory.recv_addr[src][dst], disp_memory.recv_count[src][dst]*sizeof(int));
					debug ("rank-%d> RECV[%d, %d] of size %d\n", rank, rank, src, mpi_memory.recv_count[src]);
				}
				else
				{
					// transmit send/recv pair: Node[src] -> Node[dst] to Node[src]
					// debug ("Sending RECV_T to %d\n", dst);
					MPI_Send(disp_memory.recv_addr[src][dst], disp_memory.recv_count[src][dst], MPI_INT, dst, RECV_TAG_OFFSET+src, MPI_COMM_WORLD);
					// debug ("Sent RECV_T to %d\n", dst);
				}
			}
		}

		// free up disp_memory
		free_disp_memory_t(&disp_memory, PE);
	}
	else
	{
		// for number of nodes
		for (int count=0; count<(PE*2); count++)
		{
			MPI_Status status;
			int incoming_msg_size;
			// receive send/recv pair for Node[rank] -> Node[dst] from Master

			// i. probe incoming message
			// debug ("rank-%d> probing for something from %d\n", rank, MASTER);
			MPI_Probe(MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			// debug ("rank-%d> probing success\n", rank);
			MPI_Get_count(&status, MPI_INT, &incoming_msg_size);
			if (status.MPI_TAG >= SEND_TAG_OFFSET)
			{
				// debug ("rank-%d> SEND_T incoming", rank);
				int dst = status.MPI_TAG - SEND_TAG_OFFSET;
				// ii. allocate buffer of probed size
				mpi_memory.send_addr[dst]  = new int [incoming_msg_size];
				mpi_memory.send_count[dst] = incoming_msg_size;
				// iii. receive send type for Node[rank] -> Node[dst]
				MPI_Recv(mpi_memory.send_addr[dst], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d> SEND[%d, %d] of size %d\n", rank, rank, dst, mpi_memory.send_count[dst]);
			
			}
			else
			{
				// debug ("rank-%d> RECV_T incoming", rank);
				int src = status.MPI_TAG;
				// ii. allocate buffer of probed size
				mpi_memory.recv_addr[src]  = new int [incoming_msg_size];
				mpi_memory.recv_count[src] = incoming_msg_size;
				// iii. receive recv type for Node[rank] <- Node[src]
				MPI_Recv(mpi_memory.recv_addr[src], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d> RECV[%d, %d] of size %d\n", rank, rank, src, mpi_memory.recv_count[src]);
			}
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
}


void MPIScatter::build_data_type()
{
	/******************************************************************************/
	//      Optimize Send/Recv Displacements to Maximize Receive Block Length
	/******************************************************************************/

	mpi_memory.send_blklen 	= new int* [PE];
	mpi_memory.recv_blklen 	= new int* [PE];

	// debug ("rank-%d> pass\n", rank);

	for (int i=0; i<PE; i++)
	{
		mpi_memory.send_blklen[i] = new int [mpi_memory.send_count[i]];
		for (int j=0; j<mpi_memory.send_count[i]; j++)
			mpi_memory.send_blklen[i][j] = 1;
		mpi_memory.recv_blklen[i] = new int [mpi_memory.recv_count[i]];
		for (int j=0; j<mpi_memory.recv_count[i]; j++)
			mpi_memory.recv_blklen[i][j] = 1;

		// debug ("rank-%d> done %d/%d\n", rank, i, PE);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	// debug ("%d - done\n", rank);	

	/******************************************************************************/
	//                        Build Send/Recv MPI Datatypes
	/******************************************************************************/

	mpi_memory.send_t 		= new MPI_Datatype [PE];
	mpi_memory.recv_t 		= new MPI_Datatype [PE];

	for (int j=0; j<PE; j++)
	{
		// build send type
		MPI_Type_indexed (
			mpi_memory.send_count[j],
			mpi_memory.send_blklen[j],
			mpi_memory.send_addr[j],
			mpi_data_t,
			&mpi_memory.send_t[j]);

		MPI_Type_commit(&mpi_memory.send_t[j]);

		// build recv type
		MPI_Type_indexed (
			mpi_memory.recv_count[j],
			mpi_memory.recv_blklen[j],
			mpi_memory.recv_addr[j],
			mpi_data_t,
			&mpi_memory.recv_t[j]);

		MPI_Type_commit(&mpi_memory.recv_t[j]);
	}

	MPI_Barrier (MPI_COMM_WORLD);
	debug ("%d - created data types\n", rank);	
}


void MPIScatter::scatter()
{
	/*******************************************************************************/
	//                        		Scatter
	/*******************************************************************************/

	assert (mpi_memory.send_count[rank]==mpi_memory.recv_count[rank]); // Local scatter cant handle this case

	// I. Perform local scatter //

	for (uint32_t disp=0; disp<mpi_memory.send_count[rank]; disp++)
	{
		message_memory[mpi_memory.recv_addr[rank][disp]] = node_memory[mpi_memory.send_addr[rank][disp]];
	}

	// II. Perform remote scatter //

	// printf ("%d - local scatter done\n", rank);	

	// 2. Perform Scatter
	for (int base=PE-1, j=1; j<PE; j++)
	{
		int toNode = mod(rank+j, PE);
		int fromNode = mod(base+rank-(j-1), PE);
		debug ("%d:%d\t[%d]->[%d]->[%d]\n", rank, j, fromNode, rank, toNode);
		MPI_Sendrecv (node_memory, 1, mpi_memory.send_t[toNode], toNode, 100, message_memory, 1, mpi_memory.recv_t[fromNode], fromNode, 100, MPI_COMM_WORLD, &status);
		// MPI_Put(node_memory, 1, mpi_memory.send_t[toNode], toNode, 0, 1, mpi_memory.recv_t[toNode], win[toNode]);
	}

	MPI_Barrier(MPI_COMM_WORLD);
}


MPIScatter::~MPIScatter()
{
	MPI_Finalize ();
}
