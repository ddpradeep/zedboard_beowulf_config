#include "neuralsim.h"
void neuron(REAL *V, REAL *u, BOOL *fired, REAL I, REAL a, REAL b, REAL c, REAL d) {
// Last two lines of for loop in http://www.izhikevich.org/publications/net.m
	REAL dV = (4e-2*(*V)*(*V)+5**V+140-*u+I);
	REAL du = a*(b*(*V)-*u);
	*V=*V+dV;
	*u=*u+du;
// First five lines of code in loop at http://www.izhikevich.org/publications/net.m
	*fired==(*V>30)?1:0;
	if(*fired) {
		*V=c;
		*u=*u+d;
	}
}
					
