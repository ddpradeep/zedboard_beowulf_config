#include "mpiscatter.h"
#include "neuralsim.h"
#include "omp.h"

int main(int argc, char** argv)
{

	MPIScatter mpiscatter (&argc, &argv);	// MPI init

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                        Generate and Distribute Graph
	/******************************************************************************/

	mpiscatter.build_random_graph(M, N);
	mpiscatter.distribute_graph_structure();
	int total_nodes = M;
	int total_edges = N;
	M = mpiscatter.get_num_nodes();		// update M to reflect num nodes in this PE
	N = mpiscatter.get_num_in_edges();	// update N to reflect num incomming edges in this PE

	/******************************************************************************/
	//                           Initialize Variables
	/******************************************************************************/

	REAL* a = (REAL*)malloc(M*sizeof(REAL));
	REAL* b = (REAL*)malloc(M*sizeof(REAL));
	REAL* c = (REAL*)malloc(M*sizeof(REAL));
	REAL* d = (REAL*)malloc(M*sizeof(REAL));

	REAL* V = (REAL*)malloc(M*sizeof(REAL));
	REAL* u = (REAL*)malloc(M*sizeof(REAL));
	REAL* I = (REAL*)malloc(M*sizeof(REAL));

	BOOL* fired = (BOOL*)malloc(M*sizeof(BOOL));

	REAL* weight = (REAL*)malloc(N*sizeof(REAL));
	
	for(int m=0;m<M;m++)
	{
		a[m]=((REAL) rand() / (RAND_MAX)) + 1;
		b[m]=((REAL) rand() / (RAND_MAX)) + 1;
		c[m]=((REAL) rand() / (RAND_MAX)) + 1;
		d[m]=((REAL) rand() / (RAND_MAX)) + 1;
		I[m]=((REAL) rand() / (RAND_MAX)) + 1;
		V[m]=((REAL) rand() / (RAND_MAX)) + 1;
		u[m]=((REAL) rand() / (RAND_MAX)) + 1;
		fired[m]=rand()%2;
	}
	for(int n=0;n<N;n++)
		weight[n]=((REAL) rand() / (RAND_MAX)) + 1;

	//	set the node_memory buffer to point to 'fired'
	mpiscatter.attach_node_memory (fired);

	// get the address of message memory buffer
	mpiscatter.create_message_memory();
	BOOL *in_fired_msg = mpiscatter.get_message_memory_addr();

	// get edges to node mapping array (col_index)
	int *col_index = mpiscatter.get_edges_to_node_mapping_addr();

	/******************************************************************************/
	//                        		Neural Simulation
	/******************************************************************************/

	// int* a_index;
	// int* col_index;
	// get_random_graph(M, N, &a_index, &col_index);

	// TIMER timer;
	// double time_synapse, time_neuron;

	// resetTime(&timer);
	// #pragma omp parallel for //shared(a_index, col_index)
	// for(int m=0;m<M;m++) {
	// 	synapse(&(I[m]), weight, fired, a_index, col_index, m);
	// }
	// time_synapse = getTime(&timer);

	// resetTime(&timer);
	// #pragma omp parallel for
	// for(int m=0;m<M;m++) {
 //                neuron(&(V[m]), &(u[m]), &(fired[m]),I[m], a[m], b[m], c[m], d[m]);
	// }
	// time_neuron = getTime(&timer);

	// int threads=omp_get_num_threads();
	// printf("%d, %d, %d, %f, %f\n",threads,M,N,time_synapse,time_neuron);

	TIMER timer;
	double *time_compute = new double [repeat_count];
	double *time_scatter = new double [repeat_count];
	double *time_total = new double [repeat_count];

	for (int i=0; i<repeat_count; i++)
	{
		// COMPUTE
		resetTime(&timer);
		#pragma omp parallel for //shared(a_index, col_index)
		for(int m=0;m<M;m++)
			synapse(&(I[m]), weight, in_fired_msg, col_index, m);
		
		#pragma omp parallel for
		for(int m=0;m<M;m++)
	        neuron(&(V[m]), &(u[m]), &(fired[m]),I[m], a[m], b[m], c[m], d[m]);

		MPI_Barrier (MPI_COMM_WORLD);
		time_compute[i] = getTime(&timer);

		// SCATTER
		resetTime(&timer);
		mpiscatter.scatter();
		time_scatter[i] = getTime(&timer);

		if (mpiscatter.get_rank() == MASTER)
		{
			if (i%10==0)
				printf ("%d", i);
			else
				printf (".");
		}

		time_total[i] = time_compute[i] + time_scatter[i];
	}

	// paranoid code
	int not_used_val = 0;
	for (int i=0; i<M; i++)
		not_used_val += fired[i];

	/******************************************************************************/
	//                        			Results
	/******************************************************************************/

	if (mpiscatter.get_rank() == MASTER)
	{
		sort (time_compute, time_compute+repeat_count);
		sort (time_scatter, time_scatter+repeat_count);
		sort (time_total, time_total+repeat_count);
		
		double avg_time_compute = time_compute [repeat_count/2];
		double avg_time_scatter = time_scatter [repeat_count/2];
		double avg_time_total 	= time_total [repeat_count/2];

		printf ("%d\n", repeat_count);
		printf("{ignore %d}, %d, %d, %d, %d, %d, %.2f%%, %f, %f, < %f, %f, %f >\n",
			(int) not_used_val,
			mpiscatter.get_total_procs(),
			omp_thread_count(),
			total_nodes,
			total_edges,
			mpiscatter.avg_outbound_count_per_node(),
			(float)mpiscatter.avg_outbound_count_per_node()*100.0/(float)mpiscatter.avg_send_count_per_node(),
			avg_time_compute,
			avg_time_scatter,
			time_total [0], avg_time_total, time_total [repeat_count-1]);
	}

	return 0;
}

