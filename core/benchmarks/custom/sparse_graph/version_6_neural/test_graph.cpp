#include "graph_support.h"

void get_2d_id(int i, int pe_x, int pe_y, int* x, int* y) {
	*x=i/pe_y;
	*y=(i-*x*pe_y)%pe_y;
}

// What a massive failure!!! Why can't the property maps on vertices be mutable??
void annotate_graph_node_with_partition_id(Graph& g, int* partitions) {
	
	typedef property_map<Graph, vertex_index_t>::type NodeIndexMap;
	NodeIndexMap node_index = get(vertex_index, g);
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodePartitionMap;
	NodePartitionMap node_partition = get(&node_property_for_graphs_t::partition, g);
		
	graph_traits<Graph>::vertex_iterator vi, vi_end;
	for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
		put(&node_property_for_graphs_t::partition, g, *vi, partitions[node_index[*vi]]);
	}

}


// build the graph machine data structures
graph_memory_t* get_gmimage_from_graph(Graph g, int partitions, int pe_x, int pe_y, int *part) 
{
	// setup data structures on a per PE basis!
	graph_memory_t *graph_memory;
	
	graph_memory = (graph_memory_t*)malloc(sizeof(graph_memory_t)*partitions);
	printf ("malloced\n");

	// this is the one thing that needs an out-edge pass.... followed by an in-edge pass
	int* packet_destination= (int*)malloc(num_edges(g)*sizeof(int));

	// setup the graph structure and edge information arrays..
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodePartitionMap;
	typedef property_map<Graph, int edge_property_for_graphs_t::*>::type EdgeIndexMap;
	typedef property_map<Graph, double edge_property_for_graphs_t::*>::type EdgeValueMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);
	NodePartitionMap node_partition = get(&node_property_for_graphs_t::partition, g);
	EdgeIndexMap edge_index = get(&edge_property_for_graphs_t::index, g);
	EdgeValueMap edge_value = get(&edge_property_for_graphs_t::val, g);

	graph_traits<Graph>::vertex_iterator vi, vi_end;

	printf ("here1\n");

	// iterate over the processing elements...
	for(int pe=0;pe<partitions;pe++) {
		printf ("part-processed\n");
		// build the graph memory structure for this pe
		graph_memory_t& graph_memory_for_pe = graph_memory[pe];

		// node and edge counting should be done on a per-PE basis!
		int node_counter=0;
		int in_edge_counter=0;
		
		// iterate over all edges...
		for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
			
			if(part[node_index[*vi]]==pe) {
				
				// record the starting offset for edges belonging to this node..
				graph_memory_for_pe.node_memory[node_counter].edge_mem_input_offset = in_edge_counter;
				graph_memory_for_pe.node_memory[node_counter].node_state = node_index[*vi];

				int in_edge_counter_per_node=0;
				graph_traits<Graph>::edge_descriptor e;
				graph_traits<Graph>::in_edge_iterator in_i, in_end;
				for (tie(in_i, in_end) = in_edges(*vi, g); in_i != in_end; ++in_i) {
					e = *in_i;
				
					graph_memory_for_pe.edge_input_memory[in_edge_counter].message_address = in_edge_counter;
					graph_memory_for_pe.edge_input_memory[in_edge_counter].edge_state = edge_value[e];
					packet_destination[edge_index[e]] = in_edge_counter;
//cout << "i=" << node_counter << " j=" << in_edge_counter_per_node << " addr=" << in_edge_counter << endl;
					
					in_edge_counter++;
					in_edge_counter_per_node++;

					// assert(in_edge_counter <= MAX_EDGES_PER_PE);

				}
					
				// count in edges per node...
				graph_memory_for_pe.node_memory[node_counter].edge_input_count = in_edge_counter_per_node;
				
				node_counter++;
				// assert(node_counter <= MAX_NODES_PER_PE);
			}
		}
		printf ("part-processed_one\n");

		graph_memory_for_pe.max_nodes = node_counter;
		graph_memory_for_pe.max_in_edges = in_edge_counter;
		
	}

	// iterate over the processing elements...
	for(int pe=0;pe<partitions;pe++) {

		// build the graph memory structure for this pe
		graph_memory_t& graph_memory_for_pe=graph_memory[pe];
		
		// need to count output edge on a per-PE basis!
		int node_counter = 0;
		int out_edge_counter=0;

		for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
		
			if(part[node_index[*vi]]==pe) {

				graph_memory_for_pe.node_memory[node_counter].edge_mem_output_offset = out_edge_counter;
		
				int out_edge_counter_per_node=0;
				graph_traits<Graph>::edge_descriptor e;
				graph_traits<Graph>::out_edge_iterator out_i, out_end;
				for (tie(out_i, out_end) = out_edges(*vi, g); out_i != out_end; ++out_i) {
					e = *out_i;
					// UGH! Src->Dest PE calculator should be identical you fucker!
					int x;
					int y;
					get_2d_id(part[node_index[target(e,g)]],pe_x,pe_y,&x,&y);
					graph_memory_for_pe.edge_output_memory[out_edge_counter].pe_address.X = x; 
					graph_memory_for_pe.edge_output_memory[out_edge_counter].pe_address.Y = y; 
					graph_memory_for_pe.edge_output_memory[out_edge_counter].graph_address = packet_destination[edge_index[e]];
//cout << "out-- i=" << node_counter << " j=" << out_edge_counter_per_node << " addr=" << packet_destination[edge_index[e]] << endl;
					
					graph_memory_for_pe.edge_output_memory[out_edge_counter].edge_state = 0; 
					
					out_edge_counter++;
					out_edge_counter_per_node++;
					// assert(out_edge_counter <= MAX_EDGES_PER_PE);
				}

				graph_memory_for_pe.node_memory[node_counter].edge_output_count = out_edge_counter_per_node;
				node_counter++;

			}
				
		}

		printf ("done\n");
		graph_memory_for_pe.max_out_edges = out_edge_counter;	
	}
	
	return graph_memory;
}

int rand_between(int start_inc, int end_excl)
{
  int r = rand() % (end_excl - start_inc) + start_inc;
  return r;
}

void get_rand_array(int* array, int M, int N) {
	for(int i=0;i<M-1;i++) {
		array[i]=rand()%(N+1); // points on a line
	}
	array[M-1]=N;
	sort(array,array+M);
	for(int i=M-1;i>0;i--) {
		array[i] -= array[i-1];
		if(array[i]<0) {
			cout << "Error with negative values" << endl;
			exit(1);
		}
	}
}

Graph build_graph(int M, int N, int *a_index, int* col_index) {

        Graph g;

        // build the graph!
        for(int m=0; m<M; m++) {
                add_vertex(g);
        }

        // add edges into the graph by constructing an edge array first..
        // typedef int Edge;
        
        typedef graph_traits<Graph>::edge_descriptor edge_type;

	for(int m=0; m<M; m++) {
		for(int n=a_index[m]; n<a_index[m+1]; n++) {
			edge_type ei = (add_edge(m, col_index[n], g)).first;
		}
	}

        // assign node properties...
        graph_traits<Graph>::vertex_iterator vi, vi_end;
        int counter=0;
        for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
                g[(*vi)].index = counter++;
                g[(*vi)].partition = -1;
        }

        return g;
}

void get_random_graph(int M, int N, int** a_index, int** col_index) {

	*a_index = (int*)malloc((M+1)*sizeof(int));
	*col_index = (int*)malloc(N*sizeof(int));
	if(*a_index==NULL || *col_index==NULL) {
		cout << "Not enough memory" << endl;
		exit(1);
	}

	int *edge_count_arr = (int*)malloc(M*sizeof(int));
	get_rand_array(edge_count_arr,M,N);
	(*a_index)[0]=0;
	for(int m=1; m<M; m++) {
		int edges = edge_count_arr[m-1];
		(*a_index)[m]=(*a_index)[m-1]+edges;
		for(int k=0;k<edges;k++) {
			(*col_index)[(*a_index)[m-1]+k]=rand()%M; // any node
		}
	}
	free(edge_count_arr);
}

int main()
{
	int PE=1;
	int M=1000000, N=1048929; //1048576;
	int rank = 0;

	int *a_index;
	int *col_index;

	int pe_x = PE;
	int pe_y = 1;
	int partitions = pe_x*pe_y;
	assert(partitions==PE);

	printf ("hi - %d\n", rank);
	get_random_graph(M, N, &a_index, &col_index);
	Graph g = build_graph(M, N, a_index, col_index);
	free (a_index);
	free (col_index);
	printf ("here - %d\n", rank);
	
	int* part = (int*)malloc(num_vertices(g)*sizeof(int));
	for(int i=0;i<num_vertices(g);i++)
	{
		int p = rand_between(0, PE);
		// printf ("%d ", p);
		part[i] = p;
	}
	annotate_graph_node_with_partition_id(g, part);
	printf ("annotated - %d\n", rank);
	graph_memory_t* graph_memory = get_gmimage_from_graph(g, partitions, pe_x, pe_y, part);
	printf ("got_gmimage - %d\n", rank);
	printf ("Graph parsed\n");

	return 0;
}