/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 

#define data_tt float
#define mpi_data_t MPI_FLOAT

#define DEBUG

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include "graph_support.h"
#include "helper.h"
#include "build_mpi_structs.h"

#define TAG_SEND_T	1
#define TAG_RECV_T	2

using namespace std;

int main (int argc, char *argv[]) 
{
	// stick_this_thread_to_core(PRIMARY_CORE);
	int PE;					// total number of processes	
	int rank;				// rank of process

	/******************************************************************************/
	//                           MPI Initialization
	/******************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;

	MPI_Init (&argc, &argv);		// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &PE);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// get current process rank
	
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %d running on %s\n", rank, processor_name);
	/******************************************************************************/

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	// 1. Get the parameters from input
	if (argc != 5)
	{
		printf ("%d \n", argc);
		printf ("Usage: <graph file> <repeat_count> <pe_x> <pe_y>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	data_tt *node_memory;
	data_tt *message_memory;	

	Graph g = get_graph_from_sparse_matrix(argv[1]);
	int repeat_count 	= atoi(argv[2]);
	int pe_x = atoi(argv[3]);
	int pe_y = atoi(argv[4]);
	int partitions = pe_x*pe_y;
	assert(partitions==PE);

	float* gatherTime 	= (float *) malloc(sizeof(float)*repeat_count);
	float* scatterTime 	= (float *) malloc(sizeof(float)*repeat_count);

	/******************************************************************************/
	//                        		Build Graph
	/******************************************************************************/
	// TODO: build graph from file and associated structures..DONE
	disp_memory_t disp_memory; // structure to contain the send and recv disp for all PEs

	if (rank == MASTER)
	{
		int* part = (int*)malloc(num_vertices(g)*sizeof(int));
		if(partitions!=1)
		{
			if(true) //(hmetis_or_patoh==0)
			{
				partition_graph_with_hmetis(g, partitions);
				read_hmetis_partition(partitions, part);
			}
			else
			{
				partition_graph_with_patoh(g, partitions);
				read_patoh_partition(partitions, part);
			}
		}
		else 
		{
			for(int i=0;i<num_vertices(g);i++)
				part[i] = 0;
		}
		annotate_graph_node_with_partition_id(g, part);
		graph_memory_t* graph_memory = get_gmimage_from_graph(g, partitions, pe_x, pe_y, part);
		build_mpi_structures(graph_memory, &disp_memory, partitions, pe_x, pe_y);
	}

	/******************************************************************************/
	//                        Distribute Send/Recv Displacements
	/******************************************************************************/
	// initialize mpi_memory data structures
	mpi_memory_t mpi_memory;
	mpi_memory.send_addr = new int* [PE];
	mpi_memory.recv_addr = new int* [PE];
	mpi_memory.send_count = new int [PE];
	mpi_memory.recv_count = new int [PE];

	if (rank == MASTER)
	{
		// for every node
		for (int i=0; i<PE; i++)
		{
			// for number of nodes
			for (int j=0; j<PE; j++)
			{
				if (i==MASTER)
				{
					// copy send/recv pair: Node[i] -> Node[j] to self
					mpi_memory.send_addr[j]  = new int [disp_memory.send_count[i][j]];
					mpi_memory.send_count[j] = disp_memory.send_count[i][j];
					memcpy(mpi_memory.send_addr[j], disp_memory.send_addr[i][j], disp_memory.send_count[i][j]*sizeof(int));

					mpi_memory.recv_addr[j]  = new int [disp_memory.recv_count[i][j]];
					mpi_memory.recv_count[j] = disp_memory.recv_count[i][j];
					memcpy(mpi_memory.recv_addr[j], disp_memory.recv_addr[i][j], disp_memory.recv_count[i][j]*sizeof(int));

					// debug ("Copied Send[%d][%d] (size: %d) & Recv[%d][%d] (size:%d) from Master\n", 
					// 	rank, j, mpi_memory.send_count[j], 
					// 	rank, j, mpi_memory.recv_count[j]);
					continue;
				}
				// transmit send/recv pair: Node[i] -> Node[j] to Node[i]
				MPI_Send(disp_memory.send_addr[i][j], disp_memory.send_count[i][j], MPI_INT, i, TAG_SEND_T, MPI_COMM_WORLD);
				MPI_Send(disp_memory.recv_addr[i][j], disp_memory.recv_count[i][j], MPI_INT, i, TAG_RECV_T, MPI_COMM_WORLD);
			}
		}

		// free up disp_memory
		free_disp_memory_t(&disp_memory, PE);
	}
	else
	{
		// for number of nodes
		for (int j=0; j<PE; j++)
		{
			MPI_Status status;
			int incoming_msg_size;
			// receive send/recv pair for Node[i] -> Node[j] from Master

			// i. probe incoming message
			MPI_Probe(MASTER, TAG_SEND_T, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_INT, &incoming_msg_size);
			// ii. allocate buffer of probed size
			mpi_memory.send_addr[j]  = new int [incoming_msg_size];
			mpi_memory.send_count[j] = incoming_msg_size;
			// iii. receive send type for Node[i] -> Node[j]
			MPI_Recv(mpi_memory.send_addr[j], incoming_msg_size, MPI_INT, MASTER, TAG_SEND_T, MPI_COMM_WORLD, &status);
			
			// i. probe incoming message
			MPI_Probe(MASTER, TAG_RECV_T, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_INT, &incoming_msg_size);
			// ii. allocate buffer of probed size
			mpi_memory.recv_addr[j]  = new int [incoming_msg_size];
			mpi_memory.recv_count[j] = incoming_msg_size;
			// iii. receive recv type for Node[i] -> Node[j]
			MPI_Recv(mpi_memory.recv_addr[j], incoming_msg_size, MPI_INT, MASTER, TAG_RECV_T, MPI_COMM_WORLD, &status);

			// debug ("Received Send[%d][%d] (size: %d) & Recv[%d][%d] (size:%d) from Master\n", 
			// 	rank, j, mpi_memory.send_count[j], 
			// 	rank, j, mpi_memory.recv_count[j]);
		}
	}

	// TODO: transmit the partitioned graph data to all nodes..Where is it?s

	/******************************************************************************/
	//                        Build Send/Recv MPI Datatypes
	/******************************************************************************/

	mpi_memory.send_t = new MPI_Datatype [PE];
	mpi_memory.recv_t = new MPI_Datatype [PE];

	// allocate block length vector of largest disp length
	int max_disp_length = 0;
	for (int j=0; j<PE; j++)
	{
		if (mpi_memory.send_count[j]>max_disp_length)
			max_disp_length = mpi_memory.send_count[j];
		if (mpi_memory.recv_count[j]>max_disp_length)
			max_disp_length = mpi_memory.recv_count[j];
	}
	mpi_memory.block_len = new int [max_disp_length];
	for (int i=0; i<max_disp_length; i++)
		mpi_memory.block_len[i] = 1;

	// debug ("Maxblock Length [%d][x] = %d\n", rank, max_disp_length);

	for (int j=0; j<PE; j++)
	{
		// build send type
		MPI_Type_indexed (
			mpi_memory.send_count[j],
			mpi_memory.block_len,
			mpi_memory.send_addr[j],
			MPI_INT,
			&mpi_memory.send_t[j]);

		MPI_Type_commit(&mpi_memory.send_t[j]);

		// build recv type
		MPI_Type_indexed (
			mpi_memory.recv_count[j],
			mpi_memory.block_len,
			mpi_memory.recv_addr[j],
			MPI_INT,
			&mpi_memory.recv_t[j]);

		MPI_Type_commit(&mpi_memory.recv_t[j]);

		debug ("max(Send[%d][%d]) : %d\n", rank, j, getMax(mpi_memory.send_addr[j], mpi_memory.send_count[j]));
		debug ("max(Recv[%d][%d]) : %d\n", rank, j, getMax(mpi_memory.recv_addr[j], mpi_memory.recv_count[j]));

		if (rank == MASTER && j==0)
		{	
			printArray(mpi_memory.send_addr[j], mpi_memory.send_count[j], " ");
			printArray(mpi_memory.recv_addr[j], mpi_memory.recv_count[j], " ");
		}
	}

	// /*******************************************************************************/
	// //                          Initialize RMA Epoch
	// /*******************************************************************************/

	// MPI_Win *win = new MPI_Win [PE];

	// int NUM_NODES = 200;		// where to get these?
	// int NUM_IN_EDGES = 200;		// where to get these?

	// // node_memory is the array of values to send
	// MPI_Alloc_mem(NUM_NODES*sizeof(data_tt), MPI_INFO_NULL, &node_memory);
	// // message data is the destination array that collects.. (or receives)
	// MPI_Alloc_mem(NUM_IN_EDGES*sizeof(data_tt), MPI_INFO_NULL, &message_memory);

	// // create the windows for remote and local access
	// for (int i=0; i<PE; i++)
	// {
	// 	if (rank == i)
	// 	{
	// 		MPI_Win_create(message_memory, NUM_IN_EDGES*sizeof(data_tt), 
	// 				sizeof(data_tt), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
	// 	}
	// 	else
	// 	{
	// 		MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
	// 	}
	// }

	// MPI_Barrier (MPI_COMM_WORLD);

	// /*******************************************************************************/
	// //                        Scatter
	// /*******************************************************************************/

	// TIME_DATA_TYPE timer;

	// for (int i=0; i<repeat_count; i++)
 //    {

 //    	MPI_Barrier (MPI_COMM_WORLD);
	// 	resetTime(&timer);

	// 	// 3. Lock the remote windows
	// 	for (int j=0; j<PE; j++)
	// 	{
	// 		int k = (j+rank) % PE;
	// 		MPI_Win_lock(MPI_LOCK_SHARED,k,0,win[k]);
	// 	}

	// 	// 4. Perform Scatter
	// 	for (int j=0; j<PE; j++)
	// 	{
	// 		int k = (j+rank) % PE;
	// 		MPI_Put(data, 1, sendType[k].newtype, k, 0, 1, recvType[k].newtype, win[k]);
	// 	}

	// 	// 5. Unlock the remote windows
	// 	for (int j=0; j<PE; j++)
	// 	{
	// 		int k = (j+rank) % PE;
	// 		MPI_Win_unlock(k, win[k]);
	// 	}

	// 	scatterTime[i] = getTime(&timer);
	// }

	// // 6. Free the windows
	// for (int i=0; i<PE; i++)
	// {
	// 	MPI_Win_free(&win[i]);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// /*******************************************************************************/
	// //                      Print Results
	// /*********************************************************************************/

	// if (rank == MASTER)
	// {
	// 	// 1. Initialize buffers for receving time data from all processes
	// 	float *gatherTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * PE);
	// 	float *scatterTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * PE);

	// 	// 2. Receive time data from all processes
	// 	for (int i=0; i<repeat_count; i++)
	// 	{
	// 		scatterTime_buffer[i] 	= scatterTime[i];
	// 		gatherTime_buffer[i] 	= gatherTime[i];
	// 	}

	// 	for (int i=1; i<PE; i++)
	// 	{
	// 		MPI_Recv (scatterTime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
	// 		MPI_Recv (gatherTime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
	// 	}

	// 	// 2. Sort the recorded timings
	// 	sort_multiple (scatterTime, scatterTime_buffer, PE, repeat_count, SORT_AVG, 0);
	// 	sort_multiple (gatherTime, gatherTime_buffer, PE, repeat_count, SORT_AVG, 0);

	// 	// 3. Get avg of each time
	// 	float scatterTimeAvg	= scatterTime[repeat_count/2];
	// 	float gatherTimeAvg 	= gatherTime[repeat_count/2] = 0;

	// 	float scatterSpeedAvg	= (float)(sizeof(data_tt)*PARTITION_SIZE*(PE)*(achieved_rmp_ratio/100.0))/(scatterTimeAvg*1048576.0);

	// 	printb ("Scat Speed: %.2fMB/s (%f ms)\n", scatterSpeedAvg, scatterTimeAvg*1000.0);

	// 	printc ("%f ms, %.2f MB/s, %f ms, %.2f MB/s\n", scatterTimeAvg*1000.0, scatterSpeedAvg, gatherTimeAvg*1000.0, gatherSpeedAvg);
	// }
	// else
	// {
	// 	MPI_Send (scatterTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	// 	MPI_Send (gatherTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	// }


	// /****************************************************************************/
	// //                    Finalize
	// /****************************************************************************/
	// free (data);
	// free (message_memory);

	MPI_Finalize ();

	return 0;
}
