#ifndef HELPER_UTIL
#define HELPER_UTIL

#include "sys/time.h"
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <cstring>
// #include <mutex>
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

using namespace std;

// #define TIME_DATA_TYPE struct timeval
#define TIME_DATA_TYPE struct timespec

#define MASTER          0
#define ADMIN_TAG       101
#define DATA_TAG        102
#define PRIMARY_CORE    0
#define SECONDARY_CORE  1
#define SEND_RECV_DELAY 150000    // us delay max (assumed)
#define NOT_ASSIGNED    -1
#define ASSIGNED        2
#define PING_DELAY      200       // us delay avg (measured)

#define SORT_MAX      1
#define SORT_MIN      2
#define SORT_AVG      3

void inline resetTime(TIME_DATA_TYPE *timer);
float inline getTime(TIME_DATA_TYPE *time1);
void sort(float values[], unsigned long size);
void sort(float values1[], float values2[], unsigned long size);

void inline resetTime(TIME_DATA_TYPE *timer)
{
  clock_gettime(CLOCK_REALTIME, timer);
}

float inline getTime(TIME_DATA_TYPE *time1)
{
    TIME_DATA_TYPE time2;
    clock_gettime(CLOCK_REALTIME, &time2);
    return (float)(( time2.tv_sec - time1->tv_sec ) + ( time2.tv_nsec - time1->tv_nsec ) / 1E9);
}

// int stick_this_thread_to_core(int core_id)
// {
//    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
//    if (core_id < 0 || core_id >= num_cores)
//       return EINVAL;

//    cpu_set_t cpuset;
//    CPU_ZERO(&cpuset);
//    CPU_SET(core_id, &cpuset);

//    pthread_t current_thread = pthread_self();    
//    return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
// }

static struct termios old, new_;

/* Initialize new_ terminal i/o settings */
void initTermios(int echo) 
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  new_ = old; /* make new_ settings same as old settings */
  new_.c_lflag &= ~ICANON; /* disable buffered i/o */
  new_.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new_); /* use these new_ terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return getch_(1);
}
 
void debug(const char* format, ...)
{
  #ifdef DEBUG
    char *new_format ;
    char const *prefix = "[DBG] \t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    // printMutex.lock();
    vprintf(new_format, args);
    // printMutex.unlock();
    va_end(args);
  #endif
}

template<typename BUFFER, typename TYPE>
void memset_seq (BUFFER *array, TYPE size)
{
  for (TYPE i=0; i<size; i++)
    array[i] = (BUFFER) i;
}

template<typename BUFFER, typename TYPE>
void memset_seq (BUFFER *array, TYPE size, BUFFER first_value)
{
  for (TYPE i=0; i<size; i++)
    array[i] = (BUFFER) (first_value + i);
}

template<typename BUFFER, typename TYPE>
void memset_val (BUFFER *array, TYPE size, BUFFER value)
{
  for (TYPE i=0; i<size; i++)
    array[i] = (BUFFER) value;
}

template<typename BUFFER, typename TYPE>
void sort (BUFFER values[], TYPE size)
{
    for (TYPE i=1; i<size; i++)
        for (TYPE j=0; j<size; j++)
            if (values[j]<values[j+1])
            {
                BUFFER temp = values[j];
                values[j] = values[j+1];
                values[j+1] = temp;
            }
}

template<typename TYPE>
void printArray (TYPE *array, long size)
{
  for (long i=0; i<size; i++)
    printf ("%d\n", (int)array[i]);
}

template<typename TYPE>
void printArray (TYPE *array, long size, const char *spacing)
{
  for (long i=0; i<size; i++)
    printf ("%d%s", (int)array[i], spacing);
  printf ("\n");
}

uint32_t random (uint32_t min, uint32_t max)
{
    uint32_t n = max - min + 1;
    uint32_t remainder = RAND_MAX % n;
    uint32_t x;
    do{
        x = uint32_t();
    }while (x >= RAND_MAX - remainder);
    return min + x % n;
}

template<typename BUFFER, typename TYPE>
void shuffle (BUFFER *input, TYPE count)
{
  srand (unsigned(time(0)));
  vector<BUFFER> inputVector;

  for (int i=0; i<count; ++i) 
    inputVector.push_back(input[i]);

  random_shuffle (inputVector.begin(), inputVector.end());

  BUFFER *vectorArray = &inputVector[0];

  memcpy (input, vectorArray, (count)*sizeof(BUFFER));
}

template<typename TYPE>
bool is_within_range (TYPE address, TYPE min, TYPE max)
{
  if ( (address >= min) && (address < max ) )
    return true;
  return false;
}

template<typename BUFFER, typename TYPE>
void new_sequence(BUFFER dest_address, TYPE min_value, TYPE max_value)
{
  for (int i=0; i<(max_value-min_value+1); ++i) 
    dest_address[i] = min_value+i;
}

template<typename TYPE>
TYPE getSum (TYPE *buffer, int count)
{
  TYPE sum = 0;

  for (int i=0; i<count; i++)
    sum += buffer[i];

  return sum;
}

template<typename TYPE>
TYPE getMax (TYPE *buffer, int count)
{
  TYPE max = 0;

  for (int i=0; i<count; i++)
    max = buffer[i]>max?buffer[i]:max;

  return max;
}

template<typename TYPE>
TYPE get2DSum (TYPE **buffer, int count)
{
  TYPE sum = 0;

  for (int i=0; i<count; i++)
    for (int j=0; j<count; j++)
      sum += buffer[i][j];

  return sum;
}

#endif