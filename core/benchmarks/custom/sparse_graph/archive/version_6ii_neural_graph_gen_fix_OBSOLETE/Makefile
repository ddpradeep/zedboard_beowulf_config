# Default Libraries #
MPI=/opt/mpich-3.1_perf
DATAFLOW_ROUTER=

# Detect environment
HOST=$(shell hostname)
ifeq ($(HOST), jetson0)
DATAFLOW_ROUTER=~/workspace/jetson/
MPI_HOSTFILE_EXT=jetson
else ifeq ($(shell uname -m), x86_64)
DATAFLOW_ROUTER=~/workspace/
MPI_HOSTFILE_EXT=x86
else
DATAFLOW_ROUTER=~/
MPI_HOSTFILE_EXT=zed
endif

# Default number of processes #
NP=2
OMP=1

# Program specific arguments #
M=120000
N=1200000
REPEAT=24

# Configure Compiler #
CC=g++
MPICC=$(MPI)/bin/mpic++
SRCC=$(wildcard *.cc)
CFLAGS=-O3 -Wno-write-strings -Wno-unused-result

# export MPICH_NEMESIS_NETMOD := mx

# Update Environment for MPI #
ifeq ($(MPI), /opt/openmpi-1.8)
	MPI_ARG=-machinefile ompi.hf.$(MPI_HOSTFILE_EXT) -prefix $(MPI)
else
	MPI_ARG=-machinefile mpich.hf.$(MPI_HOSTFILE_EXT)
endif

export PATH := $(PATH):$(MPI)/bin
export LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(MPI)/lib
#cp ~/dataflow_router/src/support/libgraph_support.so . && $(MPICC) mpiscatter-test.cpp $(INC) -L./ -lgraph_support $(CFLAGS) -o graph_mpi.o

export OMP_NUM_THREADS=$(OMP)

all: neuralsim.o

neuralsim.o: libmpiscatter.so libgraph_support.so neuralsim.h neuralsim.cpp $(wildcard *.cc)
	$(MPICC) $(CFLAGS) -L. neuralsim.cpp $(wildcard *.cc) -o neuralsim.o -fopenmp -lmpiscatter -lgraph_support

libmpiscatter.so: mpiscatter.h mpiscatter.cpp build_mpi_structs.h
	$(MPICC) $(CFLAGS) -fPIC -c mpiscatter.cpp -o mpiscatter.o
	$(MPICC) -shared  -Wl,-soname,libmpiscatter.so -o libmpiscatter.so mpiscatter.o

libgraph_support.so:
	cp $(DATAFLOW_ROUTER)dataflow_router/src/support/libgraph_support.so .

run: all
	mpirun $(MPI_ARG) -np $(NP) ./neuralsim.o $(M) $(N) $(REPEAT)

# <--
scatter-test.o: libmpiscatter.so libgraph_support.so mpiscatter-test.cpp
	$(MPICC) -L. mpiscatter-test.cpp -o scatter-test.o -lmpiscatter -lgraph_support

run_test: scatter-test.o
	mpirun $(MPI_ARG) -np $(NP) ./scatter-test.o $(M) $(N) $(REPEAT)
# --> for MPI only testing

clean:
	rm -rf *.so *.o

test: libgraph_support.so
	$(MPICC) -g test_graph.cpp -o test_graph.o
