#!/bin/bash

# nodes
for ((M=2**20; M>=2**10; M=M/2))
{
	# edges
	for ((N=2**21; N>=M; N=N/2))
	{
		# mpi nodes
		for ((NP=4; NP>=1; NP=NP/2))
		{
			# omp threads
			for ((OMP=2; OMP>=1; OMP=OMP/2))
			{
				if ! (( M == N ))
				then
					echo "make run NP=$NP OMP=$OMP M=$M N=$N | grep ignore >> results_arm_zedwulf.csv"
					make run NP=$NP OMP=$OMP M=$M N=$N | grep ignore >> results_arm_zedwulf.csv
				fi
			}
		}
	}
}
