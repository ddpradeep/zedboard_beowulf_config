#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "mmio.h"
#include "graph_support.h"

using namespace std;
using namespace boost;

void partition_graph_with_hmetis(Graph g, int partitions) 
{

	// Call to HMETIS that needs careful construction... Grr...
	int nvtxs = num_vertices(g);
	int nhedges = num_edges(g);

	// input node and edge weights..
	// set to 1 for now..
	int* vwgts = (int*)malloc(nvtxs*sizeof(int));
	for(int i=0; i<nvtxs; i++) {
		vwgts[i]=1;
	}
	int* hewgts = (int*)malloc(nhedges*sizeof(int));
	for(int i=0; i<nhedges; i++) {
		hewgts[i]=1;
	}

	// get the property map for vertex indices
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);

	int options[9]={0};  // options[0]=0 runs hmetis with default options..

	// outputs of hmetis
	int edgecut=-1;

	// Write out hmetis call
	int self_edges = 0;
	graph_traits<Graph>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
		if(node_index[source(*ei,g)]==node_index[target(*ei,g)]) {
			self_edges++;
		}
	}
	std::ofstream hmetis_input;
	hmetis_input.open("graph.hgr");
	hmetis_input << (nhedges-self_edges) << " " << nvtxs << endl;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
		if(node_index[source(*ei,g)]!=node_index[target(*ei,g)]) {
			hmetis_input << node_index[source(*ei, g)]+1 << " " << node_index[target(*ei, g)]+1 << endl;
		}
	}
	hmetis_input.close();

	std::ostringstream command;
	command << "/opt/hmetis/Linux-x86_64/hmetis2.0pre1 graph.hgr ";
	command << partitions << " > /dev/null" ;

	system(command.str().c_str());

}

void partition_graph_with_patoh(Graph g, int partitions) 
{

	// Call to PaToH that needs careful construction... Grr...
	int nvtxs = num_vertices(g);
	int nhedges = num_edges(g);

	// input node and edge weights..
	// set to 1 for now..
	int* vwgts = (int*)malloc(nvtxs*sizeof(int));
	for(int i=0; i<nvtxs; i++) {
		vwgts[i]=1;
	}
	int* hewgts = (int*)malloc(nhedges*sizeof(int));
	for(int i=0; i<nhedges; i++) {
		hewgts[i]=1;
	}

	// get the property map for vertex indices
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);

	// outputs of PaToH
	int edgecut=-1;

	// Write out PaToH input files
	int self_edges = 0;
	int pins = 0;
	graph_traits<Graph>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
		if(node_index[source(*ei,g)]==node_index[target(*ei,g)]) {
			self_edges++;
		} else {
			pins=pins+2;
		}
	}
	std::ofstream patoh_input;
	patoh_input.open("graph.hgr");
	// 4 integers on first line
	// 0-index, nodes, nets, pins
	patoh_input << "0 " << nvtxs << " " << (nhedges-self_edges) << " " << pins << endl;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
		if(node_index[source(*ei,g)]!=node_index[target(*ei,g)]) {
			patoh_input << node_index[source(*ei, g)] << " " << node_index[target(*ei, g)] << endl;
		}
	}
	patoh_input.close();

	std::ostringstream command;
	command << "/opt/patoh/patoh graph.hgr ";
	command << partitions << " > /dev/null" ;

	system(command.str().c_str());

}

void read_hmetis_partition(int partitions, int* part) {
	std::ostringstream filename;
	filename << "graph.hgr.part.";
	filename << partitions;

	// read back the partition ids...
	ifstream partition_file;
	partition_file.open(filename.str().c_str());
	
	string line;
	int node_index=0;
	if(partition_file.is_open()) {
		while(partition_file.good()) {
			getline(partition_file, line);
			if (!line.empty())
			{
				int partition_id = atoi(line.c_str());
				part[node_index++] = partition_id;
			}
		}
	}
}

void read_patoh_partition(int partitions, int* part) {
	std::ostringstream filename;
	filename << "graph.hgr.part.";
	filename << partitions;

	// read back the partition ids...
	ifstream partition_file;
	partition_file.open(filename.str().c_str());
	
	string line;
	int node_index=0;
	if(partition_file.is_open()) {
		while(partition_file.good()) {
			while(getline(partition_file, line,' ')) {
				int partition_id = atoi(line.c_str());
				part[node_index++] = partition_id;
				//cout << "node=" << node_index << ",partition="<< partition_id << endl;
			}
		}
	}
}
// What a massive failure!!! Why can't the property maps on vertices be mutable??
void annotate_graph_node_with_partition_id(Graph& g, int* partitions) {
	
	typedef property_map<Graph, vertex_index_t>::type NodeIndexMap;
	NodeIndexMap node_index = get(vertex_index, g);
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodePartitionMap;
	NodePartitionMap node_partition = get(&node_property_for_graphs_t::partition, g);
		
	graph_traits<Graph>::vertex_iterator vi, vi_end;
	for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
		put(&node_property_for_graphs_t::partition, g, *vi, partitions[node_index[*vi]]);
	}

}

void write_graph_file(Graph g) 
{

	// Call to HMETIS that needs careful construction... Grr...
	int nvtxs = num_vertices(g);
	int nhedges = num_edges(g);

	// get the property map for vertex indices
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);

	std::ofstream router_input;
	router_input.open("autogen.graph");
	graph_traits<Graph>::edge_iterator ei, ei_end;
	for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
		if(node_index[source(*ei,g)]!=node_index[target(*ei,g)]) {
			router_input << node_index[source(*ei, g)] << ",";
			router_input << node_index[target(*ei, g)] << ",";
			router_input << endl;
		}
	}
	router_input.close();

}

void write_place_file(Graph g) 
{

	// Call to HMETIS that needs careful construction... Grr...
	int nvtxs = num_vertices(g);
	int nhedges = num_edges(g);

	// get the property map for vertex indices
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodePartitionMap;
	NodePartitionMap node_partition = get(&node_property_for_graphs_t::partition, g);
	

	std::ofstream router_input;
	router_input.open("autogen.place");
	graph_traits<Graph>::vertex_iterator vi, vi_end;
	for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
		router_input << node_index[*vi] << "," << node_partition[*vi] << endl;
	}
	router_input.close();

}

void get_2d_id(int i, int pe_x, int pe_y, int* x, int* y) {
	*x=i/pe_y;
	*y=(i-*x*pe_y)%pe_y;
}

// build the graph machine data structures
graph_memory_t* get_gmimage_from_graph(Graph g, int partitions, int pe_x, int pe_y, int *part) 
{
	// setup data structures on a per PE basis!
	graph_memory_t *graph_memory;
	
	graph_memory = (graph_memory_t*)malloc(sizeof(graph_memory_t)*partitions);
	
	// this is the one thing that needs an out-edge pass.... followed by an in-edge pass
	int* packet_destination= (int*)malloc(num_edges(g)*sizeof(int));

	// setup the graph structure and edge information arrays..
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodeIndexMap;
	typedef property_map<Graph, int node_property_for_graphs_t::*>::type NodePartitionMap;
	typedef property_map<Graph, int edge_property_for_graphs_t::*>::type EdgeIndexMap;
	typedef property_map<Graph, double edge_property_for_graphs_t::*>::type EdgeValueMap;
	NodeIndexMap node_index = get(&node_property_for_graphs_t::index, g);
	NodePartitionMap node_partition = get(&node_property_for_graphs_t::partition, g);
	EdgeIndexMap edge_index = get(&edge_property_for_graphs_t::index, g);
	EdgeValueMap edge_value = get(&edge_property_for_graphs_t::val, g);

	graph_traits<Graph>::vertex_iterator vi, vi_end;

	// iterate over the processing elements...
	for(int pe=0;pe<partitions;pe++) {

		// build the graph memory structure for this pe
		graph_memory_t& graph_memory_for_pe = graph_memory[pe];

		// node and edge counting should be done on a per-PE basis!
		int node_counter=0;
		int in_edge_counter=0;
		
		// iterate over all edges...
		for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
			
			if(part[node_index[*vi]]==pe) {
				
				// record the starting offset for edges belonging to this node..
				graph_memory_for_pe.node_memory[node_counter].edge_mem_input_offset = in_edge_counter;
				graph_memory_for_pe.node_memory[node_counter].node_state = node_index[*vi];

				int in_edge_counter_per_node=0;
				graph_traits<Graph>::edge_descriptor e;
				graph_traits<Graph>::in_edge_iterator in_i, in_end;
				for (tie(in_i, in_end) = in_edges(*vi, g); in_i != in_end; ++in_i) {
					e = *in_i;
				
					graph_memory_for_pe.edge_input_memory[in_edge_counter].message_address = in_edge_counter;
					graph_memory_for_pe.edge_input_memory[in_edge_counter].edge_state = edge_value[e];
					packet_destination[edge_index[e]] = in_edge_counter;
					
					in_edge_counter++;
					in_edge_counter_per_node++;

					assert(in_edge_counter <= MAX_EDGES_PER_PE);

				}
					
				// count in edges per node...
				graph_memory_for_pe.node_memory[node_counter].edge_input_count = in_edge_counter_per_node;
				
				node_counter++;
				assert(node_counter <= MAX_NODES_PER_PE);
			}
		}

		graph_memory_for_pe.max_nodes = node_counter;
		graph_memory_for_pe.max_in_edges = in_edge_counter;
		
	}

	// iterate over the processing elements...
	for(int pe=0;pe<partitions;pe++) {

		// build the graph memory structure for this pe
		graph_memory_t& graph_memory_for_pe=graph_memory[pe];
		
		// need to count output edge on a per-PE basis!
		int node_counter = 0;
		int out_edge_counter=0;

		for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
		
			if(part[node_index[*vi]]==pe) {

				graph_memory_for_pe.node_memory[node_counter].edge_mem_output_offset = out_edge_counter;
		
				int out_edge_counter_per_node=0;
				graph_traits<Graph>::edge_descriptor e;
				graph_traits<Graph>::out_edge_iterator out_i, out_end;
				for (tie(out_i, out_end) = out_edges(*vi, g); out_i != out_end; ++out_i) {
					e = *out_i;
					// UGH! Src->Dest PE calculator should be identical you fucker!
					int x;
					int y;
					get_2d_id(part[node_index[target(e,g)]],pe_x,pe_y,&x,&y);
					graph_memory_for_pe.edge_output_memory[out_edge_counter].pe_address.X = x; 
					graph_memory_for_pe.edge_output_memory[out_edge_counter].pe_address.Y = y; 
					graph_memory_for_pe.edge_output_memory[out_edge_counter].graph_address = packet_destination[edge_index[e]];
					
					graph_memory_for_pe.edge_output_memory[out_edge_counter].edge_state = 0; 
					
					out_edge_counter++;
					out_edge_counter_per_node++;
					assert(out_edge_counter <= MAX_EDGES_PER_PE);
				}

				graph_memory_for_pe.node_memory[node_counter].edge_output_count = out_edge_counter_per_node;
				node_counter++;

			}
				
		}

		graph_memory_for_pe.max_out_edges = out_edge_counter;	
	}
	
	return graph_memory;
}


void write_gmimage_to_header(graph_memory_t* graph_memory, int partitions, int pe_x, int pe_y) {

	ofstream fp;
	fp.open ("data_memory.h");
	//serialize for individual partition elements
	for(int pe=0;pe<partitions;pe++) {
		// TODO: replace with VPR-extracted 2D placement
		int x;
		int y;
		get_2d_id(pe,pe_x,pe_y,&x,&y);

		stringstream pe_str1;
		pe_str1 << "_PE_" << x << "_" << y;
		string pe_str = pe_str1.str();

		// build the graph memory structure for this pe
		graph_memory_t *graph_memory_for_pe=&graph_memory[pe];
		
		fp << "#define NODE_COUNT" << pe_str << " " << graph_memory_for_pe->max_nodes << endl;
		fp << "#define EDGE_INPUT_COUNT" << pe_str << " " << graph_memory_for_pe->max_in_edges << endl;
		fp << "#define EDGE_OUTPUT_COUNT" << pe_str << " " << graph_memory_for_pe->max_out_edges << endl;		

		fp << "#define NODE_MEM_BLK" << pe_str  << " \\" << endl;
		fp << "{\\" << endl;
		for(int i=0;i<graph_memory_for_pe->max_nodes;i++) {	
			node_memory_t* node = &(graph_memory_for_pe->node_memory[i]);
			
			fp << "\t{";
			fp << "" << node->edge_input_count << ",";
			fp << "" << node->edge_output_count << ",";
			fp << "" << node->edge_mem_input_offset << ",";
			fp << "" << node->edge_mem_output_offset << ",";
			fp << "" << node->node_state << "";
			fp << "},\\" << endl;
		}
		fp << "}" << endl;
		
		fp << "#define INEDGE_MEM_BLK" << pe_str << " \\" << endl;
		fp << "{\\" << endl;
		for(int i=0; i<graph_memory_for_pe->max_in_edges; i++) {
			edge_input_memory_t* edge_input  =  &(graph_memory_for_pe->edge_input_memory[i]);

			// hard wire edge state to 0
			fp << "\t{" << edge_input->message_address << ",0},\\" << endl;
			//fp << edge_input->edge_state;
			
		}
		fp << "}" << endl;
		
		fp << "#define OUTEDGE_MEM_BLK" << pe_str << " \\" << endl;
		fp << "{\\" << endl;
		for(int i=0; i<graph_memory_for_pe->max_out_edges; i++) {
			edge_output_memory_t* edge_output = &(graph_memory_for_pe->edge_output_memory[i]);
			pe_address_t* pe_addr = &(edge_output->pe_address);
			fp << "\t{";
			fp << "{" << (int)pe_addr->X << "," << (int)pe_addr->Y << "}";
			fp << "," << edge_output->graph_address;
			fp << "," << edge_output->edge_state;
			fp << "},\\" << endl;
			
		}
		fp << "}" << endl;
	}
	fp.close();

}

void write_graph_properties(Graph g, graph_memory_t* graph_memory, int partitions, int pe_x, int pe_y) {
	
	ofstream fp;
	fp.open ("graphstats.dat", ios::out | ios::app);
	//serialize for individual partition elements
	for(int pe=0;pe<partitions;pe++) {

		// build the graph memory structure for this pe
		graph_memory_t *graph_memory_for_pe=&graph_memory[pe];
		int nodes =  graph_memory_for_pe->max_nodes;
		int in_edges = graph_memory_for_pe->max_in_edges;
		int out_edges = graph_memory_for_pe->max_out_edges;		
		fp << nodes << "," << in_edges << "," << out_edges << endl;
//		for(int i=0;i<graph_memory_for_pe->max_nodes;i++) {	
//			node_memory_t* node = &(graph_memory_for_pe->node_memory[i]);
//			fp << "" << node->edge_input_count << ",";
//			fp << "" << node->edge_output_count << ",";
//		}
	
	}
	fp.close();

}

// void build_mpi_structures(graph_memory_t* graph_memory, 
// 		mpi_memory_t* mpi_memory,
// 		int partitions, int pe_x, int pe_y) {
	
// 	//serialize for individual partition elements
// 	for(int pe_src=0;pe_src<partitions;pe_src++) {

// 		// build the graph memory structure for this pe
// 		graph_memory_t *graph_memory_for_pe=&graph_memory[pe_src];
// 		int nodes =  graph_memory_for_pe->max_nodes;
// 		// construct PUT for each pe_dest separately..
// 		for(int pe_dest=0;pe_dest<partitions;pe_dest++) {
// 			send_counter=0;
// 			// TODO: find a way to store the addresses into mpi_memory_t somehow..
// 			send_addr = malloc(*sizeof(int));
// 			recv_addr = malloc(*sizeof(int));
// 			// loop over all nodes in pe
// 			for(int i=0;i<graph_memory_for_pe->max_nodes;i++) {	
// 				node_memory_t* node = &(graph_memory_for_pe->node_memory[i]);
// 				int output_edges_for_node = node->edge_output_count;
// 				// loop over all output edges for node
// 				for(int j=0;j<output_edges_for_node;j++) {
// 					edge_output_memory_t* edge = 
// 						&(graph_memory_for_pe->edge_output_memory[i]);
// 					// collect send/recv information
// 					int address = edge->graph_address;
// 					pe_address_t pe_edge = edge->pe_address;
// 					// check if the destination matches pe_dest
// 					if(pe_edge.X*pe_y+pe_edge.Y==pe_dest) {
// 						// do stuff to build coalesced PUT...
// 						// where to get data from = node[i]
// 						send_addr[send_counter]=i;
// 						recv_addr[send_counter]=address;
// 						send_counter++;
// 					}
// 				}
// 			}
// 		}
// 	}

// }
