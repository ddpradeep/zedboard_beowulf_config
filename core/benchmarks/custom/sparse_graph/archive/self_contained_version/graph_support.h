#ifndef graph_support_H
#define graph_support_H

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <fstream> 
#include "graph_memory.h"

using namespace std;
using namespace boost;

struct edge_property_for_graphs_t {
	int index;
	double val;
};	

struct node_property_for_graphs_t {
	int index;
	int partition;
};

typedef adjacency_list<vecS, vecS, bidirectionalS, node_property_for_graphs_t, edge_property_for_graphs_t > Graph;

// convert a sparse matrix to a graph
Graph get_graph_from_sparse_matrix(char* matrix_file);
int read_matrix(char* matrix_file, int** row, int** col, double** val, int* M, int* N, int* nz);
Graph build_graph(int* row, int* col, double* val, int M, int N, int nz);
void write_graph(Graph g);

// partitioning the graph
void partition_graph_with_hmetis(Graph g, int partitions);
void partition_graph_with_patoh(Graph g, int partitions);
void read_hmetis_partition(int partitions, int *part);
void read_patoh_partition(int partitions, int *part);
void annotate_graph_node_with_partition_id(Graph &g, int* part);
graph_memory_t* get_gmimage_from_graph(Graph g, int partitions, int pe_x, int pe_y, int *part);

// write graph file for router
void write_graph_file(Graph g);
void write_place_file(Graph g);
void write_gmimage_to_header(graph_memory_t* graph_memory, int partitions, int pe_x, int pe_y);
void write_graph_properties(Graph g, graph_memory_t* graph_memory, int partitions, int pe_x, int pe_y);

// mpi-related structures
void build_mpi_structures(Graph g, graph_memory_t* graph_memory, int partitions, int pe_x, int pe_y);

#endif
