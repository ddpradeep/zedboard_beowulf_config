#!/bin/bash

REPEAT_COUNT=24
START_NODE_SIZE=2
END_NODE_SIZE=32
MTX_FILES_ONLY_DIR=mtx

WORKLOAD_FILE="workload.txt"

rm -rf $WORKLOAD_FILE

for MTX_FILE in $(ls $MTX_FILES_ONLY_DIR -S)
do
	for NP in $(seq $END_NODE_SIZE -1 $START_NODE_SIZE)
	do
		for SCATTER_TYPE in 0 1 2 3
		do
			for CACHE_STATUS in 0 1
			do
				for OVERLOAD in 0 1
				do
					CMD="make run MPI=/opt/mpich-3.1 REPEAT=$REPEAT_COUNT MTX_FILE=$MTX_FILES_ONLY_DIR/$MTX_FILE NP=$NP SCATTER_TYPE=$SCATTER_TYPE R_CACHE=$CACHE_STATUS OVERLOAD=$OVERLOAD SEP_PROFILE=0 FINE_GRAINED=0"
					echo "$CMD" >> $WORKLOAD_FILE
				done
			done
		done

		for SCATTER_TYPE in 0 1
		do
			for CACHE_STATUS in 0 1
			do
				for OVERLOAD in 0 1
				do
					CMD="make run MPI=/opt/mpich-3.1 REPEAT=12 MTX_FILE=$MTX_FILES_ONLY_DIR/$MTX_FILE NP=$NP SCATTER_TYPE=$SCATTER_TYPE R_CACHE=$CACHE_STATUS OVERLOAD=$OVERLOAD SEP_PROFILE=0 FINE_GRAINED=1"
					echo "$CMD" >> $WORKLOAD_FILE
				done
			done
		done

		for SCATTER_TYPE in 1
		do
			for CACHE_STATUS in 0 1
			do
				CMD="make run MPI=/opt/mpich-3.1 REPEAT=$REPEAT_COUNT MTX_FILE=$MTX_FILES_ONLY_DIR/$MTX_FILE NP=$NP SCATTER_TYPE=$SCATTER_TYPE R_CACHE=$CACHE_STATUS OVERLOAD=0 SEP_PROFILE=1 FINE_GRAINED=0"
				echo "$CMD" >> $WORKLOAD_FILE
			done
		done
	done
done