#include "mpiscatter.h"
#include "neuralsim.h"
#include "fpga_stream.h"
#include "omp.h"

#include <thread>
#include <mutex>

#define CORE_ID_MAIN	0
#define CORE_ID_SEND_PL	0
#define CORE_ID_RECV_PL 1

// neural variables
BOOL* fired;
REAL* fpga_stream;

// mpi variables
MPIScatter *mpiscatter;
int *col_index;

// fpga variables
FPGA *PL;
REAL *fpga_receive_stream;

// thread variables
mutex loop_mutex;
int shared_loop_start_ctr = 0;
int shared_loop_end_ctr = 0;
int shared_loop_swap_ctr = 0;

void send_to_pl(int repeat_count, double *time_taken);
void recv_fr_pl(int repeat_count, double *time_taken);

int main(int argc, char** argv)
{
	stick_this_thread_to_core (CORE_ID_MAIN);

	mpiscatter = new MPIScatter(&argc, &argv);	// MPI init

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                        Generate and Distribute Graph
	/******************************************************************************/

	mpiscatter->build_random_graph(M, N);
	mpiscatter->distribute_graph_structure();
	int total_nodes = M;
	int total_edges = N;
	M = mpiscatter->get_num_nodes();		// update M to reflect num nodes in this PE
	N = mpiscatter->get_num_in_edges();	// update N to reflect num incomming edges in this PE

	/******************************************************************************/
	//                           Initialize Variables
	/******************************************************************************/

	fpga_stream 	= new REAL [FPGA_STREAM_NUM_VECTORS*M + FPGA_STREAM_NUM_EDGE_DESCRIPTOR*N];
	fired 			= (BOOL*)malloc(M*sizeof(BOOL));

	// get edges to node mapping array (col_index)
	col_index = mpiscatter->get_edges_to_node_mapping_addr();

	// initialize all variables (excpet incoming edges)
	for(int m=0, edges_before_this_node=0;m<M;m++)
	{
		int offset_to_this_node = m*FPGA_STREAM_NUM_VECTORS + edges_before_this_node*FPGA_STREAM_NUM_EDGE_DESCRIPTOR;

		*(int *)&fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_num_edges] = (int) col_index[m];
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_a]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_b]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_c]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_d]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_I]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_V]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_u]	= ((REAL) rand() / (RAND_MAX)) + 1;
		fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_fired] = fired[m] = rand()%2;

		for (int n=0; n<col_index[m]; n++)
		{
			int offset_to_first_edge = offset_to_this_node + FPGA_STREAM_OFFSET_edge;
			fpga_stream [offset_to_first_edge + (n*FPGA_STREAM_NUM_EDGE_DESCRIPTOR+1)]	= ((REAL) rand() / (RAND_MAX)) + 1;
		}

		edges_before_this_node += col_index[m];
	}

	//	set the node_memory buffer to point to 'fired (old)'
	mpiscatter->attach_node_memory (fired);

	// get the address of message memory buffer (new)
	// mpiscatter->create_message_memory();
	// in_fired_msg = mpiscatter->get_message_memory_addr();
	// in_fired_msg_old = new BOOL [N];
	mpiscatter->attach_message_memory (fpga_stream);

	/******************************************************************************/
	//                        		Neural Simulation
	/******************************************************************************/

	TIMER timer;
	double *time_pl_send = new double [repeat_count];
	double *time_pl_recv = new double [repeat_count];
	double *time_compute_comms = new double [repeat_count];
	double *time_compute_post_process = new double [repeat_count];
	double *time_scatter = new double [repeat_count];
	double *time_total = new double [repeat_count];

	// init fpga stuff
	fpga_receive_stream = new REAL [4*mpiscatter->get_num_nodes()];
	if (mpiscatter->get_rank() != MASTER)
		PL = new FPGA ("/dev/xillybus_read_32", "/dev/xillybus_write_32");
	thread send_to_pl_thread (send_to_pl, repeat_count, time_pl_send);
	thread recv_fr_pl_thread (recv_fr_pl, repeat_count, time_pl_recv);

	// record timings
	for (int i=0; i<repeat_count; i++)
	{
		// wait for all threads to reach this
		MPI_Barrier (MPI_COMM_WORLD);
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		// compute
		// if (mpiscatter->get_rank() != MASTER)
		// {
		// 	PL->send (fpga_stream, transfer_size);
		// 	PL->recv (fpga_receive_stream, 4*mpiscatter->get_num_nodes());
		// }

		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (100); }

		time_compute_comms[i] = max (time_pl_send[i], time_pl_recv[i]);

		if (mpiscatter->get_rank() != MASTER)
		{
			resetTime(&timer);
			for(int m=0, edges_before_this_node=0; m<mpiscatter->get_num_nodes(); m++)
			{
				// update fpga_stream by scattering fpga_receive_stream 
				int offset_to_this_node = m*FPGA_STREAM_NUM_VECTORS + edges_before_this_node*FPGA_STREAM_NUM_EDGE_DESCRIPTOR;

				fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_I]	= fpga_receive_stream[m*4 + 0];
				fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_V]	= fpga_receive_stream[m*4 + 1];
				fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_u]	= fpga_receive_stream[m*4 + 2];
				fpga_stream [offset_to_this_node + FPGA_STREAM_OFFSET_fired]	= fpga_receive_stream[m*4 + 3];
				fired[m] = fpga_receive_stream[m*4 + 3];

				edges_before_this_node += col_index[m];
			}
			time_compute_post_process[i] = getTime(&timer);

			PL->reset_pl();
		}

		MPI_Barrier(MPI_COMM_WORLD);

		// mpi scatter
		resetTime(&timer);
		mpiscatter->scatter();
		time_scatter[i] = getTime(&timer);

		time_total[i] = (time_compute_comms[i]+time_compute_post_process[i]) + time_scatter[i];

		if (mpiscatter->get_rank() == MASTER)
		{
			if (i%10==0)
				printf ("%d", i);
			else
				printf (".");
		}

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
		// printf ("%d rank - %d done\n", mpiscatter->get_rank(), i);
	}

	send_to_pl_thread.join();
	recv_fr_pl_thread.join();

	// printf ("%d - here M = %d\n", mpiscatter->get_rank(), M);
	// paranoid code
	BOOL not_used_val = 0;
	for (int i=0; i<M; i++)
		not_used_val += fired[i];

	/******************************************************************************/
	//                        			Results
	/******************************************************************************/

	if (mpiscatter->get_rank() == MASTER)
	{
		delete [] time_compute_comms, time_compute_post_process, time_scatter, time_total;
		time_compute_comms 			= new double [mpiscatter->get_total_procs()];
		time_compute_post_process 	= new double [mpiscatter->get_total_procs()];
		time_scatter 				= new double [mpiscatter->get_total_procs()];
		time_total 					= new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_ub_MBps = new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_lb_MBps = new double [mpiscatter->get_total_procs()];

		// get compute time from nodes
		for (int pe=0; pe<mpiscatter->get_total_procs(); pe++)
		{
			MPI_Status status;
			MPI_Recv (time_compute_comms+pe, 1, MPI_DOUBLE, pe+1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv (time_compute_post_process+pe, 1, MPI_DOUBLE, pe+1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (time_scatter+pe, 1, MPI_DOUBLE, pe+1, 2, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total+pe, 1, MPI_DOUBLE, pe+1, 3, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_lb_MBps+pe, 1, MPI_DOUBLE, pe+1, 4, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_ub_MBps+pe, 1, MPI_DOUBLE, pe+1, 5, MPI_COMM_WORLD, &status);
		}

		sort (acp_bandwidth_lb_MBps, acp_bandwidth_lb_MBps+mpiscatter->get_total_procs());
		sort (acp_bandwidth_ub_MBps, acp_bandwidth_ub_MBps+mpiscatter->get_total_procs());
		sort (time_compute_comms, time_compute_comms+mpiscatter->get_total_procs());
		sort (time_compute_post_process, time_compute_post_process+mpiscatter->get_total_procs());
		sort (time_scatter, time_scatter+mpiscatter->get_total_procs());
		sort (time_total, time_total+mpiscatter->get_total_procs());

		double avg_acp_bandwidth_lb_MBps = acp_bandwidth_lb_MBps [mpiscatter->get_total_procs()/2];
		double avg_acp_bandwidth_ub_MBps = acp_bandwidth_ub_MBps [mpiscatter->get_total_procs()/2];
		double avg_time_compute_comms = time_compute_comms [mpiscatter->get_total_procs()/2];
		double avg_time_compute_post_process = time_compute_post_process [mpiscatter->get_total_procs()/2];
		double avg_time_compute = avg_time_compute_comms + avg_time_compute_post_process;
		double avg_time_scatter = time_scatter [mpiscatter->get_total_procs()/2];
		double avg_time_total 	= time_total [mpiscatter->get_total_procs()/2];

		printf ("%d\n", repeat_count);
		printf("{ignore %d}, %d, %d, %d, %d, %d, %.2f%%, < %.2f %.2f >, < %f, %f, %f >, %f, < %f, %f, %f >\n",
			(int) not_used_val,
			mpiscatter->get_total_procs(),
			omp_get_num_threads(),
			total_nodes,
			total_edges,
			mpiscatter->avg_outbound_count_per_node(),
			(float)mpiscatter->avg_outbound_count_per_node()*100.0/(float)mpiscatter->avg_send_count_per_node(),
			avg_acp_bandwidth_lb_MBps,
			avg_acp_bandwidth_ub_MBps,
			avg_time_compute_comms,
			avg_time_compute_post_process,
			avg_time_compute,
			avg_time_scatter,
			time_total [0], avg_time_total, time_total [mpiscatter->get_total_procs()-1]);
	}
	else
	{
		sort (time_compute_comms, time_compute_comms+repeat_count);
		sort (time_compute_post_process, time_compute_post_process+repeat_count);
		sort (time_scatter, time_scatter+repeat_count);
		sort (time_total, time_total+repeat_count);
		MPI_Send (time_compute_comms+repeat_count/2, 1, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
		MPI_Send (time_compute_post_process+repeat_count/2, 1, MPI_DOUBLE, MASTER, 1, MPI_COMM_WORLD);
		MPI_Send (time_scatter+repeat_count/2, 1, MPI_DOUBLE, MASTER, 2, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count/2, 1, MPI_DOUBLE, MASTER, 3, MPI_COMM_WORLD);
		
		double avg_time_compute_comms = time_compute_comms[repeat_count/2];
		int ps_pl_traffic = FPGA_STREAM_NUM_VECTORS*mpiscatter->get_num_nodes() + FPGA_STREAM_NUM_EDGE_DESCRIPTOR*mpiscatter->get_num_in_edges();
		int pl_ps_traffic = 4*mpiscatter->get_num_nodes();
		int total_traffic_ub_bytes = (2*ps_pl_traffic)*sizeof(REAL);
		int total_traffic_lb_bytes = (2*pl_ps_traffic)*sizeof(REAL);

		double acp_bandwidth_ub_MBps = ((double)total_traffic_ub_bytes/(1000000.0))/avg_time_compute_comms;
		double acp_bandwidth_lb_MBps = ((double)total_traffic_lb_bytes/(1000000.0))/avg_time_compute_comms;

		MPI_Send (&acp_bandwidth_lb_MBps, 1, MPI_DOUBLE, MASTER, 4, MPI_COMM_WORLD);
		MPI_Send (&acp_bandwidth_ub_MBps, 1, MPI_DOUBLE, MASTER, 5, MPI_COMM_WORLD);
	}

	return 0;
}

void send_to_pl (int repeat_count, double *time_taken)
{
	stick_this_thread_to_core (CORE_ID_SEND_PL);

	int transfer_size = FPGA_STREAM_NUM_VECTORS*mpiscatter->get_num_nodes() + FPGA_STREAM_NUM_EDGE_DESCRIPTOR*mpiscatter->get_num_in_edges();

	// old send buffer to new recv buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{	
		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		resetTime(&timer);
		if (mpiscatter->get_rank() != MASTER)
			PL->send (fpga_stream, transfer_size);
		time_taken[i] = getTime(&timer);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (100); }
	}

	debug ("%d - pls exiting\n", mpiscatter->get_rank());
}

void recv_fr_pl (int repeat_count, double *time_taken)
{
	stick_this_thread_to_core (CORE_ID_RECV_PL);

	// old send buffer to new recv buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{	
		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		resetTime(&timer);
		if (mpiscatter->get_rank() != MASTER)
			PL->recv (fpga_receive_stream, 4*mpiscatter->get_num_nodes());
		time_taken[i] = getTime(&timer);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (100); }
	}

	debug ("%d - plr exiting\n", mpiscatter->get_rank());
}