#!/bin/bash

cp version_3*/* .
for np in {15..16}; do make run NP=$np REPEAT=100 | grep PARALLEL_LOCAL_DIRECT >> results_sendrecv.txt; done

cp version_2*/* .
for np in {15..16}; do make run NP=$np REPEAT=100 | grep PARALLEL_LOCAL_DIRECT >> results_put_opti.txt; done

cp version_1*/* .
for np in {15..16}; do make run NP=$np REPEAT=100 | grep PARALLEL_LOCAL_DIRECT >> results_put.txt; done

