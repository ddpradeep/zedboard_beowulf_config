#include "fpga_stream.h"

FPGA::FPGA (char *readfile, char *writefile)
{
	config_console();

	assert ( open_write(writefile) && open_read(readfile) );
	reset_pl();	
}

FPGA::~FPGA()
{
	// printf ("deleting\n");
	close(fd_write);
	close(fd_read);
}

void FPGA::reset_pl()
{
	uint32_t reset_seq[] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
	int max_recv_size = 8000000;

	int *null_buff = new int [max_recv_size];

	int rc_w = write(fd_write, reset_seq, 3*sizeof(uint32_t));
	assert (rc_w == 3*sizeof(uint32_t));

	int rc_r = read (fd_read, null_buff, max_recv_size*sizeof(int));
	delete [] null_buff;

	// printf ("creared %d bytes from pl stream\n", rc_r);
}

/* config_console() does some good-old-UNIX vodoo standard input, so that
   read() won't wait for a carriage-return to get data. It also catches
   CTRL-C and other nasty stuff so it can return the terminal interface to
   what is was start_time. In short, a lot of mumbo-jumbo, with nothing relevant
   to Xillybus.
 */

void FPGA::config_console()
{
	struct termio console_attributes;

	if (ioctl(0, TCGETA, &console_attributes) != -1)
	{
		// If we got here, we're reading from console

		console_attributes.c_lflag &= ~ICANON; // Turn off canonical mode
		console_attributes.c_cc[VMIN] = 1; // One character at least
		console_attributes.c_cc[VTIME] = 0; // No timeouts

		if (ioctl(0, TCSETAF, &console_attributes) == -1)
			fprintf(stderr, "Warning: Failed to set console to char-by-char\n");
	}
}

int FPGA::open_read (char *file)
{
	fd_read = open(file, O_RDONLY);
	if (fd_read < 0)
	{
		if (errno == ENODEV)
		fprintf(stderr, "(Maybe %s a write-only file?)\n", file);

		perror("Failed to open writefile");
		exit(1);

		return false;
	}
	return true;
}

int FPGA::open_write (char *file)
{
	fd_write = open(file, O_WRONLY);
	if (fd_write < 0)
	{
		if (errno == ENODEV)
		fprintf(stderr, "(Maybe %s a read-only file?)\n", file);

		perror("Failed to open writefile");
		exit(1);

		return false;
	}
	return true;
}


int FPGA::send (REAL *send_buf, int send_count)
{
	// 2. Write data to stream
	int donebytes = 0;
	while (donebytes < send_count*sizeof(REAL))
	{
		int rc_w  = write(fd_write, send_buf+donebytes/sizeof(REAL), send_count*sizeof(REAL) - donebytes);
		int trash = write(fd_write, NULL, 0);

		if ((rc_w < 0) && (errno == EINTR))
			continue;

	    if (rc_w <= 0) 
	    {
			perror("write() failed");
			exit(1);
	    }

	    donebytes += rc_w;
	}

	if (donebytes != send_count*sizeof(REAL))
	{
		printf ("send %d of %d bytes\n", donebytes, send_count*sizeof(REAL));
	}

	// printf ("sent %d bytes (%dx9 + %d) \n", rc_w, rc_w/(4*9), (rc_w - rc_w/(4*9))/(2*4));
	// printf ("sent :\t");
	// for (int i=0; i<send_count; i++)
	// 	printf ("%d ", *((int *) &send_buf[i]));
	// printf ("\n");
}

int FPGA::recv (REAL *recv_buf, int recv_count)
{
	// 2. Write data to stream
	int donebytes = 0;
	while (donebytes < recv_count*sizeof(REAL))
	{
     	int rc = read (fd_read, recv_buf+donebytes/sizeof(REAL), recv_count*sizeof(REAL) - donebytes);
		if ((rc < 0) && (errno == EINTR))
			continue;

		if (rc < 0)
		{
			perror("read() failed");
			exit(1);
		}

		if (rc == 0)
		{
			fprintf(stderr, "Reached read EOF!? Should never happen.\n");
			exit(0);
		}

		donebytes += rc;
		// printf ("%d/%d done\n", donebytes, recv_count*sizeof(REAL));
		// printf ("recv :\t");
		// for (int i=0; i<recv_count; i++)
		// 	printf ("%d ", *((int *) &recv_buf[i]));
		// printf ("\n");
	}
	
	// if (donebytes != recv_count*sizeof(REAL))
	// {
	// 	printf ("recv %d of %d bytes\n", donebytes, recv_count*sizeof(REAL));
	// }

	// printf ("recv :\t");
	// for (int i=0; i<recv_count; i++)
	// 	printf ("%d ", *((int *) &recv_buf[i]));
	// printf ("\n");
}