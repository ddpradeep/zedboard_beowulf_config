#ifndef __FPGASTREAM_H__
#define __FPGASTREAM_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include <assert.h>
#include <stdint.h>
#include "neuralsim.h"

#define FPGA_STREAM_NUM_VECTORS			9
#define FPGA_STREAM_OFFSET_num_edges	0
#define FPGA_STREAM_OFFSET_a			1
#define FPGA_STREAM_OFFSET_b			2
#define FPGA_STREAM_OFFSET_c			3
#define FPGA_STREAM_OFFSET_d 			4
#define FPGA_STREAM_OFFSET_I			5
#define FPGA_STREAM_OFFSET_V			6
#define FPGA_STREAM_OFFSET_u 			7
#define FPGA_STREAM_OFFSET_fired		8

#define FPGA_STREAM_NUM_EDGE_DESCRIPTOR 2
#define FPGA_STREAM_OFFSET_edge 		9
#define FPGA_STREAM_OFFSET_weight 		9

class FPGA
{
	int fd_write, fd_read;

	void config_console();

public:
	FPGA (char *read_file, char *write_file);
	~FPGA ();

	int open_read(char *file);
	int open_write(char *file);

	void reset_pl();

	int send (REAL *send_buf, int send_count);
	int recv (REAL *recv_buf, int recv_count);

};

#endif