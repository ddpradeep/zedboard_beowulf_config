#include "mpiscatter.h"
#include <iostream>

using namespace std;


MPIScatter::MPIScatter(int *argc, char ***argv)
{
	/******************************************************************************/
	//                           MPI Initialization
	/******************************************************************************/
	init_rand();
	MPI_Init (argc, argv);		// initialization of MPI environment
	// int provided = 0;
	// MPI_Init_thread (argc, argv, MPI_THREAD_MULTIPLE, &provided);		// initialization of MPI environment
	// assert (provided == MPI_THREAD_MULTIPLE);

	MPI_Comm_size (MPI_COMM_WORLD, &PE);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// get current process rank
	
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %d running on %s\n", rank, processor_name);
	/******************************************************************************/

	// reduce PE size by 1 -> ignore MASTER 
 	PE -= 1;
}


int MPIScatter::get_rank()
{
	return rank;
}


int MPIScatter::get_total_procs()
{
	return PE;
}


int MPIScatter::avg_send_count_per_node()
{
	return avg_send_count;
}


int MPIScatter::avg_outbound_count_per_node()
{
	return avg_outbound_count;
}


void MPIScatter::MPI_Sendnext(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype)
{
	// 1. Proc 0: send only
	if (rank == 0)
		MPI_Send(sendbuf, sendcount, sendtype, rank+1, ADMIN_TAG, MPI_COMM_WORLD);

	// 2. Procs 1 - PE-2: sendrecv
	else if (rank < PE)
		MPI_Sendrecv(sendbuf, sendcount, sendtype, rank+1, ADMIN_TAG, recvbuf, recvcount, recvtype, rank-1, ADMIN_TAG, MPI_COMM_WORLD, &status);
	
	// 3. Proc PE-1: recv only
	else
		MPI_Recv(recvbuf, recvcount, recvtype, rank-1, ADMIN_TAG, MPI_COMM_WORLD, &status);
}


void MPIScatter::build_random_graph(int M, int N)
{
	/******************************************************************************/
	//                        		Build Graph
	/******************************************************************************/

	int *a_index;
	int *col_index;

	int pe_x = PE;
	int pe_y = 1;
	int partitions = pe_x*pe_y;
	assert(partitions==PE);

	if (rank == MASTER)
	{
		get_random_graph(M, N, &a_index, &col_index);

		// print_array (a_index, M, "a_index\t: ", "%d ");
		// print_array (col_index, N, "c_index\t: ", "%d ");
		
		int* part = (int*)malloc(M*sizeof(int));
		for(int i=0;i<M;i++)
		{
			int p = rand_between(0, PE);
			part[i] = p;
		}

		printf ("building...");
		build_mpi_structures(a_index, col_index, part, M, N, PE, &disp_memory);
		free (a_index);
		free (col_index);
		update_edges_for_fpga_streaming(&disp_memory);
	}
}


void MPIScatter::prepare_sendrecv_buffer()
{
	/******************************************************************************/
	//                        	Allocate Send/Recv Buffers
	/******************************************************************************/

	// Procs 0 to PE-2 only -> last proc chill..
	if (rank<PE)
	{
		MPI_Scatter(disp_memory.num_nodes, 1, MPI_INT, &num_nodes, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
		MPI_Scatter(disp_memory.num_in_edges, 1, MPI_INT, &num_in_edges, 1, MPI_INT, MASTER, MPI_COMM_WORLD);
	}

	// shift current data to next  process
	debug ("old PE%d: Num Nodes: %d\tNum In Edges: %d\n", rank, num_nodes, num_in_edges);

	MPI_Sendnext (&num_nodes, 1, MPI_INT, &mpi_memory.num_nodes, 1, MPI_INT);
	MPI_Sendnext (&num_in_edges, 1, MPI_INT, &mpi_memory.num_in_edges, 1, MPI_INT);

	// update num_nodes
	num_nodes 		= mpi_memory.num_nodes;
	num_in_edges 	= mpi_memory.num_in_edges;

	debug ("new PE%d: Num Nodes: %d\tNum In Edges: %d\n", rank, num_nodes, num_in_edges);

	/******************************************************************************/
	//                   Distribute Incomming Edge to Node Mapping
	/******************************************************************************/

	// allocate edge to node mapping
	mpi_memory.in_edges_for_node = new int [num_nodes];

	if (rank == MASTER)
	{
		// for number of nodes
		for (int dst=0; dst<PE; dst++)
		{
			MPI_Send(disp_memory.edges_for_node[dst], disp_memory.num_nodes[dst], MPI_INT, dst+1, ADMIN_TAG, MPI_COMM_WORLD);
		}
	}
	else
	{
		MPI_Recv(mpi_memory.in_edges_for_node, num_nodes, MPI_INT, MASTER, ADMIN_TAG, MPI_COMM_WORLD, &status);
	}

	// check if edge to node mapping makes sense
	int total_in_edges=0;
	for (int i=0; i<num_nodes; i++)
		total_in_edges += mpi_memory.in_edges_for_node[i];
	assert (num_in_edges == total_in_edges);

	/******************************************************************************/
	//						Average Outbound Traffic
	/******************************************************************************/

	if (rank == MASTER)
	{
		int *total_send_count = new int [PE];
		int *total_outbound_count = new int [PE];
		// for every node
		for (int i=0; i<PE; i++)
		{
			total_send_count[i] = 0;
			// for every other node
			for (int j=0; j<PE; j++)
				// count the number of outgoing edges EXCLUDING self
					total_send_count[i] += disp_memory.send_count[i][j];

			total_outbound_count[i] = 0;
			// for every other node
			for (int j=0; j<PE; j++)
				// count the number of outgoing edges EXCLUDING self
				if (i != j)
					total_outbound_count[i] += disp_memory.send_count[i][j];

			debug ("PE%d: Num Out Edges: %d/%d\n", i, total_outbound_count[i], total_send_count[i]);
		}

		// calculate average outbound traffic
		avg_send_count = 0;
		for (int i=0; i<PE; i++)
			avg_send_count += total_send_count[i];
		avg_send_count /= PE;

		avg_outbound_count = 0;
		for (int i=0; i<PE; i++)
			avg_outbound_count += total_outbound_count[i];
		avg_outbound_count /= PE;

		delete [] total_send_count;
		delete [] total_outbound_count;

		debug ("Avg_Send_cnt: %d/%d\n", avg_outbound_count, avg_send_count);
	}
}


void MPIScatter::distribute_sendrecv_buffer()
{
	/******************************************************************************/
	//                        Distribute Send/Recv Displacements
	/******************************************************************************/

	// initialize mpi_memory data structures
	mpi_memory.send_addr = new int* [PE];
	mpi_memory.recv_addr = new int* [PE];
	mpi_memory.send_count = new int [PE];
	mpi_memory.recv_count = new int [PE];

	if (rank == MASTER)
	{
		// printf ("Preparing to send...\n");
		// for every node
		for (int src=0; src<PE; src++)
		{
			// for number of nodes
			for (int dst=0; dst<PE; dst++)
			{
				MPI_Send(disp_memory.send_addr[src][dst], disp_memory.send_count[src][dst], MPI_INT, src+1, SEND_TAG_OFFSET+dst, MPI_COMM_WORLD);
				MPI_Send(disp_memory.recv_addr[src][dst], disp_memory.recv_count[src][dst], MPI_INT, dst+1, RECV_TAG_OFFSET+src, MPI_COMM_WORLD);
			}
		}

		// free up disp_memory
		free_disp_memory_t(&disp_memory, PE);
	}
	else
	{
		// for number of nodes
		for (int count=0; count<(PE*2); count++)
		{
			MPI_Status status;
			int incoming_msg_size;
			// receive send/recv pair for Node[rank] -> Node[dst] from Master

			// i. probe incoming message
			// debug ("rank-%d> probing for something from %d\n", rank, MASTER);
			MPI_Probe(MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			// debug ("rank-%d> probing success\n", rank);
			MPI_Get_count(&status, MPI_INT, &incoming_msg_size);
			if (status.MPI_TAG >= SEND_TAG_OFFSET)
			{
				// debug ("rank-%d> SEND_T incoming", rank);
				int dst = status.MPI_TAG - SEND_TAG_OFFSET;
				// ii. allocate buffer of probed size
				mpi_memory.send_addr[dst]  = new int [incoming_msg_size];
				mpi_memory.send_count[dst] = incoming_msg_size;
				// iii. receive send type for Node[rank] -> Node[dst]
				MPI_Recv(mpi_memory.send_addr[dst], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d: SEND[%d->%d] of size %d\n", rank, rank, dst+1, mpi_memory.send_count[dst]);
			
			}
			else
			{
				// debug ("rank-%d> RECV_T incoming", rank);
				int src = status.MPI_TAG;
				// ii. allocate buffer of probed size
				mpi_memory.recv_addr[src]  = new int [incoming_msg_size];
				mpi_memory.recv_count[src] = incoming_msg_size;
				// iii. receive recv type for Node[rank] <- Node[src]
				MPI_Recv(mpi_memory.recv_addr[src], incoming_msg_size, MPI_INT, MASTER, status.MPI_TAG, MPI_COMM_WORLD, &status);
				debug ("rank-%d: RECV[%d->%d] of size %d\n", rank, src+1, rank, mpi_memory.recv_count[src]);
			}
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
}

void MPIScatter::build_data_type()
{
	/******************************************************************************/
	//      Optimize Send/Recv Displacements to Maximize Receive Block Length
	/******************************************************************************/

	if (rank != MASTER)
	{
		mpi_memory.send_blklen 	= new int* [PE];
		mpi_memory.recv_blklen 	= new int* [PE];

		// debug ("rank-%d> pass\n", rank);

		for (int i=0; i<PE; i++)
		{
			mpi_memory.send_blklen[i] = new int [mpi_memory.send_count[i]];
			for (int j=0; j<mpi_memory.send_count[i]; j++)
				mpi_memory.send_blklen[i][j] = 1;
			mpi_memory.recv_blklen[i] = new int [mpi_memory.recv_count[i]];
			for (int j=0; j<mpi_memory.recv_count[i]; j++)
				mpi_memory.recv_blklen[i][j] = 1;

			// debug ("rank-%d> done %d/%d\n", rank, i, PE);
		}

		// MPI_Barrier(MPI_COMM_WORLD);
		// debug ("%d - done\n", rank);

		/******************************************************************************/
		//                        Build Send/Recv MPI Datatypes
		/******************************************************************************/

		mpi_memory.send_t 		= new MPI_Datatype [PE];
		mpi_memory.recv_t 		= new MPI_Datatype [PE];

		for (int j=0; j<PE; j++)
		{
			// build send type
			MPI_Type_indexed (
				mpi_memory.send_count[j],
				mpi_memory.send_blklen[j],
				mpi_memory.send_addr[j],
				mpi_data_t,
				&mpi_memory.send_t[j]);

			MPI_Type_commit(&mpi_memory.send_t[j]);

			// build recv type
			MPI_Type_indexed (
				mpi_memory.recv_count[j],
				mpi_memory.recv_blklen[j],
				mpi_memory.recv_addr[j],
				mpi_data_t,
				&mpi_memory.recv_t[j]);

			MPI_Type_commit(&mpi_memory.recv_t[j]);
		}
		assert (mpi_memory.send_count[rank-1]==mpi_memory.recv_count[rank-1]); // Local scatter cant handle this case
		for (int src=0; src<PE; src++)
		{
			for (int i=0; i<mpi_memory.recv_count[src]; i++)
				assert (mpi_memory.recv_addr[src][i] < (FPGA_STREAM_NUM_VECTORS*num_nodes + FPGA_STREAM_NUM_EDGE_DESCRIPTOR*num_in_edges));
		}
	}

	MPI_Barrier (MPI_COMM_WORLD);
	if (rank == MASTER)
		printf ("done\n");
	// while (true);
}

void MPIScatter::create_node_memory()
{
	node_memory = new data_tt[num_nodes];

	for (int i=0; i<num_nodes; i++)
		node_memory[i] = rand_between(0, 100);	// assign random values
}

void MPIScatter::create_message_memory()
{
	message_memory = new data_tt[num_in_edges];

	memset (message_memory, 0, num_in_edges*sizeof(data_tt));  // set it to zero
}

void MPIScatter::attach_node_memory(data_tt *new_buff_addr)
{
	node_memory = &new_buff_addr[0];
}

void MPIScatter::attach_message_memory(data_tt *new_buff_addr)
{
	message_memory = &new_buff_addr[0];
}

data_tt *MPIScatter::get_node_memory_addr()
{
	return node_memory;
}

data_tt *MPIScatter::get_message_memory_addr()
{
	return message_memory;
}

int *MPIScatter::get_edges_to_node_mapping_addr()
{
	return mpi_memory.in_edges_for_node;
}

void MPIScatter::distribute_graph_structure() 
{
	prepare_sendrecv_buffer();
	distribute_sendrecv_buffer();
	build_data_type();
}

int MPIScatter::get_num_nodes()
{
	return num_nodes;
}

int MPIScatter::get_num_in_edges()
{
	return num_in_edges;
}

void MPIScatter::scatter()
{
	/*******************************************************************************/
	//                        		Scatter
	/*******************************************************************************/

	if (rank != MASTER)
	{
		// I. Perform local scatter //

		for (uint32_t disp=0; disp<mpi_memory.send_count[rank-1]; disp++)
		{
			message_memory[mpi_memory.recv_addr[rank-1][disp]] = node_memory[mpi_memory.send_addr[rank-1][disp]];
		}

		// II. Perform remote scatter //

		// printf ("%d - local scatter done\n", rank);	

		// 2. Perform Scatter
		for (int base=PE-1, j=1; j<PE; j++)
		{
			int toNode = mod(rank-1+j, PE);
			int fromNode = mod(base+rank-1-(j-1), PE);
			// printf ("%d:%d\t[%d]->[%d]->[%d]\n", rank, j, fromNode+1, rank, toNode+1);
			MPI_Sendrecv (node_memory, 1, mpi_memory.send_t[toNode], toNode+1, 100, message_memory, 1, mpi_memory.recv_t[fromNode], fromNode+1, 100, MPI_COMM_WORLD, &status);
			// MPI_Put(node_memory, 1, mpi_memory.send_t[toNode], toNode, 0, 1, mpi_memory.recv_t[toNode], win[toNode]);
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
}


MPIScatter::~MPIScatter()
{
	MPI_Finalize ();
}
