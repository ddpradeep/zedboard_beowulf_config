#!/bin/bash

REPEAT_COUNT=24
START_NODE_SIZE=2
END_NODE_SIZE=32
MTX_FILES_ONLY_DIR=mtx

OUTPUT_FILE="result/output_openmpi.txt"

# rm -rf $OUTPUT_FILE

for MTX_FILE in $(ls $MTX_FILES_ONLY_DIR -S)
do
	for NP in $(seq $END_NODE_SIZE -1 $START_NODE_SIZE)
	do
		for SCATTER_TYPE in 0
		do
			for CACHE_STATUS in 0
			do
				for OVERLOAD in 0
				do
					CMD="make run MPI=/opt/openmpi-1.8 REPEAT=$REPEAT_COUNT MTX_FILE=$MTX_FILES_ONLY_DIR/$MTX_FILE NP=$NP SCATTER_TYPE=$SCATTER_TYPE R_CACHE=$CACHE_STATUS OVERLOAD=$OVERLOAD SEP_PROFILE=0 FINE_GRAINED=0"
					echo "$CMD"
					CMD_OUTPUT=$($CMD)
					CMD_OUTPUT=$(echo "$CMD_OUTPUT" | tail -1)
					echo "$CMD_OUTPUT"
					echo $CMD_OUTPUT >> $OUTPUT_FILE
				done
			done
		done
	done
done