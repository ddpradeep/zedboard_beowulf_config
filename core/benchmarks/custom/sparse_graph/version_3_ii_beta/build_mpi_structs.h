#include "mpi.h"
#include "helper.h"
#include "graph_support.h"
#include <iostream>
using namespace std;

#define MASTER	0
#define SLAVE	11

typedef struct
{
	int pe_count;		// stores the number of PEs [N] *redundant?*
	int *num_nodes;		// number of hosting nodes [send buffer]
	int *num_in_edges;	// number of incoming edges [recv buffer]
	int ***send_addr;	// 2D array of send disp_v vectors [N*N]
	int ***recv_addr;	// 2D array of recv disp_v vectors [N*N]
	int **send_count;	// 2D array containing length of each send vector [N*N]
	int **recv_count;	// 2D array containing length of each recv vector [N*N]
} disp_memory_t;

typedef struct
{
	int *num_nodes;				// 1D array of number of graph nodes in each PE
	int *num_in_edges;			// 1D array of number of incoming edges in each PE
	int **send_addr;			// 1D array of send disp_v vectors
	int **recv_addr;			// 1D array of recv disp_v vectors
	int *send_count;			// 1D array containing length of each send vector
	int *recv_count;			// 1D array containing length of each recv vector
	int **send_blklen;				// 1D array containing block len (all ones)
	int **recv_blklen;				// 1D array containing block len (all ones)
	MPI_Datatype *send_t;		// 1D array of corresponding MPI send data type
	MPI_Datatype *recv_t;		// 1D array of corresponding MPI recv data type
}mpi_memory_t;

void free_disp_memory_t(disp_memory_t *disp_memory, int partitions)
{
	for (int i=0; i<partitions; i++)
	{
		for (int j=0; j<partitions; j++)
		{
			delete [] disp_memory->send_addr[i][j];
			delete [] disp_memory->recv_addr[i][j];
		}
		delete [] disp_memory->send_addr[i];
		delete [] disp_memory->recv_addr[i];
		delete [] disp_memory->send_count[i];
		delete [] disp_memory->recv_count[i];
	}
	delete [] disp_memory->send_addr;
	delete [] disp_memory->recv_addr;
	delete [] disp_memory->send_count;
	delete [] disp_memory->recv_count;
	delete [] disp_memory->num_nodes;
	delete [] disp_memory->num_in_edges;
}

void free_all_mpi_memory_except (mpi_memory_t *mpi_memory, int partitions, int rank)
{
	for (int i=0; i<partitions; i++)
	{
		if (rank == i)
			continue;
		printf ("rank-%d>\tdeleted %d\n", rank, i);
		delete [] mpi_memory->send_addr[i];
		delete [] mpi_memory->recv_addr[i];
		delete [] mpi_memory->send_blklen[i];
		delete [] mpi_memory->recv_blklen[i];
	}
	// delete [] mpi_memory->send_count;
	// delete [] mpi_memory->recv_count;
	// delete [] mpi_memory->send_blklen;
	// delete [] mpi_memory->recv_blklen;
	// delete [] mpi_memory->num_nodes;
	// delete [] mpi_memory->num_in_edges;
	// delete [] mpi_memory->send_t;
	// delete [] mpi_memory->recv_t;
}

void free_mpi_memory_t(mpi_memory_t *mpi_memory, int partitions)
{
	for (int i=0; i<partitions; i++)
	{
		delete [] mpi_memory->send_addr[i];
		delete [] mpi_memory->recv_addr[i];
		delete [] mpi_memory->send_blklen[i];
		delete [] mpi_memory->recv_blklen[i];
	}
	delete [] mpi_memory->send_count;
	delete [] mpi_memory->recv_count;
	delete [] mpi_memory->send_blklen;
	delete [] mpi_memory->recv_blklen;
	delete [] mpi_memory->num_nodes;
	delete [] mpi_memory->num_in_edges;
	delete [] mpi_memory->send_t;
	delete [] mpi_memory->recv_t;
}

void get_random_graph(int M, int N, int** a_index, int** col_index) {

	*a_index = (int*)malloc((M+1)*sizeof(int));
	*col_index = (int*)malloc(N*sizeof(int));
	if(*a_index==NULL || *col_index==NULL) {
		cout << "Not enough memory" << endl;
		exit(1);
	}

	int *edge_count_arr = (int*)malloc(M*sizeof(int));
	get_rand_array(edge_count_arr,M,N);
	(*a_index)[0]=0;
	for(int m=1; m<M; m++) {
		int edges = edge_count_arr[m-1];
		(*a_index)[m]=(*a_index)[m-1]+edges;
		for(int k=0;k<edges;k++) {
			(*col_index)[(*a_index)[m-1]+k]=rand()%M; // any node
		}
	}
	free(edge_count_arr);
}

void build_mpi_structures(disp_memory_t* disp_memory, int g_nodes, int g_edges, int partitions) 
{
	int *a_index;
	int *col_index;

	// initialize disp_memory_t
	disp_memory->pe_count = partitions; // stores the number of PEs
	disp_memory->num_nodes = new int [partitions];
	disp_memory->num_in_edges = new int [partitions];
	disp_memory->send_addr = new int** [partitions]; // 2D array of send vectors
	disp_memory->recv_addr = new int** [partitions]; // 2D array of recv vectors

	for (int i=0; i<partitions; i++)
	{
		disp_memory->send_addr[i] = new int* [partitions];
		disp_memory->recv_addr[i] = new int* [partitions];
	}

	disp_memory->send_count = new int* [partitions]; // 2D array of send vector length
	disp_memory->recv_count = new int* [partitions]; // 2D array of recv vector length
	for (int i=0; i<partitions; i++)
	{
		disp_memory->send_count[i] = new int [partitions];
		disp_memory->recv_count[i] = new int [partitions];
	}

	// get a random graph structure in CSR format
	get_random_graph(g_nodes, g_edges, &a_index, &col_index);

	// allocate g_nodes to pe_nodes
	int *pe_nodes_alloc = new int [partitions+1];
	for (int i=0, float step=g_nodes/partitions; i<partitions; i++)
		pe_nodes_alloc[i] = (int)(i*step);
	pe_nodes_alloc[partitions] = g_nodes;

	// for every pe
	for (int pe_node=0; pe_node<partitions; pe_node++)
	{
		// get number of graph nodes and incoming edges
		disp_memory->num_nodes[pe_src] = pe_nodes_alloc[pe_node+1] - pe_nodes_alloc[pe_node];
		disp_memory->num_in_edges[pe_src] = graph_memory_for_pe->max_in_edges;

		// for every g_node in pe
		for (int g_node=pe_nodes_alloc[pe_node]; g_node<pe_nodes_alloc[pe_node+1]; g_node++)
		{
			// for every outgoing edge in g_node
			for(int edge=a_index[g_node];edge<a_index[g_node+1];edge++)
			{
				// in g_node terms, state the src and dst of edge
				src_g_node = g_node;
				dst_g_node = col_index[edge];

				// in disp terms, state the src and dst of edge

				// col_index[edge] tells which g_node to current g_node's value

				// 1) find out which pe_node it belongs to

				// 2) find out the displacement  
			}
		}
	}
}

void build_mpi_structures(graph_memory_t* graph_memory, disp_memory_t* disp_memory,
		int partitions, int pe_x, int pe_y) 
{
	// initialize disp_memory_t
	disp_memory->pe_count = partitions; // stores the number of PEs
	disp_memory->send_addr = new int** [partitions]; // 2D array of send vectors
	disp_memory->recv_addr = new int** [partitions]; // 2D array of recv vectors
	disp_memory->num_nodes = new int [partitions];
	disp_memory->num_in_edges = new int [partitions];

	for (int i=0; i<partitions; i++)
	{
		disp_memory->send_addr[i] = new int* [partitions];
		disp_memory->recv_addr[i] = new int* [partitions];
	}

	disp_memory->send_count = new int* [partitions]; // 2D array of send vector length
	disp_memory->recv_count = new int* [partitions]; // 2D array of recv vector length
	for (int i=0; i<partitions; i++)
	{
		disp_memory->send_count[i] = new int [partitions];
		disp_memory->recv_count[i] = new int [partitions];
	}
	
	// debug ("Partitions: %d\n", partitions);

	//serialize for individual partition elements
	for(int pe_src=0;pe_src<partitions;pe_src++) 
	{
		// build the graph memory structure for this pe
		graph_memory_t *graph_memory_for_pe=&graph_memory[pe_src];
		int nodes =  graph_memory_for_pe->max_nodes;

		// get number of graph nodes and incoming edges
		disp_memory->num_nodes[pe_src] = nodes;
		disp_memory->num_in_edges[pe_src] = graph_memory_for_pe->max_in_edges;

		// construct PUT for each pe_dest separately..
		for(int pe_dest=0;pe_dest<partitions;pe_dest++) 
		{
			// TODO: find a way to store the addresses into disp_memory_t somehow..
			vector<int> send_addr_vect;
			vector<int> recv_addr_vect;
			// send_addr = malloc(*sizeof(int));
			// recv_addr = malloc(*sizeof(int));
			// loop over all nodes in pe
			for(int i=0;i<graph_memory_for_pe->max_nodes;i++) 
			{	
				node_memory_t* node = &(graph_memory_for_pe->node_memory[i]);
				int output_edges_for_node = node->edge_output_count;
				// loop over all output edges for node
				for(int j=0;j<output_edges_for_node;j++) 
				{
					// collect send/recv information
					edge_output_memory_t* edge = &(graph_memory_for_pe->edge_output_memory[node->edge_mem_output_offset+j]);
					int address = edge->graph_address;
					pe_address_t pe_edge = edge->pe_address;
					// cout << "i=" <<i << "j=" << j << " addr=" << address << " x=" <<(int)pe_edge.X<< ",y=" <<(int)pe_edge.Y <<endl;
					// check if the destination matches pe_dest
					if(pe_edge.X*pe_y+pe_edge.Y==pe_dest) 
					{
						// do stuff to build coalesced PUT...
						// where to get data from = node[i]
						send_addr_vect.push_back(i);
						recv_addr_vect.push_back(address);
						// send_addr[send_counter]=i;
						// recv_addr[send_counter]=address;
						// send_counter++;
					}
				}
			}

			// get pointer to vector
			int *send_addr_ptr = &send_addr_vect[0];
			int *recv_addr_ptr = &recv_addr_vect[0];

			// allocate memory for arrays
			disp_memory->send_addr[pe_src][pe_dest] = new int[send_addr_vect.size()];
			disp_memory->recv_addr[pe_src][pe_dest] = new int[recv_addr_vect.size()];

			// copy vector to array
			memcpy (disp_memory->send_addr[pe_src][pe_dest], send_addr_ptr, send_addr_vect.size()*sizeof(int));
			memcpy (disp_memory->recv_addr[pe_src][pe_dest], recv_addr_ptr, recv_addr_vect.size()*sizeof(int));

			// store the array lengths
			disp_memory->send_count[pe_src][pe_dest] = send_addr_vect.size();
			disp_memory->recv_count[pe_src][pe_dest] = recv_addr_vect.size();

			// free vector
			send_addr_vect.clear();
			recv_addr_vect.clear();
			
			// debug ("Send[%d][%d]: %d\tRecv[%d][%d]: %d\n",
			// 	pe_src,
			// 	pe_dest,
			// 	disp_memory->send_count[pe_src][pe_dest],
			// 	pe_src,
			// 	pe_dest,
			// 	disp_memory->recv_count[pe_src][pe_dest]);
		}
	}

	// Print the total send and recv size
	debug ("\n");
	debug ("TSend: %d\tTRecv: %d\n\n", 
		get2DSum(disp_memory->send_count, disp_memory->pe_count),
		get2DSum(disp_memory->recv_count, disp_memory->pe_count));

}

int compress_disp_blklen (int **disp_p, int *disp_len, int **blklen_p)
{
	vector <int> temp_disp, temp_blklen;

	int *disp_vect = *disp_p;

	int i = 0, old_disp = 0, blklen = 0;
	while (i<*disp_len)
	{
		temp_disp.push_back( disp_vect[i]);
		old_disp 	= disp_vect[i];
		blklen 		= 1;

		while ( (++i<*disp_len) && (disp_vect[i]==(old_disp+1)) )
		{
			old_disp 	= disp_vect[i];
			blklen++;
		}

		temp_blklen.push_back(blklen);
	}

	// printf ("here\n");

	assert (temp_disp.size() == temp_blklen.size());
	*disp_len = temp_disp.size();

	// printf ("here2\n");

	delete [] *disp_p;	// delete old space (as new might be smaller)
	*disp_p = new int [temp_disp.size()];
	copy(temp_disp.begin(), temp_disp.end(), *disp_p);
	temp_disp.clear();

	// printf ("here3\n");

	*blklen_p = new int [temp_blklen.size()];
	copy(temp_blklen.begin(), temp_blklen.end(), *blklen_p);
	temp_blklen.clear();

	// printf ("here4\n");

	// return total sum of blocklen
	int sum = 0;
	for (int i=0; i<*disp_len; i++)
		sum += (*blklen_p)[i];

	return sum;
}