#!/bin/zsh

rm -f benchmark-N.csv
for NP in {2..14}; do
	for M in 100000; do
		for N in 100000 200000 400000 800000 1000000 1200000; do
			make run NP=$NP REPEAT=10 M=$M N=$N | tail -n 1 >> benchmark-N.csv | cat
		done
	done
done
