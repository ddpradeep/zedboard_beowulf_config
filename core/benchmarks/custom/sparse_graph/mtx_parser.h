#ifndef MTX_PARSER_H_
#define MTX_PARSER_H_

#include <string>
#include "graph_support.h"
#include "mmio.h"

using namespace std;
int read_matrix(const char* matrix_file, int** row, int** col, double** val, int* M, int* N, int* nz);

Graph get_graph_from_sparse_matrix(char* matrix_file)         
{
          // shared matrix variables..
          int M, N, nz;
          int *row;
          int *col;
          double *val;

          read_matrix((const char *)matrix_file, &row, &col, &val, &M, &N, &nz);
          return build_graph(row, col, val, M, N, nz);
}

// 1. input the graph from a variety of formats...
// For now, only supporting the sparse matrix format!
int read_matrix(const char* matrix_file, int** row, int** col, double** val, int* M, int* N, int* nz)
{
	return mm_read_unsymmetric_sparse(matrix_file, M, N, nz, val, row, col);
}


// 2. Convert the Boost graph into the Graph Machine format..
// Construct a single PE memory image for the graph we just built..
Graph build_graph(int* row, int* col, double* val, int M, int N, int nz) {

        Graph g;

        // build the graph!
        for(int i=0; i<M; i++) {
                add_vertex(g);
        }

        // add edges into the graph by constructing an edge array first..
        typedef std::pair<int, int> Edge;
        typedef graph_traits<Graph>::edge_descriptor edge_type;

        Edge *edge_array = (Edge*)malloc(nz*sizeof(Edge));
        edge_property_for_graphs_t *edge_properties = (edge_property_for_graphs_t*)malloc(nz*sizeof(edge_property_for_graphs_t));
        for(int i=0; i<nz; i++) {
                edge_array[i] = Edge (col[i],row[i]);
                edge_type ei = (add_edge(edge_array[i].first, edge_array[i].second, g)).first;
                edge_properties[i].val = val[i];
                edge_properties[i].index = i;

                put(&edge_property_for_graphs_t::val, g, ei, val[i]);
                put(&edge_property_for_graphs_t::index, g, ei, i);

        }

        // assign node properties...
        graph_traits<Graph>::vertex_iterator vi, vi_end;
        int counter=0;
        for (tie(vi, vi_end) = vertices(g); vi != vi_end; ++vi) {
                g[(*vi)].index = counter++;
                g[(*vi)].partition = -1;
        }

        return g;
}


#endif
