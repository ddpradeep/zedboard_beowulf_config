#include "mpiscatter.h"
#include "neuralsim.h"
#include "omp.h"

#include <thread>
#include <mutex>

#define CORE_ID_MAIN	0
#define CORE_ID_COMPUTE	0
#define CORE_ID_SCATTER	1

// neural variables
REAL* a;
REAL* b;
REAL* c;
REAL* d;
REAL* V;
REAL* u;
REAL* I;
BOOL* fired;
BOOL* fired_old;
REAL* weight;

// mpi variables
MPIScatter *mpiscatter;
BOOL *in_fired_msg;
BOOL *in_fired_msg_old;
int *col_index;

// thread variables
mutex loop_mutex;
int shared_loop_start_ctr = 0;
int shared_loop_end_ctr = 0;
int shared_loop_swap_ctr = 0;
void compute (int M, int repeat_count, double *time_compute);
void scatter (double repeat_count, double *time_scatter);


int main(int argc, char** argv)
{
	stick_this_thread_to_core (CORE_ID_MAIN);

	mpiscatter = new MPIScatter(&argc, &argv);	// MPI init

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                        Generate and Distribute Graph
	/******************************************************************************/

	mpiscatter->build_random_graph(M, N);
	mpiscatter->distribute_graph_structure();
	int total_nodes = M;
	int total_edges = N;
	M = mpiscatter->get_num_nodes();		// update M to reflect num nodes in this PE
	N = mpiscatter->get_num_in_edges();	// update N to reflect num incomming edges in this PE

	/******************************************************************************/
	//                           Initialize Variables
	/******************************************************************************/

	a = (REAL*)malloc(M*sizeof(REAL));
	b = (REAL*)malloc(M*sizeof(REAL));
	c = (REAL*)malloc(M*sizeof(REAL));
	d = (REAL*)malloc(M*sizeof(REAL));

	V = (REAL*)malloc(M*sizeof(REAL));
	u = (REAL*)malloc(M*sizeof(REAL));
	I = (REAL*)malloc(M*sizeof(REAL));

	fired = (BOOL*)malloc(M*sizeof(BOOL));
	fired_old = (BOOL*)malloc(M*sizeof(BOOL));
	weight = (REAL*)malloc(N*sizeof(REAL));
	
	for(int m=0;m<M;m++)
	{
		a[m]=((REAL) rand() / (RAND_MAX)) + 1;
		b[m]=((REAL) rand() / (RAND_MAX)) + 1;
		c[m]=((REAL) rand() / (RAND_MAX)) + 1;
		d[m]=((REAL) rand() / (RAND_MAX)) + 1;
		I[m]=((REAL) rand() / (RAND_MAX)) + 1;
		V[m]=((REAL) rand() / (RAND_MAX)) + 1;
		u[m]=((REAL) rand() / (RAND_MAX)) + 1;
		fired_old[m]=fired[m]=rand()%2;
	}
	for(int n=0;n<N;n++)
		weight[n]=((REAL) rand() / (RAND_MAX)) + 1;

	//	set the node_memory buffer to point to 'fired (old)'
	mpiscatter->attach_node_memory (fired_old);

	// get the address of message memory buffer (new)
	mpiscatter->create_message_memory();
	in_fired_msg = mpiscatter->get_message_memory_addr();
	in_fired_msg_old = new BOOL [N];

	// get edges to node mapping array (col_index)
	col_index = mpiscatter->get_edges_to_node_mapping_addr();

	/******************************************************************************/
	//                        		Neural Simulation
	/******************************************************************************/

	TIMER timer1, timer2;
	double *time_compute = new double [repeat_count];
	double *time_scatter = new double [repeat_count];
	double *time_total = new double [repeat_count];

	thread compute_thread (compute, M, repeat_count, time_compute);
	thread scatter_thread (scatter, repeat_count, time_scatter);

	// record timings
	for (int i=0; i<repeat_count; i++)
	{
		resetTime(&timer1);

		// wait for all threads to reach this	
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		// resetTime(&timer2);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		time_total[i] = max (time_compute[i], time_scatter[i]);
		// time_total[i] = time_total[i] - (getTime(&timer1) - time_total[i]);

		swap (fired, fired_old);
		swap (in_fired_msg, in_fired_msg_old);
		mpiscatter->attach_node_memory (fired_old);
		mpiscatter->attach_message_memory (in_fired_msg);

		if (mpiscatter->get_rank() == MASTER)
		{
			if (i%10==0)
				printf ("%d", i);
			else
				printf (".");
		}

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
	}

	compute_thread.join();
	scatter_thread.join();

	// paranoid code
	int not_used_val = 0;
	for (int i=0; i<M; i++)
		not_used_val += fired[i];

	/******************************************************************************/
	//                        			Results
	/******************************************************************************/

	if (mpiscatter->get_rank() == MASTER)
	{
		delete [] time_compute, time_scatter, time_total;
		time_compute 	 			= new double [mpiscatter->get_total_procs()];
		time_scatter 				= new double [mpiscatter->get_total_procs()];
		time_total 					= new double [mpiscatter->get_total_procs()];
		
		// get compute time from nodes
		for (int pe=0; pe<mpiscatter->get_total_procs(); pe++)
		{
			MPI_Status status;
			MPI_Recv (time_compute+pe, 1, MPI_DOUBLE, pe+1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv (time_scatter+pe, 1, MPI_DOUBLE, pe+1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total+pe, 1, MPI_DOUBLE, pe+1, 2, MPI_COMM_WORLD, &status);
		}

		sort (time_compute, time_compute+mpiscatter->get_total_procs());
		sort (time_scatter, time_scatter+mpiscatter->get_total_procs());
		sort (time_total, time_total+mpiscatter->get_total_procs());

		double avg_acp_bandwidth_lb_MBps = 0;
		double avg_acp_bandwidth_ub_MBps = 0;
		double avg_time_compute_comms = 0;
		double avg_time_compute_post_process = 0;
		double avg_time_compute = time_compute [mpiscatter->get_total_procs()/2];
		double avg_time_scatter = time_scatter [mpiscatter->get_total_procs()/2];
		double avg_time_total 	= time_total [mpiscatter->get_total_procs()/2];

		printf ("%d\n", repeat_count);
		printf("{ignore %d}, %d, %d, %d, %d, %d, %.2f%%, < %.2f %.2f >, < %f, %f, %f >, %f, < %f, %f, %f >\n",
			(int) not_used_val,
			mpiscatter->get_total_procs(),
			omp_thread_count(),
			total_nodes,
			total_edges,
			mpiscatter->avg_outbound_count_per_node(),
			(float)mpiscatter->avg_outbound_count_per_node()*100.0/(float)mpiscatter->avg_send_count_per_node(),
			avg_acp_bandwidth_lb_MBps,
			avg_acp_bandwidth_ub_MBps,
			avg_time_compute_comms,
			avg_time_compute_post_process,
			avg_time_compute,
			avg_time_scatter,
			time_total [0], avg_time_total, time_total [mpiscatter->get_total_procs()-1]);
	}
	else
	{
		sort (time_compute, time_compute+repeat_count);
		sort (time_scatter, time_scatter+repeat_count);
		sort (time_total, time_total+repeat_count);
		MPI_Send (time_compute+repeat_count/2, 1, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
		MPI_Send (time_scatter+repeat_count/2, 1, MPI_DOUBLE, MASTER, 1, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count/2, 1, MPI_DOUBLE, MASTER, 2, MPI_COMM_WORLD);
	}

	return 0;
}


void compute (int M, int repeat_count, double *time_compute)
{
	stick_this_thread_to_core (CORE_ID_COMPUTE);

	// old recv buffer to new send buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{
		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		resetTime(&timer);
		#pragma omp parallel for //shared(a_index, col_index)
		for(int m=0;m<M;m++)
			synapse(&(I[m]), weight, in_fired_msg_old, col_index, m);
		
		#pragma omp parallel for
		for(int m=0;m<M;m++)
	        neuron(&(V[m]), &(u[m]), &(fired[m]),I[m], a[m], b[m], c[m], d[m]);
		time_compute[i] = getTime(&timer);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// wait for swap of buffers to be over
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
	}
}

void scatter (double repeat_count, double *time_scatter)
{
	stick_this_thread_to_core (CORE_ID_SCATTER);

	// old send buffer to new recv buffer
	TIMER timer;

	for (int i=0; i<repeat_count; i++)
	{
		MPI_Barrier (MPI_COMM_WORLD);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_start_ctr++;
		loop_mutex.unlock();
		while (shared_loop_start_ctr!=(i*3+3)) { usleep (10); }

		resetTime(&timer);
		mpiscatter->scatter();
		time_scatter[i] = getTime(&timer);

		// wait for all threads to reach this
		loop_mutex.lock();
		shared_loop_end_ctr++;
		loop_mutex.unlock();
		while (shared_loop_end_ctr!=(i*3+3)) { usleep (10); }

		// wait for swap of buffers to be over
		loop_mutex.lock();
		shared_loop_swap_ctr++;
		loop_mutex.unlock();
		while (shared_loop_swap_ctr!=(i*3+3)) { usleep (10); }
	}
}