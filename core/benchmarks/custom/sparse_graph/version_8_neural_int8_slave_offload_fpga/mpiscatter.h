#ifndef __MPISCATTER_H__
#define __MPISCATTER_H__

#define data_tt uint8_t
#define mpi_data_t MPI_UINT8_T

#include "helper.h"
#include "build_mpi_structs.h"
#include "mpi.h"

class MPIScatter
{
private:
	int PE;					// total number of processes	
	int rank;				// rank of process
	int num_nodes;			// number of graph nodes in this process
	int num_in_edges;		// number of incoming edges to this process
	int not_used_val;
	int avg_send_count;	// average send edges per node
	int avg_outbound_count;

	disp_memory_t disp_memory; // structure to contain the send and recv disp for all PEs
	mpi_memory_t mpi_memory;

	data_tt *node_memory;
	data_tt *message_memory;

	MPI_Status status;
	MPI_Group comm_group, group;

	static const int TAG_SEND_T = 1;
	static const int TAG_RECV_T = 2;

	static const int SEND_TAG_OFFSET = 1000;
	static const int RECV_TAG_OFFSET = 0;
public:
	MPIScatter(int *argc, char ***argv);
	~MPIScatter();

	/* use virtual otherwise linker will try to perform static linkage */
	virtual void build_random_graph(int M, int N);

	virtual void distribute_graph_structure(); // short cut for calling the next three functions

	virtual void prepare_sendrecv_buffer();
	virtual void distribute_sendrecv_buffer();
	virtual void build_data_type();

	virtual void scatter();

	void create_node_memory();
	void create_message_memory();

	void attach_node_memory(data_tt *new_buff_addr);
	void attach_message_memory(data_tt *new_buff_addr);

	data_tt* get_node_memory_addr();
	data_tt* get_message_memory_addr();

	int* get_edges_to_node_mapping_addr();

	int get_rank();
	int get_total_procs();
	int get_num_nodes();
	int get_num_in_edges();
	int avg_send_count_per_node();
	int avg_outbound_count_per_node();

	void MPI_Sendnext(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype);
};

#endif
