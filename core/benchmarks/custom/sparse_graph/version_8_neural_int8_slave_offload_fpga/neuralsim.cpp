#include "mpiscatter.h"
#include "neuralsim.h"
#include "fpga_stream.h"
#include "omp.h"

// neural variables
BOOL* fired;
REAL* fpga_stream;

// mpi variables
MPIScatter *mpiscatter;
int *col_index;

// fpga variables
FPGA *PL;

int main(int argc, char** argv)
{
	mpiscatter = new MPIScatter(&argc, &argv);	// MPI init

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<nodes (M)>\t<edges (N)>\t<repeat_count>\n\\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int repeat_count = atoi(argv[3]);

	/******************************************************************************/
	//                        Generate and Distribute Graph
	/******************************************************************************/

	mpiscatter->build_random_graph(M, N);
	mpiscatter->distribute_graph_structure();
	int total_nodes = M;
	int total_edges = N;
	M = mpiscatter->get_num_nodes();		// update M to reflect num nodes in this PE
	N = mpiscatter->get_num_in_edges();	// update N to reflect num incomming edges in this PE
	int transfer_size = max(M, N);

	/******************************************************************************/
	//                           Initialize Variables
	/******************************************************************************/

	fpga_stream 	= new REAL [transfer_size];
	fired 			= (BOOL*)malloc(M*sizeof(BOOL));

	// get edges to node mapping array (col_index)
	col_index = mpiscatter->get_edges_to_node_mapping_addr();

	// initialize all variables (excpet incoming edges)
	for(int m=0;m<M;m++)
		fired[m] = rand()%2;

	for(int m=0, edges_before_this_node=0;m<transfer_size;m++)
		fpga_stream [m] = rand()%2;

	//	set the node_memory buffer to point to fired
	mpiscatter->attach_node_memory (fired);
	mpiscatter->attach_message_memory (fpga_stream);

	/******************************************************************************/
	//                        		Neural Simulation
	/******************************************************************************/

	TIMER timer;
	double *time_compute_comms = new double [repeat_count];
	double *time_compute_post_process = new double [repeat_count];
	double *time_scatter = new double [repeat_count];
	double *time_total = new double [repeat_count];

	// init fpga stuff
	transfer_size -= transfer_size%4;
	REAL *receive_stream = new REAL [mpiscatter->get_num_nodes()];
	if (mpiscatter->get_rank() != MASTER)
		PL = new FPGA ("/dev/xillybus_read_32", "/dev/xillybus_write_32");

	// record timings
	for (int i=0; i<repeat_count; i++)
	{
		// wait for all threads to reach this
		MPI_Barrier (MPI_COMM_WORLD);

		// compute
		if (mpiscatter->get_rank() != MASTER)
		{
			resetTime(&timer);
			PL->send (fpga_stream, transfer_size);
			PL->recv (receive_stream, mpiscatter->get_num_nodes()-mpiscatter->get_num_nodes()%4);
			time_compute_comms[i] = getTime(&timer);
			// delete PL;

			time_compute_post_process[i] = 0;

			PL->reset_pl();
		}

		MPI_Barrier(MPI_COMM_WORLD);

		// mpi scatter
		resetTime(&timer);
		mpiscatter->scatter();
		time_scatter[i] = getTime(&timer);

		time_total[i] = (time_compute_comms[i]+time_compute_post_process[i]) + time_scatter[i];

		if (mpiscatter->get_rank() == MASTER)
		{
			if (i%10==0)
				printf ("%d", i);
			else
				printf (".");
		}
	}

	// paranoid code
	BOOL not_used_val = 0;
	for (int i=0; i<M; i++)
		not_used_val += fired[i];

	/******************************************************************************/
	//                        			Results
	/******************************************************************************/

	if (mpiscatter->get_rank() == MASTER)
	{
		delete [] time_compute_comms, time_compute_post_process, time_scatter, time_total;
		time_compute_comms 			= new double [mpiscatter->get_total_procs()];
		time_compute_post_process 	= new double [mpiscatter->get_total_procs()];
		time_scatter 				= new double [mpiscatter->get_total_procs()];
		time_total 					= new double [mpiscatter->get_total_procs()];

		double *time_total_min		= new double [mpiscatter->get_total_procs()];
		double *time_total_max		= new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_ub_MBps = new double [mpiscatter->get_total_procs()];
		double *acp_bandwidth_lb_MBps = new double [mpiscatter->get_total_procs()];

		// get compute time from nodes
		for (int pe=0; pe<mpiscatter->get_total_procs(); pe++)
		{
			MPI_Status status;
			MPI_Recv (time_compute_comms+pe, 1, MPI_DOUBLE, pe+1, 0, MPI_COMM_WORLD, &status);
			MPI_Recv (time_compute_post_process+pe, 1, MPI_DOUBLE, pe+1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (time_scatter+pe, 1, MPI_DOUBLE, pe+1, 2, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total+pe, 1, MPI_DOUBLE, pe+1, 3, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total_min+pe, 1, MPI_DOUBLE, pe+1, 4, MPI_COMM_WORLD, &status);
			MPI_Recv (time_total_max+pe, 1, MPI_DOUBLE, pe+1, 5, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_lb_MBps+pe, 1, MPI_DOUBLE, pe+1, 6, MPI_COMM_WORLD, &status);
			MPI_Recv (acp_bandwidth_ub_MBps+pe, 1, MPI_DOUBLE, pe+1, 7, MPI_COMM_WORLD, &status);
		}

		sort (acp_bandwidth_lb_MBps, acp_bandwidth_lb_MBps+mpiscatter->get_total_procs());
		sort (acp_bandwidth_ub_MBps, acp_bandwidth_ub_MBps+mpiscatter->get_total_procs());
		sort (time_compute_comms, time_compute_comms+mpiscatter->get_total_procs());
		sort (time_compute_post_process, time_compute_post_process+mpiscatter->get_total_procs());
		sort (time_scatter, time_scatter+mpiscatter->get_total_procs());
		sort (time_total, time_total+mpiscatter->get_total_procs());
		sort (time_total_min, time_total_min+mpiscatter->get_total_procs());
		sort (time_total_max, time_total_max+mpiscatter->get_total_procs());

		double avg_acp_bandwidth_lb_MBps = acp_bandwidth_lb_MBps [mpiscatter->get_total_procs()/2];
		double avg_acp_bandwidth_ub_MBps = acp_bandwidth_ub_MBps [mpiscatter->get_total_procs()/2];
		double avg_time_compute_comms = time_compute_comms [mpiscatter->get_total_procs()/2];
		double avg_time_compute_post_process = time_compute_post_process [mpiscatter->get_total_procs()/2];
		double avg_time_compute = avg_time_compute_comms + avg_time_compute_post_process;
		double avg_time_scatter = time_scatter [mpiscatter->get_total_procs()/2];
		double avg_time_total 	= time_total [mpiscatter->get_total_procs()/2];
		double min_time_total 	= time_total_min [mpiscatter->get_total_procs()/2];
		double max_time_total 	= time_total_max [mpiscatter->get_total_procs()/2];

		printf ("%d\n", repeat_count);
		// printf("{ignore %d}, %d, %d, %d, %d, %d, %.2f%%, < %.2f %.2f >, < %f, %f, %f >, %f, < %f, %f, %f >\n",
		printf("{ignore %d}, %d, %d, %d, %d, %.2f%%, %.2f, %.2f, %f, %f, %f, %f, %f\n",
			(int) not_used_val,
			mpiscatter->get_total_procs()*omp_get_num_threads(),
			// omp_get_num_threads(),
			total_nodes,
			total_edges,
			mpiscatter->avg_outbound_count_per_node(),
			(float)mpiscatter->avg_outbound_count_per_node()*100.0/(float)mpiscatter->avg_send_count_per_node(),
			avg_acp_bandwidth_lb_MBps,
			avg_acp_bandwidth_ub_MBps,
			// avg_time_compute_comms,
			// avg_time_compute_post_process,
			avg_time_compute,
			avg_time_scatter,
			min_time_total, avg_time_total, max_time_total);
	}
	else
	{
		sort (time_compute_comms, time_compute_comms+repeat_count);
		sort (time_compute_post_process, time_compute_post_process+repeat_count);
		sort (time_scatter, time_scatter+repeat_count);
		sort (time_total, time_total+repeat_count);
		MPI_Send (time_compute_comms+repeat_count/2, 1, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
		MPI_Send (time_compute_post_process+repeat_count/2, 1, MPI_DOUBLE, MASTER, 1, MPI_COMM_WORLD);
		MPI_Send (time_scatter+repeat_count/2, 1, MPI_DOUBLE, MASTER, 2, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count/2, 1, MPI_DOUBLE, MASTER, 3, MPI_COMM_WORLD);
		MPI_Send (time_total+0, 1, MPI_DOUBLE, MASTER, 4, MPI_COMM_WORLD);
		MPI_Send (time_total+repeat_count-1, 1, MPI_DOUBLE, MASTER, 5, MPI_COMM_WORLD);

		double avg_time_compute_comms = time_compute_comms[repeat_count/2];
		int ps_pl_traffic = transfer_size;
		int pl_ps_traffic = mpiscatter->get_num_nodes();
		int total_traffic_ub_bytes = (2*ps_pl_traffic)*sizeof(REAL);
		int total_traffic_lb_bytes = (2*pl_ps_traffic)*sizeof(REAL);

		double acp_bandwidth_ub_MBps = ((double)total_traffic_ub_bytes/(1000000.0))/avg_time_compute_comms;
		double acp_bandwidth_lb_MBps = ((double)total_traffic_lb_bytes/(1000000.0))/avg_time_compute_comms;

		MPI_Send (&acp_bandwidth_lb_MBps, 1, MPI_DOUBLE, MASTER, 6, MPI_COMM_WORLD);
		MPI_Send (&acp_bandwidth_ub_MBps, 1, MPI_DOUBLE, MASTER, 7, MPI_COMM_WORLD);
	}

	return 0;
}