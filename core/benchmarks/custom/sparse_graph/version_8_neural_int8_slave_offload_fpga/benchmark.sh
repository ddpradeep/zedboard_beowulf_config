#!/bin/bash

# for M in 1000000 2000000 4000000 8000000 12000000 16000000 24000000 32000000
# do
# 	for N in 1000000 2000000 4000000 8000000 12000000 16000000 24000000 32000000
# 	do
# 		OUTPUT=$(make run NP=33 OMP=1 REPEAT=50 M=$M N=$N)
# 		echo -n "zedwulf, " >> zedwulf.csv
# 		echo "$OUTPUT"
# 		echo "$OUTPUT" | grep ignore >> zedwulf.csv
# 	done
# done

for NP in 33 17 9 5 3 2
do
	for M in 4000000
        do
        	for N in 4000000
        	do
                	OUTPUT=$(make run NP=$NP OMP=1 REPEAT=50 M=$M N=$N)
                	echo "$OUTPUT"
                	echo "$OUTPUT" | grep ignore >> zedwulf-1_to_32.csv
        	done
	done
done

