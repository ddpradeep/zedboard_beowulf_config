#!/bin/bash

NODE_END=26
OUTPUT_FILE="results/throughput_test.txt"

rm -rf $OUTPUT_FILE

for (( size=2; size<=NODE_END; ))
do
	./run MultipleOneToOneSyncTransfer $size 1 10000000 10 >> $OUTPUT_FILE
	((size+=2))
done

exit
