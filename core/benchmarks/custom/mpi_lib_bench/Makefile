# Default Libraries #
MPI=/opt/mpich-3.1
BGL=/opt/bgl

# Default number of processes #
NP=2

# Program specific arguments #
SYNC_COMM=1
ASYNC_COMM=2
BARRIER=4

REPEAT=24
BENCH_TYPE=$(ASYNC_COMM)
TRANSFER_COUNT=1
SIZE=1024000

# Configure Compiler #
CC=g++
MPICC=$(MPI)/bin/mpic++
SRCC=$(wildcard *.cc)
INC=-I/opt/valgrind_mpich/include
LIB=-L$(BGL)/lib -L./ -lgraph_support
#-L/opt/mpiP/lib -lmpiP
CFLAGS=-O3 -Wno-write-strings -Wno-unused-result

# Update Environment for MPI #
ifeq ($(MPI), /opt/openmpi-1.8)
	MPI_ARG=-machinefile ompi.hf -prefix $(MPI)
#	export PATH := $(PATH):/opt/tau-2.23_openmpi/x86_64/bin/
#	export TAU_MAKEFILE := /opt/tau-2.23_openmpi/x86_64/lib/Makefile.tau-mpi-pdt
else
	MPI_ARG=-machinefile mpich.hf
#	export PATH := $(PATH):/opt/tau-2.23_mpich/x86_64/bin/
#	export TAU_MAKEFILE := /opt/tau-2.23_mpich/x86_64/lib/Makefile.tau-mpi-pdt
endif

export PATH := $(PATH):$(MPI)/bin
export LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(MPI)/lib

all: mpi_bench.o

mpi_bench.o: mpi_bench.cpp $(SRCC)
	$(MPICC) -g mpi_bench.cpp $(INC) $(LIB) $(CFLAGS) -o mpi_bench.o

run:
	mpirun $(MPI_ARG) -np $(NP) ./mpi_bench.o $(BENCH_TYPE) $(SIZE) $(TRANSFER_COUNT) $(REPEAT)

valg:
	mpirun $(MPI_ARG) -np $(NP) /opt/valgrind_mpich/bin/valgrind --tool=callgrind --instr-atstart=no ./mpi_bench.o $(MTX_FILE) $(BENCH_TYPE) $(SIZE) $(TRANSFER_COUNT) $(REPEAT)

prof:
	tau_cxx.sh -c mpi_bench.cpp $(INC) $(LIB) $(CFLAGS) -o mpi_bench.tau.o
	tau_cxx.sh mpi_bench.tau.o -o mpi_bench.o
	rm *.tau.o
clean:
	rm -rf *.o
	rm -rf profile*

superclean: clean
	rm -rf *.tau
	rm -rf *.txt
	rm -rf callgrind*
	rm -rf mpi_bench.o.*
