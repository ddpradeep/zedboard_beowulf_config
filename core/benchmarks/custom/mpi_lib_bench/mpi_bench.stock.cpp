/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 

#define data_tt int
#define mpi_data_t MPI_INT

// #define DEBUG

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "helper.h"

#define SYNC_COMM	1
#define ASYNC_PUT	2
#define ASYNC_GET	3
#define BARRIER 	4

using namespace std;

int main (int argc, char *argv[]) 
{
	// stick_this_thread_to_core(PRIMARY_CORE);
	init_rand();
	int total_procs;					// total number of processes	
	int rank;				// rank of process
	int num_nodes;			// number of graph nodes in this process
	int num_in_edges;		// number of incoming edges to this process
	int not_used_val;

	/******************************************************************************/
	//                           MPI Initialization
	/******************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;

	MPI_Init (&argc, &argv);		// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_procs);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	// get current process rank
	
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %d running on %s\n", rank, processor_name);
	/******************************************************************************/

	/******************************************************************************/
	//                           Parse Input Arguments
	/******************************************************************************/

	// 1. Get the parameters from input
	if (argc != 5)
	{
		printf ("%d \n", argc);
		printf ("Usage: \t<{Sync_Comm(1), ASync_Comm(2), Barrier(3)} <vector_size(bytes)> <number of transfers> <repeat_count>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	if (total_procs!=2)
	{
		printf ("Launch with only 2 processes!");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	data_tt *data;
	TIME_DATA_TYPE timer;

	int bench_type = atoi(argv[1]);
	int vector_size = atoi(argv[2])/sizeof(data_tt);
	int transfer_count = atoi(argv[3]);
	int repeat_count = atoi(argv[4]);

	float *sendTime 	= new float[repeat_count*2];
	float *recvTime 	= &sendTime[repeat_count];

	/******************************************************************************/
	//                        		Allocate Buffer
	/******************************************************************************/

	data = new data_tt[vector_size];

	if (rank == MASTER)
		memset_seq (data, vector_size, 10);
	else
		memset_val (data, vector_size, 0);

	/******************************************************************************/
	//                        		Benchmark
	/******************************************************************************/

	switch (bench_type)
	{
		case BARRIER:
			for (int i=0; i<repeat_count; i++)
		    {
		    	MPI_Barrier (MPI_COMM_WORLD);
		    	resetTime(&timer);
		    	MPI_Barrier (MPI_COMM_WORLD);
		    	sendTime [i] = getTime(&timer);
		    }
		    break;

		case SYNC_COMM:
			for (int i=0; i<repeat_count; i++)
		    {
		    	if (rank == SLAVE)
		    	{
		    		for (int j=0; j<vector_size; j++)
		                if (data[j]!=0)
		                    printf ("GET: Reset Transfer Error! Expected %d, received %d\n\n", 0, data[j]);
		        }

		        MPI_Barrier (MPI_COMM_WORLD);

		        if (rank == MASTER)
		        {
		            resetTime(&timer);
		            MPI_Ssend (data, vector_size, mpi_data_t, SLAVE, DATA_TAG, MPI_COMM_WORLD);                             
		            sendTime [i] = getTime(&timer);                            // Get time to send data
		        }
		        else
		        {
		            resetTime(&timer);
		            MPI_Recv (data, vector_size, mpi_data_t, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		            recvTime [i] = getTime(&timer);                            // Get time to recv data
		            
		            for (int j=0; j<vector_size; j++)
		                if (data[j]!=j+10)
		                    printf ("GET: Data Transfer Error! Expected %d, received %d\n\n", j, data[j]);

		            memset_val (data, vector_size, 0);
		        }
		    }
		    break;
		case ASYNC_PUT:
		case ASYNC_GET:
			// 1. Create windows
			MPI_Win *win = new MPI_Win [total_procs];
			for (int i=0; i<total_procs; i++)
			{
				if (rank == i)
				{
					MPI_Win_create(data, vector_size*sizeof(data_tt), sizeof(data_tt), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
				}
				else
				{
					MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
				}
			}
			MPI_Barrier (MPI_COMM_WORLD);

			// 2. Perform remote puts/gets
			for (int i=0; i<repeat_count; i++)
			{
				switch (bench_type)
				{
					case ASYNC_PUT:
						if (rank == MASTER)
						{
					    	MPI_Barrier (MPI_COMM_WORLD);

					    	resetTime(&timer);

							MPI_Win_lock(MPI_LOCK_SHARED,SLAVE,0,win[SLAVE]);
							MPI_Put(data, vector_size, mpi_data_t, SLAVE, 0, vector_size, mpi_data_t, win[SLAVE]);
							MPI_Win_unlock(SLAVE, win[SLAVE]);

							sendTime [i] = getTime(&timer);

							MPI_Barrier (MPI_COMM_WORLD);
						}
						else
						{
							memset_val (data, vector_size, 0);

							for (int j=0; j<vector_size; j++)
				                if (data[j]!=0)
				                    printf ("GET: Reset Transfer Error! Expected %d, received %d\n\n", 0, data[j]);

							MPI_Barrier (MPI_COMM_WORLD);
							MPI_Barrier (MPI_COMM_WORLD);

							for (int j=0; j<vector_size; j++)
								if (data[j]!=j+10)
									printf ("GET: Data Transfer Error! Expected %d, received %d\n\n", j, data[j]);
						}
						break;
					case ASYNC_GET:
						if (rank == SLAVE)
						{
							memset_val (data, vector_size, 0);

							for (int j=0; j<vector_size; j++)
				                if (data[j]!=0)
				                    printf ("GET: Reset Transfer Error! Expected %d, received %d\n\n", 0, data[j]);

					    	resetTime(&timer);

							MPI_Win_lock(MPI_LOCK_SHARED,MASTER,0,win[MASTER]);
							MPI_Get(data, vector_size, mpi_data_t, MASTER, 0, vector_size, mpi_data_t, win[MASTER]);
							MPI_Win_unlock(MASTER, win[MASTER]);

							recvTime [i] = getTime(&timer);

							for (int j=0; j<vector_size; j++)
								if (data[j]!=j+10)
									printf ("GET: Data Transfer Error! Expected %d, received %d\n\n", j, data[j]);
						}
						break;

				}
			}
			
			// 3. Free the windows
			MPI_Barrier (MPI_COMM_WORLD);
			for (int i=0; i<total_procs; i++)
				MPI_Win_free(&win[i]);

			break;
	}

	/*******************************************************************************/
	//                      		Print Results
	/*******************************************************************************/

	if (rank == MASTER)
	{
		float *maxTime 	= new float[repeat_count];

		MPI_Recv (recvTime, repeat_count, MPI_FLOAT, SLAVE, ADMIN_TAG, MPI_COMM_WORLD, &status);
		
		sort_multiple (maxTime, sendTime, total_procs, repeat_count, SORT_MAX, 0);

		float avgSendTime = maxTime[repeat_count/2];
		float avgSendBw = (float)(vector_size*sizeof(data_tt)*transfer_count)/(float)(avgSendTime*1024.0*1024.0);

		// printf ("ignore{%d}, benchmark: %d, data_size: %d bytes, transfer_count: %d, Total: %f ms, %.2f MB/s\n", 
		//	not_used_val, 
		//	bench_type,
		//	vector_size*(int)sizeof(data_tt),
		//	transfer_count,
		//	avgSendTime*1000.0,
		//	avgSendBw);

		printf ("%d, %d, %f, %f\n",
			vector_size*(int)sizeof(data_tt),
		 	transfer_count,
		 	avgSendTime*1000.0,
		 	avgSendBw);
	}
	else
	{
		switch (bench_type)
		{
			case SYNC_COMM:
			case ASYNC_PUT:
			case ASYNC_GET:
				MPI_Send (recvTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
				break;
			case BARRIER:
				MPI_Send (sendTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
				break;
		}
	}


	/****************************************************************************/
	//                    				Finalize
	/****************************************************************************/
	MPI_Finalize ();

	return 0;
}
