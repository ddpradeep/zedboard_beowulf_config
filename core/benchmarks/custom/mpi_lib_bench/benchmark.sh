#!/bin/bash

DATA_SIZE_START=4			# 4 bytes
DATA_SIZE_END=64*1024*1024	# 64MB

for lib in mpich-3.1 openmpi-1.8
do
	make MPI=/opt/$lib -B
	for type in 1 2
	do
		for (( size=DATA_SIZE_START; size<=DATA_SIZE_END; size*=2))					# Varying Datasize
		do
			sync
			sleep 1
			OUTPUT=$(sudo make run MPI=/opt/$lib BENCH_TYPE=$type SIZE=$size) 
			echo $OUTPUT >> $lib$type.txt
			echo $OUTPUT
		done
	done
done
