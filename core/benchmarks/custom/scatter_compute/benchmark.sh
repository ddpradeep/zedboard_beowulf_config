#!/bin/bash

REPEAT=24
START_NODE_SIZE=1
END_NODE_SIZE=2
SIZE=1024*1024

OUTPUT_FILE="results.txt"

# rm -rf $OUTPUT_FILE

for NP in 1
do
	for SIZE in 1 2 4 8 16
	do
		for RMA in 0
		do
				CMD="make run_seq MPI=/opt/mpich-3.1 NP=1 SIZE=$((SIZE*1024*1024)) REPEAT=$REPEAT RMA=$RMA"
				echo "$CMD"
				CMD_OUTPUT=$($CMD)
				CMD_OUTPUT=$(echo "$CMD_OUTPUT" | tail -1)
				echo "$CMD_OUTPUT"
				echo $CMD_OUTPUT >> $OUTPUT_FILE
		done
	done
done


# for NP in 2
# do
# 	for SIZE in 1 2 4 8 16 32
# 	do
# 		for RMA in 0 10 20 30 40 50
# 		do
# 				CMD="make run_seq MPI=/opt/mpich-3.1 SIZE=$((SIZE*1024*1024)) REPEAT=$REPEAT RMA=$RMA"
# 				echo "$CMD"
# 				CMD_OUTPUT=$($CMD)
# 				CMD_OUTPUT=$(echo "$CMD_OUTPUT" | tail -1)
# 				echo "$CMD_OUTPUT"
# 				echo $CMD_OUTPUT >> $OUTPUT_FILE
# 		done
# 	done
# done
