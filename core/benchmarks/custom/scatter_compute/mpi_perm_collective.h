#ifndef MPI_PERM_COLLECTIVE
#define MPI_PERM_COLLECTIVE

#include "helper.h"
#include "mpi.h"

typedef struct 
{
	int count;
	int *disp_len;
	int *disp;
	MPI_Datatype oldtype;
	MPI_Datatype newtype;
} mpi_indexed_info;

template <typename BUFFER>
int build_mpi_datatype_send (BUFFER *data, unsigned long size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(i);
			uint32_t blocklength = 1;
			i++;
			// while ( (i<size) && is_within_range(data[i], to_node*size, (to_node+1)*size) ) // && data[i]==(old_address+1) )
			// {
			// 	blocklength++;

			// 	old_address = data[i];
			// 	i++;
			// }
			blocklengths.push_back(blocklength);
			count++;
		}
		else
			i++;
	}

	// 2. Copy them to the required format
	datatype_info->disp_len	= new int [count];
	datatype_info->disp 	= new int [count];

	memcpy (datatype_info->disp_len, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->disp, &displacements[0], count*sizeof(uint32_t));
	datatype_info->count = count;
	datatype_info->oldtype = mpi_data_t;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->disp_len,
		datatype_info->disp,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);

	// delete [] datatype_info->disp_len;
	// delete [] datatype_info->disp;

	// print (datatype_info->disp_len, datatype_info->count, " ");
	// print (datatype_info->disp, datatype_info->count, " ");

	return getSum (datatype_info->disp_len, datatype_info->count);

	// for (int i=0; i<count; i++)
	// {
	// 	for (int j=0; j<blocklengths.at(i); j++)
	// 		printf ("%d, ", displacements.at(i)+j);
	// 	printf ("\n");
	// }

	// MPI_Type_indexed (count, blocklens[], offsets[], old_type,*newtype)
}

template<typename BUFFER>
int build_mpi_datatype_recv (BUFFER *data, unsigned long size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(data[i]);
			uint32_t blocklength = 1;
			i++;
			// while ( (i<size) && is_within_range(data[i], to_node*size, (to_node+1)*size) && data[i]==(old_address+1) )
			// {
			// 	blocklength++;

			// 	old_address = data[i];
			// 	i++;
			// }
			blocklengths.push_back(blocklength);
			count++;
		}
		else
			i++;
	}

	// 2. Copy them to the required format
	datatype_info->disp_len	= new int [count];
	datatype_info->disp 	= new int [count];

	memcpy (datatype_info->disp_len, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->disp, &displacements[0], count*sizeof(uint32_t));

	for (int i=0; i<count; i++)
		datatype_info->disp[i] -= (to_node*size);

	datatype_info->count = count;
	datatype_info->oldtype = mpi_data_t;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->disp_len,
		datatype_info->disp,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);

	// delete [] datatype_info->disp_len;
	// delete [] datatype_info->disp;

	// print (datatype_info->disp_len, datatype_info->count, " ");
	// print (datatype_info->disp, datatype_info->count, " ");

	return getSum (datatype_info->disp_len, datatype_info->count);

	// for (int i=0; i<count; i++)
	// {
	// 	for (int j=0; j<blocklengths.at(i); j++)
	// 		printf ("%d, ", displacements.at(i)+j);
	// 	printf ("\n");
	// }

	// MPI_Type_indexed (count, blocklens[], offsets[], old_type,*newtype)
}
#endif