/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define data_tt 	int
#define mpi_data_t	MPI_INT

// #define DEBUG

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper.h"
#include "mpi_perm_collective.h"
#include <future>
#include <chrono>
#include <unistd.h>
#include <assert.h>


using namespace std;

void compute (data_tt *recv_old, data_tt *send_new);

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(PRIMARY_CORE);
	init_rand();
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	data_tt 	*send_buf;								// vector global to all node [scatter/compute src/dst]
	data_tt 	*recv_buf;						// vector local to each node

	float achieved_rma;

	float *computeTime, *scatterTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win *win;

	int mpi_thread_suuport;

	// MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &mpi_thread_suuport);
	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank
	
	if (mpi_thread_suuport != MPI_THREAD_MULTIPLE)
	    debug("[%lu]\tThis MPI implementation does not support multiple threads\n", rank);

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	debug ("Process %lu running on %s\n", rank, processor_name);
	/***********************************************************************************************/

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: <total_size> <repeat_count> <%% rma>\nMemory Requirement > 8(+1) x partition_size\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	unsigned long partition_size		= (atoi(argv[1])/total_proc)/sizeof(data_tt);
	if (partition_size%2 == 1) 
		partition_size += 1;
	unsigned long repeat_count 		= atoi(argv[2]);
	unsigned long rma 				= atoi(argv[3]);

	unsigned long total_size 		= partition_size*total_proc;
	uint32_t *dest_address 			= new uint32_t [partition_size];

	computeTime 	= (float *) malloc(sizeof(float)*repeat_count);
    scatterTime = (float *) malloc(sizeof(float)*repeat_count);

	/***********************************************************************************************/
	/*                        		Generate Permutated Address Space
	/***********************************************************************************************/

	if (rank == MASTER)
	{
		uint32_t *global_dest_address = new uint32_t [total_size];
		achieved_rma = get_random_address (global_dest_address, total_proc, partition_size, rma);
		// print (global_dest_address, total_size, " ");

		MPI_Scatter(global_dest_address, partition_size, MPI_UINT32_T, dest_address, partition_size, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);
		// printArray (global_dest_address, total_size, " ");
		printf ("sc_seq, %d nodes, %lu, %.2f%%, ", total_proc, partition_size*sizeof(data_tt), achieved_rma);

		delete [] global_dest_address;
	}
	else
	{
		MPI_Scatter(NULL, partition_size, MPI_UINT32_T, dest_address, partition_size, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************************************************************/
	/*                                      Initialize Scatter
	/***********************************************************************************************/

	win = new MPI_Win [total_proc];

	MPI_Alloc_mem(partition_size*sizeof(data_tt), MPI_INFO_NULL, &send_buf);
	MPI_Alloc_mem(partition_size*sizeof(data_tt), MPI_INFO_NULL, &recv_buf);

	memset_seq(send_buf, partition_size, rank*partition_size);
	memset_val(recv_buf, partition_size, 0);

	mpi_indexed_info		*sendType	= new mpi_indexed_info [total_proc];
	mpi_indexed_info		*recvType	= new mpi_indexed_info [total_proc];

	// 1. Create the windows for remote and local access
	for (int i=0; i<total_proc; i++)
	{
		if (rank == i)
			MPI_Win_create(recv_buf, partition_size*sizeof(data_tt), sizeof(data_tt), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
    	else
			MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
	}

	TIME_DATA_TYPE timer;

	// 2. Build the sending and receiving send_buf type
	for (int i=0; i<total_proc; i++)
	{

		int send_count = build_mpi_datatype_send (dest_address, partition_size, i, &sendType[i]);
		int recv_count = build_mpi_datatype_recv (dest_address, partition_size, i, &recvType[i]);

		assert (send_count == recv_count);
	}

	// if (rank == MASTER)
	// {
	// 	printf ("\n");
	// 	printArray(sendType[rank].disp, sendType[rank].count, " ");
	// 	printArray(recvType[rank].disp, recvType[rank].count, " ");
	// }

	// 3. Generate local addresses for computation (array-indir)
	uint32_t *src_addr = new uint32_t[partition_size];
	uint32_t *dst_addr = new uint32_t[partition_size];
	
	get_random_address (src_addr, 0, partition_size);
	get_random_address (dst_addr, 0, partition_size);

	MPI_Barrier (MPI_COMM_WORLD);

	for (int i=0; i<repeat_count; i++)
    {
    	/***********************************************************************************************/
		/*                        					Compute
		/***********************************************************************************************/

		//----------- start timer ------------//
		resetTime(&timer);

		for (uint32_t disp=0; disp<partition_size; disp++)
			send_buf[dst_addr[disp]] = recv_buf[src_addr[disp]];

		//----------- stop timer ------------//
		computeTime[i] = getTime(&timer);

    	/***********************************************************************************************/
		/*                        					Scatter
		/***********************************************************************************************/

    	MPI_Barrier (MPI_COMM_WORLD);

		//----------- start timer ------------//
		resetTime(&timer);

		/* (I) Local Scatter */
		for (uint32_t disp=0; disp<recvType[rank].count; disp++)
			recv_buf[recvType[rank].disp[disp]] = send_buf[sendType[rank].disp[disp]];

		/* (II) Global Scatter */

		// i. Lock the remote windows
		for (int j=0; j<total_proc; j++)
		{
			int k = (j+rank) % total_proc;
			if (k==rank)	continue;
			MPI_Win_lock(MPI_LOCK_SHARED,k,0,win[k]);
		}

		// ii. Perform Scatter
		for (int j=0; j<total_proc; j++)
		{
			int k = (j+rank) % total_proc;
			if (k==rank)	continue;
			MPI_Put(send_buf, 1, sendType[k].newtype, k, 0, 1, recvType[k].newtype, win[k]);
		}

		// iii. Unlock the remote windows
		for (int j=0; j<total_proc; j++)
		{
			int k = (j+rank) % total_proc;
			if (k==rank)	continue;
			MPI_Win_unlock(k, win[k]);
		}

		//----------- stop timer ------------//
		scatterTime[i] = getTime(&timer);

		MPI_Barrier (MPI_COMM_WORLD);
	}
	
	// 6. Free the windows
	for (int i=0; i<total_proc; i++)
    {
	    MPI_Win_free(&win[i]);
	    MPI_Type_free(&sendType[i].newtype);
	    MPI_Type_free(&recvType[i].newtype);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

	if (rank == MASTER)
    {
    	// 1. Initialize buffers for receving time send_buf from all processes
    	float *computeTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * total_proc);
    	float *scatterTime_buffer 	= (float *) malloc(sizeof(float)*repeat_count * total_proc);

    	// 2. Receive time send_buf from all processes
    	for (int i=0; i<repeat_count; i++)
    	{
    		scatterTime_buffer[i] 	= scatterTime[i];
    		computeTime_buffer[i] 	= computeTime[i];
    	}

    	for (int i=1; i<total_proc; i++)
    	{
    		MPI_Recv (scatterTime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    		MPI_Recv (computeTime_buffer+i*repeat_count, repeat_count, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    	}

	    // 2. Sort the recorded timings
	    sort_multiple (scatterTime, scatterTime_buffer, total_proc, repeat_count, SORT_MAX, 0);
	    sort_multiple (computeTime, computeTime_buffer, total_proc, repeat_count, SORT_MAX, 0);

	    // 3. Get avg of each time
	    float scatterTimeAvg	= scatterTime[repeat_count/2];
	    float computeTimeAvg 	= computeTime[repeat_count/2];

	    float scatterSpeedAvg	= (float)(sizeof(data_tt)*partition_size*(total_proc)*(achieved_rma/100.0))/(scatterTimeAvg*1048576.0);
	    // float computeSpeedAvg	= (float)(sizeof(data_tt)*partition_size*(total_proc)*(achieved_rma/100.0))/(computeTimeAvg*1048576.0);
	    float computeSpeedAvg	= 0;
		
		printf ("%f ms, %f ms, %.2f MB/s\n", computeTimeAvg*1000.0, scatterTimeAvg*1000.0, scatterSpeedAvg);
	}
	else
	{
		MPI_Send (scatterTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
		MPI_Send (computeTime, repeat_count, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	}


	/***********************************************************************************************/
	/*                                       	Finalize
	/***********************************************************************************************/
	free (send_buf);
	free (recv_buf);

    MPI_Finalize ();

    return 0;
}