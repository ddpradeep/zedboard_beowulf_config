/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO 		0
#define MIX_REMOTE_LOCAL_ACCESS	0

#define MASTER 0
#define NOT_ASSIGNED -1

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int * A;										// vector A
	int * B;										// vector B
	int * C;										// vector A + B
	
	int *randLoc;
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	int repeatCount = 1;
	int loopCounter=repeatCount;					// Repeats for averaging
	unsigned long size;
	long long int i, j, k;	// vector size
	unsigned long VECTOR_SIZE, COMPLEXITY, REMOTE_ACCESS_RATIO;						// elements per process	
	
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win *winA, *winB;

	/* Start Profiling */
	struct timeval time1, time2, time3, time4;
	float timeAcc = 0, computeTime=0, timeSend, timeRecv;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank
	
    //MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
	int minArrayPtr, maxArrayPtr;

	/***************************************************************************/
	/* Initialize A, B and allocate C */
	/***************************************************************************/

	VECTOR_SIZE 		= atoi(argv[1]);
	COMPLEXITY 			= atoi(argv[2]);
	REMOTE_ACCESS_RATIO = atoi(argv[3]);
	winA = (MPI_Win *) malloc(sizeof(MPI_Win)*(total_proc+1));
	winB = (MPI_Win *) malloc(sizeof(MPI_Win)*(total_proc+1));

	if (rank == MASTER)
	{
		#if INFO==1
			printf ("Vector Size:\t%lu (%.2fKB)\tComplexity : %lu operations\t%lu%% Remote Mem Access\n", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, REMOTE_ACCESS_RATIO);
		#else
			printf ("%lu, %.2f, %lu, %f, ", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, (float)REMOTE_ACCESS_RATIO/100.0);
		#endif
	}

	size = total_proc * VECTOR_SIZE;
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &A);
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &B);
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &C);

	MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

	for(i=0;i<total_proc * VECTOR_SIZE;i++)
	{
		A[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
		B[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
	}

	minArrayPtr = rank*VECTOR_SIZE;							// The min index of local memory
	maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;				// The max index of local memory

	if (rank == MASTER) 
	{
		for(i=0;i<total_proc * VECTOR_SIZE;i++)
		{
			A[i] = i;		// Allocate NOT_ASSIGNED value
			B[i] = i;		// Allocate NOT_ASSIGNED value
		}

		MPI_Win_create(A, size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winA[total_proc]);
        MPI_Win_create(B, size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winB[total_proc]);
    }
    else
	{
		MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winA[total_proc]); 
        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winB[total_proc]);
    }

    if (rank!=MASTER)
    {
        resetTime(&time1);
        // printf ("Bef:A[%d]=%d\n", minArrayPtr, A[minArrayPtr]);
        // printf ("Bef:A[%d]=%d\n", minArrayPtr-1, A[minArrayPtr-1]);
        MPI_Win_lock(MPI_LOCK_SHARED,MASTER,0,winA[total_proc]);
        MPI_Win_lock(MPI_LOCK_SHARED,MASTER,0,winB[total_proc]);
        MPI_Get(A+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winA[total_proc]);
		MPI_Get(B+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winB[total_proc]);
		MPI_Win_unlock(MASTER,winA[total_proc]);
        MPI_Win_unlock(MASTER,winB[total_proc]);
        // printf ("Aft:A[%d]=%d\n", minArrayPtr, A[minArrayPtr]);
        // printf ("Aft:A[%d]=%d\n", minArrayPtr-1, A[minArrayPtr-1]);
		// timeAcc = getTime(&time1);						// Transfer latency
		timeSend = getTime(&time1);
		// printf ("Time Val (%d): %f ms\n", rank, timeAcc*1000.0);
	}
	else
		timeAcc = 0;
	/***************************************************************************/

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Index Generator */
	/***************************************************************************/
	randLoc = (int *) malloc(sizeof(int)*VECTOR_SIZE);
	int tempRand;

	// 1. Allocate the RemMemory Indexes first
	for (i=0; i<VECTOR_SIZE; i++)
	{
			tempRand = minArrayPtr;
			while (tempRand>=minArrayPtr && tempRand<=maxArrayPtr)
				tempRand = rand () % size;
			randLoc[i] = tempRand;							// While the random index in outside local memory boundaries, assign it
	}

	// 2. Allocate the InMemory Indexes
	long tempSize = VECTOR_SIZE*(float)(100.0-REMOTE_ACCESS_RATIO)/100.0;
	#if MIX_REMOTE_LOCAL_ACCESS==1
		int *history = (int *)malloc (sizeof(int) * tempSize);
		for (i=0; i<tempSize; i++)
		{
			int temp;
			int found;
			do
			{
				temp = rand()%VECTOR_SIZE;
				found = 0;
				for (j=0; j<i; j++)
					if (history[j]==temp)
						found = 1;
			} while (found==1);
			history [i] = temp;
			randLoc[temp] =  (rand () % VECTOR_SIZE) + minArrayPtr;
		}
		free (history);
	#else
		for (i=0; i<tempSize; i++)
		{
			randLoc[i] =  (rand () % VECTOR_SIZE) + minArrayPtr;
		}
	#endif
	for (i=0, j=0, k=0; i<VECTOR_SIZE; i++)
		if (randLoc[i]>=minArrayPtr && randLoc[i]<=maxArrayPtr)
			j++;											// Count number of InMemory accesses
		else
			k++;											// Count number of RemMemory accesses
	//printf ("(Process-%d) InMemory: %lld\tRemMemory : %lld (%.2f%%)\n", rank, j, k, (float)k*100.0/(float)VECTOR_SIZE);
	//printf ("(%d) %d, %d \n", rank, minArrayPtr, maxArrayPtr);

	/***************************************************************************/

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Core Benchmarking */
	/***************************************************************************/

	// 1. Create memory handles for hosting local memory and leeching from remote memories
	for (i=0; i<total_proc; i++)
	{
		if (rank == i)
		{
			MPI_Win_create(A+minArrayPtr, VECTOR_SIZE*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winA[i]);
        	MPI_Win_create(B+minArrayPtr, VECTOR_SIZE*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winB[i]);
    	}
    	else
		{
			MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winA[i]); 
	        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winB[i]);
		}
	}

	// 2. Get access to remote memories
	for (k=0; k<total_proc; k++)
		if (k!=rank)
		{

			MPI_Win_lock(MPI_LOCK_SHARED,k,MPI_MODE_NOCHECK,winA[k]);
    		MPI_Win_lock(MPI_LOCK_SHARED,k,MPI_MODE_NOCHECK,winB[k]);
		}

	/* START MEASURING TIME */
	resetTime(&time2);

	// 3. Perform computation and remote puts
	for (k=0; k<VECTOR_SIZE; k++)
		// Local Put
		if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr)
		{
			for (j=0; j<COMPLEXITY; j++)
				C[i] = A[i]+B[i];
		}
		// Remote Put
		else
		{
			for (j=0; j<COMPLEXITY; j++)
				C[i] = A[i]+B[i];
			i=randLoc[k]/VECTOR_SIZE;
			MPI_Put(A+randLoc[k], 1, MPI_INT, i, randLoc[k]%VECTOR_SIZE, 1, MPI_INT, winA[i]);
			MPI_Put(B+randLoc[k], 1, MPI_INT, i, randLoc[k]%VECTOR_SIZE, 1, MPI_INT, winB[i]);
			//printf ("[%d, %d] A[%d]=%d\n", minArrayPtr, maxArrayPtr, randLoc[k], A[randLoc[k]]);
		}

	// 4. Ensure all remote puts are complete
	for (k=0; k<total_proc; k++)
		if (k!=rank)
		{
			MPI_Win_unlock(k, winA[k]);
			MPI_Win_unlock(k, winB[k]);
		}
	
	/* STOP MEASURING TIME */	
	computeTime = getTime(&time2);							// Get time spent on local and RMA processing

	// 5. Subtract the time taken in processing if statements
	resetTime(&time3);
	for (k=0; k<VECTOR_SIZE; k++)
		if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr);
		else
			i=randLoc[k]/VECTOR_SIZE;
	computeTime -= getTime(&time3);

	// 6. Some sanity measures to counter negative timing (it happens for negligible vector sizes!)
	if ((computeTime<0.0000000001) || (COMPLEXITY==0))
		computeTime=0;

	//printf ("(%d) done \n", rank);
	MPI_Barrier (MPI_COMM_WORLD);
	
	// Gather Operation
	if (rank != MASTER)
	{
		// printf ("Time Val (%d): %f\n", rank, timeAcc);
		resetTime(&time4);
		MPI_Win_lock(MPI_LOCK_SHARED,MASTER,MPI_MODE_NOCHECK,winA[total_proc]);
	    MPI_Win_lock(MPI_LOCK_SHARED,MASTER,MPI_MODE_NOCHECK,winB[total_proc]);
		MPI_Put(A+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winA[total_proc]);
		MPI_Put(B+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winB[total_proc]);
		MPI_Win_unlock(MASTER, winA[total_proc]);
		MPI_Win_unlock(MASTER, winB[total_proc]);
		// timeAcc += getTime(&time4);							// Get time spent on local and RMA processing
		timeRecv = getTime (&time4);
		// printf ("Time Val N (%d): %f\n", rank, timeAcc);
	}

	#if INFO!=1
		if (rank == MASTER)
		{
			float timeAccMax = timeAcc;
			float timeSendMax = timeSend;
			float timeRecvMax = timeRecv;
			float computeTimeMax = computeTime;
			// float totaltimeMax = timeAcc<0.0000001?0:timeAccMax + computeTimeMax;
			float totalTimeMax = timeSend + computeTime + timeRecv;

			for (i=1; i<total_proc; i++)
			{
				MPI_Recv (&timeSend, 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
				MPI_Recv (&computeTime, 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
				MPI_Recv (&timeRecv, 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
				// if ((timeSend+computeTime)>totaltimeMax)
				// {
				// 	timeAccMax = timeAcc;
				// 	computeTimeMax = computeTime;
				// }
				timeSendMax += timeSend;
				timeRecvMax += timeRecv;
				if (computeTimeMax<computeTime)
					computeTimeMax = computeTime;
			}

			timeSendMax /= (float)(total_proc-1);
			timeRecvMax /= (float)(total_proc-1);

			// computeTimeMax /= (float)total_proc;
			//timeAcc += getTime(&time4);
			printf ("%f, %f, %f, %f\n", timeSendMax*1000.0, timeRecvMax*1000.0, computeTimeMax*1000.0, (timeSendMax+computeTimeMax+timeRecvMax)*1000);
		}
		else
		{
			MPI_Send (&timeSend, 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
			MPI_Send (&computeTime, 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
			MPI_Send (&timeRecv, 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		}
	#else
		printf ("(Process %d) Size: %lu (%.2fKB) Network Overhead:  %f ms Processing Time: %f ms\n", rank, VECTOR_SIZE, (float)(sizeof(int)*VECTOR_SIZE)/(1024.0), timeAcc*1000.0, computeTime*1000.0);
	#endif
    /***************************************************************************/

	free (A);
	free (B);
	free (C);

	//MPI_Group_free(&group);
    MPI_Group_free(&comm_group);
    for (i=0; i<=total_proc; i++)
    {
	    MPI_Win_free(&winA[i]);
	    MPI_Win_free(&winB[i]);
	}
    MPI_Finalize ();

	return 0;
}
