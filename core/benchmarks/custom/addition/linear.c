#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO        0

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
    int * a;                    // vector A
    int * b;                    // vector B
    int * c;                    // vector A + B

    int repeatCount = 1;        // total number of test cases
    int total_proc;
    long long int i, n ;        // vector size   
	unsigned long VECTOR_SIZE, COMPLEXITY;
	/* Initialize Vector */
    total_proc = atoi(argv[1]);
	VECTOR_SIZE = atoi(argv[2]);
	COMPLEXITY  = atoi(argv[3]);
	#if INFO==1
        printf ("Vector Size:\t%lu (%.2fKB)\tComplexity : %lu operations\n\n", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY);
    #else
        printf ("%d, %lu, %.2f, %lu, %f, ", total_proc, total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, 0.0);
    #endif
		
    /* Start Profiling */
    struct timeval time1;

    n = total_proc * VECTOR_SIZE;

    int * ap;
    int * bp;
    int * cp;
    
    a = (int *) malloc(sizeof(int)*n);  // malloc vector A
    b = (int *) malloc(sizeof(int)*n);  // malloc vector B
    c = (int *) malloc(sizeof(int)*n);  // malloc vector C
    
    for(i=0;i<n;i++)                    // Random assign vector A
        a[i] = i;
    for(i=0;i<n;i++)                    // Random assign vector B
        b[i] = n-i;

    // Perform C = A + B for allocated elements
    int  j;
    resetTime(&time1);
	for (j=0; j<COMPLEXITY; j++)
	for(i=0;i<n;i++)
        c[i] = a[i]+b[i];

    float computeTime = getTime(&time1);
    #if INFO==1
        printf("Time taken:\t%f s\n", getTime(&time1));
    #else
        printf ("%f, %f, %f, %f\n", 0.0, 0.0, (float)computeTime*1000.0, (float)computeTime*1000.0);
    #endif
    return 0;
}
