 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO 		0

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

#define MASTER 0

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int * a;										// vector A
	int * b;										// vector B
	int * c;										// vector A + B
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	int repeatCount = 1;
	int loopCounter=repeatCount;					// Repeats for averaging
	long long int i, j;	// vector size
	unsigned long VECTOR_SIZE, COMPLEXITY;						// elements per process	
	
	MPI_Status status;

	/* Start Profiling */
	struct timeval timeT, timeC, timeS, timeR;
	float timeAcc = 0, computeTime, timeSend, timeRecv;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank
	
	int * ap;
	int * bp;
	int * cp;

	if (rank == MASTER) // For master process
	{
		/* Initialize Vector */
		VECTOR_SIZE  = atoi(argv[1]);
		COMPLEXITY = atoi(argv[2]);
		#if INFO==1
			printf ("Vector Size:\t%lu (%.2fKB)\tComplexity : %lu operations\n\n", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY);
		#else
			printf ("%lu, %.2f, %lu, %f, ", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, 0.0);
		#endif
		
		MPI_Bcast (&VECTOR_SIZE, 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);		// Broadcast repeatCount
		MPI_Bcast (&COMPLEXITY , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);		// Broadcast repeatCount
		
		a = (int *) malloc(sizeof(int)*total_proc * VECTOR_SIZE);	// malloc vector A
		b = (int *) malloc(sizeof(int)*total_proc * VECTOR_SIZE);	// malloc vector B
		c = (int *) malloc(sizeof(int)*total_proc * VECTOR_SIZE);	// malloc vector C
		
		for(i=0;i<total_proc * VECTOR_SIZE;i++)					// Random assign vector A
			a[i] = i;
		for(i=0;i<total_proc * VECTOR_SIZE;i++)					// Random assign vector B
			b[i] = total_proc * VECTOR_SIZE-i;
				
		ap = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		bp = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		cp = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		
		// resetTime(&timeT);
		resetTime(&timeS);
		
		MPI_Scatter(a, VECTOR_SIZE, MPI_INT, ap, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector A
		MPI_Scatter(b, VECTOR_SIZE, MPI_INT, bp, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector B
		
		timeSend = getTime(&timeS);

		// Perform C = A + B for allocated elements

		resetTime(&timeC);
		for(i=0;i<VECTOR_SIZE;i++)
			for (j=0; j<COMPLEXITY; j++)
				cp[i] = ap[i]+bp[i];
		// MPI_Barrier (MPI_COMM_WORLD);
		computeTime = getTime(&timeC);

		MPI_Barrier(MPI_COMM_WORLD);

		resetTime(&timeR);

		MPI_Gather(cp, VECTOR_SIZE, MPI_INT, c, VECTOR_SIZE, MPI_INT, MASTER, MPI_COMM_WORLD);// Gather vector C
		//MPI_Barrier(MPI_COMM_WORLD);
		// timeAcc = getTime(&timeT);
		timeRecv= getTime(&timeR);
		
		timeAcc = timeSend + computeTime + timeRecv;

		float temp;
		for (i=1; i<total_proc; i++)
		{
			MPI_Recv (&temp, 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
		}

		#if INFO==1
			printf ("Computation (P0)\t\t%.4f ms [%.2f, %.2f]\n",	(float)computeTime*1000.0, 0.0, 0.0);
			printf ("\nCommunication \t\t\t%.2f ms [%.2f, %.2f]\n\n",	(float)(timeAcc - computeTime)*1000.0, 0.0, 0.0);
			printf("Time time taken:\t%f s\n", timeAcc/(float)repeatCount);
		#else
			printf ("%f, %f, %f, %f\n", timeSend*1000.0, timeRecv*1000.0, (float)computeTime*1000.0, (float)timeAcc*1000.0);
		#endif

		free (a);
		free (b);
		free (c);
		free (ap);
		free (bp);
		free (cp);
	}
	else			// For slave processes
	{
		MPI_Bcast (&VECTOR_SIZE, 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);						// Receive repeatCount
		MPI_Bcast (&COMPLEXITY , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);						// Receive vector size
		ap = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		bp = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		cp = (int *) malloc(sizeof(int)*VECTOR_SIZE);
			
		MPI_Scatter(a, VECTOR_SIZE, MPI_INT, ap, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector A
		MPI_Scatter(b, VECTOR_SIZE, MPI_INT, bp, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector B
		
		// Perform C = A + B for allocated elements
		resetTime(&timeC);
		for (j=0; j<COMPLEXITY; j++)
			for(i=0;i<VECTOR_SIZE;i++)
				cp[i] = ap[i]+bp[i];

		// MPI_Barrier (MPI_COMM_WORLD);
		computeTime = getTime(&timeC);

		MPI_Barrier(MPI_COMM_WORLD);

		MPI_Gather(cp, VECTOR_SIZE, MPI_INT, c, VECTOR_SIZE, MPI_INT, MASTER, MPI_COMM_WORLD);	// Initiate gathering of vector C
		//MPI_Barrier(MPI_COMM_WORLD);
		
		MPI_Send (&computeTime, 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		
		free (ap);
		free (bp);
		free (cp);
	}
	
	MPI_Finalize ();
	//printf ("Process %d Finished\n", rank);

	return 0;
}
