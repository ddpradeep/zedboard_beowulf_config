 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

//#define DEBUG

#define MASTER 0

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int *vector;
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	
	int accumPtr = 0;
	long long int i, j;
	unsigned long VECTOR_SIZE, REPEAT_COUNT;
	
	MPI_Status status;

	struct timeval timeT;
	float *timeAcc;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank

	if (rank == MASTER) // MASTER
	{
		/* Initialize Vector */
		VECTOR_SIZE  = atoi(argv[1]);
		VECTOR_SIZE /= sizeof(int);					// Get Vector Size in Bytes
		REPEAT_COUNT = atoi(argv[2]);				// Repeat Count (Exponential)
		MPI_Send (&VECTOR_SIZE, 1, MPI_LONG_LONG, 1, 1, MPI_COMM_WORLD);
		MPI_Send (&REPEAT_COUNT, 1, MPI_LONG_LONG, 1, 1, MPI_COMM_WORLD);
		
		// Allocate vector
		vector = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		// Allocate Time Allocator
		timeAcc = (float *) malloc(sizeof(float)*REPEAT_COUNT);
		#ifdef DEBUG
			printf ("Vector Size:\t%lu (%.2f KB) looping x%lu\n\n", VECTOR_SIZE/sizeof(int), (float)sizeof(int)*(float)VECTOR_SIZE/(1024.0), REPEAT_COUNT);
		#else
			printf ("%lu\t", VECTOR_SIZE*sizeof(int));
		#endif
		for(i=0;i<VECTOR_SIZE;i++)
			vector[i] = i;
			
		// About to start benchmark
		MPI_Barrier (MPI_COMM_WORLD);
		
		/* CORE: Send Data */
		for (accumPtr=0; accumPtr<REPEAT_COUNT; accumPtr++)
		{
			resetTime(&timeT);
			for (i=0; i<REPEAT_COUNT; i++)
			{
				MPI_Send (vector, VECTOR_SIZE, MPI_INT, 1, 1, MPI_COMM_WORLD);
			}
			timeAcc [accumPtr] = getTime(&timeT);
			// Prepare to start next benchmark
			MPI_Barrier (MPI_COMM_WORLD);
		}
		
		/* Calculate Results */
		for (i=0; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1; j++)
				if (timeAcc[j]<timeAcc[j+1])
				{
					float temp = timeAcc[j];
					timeAcc[j] = timeAcc[j+1];
					timeAcc[j+1] = temp;
				}
		MPI_Barrier (MPI_COMM_WORLD);
		
		/* Results */
		// For Sending
		#ifdef DEBUG
			printf ("Sending \t\t%.2f us [%.2f, %.2f]\n",	timeAcc[REPEAT_COUNT/2]*1000000.0/REPEAT_COUNT,
															timeAcc[REPEAT_COUNT-1]*1000000.0/REPEAT_COUNT, 
															timeAcc[0]*1000000.0/REPEAT_COUNT);
		#else
			float min, max, avg;
			avg = timeAcc[REPEAT_COUNT/2]*1000000.0/REPEAT_COUNT;
			min = timeAcc[REPEAT_COUNT-1]*1000000.0/REPEAT_COUNT;
			max = timeAcc[0]*1000000.0/REPEAT_COUNT;
		#endif
		
		// For Receiving
			MPI_Recv (&timeAcc[0], 1, MPI_FLOAT, 1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (&timeAcc[REPEAT_COUNT/2], 1, MPI_FLOAT, 1, 1, MPI_COMM_WORLD, &status);
			MPI_Recv (&timeAcc[REPEAT_COUNT-1], 1, MPI_FLOAT, 1, 1, MPI_COMM_WORLD, &status);
		#ifdef DEBUG
			printf ("Receiving \t\t%.2f us [%.2f, %.2f]\n",	timeAcc[REPEAT_COUNT/2]*1000000.0/REPEAT_COUNT,
															timeAcc[REPEAT_COUNT-1]*1000000.0/REPEAT_COUNT, 
															timeAcc[0]*1000000.0/REPEAT_COUNT);
		#else
			avg += timeAcc[REPEAT_COUNT/2]*1000000.0/REPEAT_COUNT;
			min += timeAcc[REPEAT_COUNT-1]*1000000.0/REPEAT_COUNT;
			max += timeAcc[0]*1000000.0/REPEAT_COUNT;
			avg /= 2.0;
			min /= 2.0;
			max /= 2.0;
			printf ("%f\t%f\t%f\n", min, max, avg);
		#endif
	}
	else			// SLAVE
	{
		/* Initialize Vector */
		MPI_Recv (&VECTOR_SIZE, 1, MPI_LONG_LONG, MASTER, rank, MPI_COMM_WORLD, &status);
		MPI_Recv (&REPEAT_COUNT, 1, MPI_LONG_LONG, MASTER, rank, MPI_COMM_WORLD, &status);
		vector = (int *) malloc(sizeof(int)*VECTOR_SIZE);	// malloc vector
		timeAcc = (float *) malloc(sizeof(float)*REPEAT_COUNT);	// malloc vector
		MPI_Barrier (MPI_COMM_WORLD);
		
		/* CORE: Receive Data */
		for (accumPtr=0; accumPtr<REPEAT_COUNT; accumPtr++)
		{
			resetTime(&timeT);
			for (i=0; i<REPEAT_COUNT; i++)
			{
				MPI_Recv (vector, VECTOR_SIZE, MPI_INT, MASTER, rank, MPI_COMM_WORLD, &status);								// Receive repeatCount from MASTER
			}
			timeAcc [accumPtr] = getTime(&timeT);
			MPI_Barrier (MPI_COMM_WORLD);
		}
		
		/* Calculate Results */
		for (i=0; i<REPEAT_COUNT-1; i++)
			for (j=0; j<REPEAT_COUNT-1; j++)
				if (timeAcc[j]<timeAcc[j+1])
				{
					float temp = timeAcc[j];
					timeAcc[j] = timeAcc[j+1];
					timeAcc[j+1] = temp;
				}
		MPI_Barrier (MPI_COMM_WORLD);
		
		/* Results */
		MPI_Send (&timeAcc[0], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		MPI_Send (&timeAcc[REPEAT_COUNT/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		MPI_Send (&timeAcc[REPEAT_COUNT-1], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
	}
	
	free (vector);
	free (timeAcc);
	MPI_Finalize();	
	
	#ifdef DEBUG
	if (rank == MASTER)	printf("\nTotal time taken:\t%f ms\n", getTime(&timeT)*1000.0);
	#endif

	return 0;
}
