 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

#define MASTER 0

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int * a;										// vector A
	int * b;										// vector B
	int * c;										// vector A + B
	
	int *randLoc;
	
	int total_proc;									// total number of processes	
	int rank, rankInGroup;							// rank of each process
	int repeatCount = 1;
	int loopCounter=repeatCount;					// Repeats for averaging
	long size;
	long long int i, j, k;	// vector size
	unsigned long VECTOR_SIZE, COMPLEXITY, REMOTE_ACCESS_RATIO;						// elements per process	
	
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	/* Start Profiling */
	struct timeval timeT, timeC;
	float timeAcc = 0, computeTime=0;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank
	
    MPI_Comm_group(MPI_COMM_WORLD, &comm_group);
	
	int * ap;
	int * bp;
	int * cp;

	if (rank == MASTER) // For master process
	{
		/***************************************************************************/
		/* Send Vector Size, Complexity & Remote Access Ratio */
		/***************************************************************************/
		VECTOR_SIZE  = atoi(argv[1]);
		COMPLEXITY = atoi(argv[2]);
		REMOTE_ACCESS_RATIO = atoi(argv[3]);
		printf ("Vector Size:\t%lu (%.2fKB)\tComplexity : %lu operations\t%lu%% Remote Mem Access\n\n", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, REMOTE_ACCESS_RATIO);
		MPI_Bcast (&VECTOR_SIZE, 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);
		MPI_Bcast (&COMPLEXITY , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);
		MPI_Bcast (&REMOTE_ACCESS_RATIO , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);
		// For accessing remote mem
		rankInGroup = 1;
        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win); 
        MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
		/***************************************************************************/
		

		/***************************************************************************/
		/* Initialize A, B and allocate C */
		/***************************************************************************/
		size = total_proc * VECTOR_SIZE;
		MPI_Alloc_mem(sizeof(int)*size, MPI_INFO_NULL, &a);
		MPI_Alloc_mem(sizeof(int)*size, MPI_INFO_NULL, &b);
		MPI_Alloc_mem(sizeof(int)*size, MPI_INFO_NULL, &c);
		for(i=0;i<total_proc * VECTOR_SIZE;i++)
			a[i] = i;
		for(i=0;i<total_proc * VECTOR_SIZE;i++)
			b[i] = total_proc * VECTOR_SIZE-i;
		/***************************************************************************/
		

		/***************************************************************************/
		/* Index Generator */
		/***************************************************************************/
		randLoc = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		int minArrayPtr, maxArrayPtr;
		minArrayPtr = rank*VECTOR_SIZE;							// The min index of local memory
		maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;				// The max index of local memory
		int tempRand;
		// 1. Allocate the RemMemory Indexes first
		for (i=0; i<VECTOR_SIZE; i++)
		{
				tempRand = minArrayPtr;
				while (tempRand>=minArrayPtr && tempRand<=maxArrayPtr)
					tempRand = rand () % size;
				randLoc[i] = tempRand;							// While not a local memory, assign it
		}
		// 2. Allocate the InMemory Indexes
		long tempSize = VECTOR_SIZE*(float)(100.0-REMOTE_ACCESS_RATIO)/100.0;
		int *history = (int *)malloc (sizeof(int) * tempSize);
		for (i=0; i<tempSize; i++)
		{
			int temp;
			int found;
			do
			{
				temp = rand()%VECTOR_SIZE;
				found = 0;
				for (j=0; j<i; j++)
					if (history[j]==temp)
						found = 1;
			} while (found==1);
			history [i] = temp;
			randLoc[temp] =  (rand () % VECTOR_SIZE) + minArrayPtr;
		}
		free (history);
		for (i=0, j=0, k=0; i<VECTOR_SIZE; i++)
			if (randLoc[i]>=minArrayPtr && randLoc[i]<=maxArrayPtr)
				j++;											// Count number of InMemory accesses
			else
				k++;											// Count number of RemMemory accesses
		printf ("(Process %d) InMemory: %lld\tRemMemory : %lld (%.2f%%)\n", rank, j, k, (float)k*100.0/(float)VECTOR_SIZE);
		/***************************************************************************/
		
		MPI_Barrier (MPI_COMM_WORLD);							// Wait until all nodes have index generated and receive their share of vectors A and B (distributed storage)
		
		/***************************************************************************/
		/* Prepare RMA Ready Memory */
		/***************************************************************************/
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &ap);
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &bp);
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &cp);
		
		resetTime(&timeT);										// Time measurement starts now
		
		MPI_Scatter(a, VECTOR_SIZE, MPI_INT, ap, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector A
		MPI_Scatter(b, VECTOR_SIZE, MPI_INT, bp, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector B
		/***************************************************************************/


		/* Initialize MPI RMA */
		
		/* End */
		
		// Perform C = A + B for allocated elements
		MPI_Win_start(group, 0, win);	// Only after MPI_Win_post
		resetTime(&timeC);
		for (k=0; k<VECTOR_SIZE; k++)
			// InMemory Access
			if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr)
			{
				i = randLoc[k]%VECTOR_SIZE;
				for (j=0; j<COMPLEXITY; j++)
					cp[i] = ap[i]+bp[i];
			}
			// RemMeory Access
			else
			{
				int remA, remB, remC;
				// Get A
				// Get B
				for(i=0;i<VECTOR_SIZE;i++)
					remC = remA + remB;
				// Put C
			}
		computeTime = getTime(&timeC);							// Get time spent on local and RMA processing
		MPI_Win_complete(win);
		//computeTime -= ifOverhead;
		

		/* Wrap-up MPI Calls */
		MPI_Gather(cp, VECTOR_SIZE, MPI_INT, c, VECTOR_SIZE, MPI_INT, MASTER, MPI_COMM_WORLD);// Gather vector C
		timeAcc = getTime(&timeT);								// Get total time consumed
		
		/***************************************************************************/
		/* Informatic Printing */
		/***************************************************************************/
		//printf ("Computation (P0)\t\t%.4f ms [%.2f, %.2f]\n",	(float)computeTime*1000.0, 0.0, 0.0);
		
		float temp;
		for (i=1; i<total_proc; i++)
		{
			MPI_Recv (&temp, 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
			//printf ("Computation (P%lld)\t\t%.4f ms [%.2f, %.2f]\n", i, (float)temp*1000.0, 0.0, 0.0);
		}
		
		//printf ("\nCommunication \t\t\t%.2f ms [%.2f, %.2f]\n\n",	(float)(timeAcc - computeTime)*1000.0, 0.0, 0.0);
		//printf("Time time taken:\t%f s\n", timeAcc/(float)repeatCount);
		free (a);
		free (b);
		free (c);
		free (ap);
		free (bp);
		free (cp);
		/***************************************************************************/
	}
	else			// For slave processes
	{
		/***************************************************************************/
		/* Send Vector Size, Complexity & Remote Access Ratio */
		/***************************************************************************/
		MPI_Bcast (&VECTOR_SIZE, 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);					// Broadcast repeatCount
		MPI_Bcast (&COMPLEXITY , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);					// Receive vector size
		MPI_Bcast (&REMOTE_ACCESS_RATIO , 1, MPI_UNSIGNED_LONG, MASTER, MPI_COMM_WORLD);		// Broadcast remoteAccessRatio
		rankInGroup = 0;
		MPI_Win_create(b, VECTOR_SIZE*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
        MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
		/***************************************************************************/

		/***************************************************************************/
		/* Index Generator */
		/***************************************************************************/
		size = total_proc * VECTOR_SIZE;
		randLoc = (int *) malloc(sizeof(int)*VECTOR_SIZE);
		int minArrayPtr, maxArrayPtr;
		minArrayPtr = rank*VECTOR_SIZE;
		maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;
		int tempRand;
		// 1. Allocate the RemMemory Indexes first
		for (i=0; i<VECTOR_SIZE; i++)
		{
				tempRand = minArrayPtr;
				while (tempRand>=minArrayPtr && tempRand<=maxArrayPtr)
					tempRand = rand () % size;
				randLoc[i] = tempRand;
		}
		// 2. Allocate the InMemory Indexes
		long tempSize = VECTOR_SIZE*(float)(100.0-REMOTE_ACCESS_RATIO)/100.0;
		int *history = (int *)malloc (sizeof(int) * tempSize);
		for (i=0; i<tempSize; i++)
		{
			int temp;
			int found;
			do
			{
				temp = rand()%VECTOR_SIZE;
				found = 0;
				for (j=0; j<i; j++)
					if (history[j]==temp)
						found = 1;
			} while (found==1);
			history [i] = temp;
			randLoc[temp] =  (rand () % VECTOR_SIZE) + minArrayPtr;
		}
		free (history);
		for (i=0, j=0, k=0; i<VECTOR_SIZE; i++)
			if (randLoc[i]>=minArrayPtr && randLoc[i]<=maxArrayPtr)
				j++;											// Count number of InMemory accesses
			else
				k++;											// Count number of RemMemory accesses
		printf ("(Process %d) InMemory: %lld\tRemMemory : %lld (%.2f%%)\n", rank, j, k, (float)k*100.0/(float)VECTOR_SIZE);
		/***************************************************************************/
		
		MPI_Barrier (MPI_COMM_WORLD);							// Wait until all nodes have index generated and receive their share of vectors A and B (distributed storage)
		
		/***************************************************************************/
		/* Prepare RMA Ready Memory */
		/***************************************************************************/
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &ap);
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &bp);
		MPI_Alloc_mem(sizeof(int)*VECTOR_SIZE, MPI_INFO_NULL, &cp);
			
		MPI_Scatter(a, VECTOR_SIZE, MPI_INT, ap, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector A
		MPI_Scatter(b, VECTOR_SIZE, MPI_INT, bp, VECTOR_SIZE, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector B
		/***************************************************************************/


		/* Initialize MPI RMA */
		
		/* End */
		
		// Perform C = A + B for allocated elements
		MPI_Win_post(group, 0, win);	// Starts RMA
		resetTime(&timeC);
		
		for (k=0; k<VECTOR_SIZE; k++)
			// InMemory Access
			if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr)
			{
				i = randLoc[k]%VECTOR_SIZE;
				for (j=0; j<COMPLEXITY; j++)
					cp[i] = ap[i]+bp[i];
			}
			// RemMeory Access
			else
			{
				int remA, remB, remC;
				// Get A
				// Get B
				for(i=0;i<VECTOR_SIZE;i++)
					remC = remA + remB;
				// Put C
			}
		
		for (j=0; j<COMPLEXITY; j++)
			for(i=0;i<VECTOR_SIZE;i++)
				cp[i] = ap[i]+bp[i];
		computeTime = getTime(&timeC);
		
		MPI_Win_wait(win);	// Waits for MPI_Win_complete
		MPI_Gather(cp, VECTOR_SIZE, MPI_INT, c, VECTOR_SIZE, MPI_INT, MASTER, MPI_COMM_WORLD);	// Initiate gathering of vector C
		//MPI_Barrier(MPI_COMM_WORLD);
		
		MPI_Send (&computeTime, 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		
		free (ap);
		free (bp);
		free (cp);
	}
	MPI_Group_free(&group);
    MPI_Group_free(&comm_group);
    MPI_Win_free(&win); 
	MPI_Finalize ();
	//printf ("Process %d Finished\n", rank);

	return 0;
}
