#include "mpi.h" 
#include "stdio.h"
#include <stdlib.h>
 
/* tests put and get with post/start/complete/wait on 2 processes */
 
#define SIZE1 100
#define SIZE2 200
 
int main(int argc, char *argv[]) 
{
    int rank, rankInGroup, nprocs, *A, *B, i;
    MPI_Group comm_group, group;
    MPI_Win win;
    int errs = 0;
 
    MPI_Init(&argc,&argv); 
    MPI_Comm_size(MPI_COMM_WORLD,&nprocs); 
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	A = (int *) malloc(sizeof(int)*SIZE2);
	B = (int *) malloc(sizeof(int)*SIZE2);
	
    // MPI_Alloc_mem(SIZE2 * sizeof(int), MPI_INFO_NULL, &A);
    // MPI_Alloc_mem(SIZE2 * sizeof(int), MPI_INFO_NULL, &B);
    MPI_Comm_group(MPI_COMM_WORLD, &comm_group);
	
    if (rank == 0) 
	{
		rankInGroup = 1;
        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win); 
        MPI_Group_incl(comm_group, 1, &rankInGroup, &group);

        for (i=0; i<SIZE2; i++) 
			A[i] = B[i] = i;
        
        MPI_Win_start(group, 0, win);	// Only after MPI_Win_post
        for (i=0; i<SIZE1; i++)
            MPI_Put(A+i, 1, MPI_INT, 1, i, 1, MPI_INT, win); 
        for (i=0; i<SIZE1; i++)
            MPI_Get(B+i, 1, MPI_INT, 1, SIZE1+i, 1, MPI_INT, win);
 
        MPI_Win_complete(win);
 
        for (i=0; i<SIZE1; i++) 
            if (B[i] != (-4)*(i+SIZE1)) {
                printf("Get Error: B[i] is %d, should be %d\n", B[i], (-4)*(i+SIZE1));fflush(stdout);
                errs++;
            }
    }
    else
	{ /* rank=1 */
		rankInGroup = 0;
		MPI_Win_create(B, SIZE2*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
        MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
        for (i=0; i<SIZE2; i++) 
			B[i] = (-4)*i;
        MPI_Win_post(group, 0, win);	// Starts RMA
        MPI_Win_wait(win);	// Waits for MPI_Win_complete

        for (i=0; i<SIZE1; i++) {
            if (B[i] != i) {
                printf("Put Error: B[i] is %d, should be %d\n", B[i], i);fflush(stdout);
                errs++;
            }
        }
    }

	printf ("Done\n");
    MPI_Group_free(&group);
    MPI_Group_free(&comm_group);
    MPI_Win_free(&win); 
    // MPI_Free_mem(A);
    // MPI_Free_mem(B);
 
    MPI_Finalize();
    return errs; 
} 