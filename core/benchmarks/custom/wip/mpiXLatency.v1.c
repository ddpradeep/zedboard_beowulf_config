 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO 			0

#define MASTER 0
#define NOT_ASSIGNED -1

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int * A;										// vector A
	int * B;										// vector B
	int * C;										// vector A + B
	
	int *randLoc;
	
	int total_proc;									// total number of processes	
	int rank, rankInGroup;							// rank of each process
	int repeatCount = 1;
	int loopCounter=repeatCount;					// Repeats for averaging
	unsigned long size;
	long long int i, j, k;	// vector size
	unsigned long VECTOR_SIZE, COMPLEXITY, REMOTE_ACCESS_RATIO;						// elements per process	
	
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win winA, winB;

	/* Start Profiling */
	struct timeval timeT, timeC;
	float timeAcc = 0, computeTime=0;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank
	
    rankInGroup = rank;
    //MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
	int minArrayPtr, maxArrayPtr;

	/***************************************************************************/
	/* Initialize A, B and allocate C */
	/***************************************************************************/

	VECTOR_SIZE 		= atoi(argv[1]);
	COMPLEXITY 			= atoi(argv[2]);
	REMOTE_ACCESS_RATIO = atoi(argv[3]);

	if (rank == 1)
	{
		#if INFO==1
			printf ("Vector Size:\t%lu (%.2fKB)\tComplexity : %lu operations\t%lu%% Remote Mem Access\n", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, REMOTE_ACCESS_RATIO);
		#else
			printf ("%lu, %.2fKB, %lu, %f, ", total_proc * VECTOR_SIZE, (float)(sizeof(int)*total_proc * VECTOR_SIZE)/(1024.0), COMPLEXITY, (float)REMOTE_ACCESS_RATIO/100.0);
		#endif
	}

	size = total_proc * VECTOR_SIZE;
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &A);
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &B);
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &C);

	MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

	for(i=0;i<total_proc * VECTOR_SIZE;i++)
	{
		A[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
		B[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
	}

	minArrayPtr = rank*VECTOR_SIZE;							// The min index of local memory
	maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;				// The max index of local memory

	if (rank == MASTER) 
	{
		for(i=0;i<total_proc * VECTOR_SIZE;i++)
		{
			A[i] = i;		// Allocate NOT_ASSIGNED value
			B[i] = i;		// Allocate NOT_ASSIGNED value
		}

		rankInGroup = (rank+1)%2;
		MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
		MPI_Win_create(A, size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winA);
        MPI_Win_create(B, size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &winB);
        MPI_Win_post(group, 0, winA);	// Starts RMA
        MPI_Win_post(group, 0, winB);	// Starts RMA

        MPI_Win_wait(winA);	// Waits for MPI_Win_complete
        MPI_Win_wait(winB);	// Waits for MPI_Win_complete
    }
    else
	{
		rankInGroup = 0;
		MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
		MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winA); 
        MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &winB); 
        resetTime(&timeC);
        MPI_Win_start(group, 0, winA);	// Only after MPI_Win_post
        MPI_Win_start(group, 0, winB);	// Only after MPI_Win_post
        MPI_Get(A+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winA);
		MPI_Get(B+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winB);
		MPI_Win_complete(winA);
		MPI_Win_complete(winB);
		timeAcc = getTime(&timeC);						// Transfer latency
		#if INFO==1
			printf ("Transfered %lu (%.2fKB) elements in %f ms\n",  VECTOR_SIZE, (float)(sizeof(int)*VECTOR_SIZE)/(1024.0), computeTime*1000);
		#endif
	}
	/***************************************************************************/
	
	/***************************************************************************/
	/* Index Generator */
	/***************************************************************************/
	randLoc = (int *) malloc(sizeof(int)*VECTOR_SIZE);
	int tempRand;
	// 1. Allocate the RemMemory Indexes first
	for (i=0; i<VECTOR_SIZE; i++)
	{
			tempRand = minArrayPtr;
			while (tempRand>=minArrayPtr && tempRand<=maxArrayPtr)
				tempRand = rand () % size;
			randLoc[i] = tempRand;							// While not A local memory, assign it
	}
	// 2. Allocate the InMemory Indexes
	long tempSize = VECTOR_SIZE*(float)(100.0-REMOTE_ACCESS_RATIO)/100.0;
	int *history = (int *)malloc (sizeof(int) * tempSize);
	for (i=0; i<tempSize; i++)
	{
		int temp;
		int found;
		do
		{
			temp = rand()%VECTOR_SIZE;
			found = 0;
			for (j=0; j<i; j++)
				if (history[j]==temp)
					found = 1;
		} while (found==1);
		history [i] = temp;
		randLoc[temp] =  (rand () % VECTOR_SIZE) + minArrayPtr;
	}
	free (history);
	for (i=0, j=0, k=0; i<VECTOR_SIZE; i++)
		if (randLoc[i]>=minArrayPtr && randLoc[i]<=maxArrayPtr)
			j++;											// Count number of InMemory accesses
		else
			k++;											// Count number of RemMemory accesses

	//printf ("(Process-%d) InMemory: %lld\tRemMemory : %lld (%.2f%%)\n", rank, j, k, (float)k*100.0/(float)VECTOR_SIZE);
	/***************************************************************************/

	/***************************************************************************/
	/* Core Benchmarking */
	/***************************************************************************/
	if (rank == MASTER) 
	{
        MPI_Win_post(group, 0, winA);	// Starts RMA
        MPI_Win_post(group, 0, winB);	// Starts RMA

        MPI_Win_wait(winA);	// Waits for MPI_Win_complete
        MPI_Win_wait(winB);	// Waits for MPI_Win_complete
    }
    else
	{
        MPI_Win_start(group, 0, winA);	// Only after MPI_Win_post
        MPI_Win_start(group, 0, winB);	// Only after MPI_Win_post
        resetTime(&timeC);
		for (k=0; k<VECTOR_SIZE; k++)
			// InMemory Access
			if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr)
			{
				for (j=0; j<COMPLEXITY; j++)
					C[i] = A[i]+B[i];
			}
			// RemMeory Access
			else
			{
				MPI_Get(A+randLoc[k], 1, MPI_INT, MASTER, randLoc[k], 1, MPI_INT, winA);
				MPI_Get(B+randLoc[k], 1, MPI_INT, MASTER, randLoc[k], 1, MPI_INT, winB);
				
				for (j=0; j<COMPLEXITY; j++)
					C[i] = A[i]+B[i];
			}
		computeTime = getTime(&timeC);							// Get time spent on local and RMA processing
		resetTime(&timeC);
		for (k=0; k<VECTOR_SIZE; k++)
			if (randLoc[k]>=minArrayPtr && randLoc[k]<=maxArrayPtr);
		computeTime -= getTime(&timeC);
		resetTime(&timeC);
		MPI_Put(A+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winA);
		MPI_Put(B+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, winB);
		MPI_Win_complete(winA);
		MPI_Win_complete(winB);
		timeAcc += getTime(&timeC);							// Get time spent on local and RMA processing
		//printf ("Overhead: %f ms\n", getTime(&timeC)*1000);
		#if INFO==1
			printf ("Processing Time: %f ms\n", computeTime*1000);
		#else
			printf ("%f ms, %f ms\n", timeAcc*1000, computeTime*1000);
		#endif
    }
    /***************************************************************************/

	free (A);
	free (B);
	free (C);

	MPI_Group_free(&group);
    MPI_Group_free(&comm_group);
    MPI_Win_free(&winA);
    MPI_Win_free(&winB);
	MPI_Finalize ();

	return 0;
}
