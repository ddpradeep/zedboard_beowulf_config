/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"

#define INFO 			0
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT

using namespace std;

atomic<bool> threadAlive(true);						// flag to denote send/receive thread status

void sendFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);
void recvFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(1);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *data;								// vector data
	unsigned long VECTOR_SIZE;						// vector length
	unsigned long size;								// data vector length after multiplication
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gTime, *pTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// get current process rank
	/***********************************************************************************************/


	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	VECTOR_SIZE 		= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);
	WORK_DURING_TRANSFER= atoi(argv[3]);
	WORK_AMOUNT 		= (unsigned long)VECTOR_SIZE*(unsigned long)(REPEAT_COUNT*100);

	gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	if (rank == MASTER)
	{
		#if INFO == 1
			printf ("Vector Size:\t%lu (%.2fKB)\tRepeat Count: %lu\n", VECTOR_SIZE, (float)(sizeof(DATA_TYPE)* VECTOR_SIZE)/(1024.0), REPEAT_COUNT);
		#else
			printf ("%lu, %.2fKB, %d, ", VECTOR_SIZE, (float)(sizeof(DATA_TYPE) * VECTOR_SIZE)/(1024.0), WORK_DURING_TRANSFER);
		#endif
	}

	// 2. Allocate memory to vector
    size = VECTOR_SIZE;
    MPI_Alloc_mem(size * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);

    // 3. Allocate a distinguishing value for master and slave
	if (rank%2 == MASTER) 
    {
        for(unsigned long i=0;i<size;i++)
            data[i] = ASSIGNED;						// allocate ASSIGNED value
    }
    else
    {
    	for(unsigned long i=0;i<size;i++)
	        data[i] = NOT_ASSIGNED;					// allocate NOT_ASSIGNED value
    }

    /***********************************************************************************************/


    /***********************************************************************************************/
    /*                                    Send/Receive Threads
    /***********************************************************************************************/
    int sum = 0;
    if (rank%2 == MASTER)
    {
    	thread sendThread(sendFunction, data, size, MPI_COMM_WORLD, rank, REPEAT_COUNT, pTime);
    	while (WORK_DURING_TRANSFER && threadAlive)
    	{
	    	for (unsigned long i=0; i<WORK_AMOUNT; i++)
	    		sum += 1;
	    }
	    #if INFO == 1
	    	printf ("[MASTER]\tSUM-S\t-> COMPLETE\t%d\n", sum);
	    #endif
    	sendThread.join();
    }
    else
    {
    	thread recvThread(recvFunction, data, size, MPI_COMM_WORLD, rank, REPEAT_COUNT, gTime);
    	while (WORK_DURING_TRANSFER && threadAlive)
    	{
	    	for (unsigned long i=0; i<WORK_AMOUNT; i++)
	    		sum += 1;
	    }
	    #if INFO == 1
	    	printf ("[SLAVE]\t\tSUM-R\t-> COMPLETE\t%d\n", sum);
	    #endif
    	recvThread.join();
    }
    /***********************************************************************************************/

    MPI_Barrier(MPI_COMM_WORLD);


    /***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

    // 1. Get gTime from Slave
    if (rank%2 == MASTER)
    	MPI_Recv (gTime, REPEAT_COUNT, MPI_FLOAT, rank+1, ADMIN_MSG, MPI_COMM_WORLD, &status);
    else
    	MPI_Send (gTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_MSG, MPI_COMM_WORLD);

	if (rank == MASTER)
    {
	    // 2. Sort the recorded timings
	    sort (pTime, gTime, REPEAT_COUNT);

	    // 3. Get avg of each time
	    float putTimeAvg = pTime[REPEAT_COUNT/2];
	    float getTimeAvg = gTime[REPEAT_COUNT/2];

	    float worstTimeAvg	= max (putTimeAvg, getTimeAvg);
		#if INFO == 1
		    printf ("Act Speed: %.2fMB/s (%f ms)\n",
		    	(float)(sizeof(DATA_TYPE)*VECTOR_SIZE)/(worstTimeAvg*1048576.0), 
		    	worstTimeAvg*1000.0);
		#else
		    printf ("%f ms, %.2f MB/s\n", worstTimeAvg*1000.0, (float)(sizeof(DATA_TYPE)*VECTOR_SIZE)/(worstTimeAvg*1048576.0));
		#endif
	}

    /***********************************************************************************************/

    free (data);
    MPI_Finalize ();

    return 0;
}

void sendFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier (MPI_COMMUNICATOR);

        resetTime(&time1);
        MPI_Send (data, size, MPI_DATA_TYPE, rank+1, rank, MPI_COMM_WORLD);                             
        timer[i] = getTime(&time1);
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    #if INFO == 1
    	printf ("[MASTER]\tSend\t-> [COMPLETED]\n");
    #endif
    threadAlive = false;
}

void recvFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	MPI_Status status;
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
	{
		MPI_Barrier (MPI_COMMUNICATOR);

		resetTime(&time1);
        MPI_Recv (data, size, MPI_DATA_TYPE, rank-1, 0, MPI_COMM_WORLD, &status);
        timer[i] = getTime(&time1);

        // Verify integrity of received data
        for (unsigned long j=0; j<size; j++)
        {
            if (data[j] != ASSIGNED)
                printf ("GET: Data Transfer Error! Expected %.1f, received %.1f\n\n", (float)j, (float)data[j]);
            
            data[j] = NOT_ASSIGNED;
        }
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    #if INFO == 1
    	printf ("[SLAVE]\t\tRecv\t-> [COMPLETED]\n");
    #endif
    threadAlive = false;
}
