/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		unsigned char
#define MPI_DATA_TYPE	MPI_UNSIGNED_CHAR
// #define DEBUG
// #define INFO 			
// #define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"
#include "helper-utils.h"

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(1);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *data;								// vector data
	unsigned long BLOCK_SIZE;						// data vector length after multiplication
	unsigned long TRANSFER_COUNT;					// number of sequential transfers of BLOCK_SIZE [BLOCK_SIZE]
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gTime, *pTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// get current process rank
	/***********************************************************************************************/

	/***************************************************************************/
	/* Initialize */
	/***************************************************************************/

	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 5)
	{
		printf ("%d \n", argc);
		printf ("Usage: <BLOCK_SIZE> <TRANSFER_COUNT> <REPEAT_COUNT> <WORK_DURING_TRANSFER[N/A]>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	BLOCK_SIZE 			= atoi(argv[1]);
	TRANSFER_COUNT 		= atoi(argv[2]);
	REPEAT_COUNT 		= atoi(argv[3]);
	WORK_DURING_TRANSFER= atoi(argv[4]);
	// WORK_AMOUNT 		= (unsigned long)BLOCK_SIZE*(unsigned long)(REPEAT_COUNT*100);

	gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	if (rank == MASTER)
	{
		printb ("Block Size:\t%lu (%.2fKB)\tTransfers:\t%lu\tRepeat Count: %lu\n", BLOCK_SIZE, (float)(sizeof(DATA_TYPE)* BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, REPEAT_COUNT);
		printc ("async, %lu, %.2fKB, x %lu, %d, ", BLOCK_SIZE*sizeof(DATA_TYPE), (float)(sizeof(DATA_TYPE) * BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, WORK_DURING_TRANSFER);
	}

	// 2. Allocate memory to vector
    MPI_Alloc_mem(BLOCK_SIZE * 2 * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
    MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

	// 3. Allocate a distinguishing value for master and slave
	if (rank%2 == MASTER) 
    {
        for(long i=0;i<BLOCK_SIZE;i++)
            data[i] = NOT_ASSIGNED;						// allocate NOT_ASSIGNED value

        MPI_Win_create(data, BLOCK_SIZE*sizeof(DATA_TYPE)*2, sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    }
    else
    {
    	for(unsigned long i=0;i<BLOCK_SIZE;i++)
	        data[i] = i;					// allocate ASSIGNED value

	    MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win); 
    }

	int minArrayPtr = 0; //rank*BLOCK_SIZE;					// The min index of local memory

    /***************************************************************************/

	/***********************************************************************************************/
    /*                                    Send/Receive Threads
    /***********************************************************************************************/

    // MPI_Barrier (MPI_COMM_WORLD);

    if (rank == MASTER)
    {
    	for (int i=0; i<REPEAT_COUNT; i++)
		{
			usleep (1000000);

			// Check if data received before calling synchronization
			printd ("Verifying before sync\n");
			int mismatch = false;
			for (int j=BLOCK_SIZE-1; j>=0; j--)
			{
				if (data[j] != (DATA_TYPE)j)
				{
					printd ("%d) [PRE_SYNC] data[%d]\tExpected %d, Received %d\n", i, j, j, (int)data[j]);
					printb ("Data transfer starts only after the sync call\n");
					printc ("SYNC_INITIATES_TRANSFER, ");
					mismatch = true;
					break;
				}
				data[j] = NOT_ASSIGNED;
			}
			if (!mismatch)
			{
				printd ("Data transfer starts from the put call\n");
				printc ("PUT_INITIATES_TRANSFER, ");
			}
				

    		MPI_Barrier (MPI_COMM_WORLD);				// Wait for data to be sent from the sending node
			printd ("Verifying after sync\n");
			for (int j=BLOCK_SIZE-1; j>=0; j--)
			{
				if (data[j] != (DATA_TYPE)j)
				{
					printf ("%d) [POST SYNC] data[%d]\tExpected %d, Received %d\n", i, j, j, (int)data[j]);
					break;
				}
				data[j] = NOT_ASSIGNED;
			}
			MPI_Barrier (MPI_COMM_WORLD);				// Hold up the sending node until verification is done here
		}
    }
    else
    {
    	struct timeval time1, time2;
		for (int i=0; i<REPEAT_COUNT; i++)
		{
			resetTime(&time1);
			MPI_Win_lock(MPI_LOCK_SHARED, MASTER, MPI_MODE_NOCHECK, win);
			for (unsigned long j=0; j<TRANSFER_COUNT; j++)
				MPI_Put(data+minArrayPtr, BLOCK_SIZE, MPI_DATA_TYPE, MASTER, minArrayPtr, BLOCK_SIZE, MPI_DATA_TYPE, win);
			
			usleep (2000000);

			resetTime(&time2);
			MPI_Win_unlock(MASTER, win);
			float sync_overhead = getTime(&time2);

			pTime [i] = getTime(&time1) - 2.0;				// Get time spent on local and RMA processing
			printf ("Put+Sync: %f ms (%.2fMB/s)\tSync: %f ms (%.2fMB/s)\n", 
				pTime[i]*1000, 
				(float)(sizeof(DATA_TYPE)*BLOCK_SIZE*TRANSFER_COUNT)/(pTime[i]*1048576.0),
				sync_overhead*1000,
				(float)(sizeof(DATA_TYPE)*BLOCK_SIZE*TRANSFER_COUNT)/(sync_overhead*1048576.0));
			printd ("\n%d/%lu)  %f ms", i, REPEAT_COUNT, pTime[i]*1000);
			MPI_Barrier (MPI_COMM_WORLD);				// Signal to the receiving node that sending in complete
			MPI_Barrier (MPI_COMM_WORLD);				// Wait until verification is done at the receiving node
		}
	}
	/***************************************************************************/

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Print Results */
	/***************************************************************************/

	// 1. Get gTime from Slave
    if (rank%2 == MASTER)
    	MPI_Recv (pTime, REPEAT_COUNT, MPI_FLOAT, rank+1, ADMIN_TAG, MPI_COMM_WORLD, &status);
    else
    	MPI_Send (pTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);

	if (rank == MASTER)
    {
	    // 2. Sort the recorded timings
	    sort (pTime, REPEAT_COUNT);

	    // 3. Get avg of each time
	    float putTimeAvg = pTime[REPEAT_COUNT/2];

	    BLOCK_SIZE *= TRANSFER_COUNT;
	    
	    float worstTimeAvg	= putTimeAvg;

		printb ("Act Speed: %.2fMB/s (%f ms)\n",
		    	(float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0), 
		    	worstTimeAvg*1000.0);
		printc ("%f ms, %.2f MB/s\n", worstTimeAvg*1000.0, (float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0));
	}

    /***************************************************************************/

	free (data);

    MPI_Group_free(&comm_group);
    MPI_Win_free(&win);
    MPI_Finalize ();

	return 0;
}
