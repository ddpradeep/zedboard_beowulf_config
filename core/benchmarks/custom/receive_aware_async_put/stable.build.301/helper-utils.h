#ifndef HELPER_UTIL
#define HELPER_UTIL

#include "sys/time.h"
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdarg.h>
#include <cstring>
#include <mutex>

#define MASTER 				0
#define ADMIN_TAG 			101
#define DATA_TAG 			102
#define PRIMARY_CORE 		0
#define SECONDARY_CORE 		1
#define SEND_RECV_DELAY		150000		// us delay max (assumed)
#define NOT_ASSIGNED 		-1
#define ASSIGNED 			2
#define PING_DELAY        200       // us delay avg (measured)

std::mutex printMutex;

void inline resetTime(struct timeval *timer);
float inline getTime(struct timeval *time1);
void sort(float values[], unsigned long size);
void sort(float values1[], float values2[], unsigned long size);

void sort (float values[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size; j++)
            if (values[j]<values[j+1])
            {
                float temp = values[j];
                values[j] = values[j+1];
                values[j+1] = temp;
            }
}

void sort (float values1[], float values2[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size-1; j++)
            if ((values1[j]+values2[j]) < (values1[j+1]+values2[j+1]))
            {
                float temp = values1[j];
                values1[j] = values1[j+1];
                values1[j+1] = temp;

                temp = values2[j];
                values2[j] = values2[j+1];
                values2[j+1] = temp;
            }

 //    printf ("\n");
	// for (unsigned long i = 0; i<size; i++)
	// 	printf ("%.3f\t%.3f\t%.3f\n", values1[i]*1000.0, values2[i]*1000.0, (values1[i]+values2[i])*1000.0);
	// printf ("\n");
}

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int stick_this_thread_to_core(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores)
      return EINVAL;

   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);

   pthread_t current_thread = pthread_self();    
   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

static struct termios old, new_;

/* Initialize new_ terminal i/o settings */
void initTermios(int echo) 
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  new_ = old; /* make new_ settings same as old settings */
  new_.c_lflag &= ~ICANON; /* disable buffered i/o */
  new_.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new_); /* use these new_ terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return getch_(1);
}
 
void printd(const char* format, ...)
{
  #ifdef DEBUG
    char *new_format ;
    char const *prefix = "[DBG] \t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printi(const char* format, ...)
{
  #ifdef INFO
    char *new_format ;
    char const *prefix = "[INFO]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printb(const char* format, ...)
{
  #ifdef BENCHMARK
    char *new_format ;
    char const *prefix = "[RSLT]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printc(const char* format, ...)
{
  #ifdef BATCH_MODE
    char *new_format ;
    char const *prefix = "";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

#endif