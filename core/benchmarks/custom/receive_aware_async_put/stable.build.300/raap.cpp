/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
// #define DEBUG
// #define INFO 		
#define BENCHMARK
// #define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"
#include "AsyncSend.h"
#include "AsyncRecv.h"
#include <future>
#include <chrono>

using namespace std;

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(PRIMARY_CORE);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *data;								// vector data
	unsigned long BLOCK_SIZE;						// data vector length after multiplication
	unsigned long TRANSFER_COUNT;					// number of sequential transfers of size [BLOCK_SIZE]
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gTime, *pTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	// MPI_Init (&argc, &argv);						// initialization of MPI environment

	int provided;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// get current process rank
	
	if (provided != MPI_THREAD_MULTIPLE)
	{
	    printf("[%d][WARN]\tThis MPI implementation does not support multiple threads\n", rank);
	    // MPI_Abort(MPI_COMM_WORLD, 1);
	}

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	printi ("Process %d running on %s\n", rank, processor_name);
	/***********************************************************************************************/


	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 5)
	{
		printf ("%d \n", argc);
		printf ("Usage: <BLOCK_SIZE> <TRANSFER_COUNT> <REPEAT_COUNT> <WORK_DURING_TRANSFER[N/A]>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	BLOCK_SIZE 			= atoi(argv[1]);
	TRANSFER_COUNT 		= atoi(argv[2]);
	REPEAT_COUNT 		= atoi(argv[3]);
	WORK_DURING_TRANSFER= atoi(argv[4]);
	// WORK_AMOUNT 		= (unsigned long)BLOCK_SIZE*(unsigned long)(REPEAT_COUNT*100);

	// gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	if (rank == MASTER)
	{
		printb ("Block Size:\t%lu (%.2fKB)\tTransfers:\t%lu\tRepeat Count: %lu\n", BLOCK_SIZE, (float)(sizeof(DATA_TYPE)* BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, REPEAT_COUNT);
		printc ("%lu, %.2fKB, x %lu, %d, ", BLOCK_SIZE, (float)(sizeof(DATA_TYPE) * BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, WORK_DURING_TRANSFER);
	}

	// 2. Allocate memory to vector
    // MPI_Alloc_mem(BLOCK_SIZE * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
    data = (DATA_TYPE*) malloc (BLOCK_SIZE * sizeof(DATA_TYPE));

    // 3. Allocate a distinguishing value for master and slave
	if (rank%2 == MASTER) 
    {
        for(unsigned long i=0;i<BLOCK_SIZE;i++)
            data[i] = ASSIGNED;						// allocate ASSIGNED value
    }
    else
    {
    	for(unsigned long i=0;i<BLOCK_SIZE;i++)
	        data[i] = NOT_ASSIGNED;					// allocate NOT_ASSIGNED value
    }

    /***********************************************************************************************/

    async_recv receiver (MPI_COMM_WORLD, 5, BLOCK_SIZE);
    async_send sender	(MPI_COMM_WORLD, 5);

    /***********************************************************************************************/
    /*                                    Send/Receive Threads
    /***********************************************************************************************/

    if (rank == MASTER)
    {
    	;	// Do nothing
    }
    else
    {
    	struct timeval time1;
    	for (long i=0; i<REPEAT_COUNT; i++)
    	{
    		resetTime(&time1);
	    	for (unsigned long j=0; j<TRANSFER_COUNT; j++)
	    		sender.ISend(data, BLOCK_SIZE, MASTER);

	    	// printf ("%lu/%lu)  Initiated\n", i, REPEAT_COUNT);
	    	sender.synchronize();
	    	pTime[i] = getTime(&time1);
	    	// printf ("%lu/%lu)  %f ms\n", i, REPEAT_COUNT, pTime[i]*1000);
	    }
    }

    sender.close();
    receiver.close();
    printi ("[%d] Gracefully exited communicator\n", rank);
    /***********************************************************************************************/

    MPI_Barrier(MPI_COMM_WORLD);

    /***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

    // 1. Get gTime from Slave
    if (rank%2 == MASTER)
    	MPI_Recv (pTime, REPEAT_COUNT, MPI_FLOAT, 1, ADMIN_TAG, MPI_COMM_WORLD, &status);
    else
    	MPI_Send (pTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);

	if (rank == MASTER)
    {
	    // 2. Sort the recorded timings
	    sort (pTime, REPEAT_COUNT);

	    // 3. Get avg of each time
	    float putTimeAvg = pTime[REPEAT_COUNT/2];

	    BLOCK_SIZE *= TRANSFER_COUNT;
	    
	    float worstTimeAvg	= putTimeAvg;
		printb ("Act Speed: %.2fMB/s (%f ms)\n",
		    	(float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0), 
		    	worstTimeAvg*1000.0);
		printc ("%f ms, %.2f MB/s\n", worstTimeAvg*1000.0, (float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0));
	}

    free (data);

    MPI_Finalize ();
    return 0;
}