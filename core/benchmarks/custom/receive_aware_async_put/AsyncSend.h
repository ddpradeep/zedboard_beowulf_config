#include "mpi.h"
#include <atomic>
#include <thread>
#include <mutex>
#include <list>
#include "helper-utils.h"

using namespace std;

#define DEF_TAG		1

class async_send 
{
    unsigned int 				qLength;
    MPI_Comm 					MPI_COMM;
    list<unsigned int> 			availHandles;
    MPI_Request 				*reqHandles;
    int 						rank;

    atomic<bool>				keepThreadsAlive;						// flag to denote send/receive thread status
    mutex						handleMutex;
    thread 						*updateAvailHandlesThread;

    int getNextAvailableHandleID();
    void waitTillAnyHandleAvailable();
    void updateAvailHandles();

  public:
    async_send (MPI_Comm MPI_COMM, unsigned int qLength);
    int ISend (DATA_TYPE *data, unsigned long size, int destination);
    void synchronize();
    void close();

    void _debug_printAvailHandles();
};

async_send::async_send (MPI_Comm MPI_COMM, unsigned int qLength)
{
  this->MPI_COMM 	= MPI_COMM;
  this->qLength 	= qLength;

  reqHandles		= new MPI_Request [qLength];

  for (unsigned int i=0; i<qLength; i++)
  {
  	reqHandles[i] 	= MPI_REQUEST_NULL;
  	availHandles.push_back (i);
  }

  MPI_Comm_rank (MPI_COMM,&rank);										// get current process rank

  keepThreadsAlive	= true;

  updateAvailHandlesThread = new thread(&async_send::updateAvailHandles, this);
}

int async_send::ISend (DATA_TYPE *buf, unsigned long size, int destination)
{
	unsigned int id = getNextAvailableHandleID();

	MPI_Isend(buf, size, MPI_DATA_TYPE, destination, DATA_TAG, MPI_COMM, &reqHandles[id]);

	printd ("[%d]\tSending %lu items to node %d\t- S-ID[%d]\n", rank, size, destination, id);
}

int async_send::getNextAvailableHandleID()
{
	while (availHandles.empty())
	{
		printd ("[%d]\tS-Waiting\n", rank);
		usleep(200);
	}

	handleMutex.lock();
	unsigned int id = availHandles.front();
	// printd ("Used [%d]\t", id);
	availHandles.pop_front();
	handleMutex.unlock();

	return id;
}

void async_send::updateAvailHandles()
{
	stick_this_thread_to_core(PRIMARY_CORE);
	printd ("[%d]\tSender Thread 1 Running\n", rank);
	int freedIndex = 1;

	do
	{
		MPI_Waitany(qLength, reqHandles, &freedIndex, MPI_STATUS_IGNORE);
		if (freedIndex != MPI_UNDEFINED)
		{
			handleMutex.lock();
			availHandles.push_back(freedIndex);
			handleMutex.unlock();
			printd ("[%d]\tFreed S-ID[%d]\n", rank, freedIndex);
			printi ("[%d]\tSending Complete\t\t+ S-ID[%d]\n", rank, freedIndex);
		}
	} while (keepThreadsAlive||(freedIndex!=MPI_UNDEFINED));

	printd ("[%d]\tSender Thread 1 Exited\n", rank);
	keepThreadsAlive = true;
}

void async_send::synchronize()
{
	while(availHandles.size() != qLength)
		usleep(5);
	// MPI_Waitall(qLength, reqHandles, MPI_STATUSES_IGNORE);
}

void async_send::close()
{
	printd ("[%d]\tSender Termination Initiated\n", rank);
	// Close updateAvailHandlesThread
	keepThreadsAlive = false;
	while (!keepThreadsAlive)
		;

	usleep (SEND_RECV_DELAY);
	printd ("[%d]\tSender Terminated\n", rank);

	MPI_Barrier(MPI_COMM);
}

void async_send::_debug_printAvailHandles()
{
	printd ("[%d]\tAvailHandle: ", rank);
	for (list<unsigned int>::const_iterator id = availHandles.begin(); id != availHandles.end(); ++id)
        printd ("%u, ", *id);
    printd ("\n");
}