#include "mpi.h"
#include <atomic>
#include <thread>
#include <mutex>
#include <list>
#include "helper-utils.h"

using namespace std;

#define DEF_TAG		1

class async_recv 
{
    unsigned int 				qLength;
    unsigned int 				defBufLength;
    MPI_Comm 					MPI_COMM;
    list<unsigned int> 			availHandles;
    MPI_Request 				*reqHandles;
    DATA_TYPE 					**recvBuffers;
    int 						rank;

    atomic<bool>				keepThread1Alive;						// flag to denote send/receive thread status
    atomic<bool>				keepThread2Alive;						// flag to denote send/receive thread status
    mutex						handleMutex;

    thread 						*updateAvailHandlesThread;
    thread 						*handleIncomingMessagesThread;

    int getNextAvailableHandleID();
    void waitTillAnyHandleAvailable();
    void updateAvailHandles();
    void handleIncomingMessages();

  public:
    async_recv (MPI_Comm MPI_COMM, unsigned int qLength, unsigned int defBufLength);
    void close();

    void _debug_printAvailHandles();
};

async_recv::async_recv (MPI_Comm MPI_COMM, unsigned int qLength, unsigned int defBufLength)
{
  this->MPI_COMM 	= MPI_COMM;
  this->qLength 	= qLength;
  this->defBufLength= defBufLength;

  reqHandles		= new MPI_Request [qLength];
  recvBuffers 		= new DATA_TYPE*  [qLength];

  for (unsigned int i=0; i<qLength; i++)
  {
  	availHandles.push_back (i);
  	reqHandles[i] 	= MPI_REQUEST_NULL;
  	recvBuffers[i]	= new DATA_TYPE [defBufLength];
  }

  MPI_Comm_rank (MPI_COMM,&rank);										// get current process rank

  keepThread1Alive	= true;
  keepThread2Alive 	= true;

  handleIncomingMessagesThread	= new thread(&async_recv::handleIncomingMessages, this);
  updateAvailHandlesThread 		= new thread(&async_recv::updateAvailHandles, this);
}

int async_recv::getNextAvailableHandleID()
{
	while (availHandles.empty())
	{
		printf ("Waiting");
		usleep(1000);
	}

	handleMutex.lock();
	unsigned int id = availHandles.front();
	printf ("[%d]Used [%d]\t", rank, id);
	availHandles.pop_front();
	handleMutex.unlock();

	return id;
}

void async_recv::handleIncomingMessages()
{
	// stick_this_thread_to_core(SECONDARY_CORE);
	MPI_Status status;
	int recvCount;
	unsigned int id = 0;
	int inboundMessage;
	printf ("[%d]Thread 1 Running\n", rank);
	while (true)
	{
		// MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM, &status);
		do
    	{
    		if (!keepThread1Alive)
    		{
    			keepThread1Alive = true;
    			printf ("[%d]Thread 1 Exited\n", rank);
    			return;
    		}
    		MPI_Iprobe(MPI_ANY_SOURCE, DATA_TAG, MPI_COMM, &inboundMessage, &status);
    	} while (!inboundMessage);

	    MPI_Get_count(&status, MPI_DATA_TYPE, &recvCount);
	    id = getNextAvailableHandleID();
	    if (recvCount > defBufLength)
	    {
	    	delete [] recvBuffers[id];
	    	recvBuffers[id]			= new DATA_TYPE [recvCount];
	    	// printf ("Recreated buffer with size: %d\n", recvCount);
	    }

	    printf ("Receiving %d items from node %d\n", recvCount, status.MPI_SOURCE);
		MPI_Irecv(recvBuffers[id], recvCount, MPI_DATA_TYPE, status.MPI_SOURCE, DATA_TAG, MPI_COMM, &reqHandles[id]);
	}
}

void async_recv::updateAvailHandles()
{
	// stick_this_thread_to_core(SECONDARY_CORE);
	printf ("[%d]Thread 2 Running\n", rank);
	MPI_Status status;
	int recvCount;
	int freedIndex = MPI_UNDEFINED;
	do
	{
		MPI_Waitany(qLength, reqHandles, &freedIndex, &status);
		// printf ("Once\n");
		if (freedIndex != MPI_UNDEFINED)
		{
			printf ("Found %d\n", freedIndex);
			MPI_Get_count(&status, MPI_DATA_TYPE, &recvCount);
			handleMutex.lock();
			availHandles.push_back(freedIndex);
			handleMutex.unlock();

			printf ("[%d]Freed [%d]\tReceived %d items from node %d\n", rank, freedIndex, recvCount, status.MPI_SOURCE);
		}
	} while (keepThread2Alive||(freedIndex!=MPI_UNDEFINED));

	printf ("[%d]Thread 2 Exited\n", rank);
	keepThread2Alive = true;
}

void async_recv::close()
{
	printf ("[%d][INFO]\tReceiver Termination Initiated\n", rank);
	MPI_Barrier(MPI_COMM);

	usleep (SEND_RECV_DELAY);
	printf ("[%d]Receiver Termination Started\n", rank);

	// Close handleIncomingMessagesThread
	keepThread1Alive = false;
	while (!keepThread1Alive)
		;

	printf ("[%d]Stage 1\n", rank);
	// Close updateAvailHandlesThread
	keepThread2Alive = false;
	while (!keepThread2Alive)
		;
	printf ("[%d]Stage 2\n", rank);

	// Delete temp buffers
	for (unsigned int i=0; i<qLength; i++)
	{
		delete [] recvBuffers[i];
		// printf ("[%d]- %d cleared\n", rank, i);
	}
	// delete [] recvBuffers;

	printf ("[%d][INFO]\tReceiver Terminated\n", rank);
}

void async_recv::_debug_printAvailHandles()
{
	printf ("[%d]AvailHandle: ", rank);
	for (list<unsigned int>::const_iterator id = availHandles.begin(); id != availHandles.end(); ++id)
        printf ("%u, ", *id);
    printf ("\n");
}