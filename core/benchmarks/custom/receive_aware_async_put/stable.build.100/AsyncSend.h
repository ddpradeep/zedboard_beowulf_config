#include "mpi.h"
#include <atomic>
#include <thread>
#include <mutex>
#include <list>
#include "helper-utils.h"

using namespace std;

#define DEF_TAG		1

class async_send 
{
    unsigned int 				qLength;
    MPI_Comm 					MPI_COMM;
    list<unsigned int> 			availHandles;
    MPI_Request 				*reqHandles;
    int 						rank;

    atomic<bool>				keepThreadsAlive;						// flag to denote send/receive thread status
    mutex						handleMutex;
    thread 						*updateAvailHandlesThread;

    int getNextAvailableHandleID();
    void waitTillAnyHandleAvailable();
    void updateAvailHandles();

  public:
    async_send (MPI_Comm MPI_COMM, unsigned int qLength);
    int ISend (DATA_TYPE *data, unsigned long size, int destination);
    void close();

    void _debug_printAvailHandles();
};

async_send::async_send (MPI_Comm MPI_COMM, unsigned int qLength)
{
  this->MPI_COMM 	= MPI_COMM;
  this->qLength 	= qLength;

  reqHandles		= new MPI_Request [qLength];

  for (unsigned int i=0; i<qLength; i++)
  {
  	reqHandles[i] 	= MPI_REQUEST_NULL;
  	availHandles.push_back (i);
  }

  MPI_Comm_rank (MPI_COMM,&rank);										// get current process rank

  keepThreadsAlive	= true;

  updateAvailHandlesThread = new thread(&async_send::updateAvailHandles, this);
}

int async_send::ISend (DATA_TYPE *buf, unsigned long size, int destination)
{
	unsigned int id = getNextAvailableHandleID();

	MPI_Isend(buf, size, MPI_DATA_TYPE, destination, DATA_TAG, MPI_COMM, &reqHandles[id]);

	printf ("Sent %lu items to node %d\n", size, destination);
}

int async_send::getNextAvailableHandleID()
{
	while (availHandles.empty())
	{
		// printf ("Waiting");
		usleep(1000);
	}

	handleMutex.lock();
	unsigned int id = availHandles.front();
	// printf ("Used [%d]\t", id);
	availHandles.pop_front();
	handleMutex.unlock();

	return id;
}

void async_send::updateAvailHandles()
{
	stick_this_thread_to_core(SECONDARY_CORE);
	int freedIndex = 1;

	do
	{
		MPI_Waitany(qLength, reqHandles, &freedIndex, MPI_STATUS_IGNORE);
		if (freedIndex != MPI_UNDEFINED)
		{
			handleMutex.lock();
			availHandles.push_back(freedIndex);
			handleMutex.unlock();
			// printf ("Freed [%d]\n", freedIndex);
		}
	} while (keepThreadsAlive||(freedIndex!=MPI_UNDEFINED));

	keepThreadsAlive = true;
}

void async_send::close()
{
	printf ("[%d][INFO]\tSender Termination Initiated\n", rank);
	// Close updateAvailHandlesThread
	keepThreadsAlive = false;
	while (!keepThreadsAlive)
		;
	
	usleep (SEND_RECV_DELAY);
	printf ("[%d][INFO]\tSender Terminated\n", rank);

	MPI_Barrier(MPI_COMM);
}

void async_send::_debug_printAvailHandles()
{
	printf ("AvailHandle: ");
	for (list<unsigned int>::const_iterator id = availHandles.begin(); id != availHandles.end(); ++id)
        printf ("%u, ", *id);
    printf ("\n");
}