/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
#define DEBUG
#define INFO 			1
#define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"
#include "AsyncSend.h"
#include "AsyncRecv.h"
#include <future>
#include <chrono>

using namespace std;

atomic<bool> threadAlive(true);						// flag to denote send/receive thread status

void sendFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);
void recvFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(PRIMARY_CORE);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *data;								// vector data
	unsigned long VECTOR_SIZE;						// vector length
	unsigned long size;								// data vector length after multiplication
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gTime, *pTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	// MPI_Init (&argc, &argv);						// initialization of MPI environment

	int provided;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// get current process rank
	
	if (provided != MPI_THREAD_MULTIPLE)
	{
	    printf("[%d][WARN]\tThis MPI implementation does not support multiple threads\n", rank);
	    // MPI_Abort(MPI_COMM_WORLD, 1);
	}

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	printi ("Process %d running on %s\n", rank, processor_name);
	/***********************************************************************************************/


	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	VECTOR_SIZE 		= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);
	WORK_DURING_TRANSFER= atoi(argv[3]);
	WORK_AMOUNT 		= (unsigned long)VECTOR_SIZE*(unsigned long)(REPEAT_COUNT*100);

	gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	if (rank == MASTER)
	{
		printb("Vector Size:\t%lu (%.2fKB)\tRepeat Count: %lu\n", VECTOR_SIZE, (float)(sizeof(DATA_TYPE)* VECTOR_SIZE)/(1024.0), REPEAT_COUNT);
		printc("%lu, %.2fKB, %d, ", VECTOR_SIZE, (float)(sizeof(DATA_TYPE) * VECTOR_SIZE)/(1024.0), WORK_DURING_TRANSFER);
	}

	// 2. Allocate memory to vector
    size = VECTOR_SIZE;
    // MPI_Alloc_mem(size * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
    data = (DATA_TYPE*) malloc (size * sizeof(DATA_TYPE));

    // 3. Allocate a distinguishing value for master and slave
	if (rank%2 == MASTER) 
    {
        for(unsigned long i=0;i<size;i++)
            data[i] = ASSIGNED;						// allocate ASSIGNED value
    }
    else
    {
    	for(unsigned long i=0;i<size;i++)
	        data[i] = NOT_ASSIGNED;					// allocate NOT_ASSIGNED value
    }

    /***********************************************************************************************/

    async_recv receiver (MPI_COMM_WORLD, 5, 20000);
    async_send sender	(MPI_COMM_WORLD, 5);

    /***********************************************************************************************/
    /*                                    Send/Receive Threads
    /***********************************************************************************************/

    if (rank == MASTER)
    {
    	sender.ISend(data, 1, 1);

    	sender.ISend(data, 200000, 1);

    }
    else
    {
    	sender.ISend(data, 100000, MASTER);

    	sender.ISend(data, 200000, MASTER);

    	sender.ISend(data, 50000, MASTER);

    	sender.ISend(data, 3, MASTER);
    }

    sender.close();
    receiver.close();
    printi ("[%d] Gracefully exited communicator\n", rank);
    /***********************************************************************************************/

    free (data);

    MPI_Finalize ();
    return 0;
}

void sendFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier (MPI_COMMUNICATOR);

        resetTime(&time1);
        MPI_Send (data, size, MPI_DATA_TYPE, rank+1, rank, MPI_COMM_WORLD);                             
        timer[i] = getTime(&time1);
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    #if INFO == 1
    	printf ("[MASTER]\tSend\t-> [COMPLETED]\n");
    #endif
    threadAlive = false;
}

void recvFunction(DATA_TYPE data[], unsigned long size, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	MPI_Status status;
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
	{
		MPI_Barrier (MPI_COMMUNICATOR);

		resetTime(&time1);
        MPI_Recv (data, size, MPI_DATA_TYPE, rank-1, 0, MPI_COMM_WORLD, &status);
        timer[i] = getTime(&time1);

        // Verify integrity of received data
        for (unsigned long j=0; j<size; j++)
        {
            if (data[j] != ASSIGNED)
                printf ("GET: Data Transfer Error! Expected %.1f, received %.1f\n\n", (float)j, (float)data[j]);
            
            data[j] = NOT_ASSIGNED;
        }
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    #if INFO == 1
    	printf ("[SLAVE]\t\tRecv\t-> [COMPLETED]\n");
    #endif
    threadAlive = false;
}
