/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 

#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
// #define DEBUG
// #define INFO 			
// #define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"

using namespace std;

atomic<bool> threadAlive(true);						// flag to denote send/receive thread status

void sendFunction(DATA_TYPE data[], unsigned long BLOCK_SIZE, unsigned long TRANSFER_COUNT, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);
void recvFunction(DATA_TYPE data[], unsigned long BLOCK_SIZE, unsigned long TRANSFER_COUNT, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[]);

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(1);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *data;								// vector data
	unsigned long BLOCK_SIZE;						// data vector length after multiplication
	unsigned long TRANSFER_COUNT;					// number of sequential transfers of size [BLOCK_SIZE]
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gTime, *pTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// get current process rank
	/***********************************************************************************************/


	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 5)
	{
		printf ("%d \n", argc);
		printf ("Usage: <BLOCK_SIZE> <TRANSFER_COUNT> <REPEAT_COUNT> <WORK_DURING_TRANSFER[1/0]>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	BLOCK_SIZE 			= atoi(argv[1]);
	TRANSFER_COUNT 		= atoi(argv[2]);
	REPEAT_COUNT 		= atoi(argv[3]);
	WORK_DURING_TRANSFER= atoi(argv[4]);
	WORK_AMOUNT 		= (unsigned long)(BLOCK_SIZE*TRANSFER_COUNT)*(unsigned long)(REPEAT_COUNT*100);

	gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	if (rank == MASTER)
	{
		printb ("Block Size:\t%lu (%.2fKB)\tTransfers:\t%lu\tRepeat Count: %lu\n", BLOCK_SIZE, (float)(sizeof(DATA_TYPE)* BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, REPEAT_COUNT);
		printc ("%lu, %.2fKB, x %lu, %d, ", BLOCK_SIZE, (float)(sizeof(DATA_TYPE) * BLOCK_SIZE)/(1024.0), TRANSFER_COUNT, WORK_DURING_TRANSFER);
	}

	// 2. Allocate memory to vector
    MPI_Alloc_mem(BLOCK_SIZE * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);

    // 3. Allocate a distinguishing value for master and slave
	if (rank%2 == MASTER) 
    {
        for(unsigned long i=0;i<BLOCK_SIZE;i++)
            data[i] = ASSIGNED;						// allocate ASSIGNED value
    }
    else
    {
    	for(unsigned long i=0;i<BLOCK_SIZE;i++)
	        data[i] = NOT_ASSIGNED;					// allocate NOT_ASSIGNED value
    }

    /***********************************************************************************************/


    /***********************************************************************************************/
    /*                                    Send/Receive Threads
    /***********************************************************************************************/
    int sum = 0;
    if (rank%2 == MASTER)
    {
    	thread sendThread(sendFunction, data, BLOCK_SIZE, TRANSFER_COUNT, MPI_COMM_WORLD, rank, REPEAT_COUNT, pTime);
    	while (WORK_DURING_TRANSFER && threadAlive)
    	{
	    	for (unsigned long i=0; i<WORK_AMOUNT; i++)
	    		sum += 1;
	    }
	    printd ("[%d]\tSUM-S\t-> COMPLETE\t%d\n", rank, sum);
    	sendThread.join();
    }
    else
    {
    	thread recvThread(recvFunction, data, BLOCK_SIZE, TRANSFER_COUNT, MPI_COMM_WORLD, rank, REPEAT_COUNT, gTime);
    	while (WORK_DURING_TRANSFER && threadAlive)
    	{
	    	for (unsigned long i=0; i<WORK_AMOUNT; i++)
	    		sum += 1;
	    }
	    printd ("[%d]\tSUM-R\t-> COMPLETE\t%d\n", rank, sum);
    	recvThread.join();
    }
    /***********************************************************************************************/

    MPI_Barrier(MPI_COMM_WORLD);


    /***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

    // 1. Get gTime from Slave
    if (rank%2 == MASTER)
    	MPI_Recv (gTime, REPEAT_COUNT, MPI_FLOAT, rank+1, ADMIN_TAG, MPI_COMM_WORLD, &status);
    else
    	MPI_Send (gTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);

	if (rank == MASTER)
    {
	    // 2. Sort the recorded timings
	    sort (pTime, gTime, REPEAT_COUNT);

	    // 3. Get avg of each time
	    float putTimeAvg = pTime[REPEAT_COUNT/2];
	    float getTimeAvg = gTime[REPEAT_COUNT/2];

	    BLOCK_SIZE *= TRANSFER_COUNT;
	    
	    float worstTimeAvg	= max (putTimeAvg, getTimeAvg);
		printb ("Act Speed: %.2fMB/s (%f ms)\n",
		    	(float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0), 
		    	worstTimeAvg*1000.0);
		printc ("%f ms, %.2f MB/s\n", worstTimeAvg*1000.0, (float)(sizeof(DATA_TYPE)*BLOCK_SIZE)/(worstTimeAvg*1048576.0));
	}

    /***********************************************************************************************/

    free (data);
    MPI_Finalize ();

    return 0;
}

void sendFunction(DATA_TYPE data[], unsigned long BLOCK_SIZE, unsigned long TRANSFER_COUNT, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier (MPI_COMMUNICATOR);

        resetTime(&time1);
        for (unsigned long j=0; j<TRANSFER_COUNT; j++)
        	MPI_Send (data, BLOCK_SIZE, MPI_DATA_TYPE, rank+1, rank, MPI_COMM_WORLD);                            
        timer[i] = getTime(&time1);
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    printd ("[%d]\tSend\t-> [COMPLETED]\n", rank);
    threadAlive = false;
}

void recvFunction(DATA_TYPE data[], unsigned long BLOCK_SIZE, unsigned long TRANSFER_COUNT, MPI_Comm MPI_COMMUNICATOR, const int rank, const long REPEAT_COUNT, float timer[])
{
	stick_this_thread_to_core(0);
	MPI_Status status;
	struct timeval time1;

	for (long i=0; i<REPEAT_COUNT; i++)
	{
		MPI_Barrier (MPI_COMMUNICATOR);

		resetTime(&time1);
		for (unsigned long j=0; j<TRANSFER_COUNT; j++)
        	MPI_Recv (data, BLOCK_SIZE, MPI_DATA_TYPE, rank-1, 0, MPI_COMM_WORLD, &status);
        timer[i] = getTime(&time1);

        // Verify integrity of received data
        for (unsigned long j=0; j<BLOCK_SIZE; j++)
        {
            if (data[j] != ASSIGNED)
                printf ("[%d]\tGET: Data Transfer Error! Expected %.1f, received %.1f\n\n", rank, (float)j, (float)data[j]);
            
            data[j] = NOT_ASSIGNED;
        }
    }

    this_thread::sleep_for(std::chrono::milliseconds(50));
    printd ("[%d]\tRecv\t-> [COMPLETED]\n", rank);
    threadAlive = false;
}
