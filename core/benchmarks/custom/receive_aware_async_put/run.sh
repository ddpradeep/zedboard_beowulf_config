#!/bin/bash

# Default Libraries
if [ -z "$MPI" ]
then
	MPI=openmpi-1.8
fi
if [ -z "$BLAS" ]
then
	BLAS=openblas
fi
# HOSTFILE=mpich.hf

##### MPI LIBRARY ######
PATH_MPI=/opt/$MPI
##### BLAS LIBRARY #####
PATH_BLAS=/opt/$BLAS
########################

export PATH=$PATH:$PATH_MPI/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_MPI/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_BLAS/lib

if [ $1 == "raap" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./raap.o $3 $4 $5 $6
	else
		mpirun -machinefile mpich.hf -np $2 ./raap.o $3 $4 $5 $6
	fi
elif [ $1 == "sync" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./sync.o $3 $4 $5 $6
	else
		mpirun -machinefile mpich.hf -np $2 ./sync.o $3 $4 $5 $6
	fi
elif [ $1 == "async" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./async.o $3 $4 $5 $6
	else
		mpirun -machinefile mpich.hf -np $2 ./async.o $3 $4 $5 $6
	fi
elif [ $1 == "async_start" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./async_start.o $3 $4 $5 $6
	else
		mpirun -machinefile mpich.hf -np $2 ./async_start.o $3 $4 $5 $6
	fi
elif [ $1 == "all" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./sync.o $3 $4 $5 $6
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./raap.o $3 $4 $5 $6
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./async.o $3 $4 $5 $6
	else
		mpirun -machinefile mpich.hf -np $2 ./sync.o $3 $4 $5 $6
		mpirun -machinefile mpich.hf -np $2 ./raap.o $3 $4 $5 $6
		mpirun -machinefile mpich.hf -np $2 ./async.o $3 $4 $5 $6
	fi
fi
# /opt/mpich-3.1/bin/mpirun -machinefile 1mpich.hf -np 2 hostname
# mpirun -machinefile 1ompi.hf -np 2 -prefix /opt/openmpi-1.8 hostname
