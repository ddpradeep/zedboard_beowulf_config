#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include "helper-utils.h"

#define STRIDED 		10
#define STRIDED_P1 		11
#define RANDOM 			20
#define RANDOM_RW		21
#define SEQUENCE_IND 	30 			// Sequential Array Indirect Access
#define SEQUENCE_DIR	31			// Sequential DIrect Access
#define SEQUENCE_MEM	32			// Sequential Memcpy
#define SEQUENCE_DIR_WR	33			// Sequential Direct Write

int main(int argc, char *argv[])
{
	unsigned int ARRAY_SIZE, REPEAT_COUNT, B_TYPE, UNIT_SIZE;
	unsigned int STRIDE_COUNT, NUM_ADDS;
	TIME_DATA_TYPE start_time_write, start_time_read, end_time_write, end_time_read;
	double *time_taken, *subtract_time;

	int loopID;
	long i, j;

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	if (argc!=6)
	{
		fprintf(stderr, "Usage: \t%s "
			"\n\t\t<Type 10-Strided(Ind), 11-Strided+1(Ind), 20-Random(Ind) 21-RandomRW(Ind), 30-Sequence(Ind), 31-Sequence(Dir), 32-Memcpy, 33-Sequence_WR(Dir)> "
			"\n\t\t<data size in Bytes> "
			"\n\t\t<stride count in Bytes> "
			"\n\t\t<Number of mem_accesses in> "
			"\n\t\t<repeat count>\n", 
			argv[0]);
		exit(1);
	}

	// Get Data size and Repeat Count from input arguments
	B_TYPE 				= atoi(argv[1]);
	ARRAY_SIZE      	= atoi(argv[2])/sizeof(DATA_TYPE);
	STRIDE_COUNT 		= atoi(argv[3])/sizeof(DATA_TYPE);
	NUM_ADDS 			= atoi(argv[4]);
	REPEAT_COUNT  		= atoi(argv[5]);

	/***********************************************************************************************/
	/*                                        Initialize
	/***********************************************************************************************/

	// Allocate and initialize send buffer of size [ARRAY_SIZE] with sequential values [0, ARRAY_SIZE-1]
	DATA_TYPE *dataBuf, *dataBufShadow;
	dataBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*ARRAY_SIZE);
	for (i=0; i<ARRAY_SIZE; i++)
		dataBuf[i] = i;

	// Placeholder for computations to prevent O3 optimization
	DATA_TYPE accum = 0;

	// Allocate time measurement arrays
	time_taken 		= (double *) malloc(sizeof(double)*REPEAT_COUNT);
	subtract_time	= (double *) malloc(sizeof(double)*REPEAT_COUNT);

	// 1. Populate address array
	unsigned int *address;
	if (B_TYPE == STRIDED)
	{
		address = (unsigned int*) malloc (sizeof(unsigned int)*NUM_ADDS);
		for (unsigned int i=0, ptr = 0; i<NUM_ADDS; i++)
		{
			ptr += STRIDE_COUNT;
			ptr %= ARRAY_SIZE;
			address[i] = ptr;
		}
	}
	if (B_TYPE == STRIDED_P1)
	{
		address = (unsigned int*) malloc (sizeof(unsigned int)*NUM_ADDS);
		for (unsigned int i=0, ptr = 0; i<NUM_ADDS; i++)
		{
			ptr += STRIDE_COUNT+1;
			ptr %= ARRAY_SIZE;
			address[i] = ptr;
		}
		// shuffle (address, NUM_ADDS); // Turn OFF randomization of strided addesses
	}
	else if ( (B_TYPE == RANDOM) || (B_TYPE == RANDOM_RW) )
	{
		address = (unsigned int*) malloc (sizeof(unsigned int)*NUM_ADDS);

		srand (unsigned(time(0)));
		vector<unsigned int> tempBuffer;
		tempBuffer.reserve(NUM_ADDS);
		for (unsigned int i=0; i<NUM_ADDS; i++) 
			tempBuffer.push_back(i%ARRAY_SIZE);
		random_shuffle (tempBuffer.begin(), tempBuffer.end());
		unsigned int *bufferArray = &tempBuffer[0];

		memcpy (address, bufferArray, (NUM_ADDS)*sizeof(unsigned int));
	}
	else if (B_TYPE == SEQUENCE_IND)
	{
		address = (unsigned int*) malloc (sizeof(unsigned int)*NUM_ADDS);
		for (unsigned int i=0; i<NUM_ADDS; i++)
		{
			address[i] = i;
		}
	}
	//print (address, 10, " ");

	for (loopID=0; loopID<REPEAT_COUNT; loopID++)
	{
		/***********************************************************************************************/
		/*                                  	Memory Benchmark
		/***********************************************************************************************/
		switch (B_TYPE)
		{
			case STRIDED:
			case STRIDED_P1:
			case RANDOM:
			case SEQUENCE_IND:
		    	// 2. Get start time
		    	resetTime(&start_time_write);

		    	// 3. Perform accumulation
				for (unsigned int i=0; i<NUM_ADDS; i++)
			    {
			      accum += dataBuf[address[i]];
			    }

			    // 4. Calculate time taken
			    time_taken [loopID]  = getTime(&start_time_write);

			    // 5. Subtract addition overhead time
			    resetTime(&start_time_write);
			    for (unsigned int i=0; i<NUM_ADDS; i++)
			      accum += address[i];
			  	subtract_time [loopID]	= getTime(&start_time_write);
			    time_taken [loopID]  	-= subtract_time [loopID];

			    UNIT_SIZE = (sizeof(DATA_TYPE));
			    break;
			case RANDOM_RW:
				dataBufShadow = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*ARRAY_SIZE);
		    	// 2. Get start time
		    	resetTime(&start_time_write);

		    	// 3. Perform accumulation
				for (unsigned int i=0; i<NUM_ADDS; i++)
			    {
			      dataBufShadow[address[i]] = dataBuf[i];
			    }

			    // 4. Calculate time taken
			    time_taken [loopID]  = getTime(&start_time_write);

			    // 5. Subtract addition overhead time
			    resetTime(&start_time_write);
			    for (unsigned int i=0; i<NUM_ADDS; i++)
			      accum += dataBufShadow[i];
			  	subtract_time [loopID]	= 0; //getTime(&start_time_write);
			    time_taken [loopID]  	-= subtract_time [loopID];

			    UNIT_SIZE = (sizeof(DATA_TYPE));

			    free (dataBufShadow);
			    break;
			
			case SEQUENCE_DIR:
				// 2. Get start time
		    	resetTime(&start_time_write);

		    	// 3. Perform accumulation
				for (unsigned int i=0; i<NUM_ADDS; i++)
			    {
			      accum += dataBuf[i];
			    }

			    // 4. Calculate time taken
			    time_taken [loopID]  = getTime(&start_time_write);

			    // 5. Subtract addition overhead time
			    resetTime(&start_time_write);
			    for (unsigned int i=0; i<NUM_ADDS; i++)
			      accum += accum;
			    subtract_time [loopID]	= getTime(&start_time_write);
			    time_taken [loopID]  	-= subtract_time [loopID];

			    UNIT_SIZE = sizeof(DATA_TYPE);

			    break;
			case SEQUENCE_MEM:
				DATA_TYPE *dataBuf2;
					dataBuf2 = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*ARRAY_SIZE);
				// 2. Get start time
		    	resetTime(&start_time_write);
				memcpy (dataBuf2, dataBuf, NUM_ADDS*sizeof(int));
				// 4. Calculate time taken
			    subtract_time [loopID]	= getTime(&start_time_write);
			    time_taken [loopID]  	-= subtract_time [loopID];

			    accum += dataBuf2[100];
			    free (dataBuf2);

			    UNIT_SIZE = (sizeof(int));

			    break;
			case SEQUENCE_DIR_WR:
				// 2. Get start time
		    	resetTime(&start_time_write);

		    	// 3. Perform accumulation
				for (unsigned int i=0; i<NUM_ADDS; i++)
			    {
			      dataBuf[i] = i;
			    }

			    // 4. Calculate time taken
			    time_taken [loopID]  = getTime(&start_time_write);

			    // 5. Subtract addition overhead time
			    // resetTime(&start_time_write);
			    for (unsigned int i=0; i<NUM_ADDS; i++)
			      accum += dataBuf[i];
			    // subtract_time [loopID]	= getTime(&start_time_write);
			    // time_taken [loopID]  	-= subtract_time [loopID];

			    UNIT_SIZE = sizeof(DATA_TYPE);
			    break;
			default:
				exit (1);
		}
	}

	/***********************************************************************************************/
	/*                                    Analyze/Print Results
	/***********************************************************************************************/

	// Sort the timing results
	sort (time_taken, REPEAT_COUNT);
	sort (subtract_time, REPEAT_COUNT);

	// Ignore 0 second results
	int readwrite_median_index = 0;
	for (i=REPEAT_COUNT/2; i>=0; i--)
	if (time_taken[i]!=0)
	{
	  readwrite_median_index = i;
	  break;
	}
	// printf ("%d ", UNIT_SIZE);
	
	printf ("mem-lat-%d, %d, %d MB, %d KB, %d Bytes, %d KB-Stride, %d Bytes-Stride, %d MAdds, %d KAdds, %d Adds, Overhead: %.3fms, %.3f ms, %.3f MB/s\n", 
		B_TYPE,
		accum,
		ftoi((ARRAY_SIZE*sizeof(DATA_TYPE))/(1024*1024.0)),
		ftoi((ARRAY_SIZE*sizeof(DATA_TYPE))/(1024.0)),
		ftoi((ARRAY_SIZE*sizeof(DATA_TYPE))/(1.0)),
		ftoi(STRIDE_COUNT*sizeof(DATA_TYPE)/(1024.0)),
		ftoi(STRIDE_COUNT*sizeof(DATA_TYPE)/(1.0)),
		ftoi(NUM_ADDS/(1024*1024.0)),
		ftoi(NUM_ADDS/(1024.0)),
		ftoi(NUM_ADDS/(1.0)),
		subtract_time[readwrite_median_index]*1000.0,
		time_taken[readwrite_median_index]*1000.0,
		(float)(UNIT_SIZE*NUM_ADDS)/time_taken[readwrite_median_index]/(1024*1024.0)
	);
	return 0;
}
