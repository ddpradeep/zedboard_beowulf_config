#!/bin/bash

DATA_SIZE_START=1024*1024		# 1MB
DATA_SIZE_END=16*1024*1024	# 16MB

RMA_START=0 			# 0 %
RMA_END=100 			# 100 %


OPS_CEILING=64*1024*1024

OUTPUT_FILE="permutation.txt"

rm -rf $OUTPUT_FILE

# for (( size=DATA_SIZE_START; size<=DATA_SIZE_END; size*=2))					# Varying Datasize
for size in 400 860 864 1320 1780 1784 2240 2700 2704 3160 3620 3624 4080 4096 134656 265216 395776 526336 656896 787456 918016
do
	for (( rma=RMA_START; rma<=RMA_END; rma+=1))	# Varying Mem Accesses
	do
		echo "./run.sh perm 2 $size $rma 10"
		./run.sh perm 2 $size $rma 10 > temp
		OUTPUT=$(cat temp | grep mem) 
		if [[ ${#OUTPUT} < 10 ]]
		then
			((rma -=1 ))
		else
			echo $OUTPUT >> $OUTPUT_FILE
			echo $OUTPUT
		fi
	done
done

echo >> $OUTPUT_FILE
