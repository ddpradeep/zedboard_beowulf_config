#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include "helper-utils.h"

#define STRIDED 		1
#define RANDOM 			2
#define SEQUENCE_IND 	30 			// Sequential Array Indirect Access
#define SEQUENCE_DIR	31			// Sequential DIrect Access
#define SEQUENCE_MEM	32			// Sequential Memcpy

int main(int argc, char *argv[])
{

  unsigned int ARRAY_SIZE, REPEAT_COUNT, B_TYPE, UNIT_SIZE;
  int STRIDE_COUNT, NUM_ADDS;
  TIME_DATA_TYPE start_time_write, start_time_read, end_time_write, end_time_read;
  double *time_taken;

  int loopID;
  long i, j;

  /***********************************************************************************************/
  /*                                       Parse Input Arguments
  /***********************************************************************************************/

  if (argc!=6)
  {
    fprintf(stderr, "Usage: %s <Type 1-Strided, 2-Random, 3-Sequence, <data size in MB> <stride count in KB> <Number of mem_accesses in 10^6> <repeat count>\n", argv[0]);
    exit(1);
  }

  // Get Data size and Repeat Count from input arguments
  B_TYPE 		= atoi(argv[1]);
  ARRAY_SIZE      = (atoi (argv[2]) * 1024 * 1024)/sizeof(DATA_TYPE);
  STRIDE_COUNT = atoi(argv[3])*1024;
  NUM_ADDS = atoi(argv[4])*1024*1024;
  REPEAT_COUNT  = atoi (argv[5]);

  /***********************************************************************************************/
  /*                                        Initialize
  /***********************************************************************************************/

  // Allocate and initialize send buffer of size [ARRAY_SIZE] with sequential values [0, ARRAY_SIZE-1]
  DATA_TYPE *dataBuf;
  dataBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*ARRAY_SIZE);
  for (i=0; i<ARRAY_SIZE; i++)
    dataBuf[i] = i;

  DATA_TYPE accum = 0;
  // Allocate time measurement arrays
  time_taken = (double *) malloc(sizeof(double)*REPEAT_COUNT);

  // 1. Populate address array
  int *address;
  if (B_TYPE == STRIDED)
  {
  	address = (int*) malloc (sizeof(int)*NUM_ADDS);
	for (int i=0, ptr = 0; i<NUM_ADDS; i++)
	{
		ptr += STRIDE_COUNT+1;
		ptr %= ARRAY_SIZE;
		address[i] = ptr;
	}
	//shuffle (address, NUM_ADDS);
  }
  else if (B_TYPE == RANDOM)
  {
  	address = (int*) malloc (sizeof(int)*NUM_ADDS);
  	new_rand_perm (address, (long)0, (long)ARRAY_SIZE);
  }
  else if (B_TYPE == SEQUENCE_IND)
  {
  	address = (int*) malloc (sizeof(int)*NUM_ADDS);
  	for (int i=0; i<NUM_ADDS; i++)
	{
		address[i] = i;
	}
  }
  //print (address, 10, " ");

  for (loopID=0; loopID<REPEAT_COUNT; loopID++)
  {
    /***********************************************************************************************/
    /*                                  	Memory Benchmark
    /***********************************************************************************************/
    switch (B_TYPE)
    {
    	case STRIDED:
    	case RANDOM:
    	case SEQUENCE_IND:
	    	// 2. Get start time
	    	resetTime(&start_time_write);

	    	// 3. Perform accumulation
    		for (int i=0; i<NUM_ADDS; i++)
		    {
		      accum += dataBuf[address[i]];
		    }

		    // 4. Calculate time taken
		    time_taken [loopID]  = getTime(&start_time_write);

		    // 5. Subtract addition overhead time
		    resetTime(&start_time_write);
		    for (int i=0; i<NUM_ADDS; i++)
		      accum += address[i];
		    time_taken [loopID]  -= getTime(&start_time_write);

		    UNIT_SIZE = (sizeof(DATA_TYPE));
		    break;
		case SEQUENCE_DIR:
			// 2. Get start time
	    	resetTime(&start_time_write);

	    	// 3. Perform accumulation
    		for (int i=0; i<NUM_ADDS; i++)
		    {
		      accum += dataBuf[i];
		    }

		    // 4. Calculate time taken
		    time_taken [loopID]  = getTime(&start_time_write);

		    // 5. Subtract addition overhead time
		    resetTime(&start_time_write);
		    for (int i=0; i<NUM_ADDS; i++)
		      accum += accum;
		    time_taken [loopID]  -= getTime(&start_time_write);

		    UNIT_SIZE = sizeof(DATA_TYPE);

		    break;
		case SEQUENCE_MEM:
			DATA_TYPE *dataBuf2;
  			dataBuf2 = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*ARRAY_SIZE);
			// 2. Get start time
	    	resetTime(&start_time_write);
			memcpy (dataBuf2, dataBuf, NUM_ADDS*sizeof(int));
			// 4. Calculate time taken
		    time_taken [loopID]  = getTime(&start_time_write);

		    accum += dataBuf2[100];
		    free (dataBuf2);

		    UNIT_SIZE = (sizeof(int));

		    break;
		default:
			exit (1);
    }
  }

  /***********************************************************************************************/
  /*                                    Analyze/Print Results
  /***********************************************************************************************/

  // Sort the timing results
  sort (time_taken, REPEAT_COUNT);

  // Ignore 0 second results
  int readwrite_median_index = 0;
  for (i=REPEAT_COUNT/2; i>=0; i--)
    if (time_taken[i]!=0)
    {
      readwrite_median_index = i;
      break;
    }
    printf ("%d ", UNIT_SIZE);
  // 2 Reads, 1 Addition
  printf ("mem-rand-bw, %d, %d MB, %d KB-Stride, %d MegaAdds, %.3f ms, %.3f MB/s\n", 
    accum,
    ftoi((ARRAY_SIZE*sizeof(DATA_TYPE))/(1024*1024.0)),
    ftoi(STRIDE_COUNT/(1024.0)),
    ftoi(NUM_ADDS/(1000*1000.0)),
    time_taken[readwrite_median_index]*1000.0,
    (float)(UNIT_SIZE*NUM_ADDS)/time_taken[readwrite_median_index]/(1024*1024.0)
    );
  return 0;
}
