#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include <exception>
#include "mpi_perm_collective.h"
#include "helper-utils.h"
#include "assert.h"

#define STRIDED 		10
#define STRIDED_P1 		11
#define RANDOM 			20
#define SEQUENCE_IND 	30 			// Sequential Array Indirect Access
#define SEQUENCE_DIR	31			// Sequential DIrect Access
#define SEQUENCE_MEM	32			// Sequential Memcpy

int main(int argc, char *argv[])
{
	uint32_t PARTITION_SIZE, REPEAT_COUNT, RMP_RATIO, VECTOR_SIZE, UNIT_SIZE;
	TIME_DATA_TYPE start_time_write, start_time_read, end_time_write, end_time_read;
	DATA_TYPE accum = 0;
	double *time_taken_direct, *time_taken_mpi, *subtract_time;

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	int PROC_COUNT, rank;

	// MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &PROC_COUNT);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	if (argc!=4)
	{
		fprintf(stderr, "Usage: \t%s <<NO OF PROCESS>> <PARTITION SIZE (BYTES)> <%% REMOTE PUTS> <REPEAT COUNT>\n", argv[0]);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	// Get Data size and Repeat Count from input arguments
	// PROC_COUNT			= atoi(argv[1]);
	PARTITION_SIZE      = atoi(argv[1])/sizeof(DATA_TYPE);
	RMP_RATIO 			= atoi(argv[2]);
	REPEAT_COUNT  		= atoi(argv[3]);
	VECTOR_SIZE 		= PARTITION_SIZE*PROC_COUNT;

	/***********************************************************************************************/
	/*                                        Initialize
	/***********************************************************************************************/

	// Allocate and initialize send buffer of size [ARRAY_SIZE] with sequential values [0, ARRAY_SIZE-1]
	DATA_TYPE *dataBuf, *dataBufShadow, *dataBufRef;
	dataBuf 		= (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*PARTITION_SIZE);
	dataBufRef 		= (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*PARTITION_SIZE);
	MPI_Alloc_mem (PARTITION_SIZE*sizeof(DATA_TYPE), MPI_INFO_NULL, &dataBufShadow);
	MPI_Win_create(dataBufShadow, PARTITION_SIZE*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
	memset_seq(dataBuf, PARTITION_SIZE, 0);

	// Allocate time measurement arrays
	time_taken_direct 		= (double *) malloc(sizeof(double)*REPEAT_COUNT);
	time_taken_mpi	 		= (double *) malloc(sizeof(double)*REPEAT_COUNT);
	subtract_time			= (double *) malloc(sizeof(double)*REPEAT_COUNT);

	memset (time_taken_direct, 0, REPEAT_COUNT*sizeof(float));
	memset (time_taken_mpi, 0, REPEAT_COUNT*sizeof(float));
	

	/***********************************************************************************************/
	/*                        		Generate Permutated Address Space
	/***********************************************************************************************/
	
	uint32_t *global_dest_address = new uint32_t [VECTOR_SIZE];
	double achieved_rmp_ratio = get_random_address (global_dest_address, PROC_COUNT, PARTITION_SIZE, RMP_RATIO);
	uint32_t *dest_address = global_dest_address+PARTITION_SIZE*rank;
	// if (rank==MASTER)
	// 	print (dest_address, PARTITION_SIZE, " ");

	/***********************************************************************************************/
	/*                        				Build Data Type
	/***********************************************************************************************/
	mpi_indexed_info		sendType;
	mpi_indexed_info		recvType;

	int send_count = build_mpi_datatype_send_uni_blklen (dest_address, PARTITION_SIZE, rank, &sendType);
	int recv_count = build_mpi_datatype_recv_uni_blklen (dest_address, PARTITION_SIZE, rank, &recvType);

	/***********************************************************************************************/
	/*                                  	Reference Scatter
	/***********************************************************************************************/

	memset(dataBufRef, 0, PARTITION_SIZE*sizeof(DATA_TYPE));
	for (uint32_t i=0; i<PARTITION_SIZE; i++)
	{
		if ( ((dest_address[i]) >= rank*PARTITION_SIZE) && ((dest_address[i]) < (rank+1)*PARTITION_SIZE) )
			dataBufRef [ dest_address[i]-rank*PARTITION_SIZE ] 		= 		dataBuf [i];
	}

	// if (rank==MASTER)
	// 	print (dataBufRef, PARTITION_SIZE, " R ");

	/***********************************************************************************************/
	/*                                  	Direct Scatter
	/***********************************************************************************************/

	// 1. Initialize the transport buffer
	DATA_TYPE *transportBuf;
	transportBuf 	= (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*PARTITION_SIZE);

	for (int loopID=0; loopID<REPEAT_COUNT; loopID++)
	{
		memset(dataBufShadow, 0, PARTITION_SIZE*sizeof(DATA_TYPE));
		uint32_t sendCount 	= 0;
		uint32_t recvCount 	= 0;

		// Flush Cache
		accum += flushCache(10);


		assert (recvType.count == sendType.count);

		// 2. Get start time
		resetTime(&start_time_write);

		// /* Send Data */
		// // For every displacement
		// for (uint32_t sDisp=0; sDisp<sendType.count; sDisp++)
		// {
		// 	// Iterate for the specified block length
		// 	for (uint32_t sBlkLen=0; sBlkLen<sendType.array_of_blocklengths[sDisp]; sBlkLen++)
		// 	{
		// 		transportBuf[sendCount++] = dataBuf[ sendType.array_of_displacements[sDisp]+sBlkLen ]; // Item to send
		// 	}
		// }

		//  Recv Data 
		// // For every displacement
		// for (uint32_t rDisp=0; rDisp<recvType.count; rDisp++)
		// {
		// 	// Iterate for the specified block length
		// 	for (uint32_t rBlkLen=0; rBlkLen<recvType.array_of_blocklengths[rDisp]; rBlkLen++)
		// 	{
		// 		dataBufShadow[ recvType.array_of_displacements[rDisp]+rBlkLen ] = transportBuf[recvCount++]; // Item to recv
		// 	}
		// }

		for (uint32_t sDisp=0; sDisp<sendType.count; sDisp++)
		{
			dataBufShadow [ recvType.array_of_displacements[sDisp] ] = dataBuf [ sendType.array_of_displacements[sDisp] ];
		}

	    // 3. Calculate time taken
	    time_taken_direct [loopID]  = getTime(&start_time_write);

	    // if (rank==MASTER)
	    // 	print (dataBufShadow, PARTITION_SIZE, " D ");

	    // Verify
	    for (uint32_t i=0; i<PARTITION_SIZE; i++)
	    {
	    	if (dataBufShadow[i] != dataBufRef[i])
	    	{
	    		printf ("[ERR] Dir[%d] expected %d, received %d\n", i, dataBufRef[i], dataBufShadow[i]);
	    		MPI_Abort(MPI_COMM_WORLD, 1);
	    	}
	    }

	    UNIT_SIZE = (sizeof(DATA_TYPE));
	}

	/***********************************************************************************************/
	/*                                  	MPI Scatter
	/***********************************************************************************************/

	// 1. Create the windows for remote and local access
	

	// for (int loopID=0; loopID<REPEAT_COUNT; loopID++)
	// {
	// 	memset(dataBufShadow, 0, PARTITION_SIZE*sizeof(DATA_TYPE));
	// 	// Flush Cache
	// 	accum += flushCache(2);

	// 	// 2. Get start time
	// 	resetTime(&start_time_write);

	// 	MPI_Win_lock(MPI_LOCK_SHARED,rank,0,win);

	// 	// 4. Perform Local Scatter
	// 	MPI_Put(dataBuf, 1, sendType.newtype, rank, 0, 1, recvType.newtype, win);

	// 	// 5. Unlock the remote windows
	// 	MPI_Win_unlock(rank, win);

	// 	// 3. Calculate time taken
	//     time_taken_mpi [loopID]  = getTime(&start_time_write);

	// 	// if (rank==MASTER)
	// 	// 	print (dataBufShadow, PARTITION_SIZE, " M ");

	// 	// Verify
	//     for (uint32_t i=0; i<PARTITION_SIZE; i++)
	//     {
	//     	if (dataBufShadow[i] != dataBufRef[i])
	//     	{
	//     		printf ("[ERR] MPI[%d] expected %d, received %d\n", i, dataBufRef[i], dataBufShadow[i]);
	//     		MPI_Abort(MPI_COMM_WORLD, 1);
	//     	}
	//     }
	// }

	/***********************************************************************************************/
	/*                                    Analyze/Print Results
	/***********************************************************************************************/

	// Sort the timing results
	sort (time_taken_direct, REPEAT_COUNT);
	sort (time_taken_mpi, REPEAT_COUNT);
	sort (subtract_time, REPEAT_COUNT);

	// Ignore 0 second results
	int median_direct = 0;
	for (int i=REPEAT_COUNT/2; i>=0; i--)
		if (time_taken_direct[i]!=0)
		{
		  median_direct = i;
		  break;
		}
	int median_mpi = 0;

	for (int i=REPEAT_COUNT/2; i>=0; i--)
		if (time_taken_mpi[i]!=0)
		{
		  median_mpi = i;
		  break;
		}

	if (rank == MASTER)
	printf ("mem-lat-perm, %d, %d MB, %d KB, %d Bytes, %.2f %% RMA, Overhead: %.3fms, DIRECT: %.3f ms, %.3f MB/s MPI: %.3f ms, %.3f MB/s\n",
		accum,
		ftoi((PARTITION_SIZE*sizeof(DATA_TYPE))/(1024*1024.0)),
		ftoi((PARTITION_SIZE*sizeof(DATA_TYPE))/(1024.0)),
		ftoi((PARTITION_SIZE*sizeof(DATA_TYPE))/(1.0)),
		achieved_rmp_ratio,
		subtract_time[median_direct]*1000.0,
		time_taken_direct[median_direct]*1000.0,
		(float)(UNIT_SIZE)/time_taken_direct[median_direct]/(1024*1024.0),
		time_taken_mpi[median_mpi]*1000.0,
		(float)(UNIT_SIZE)/time_taken_mpi[median_mpi]/(1024*1024.0)
	);

	/***********************************************************************************************/
	/*                                    Clean Up & Exit
	/***********************************************************************************************/
	// MPI_Abort(MPI_COMM_WORLD, 1);

    // MPI_Win_free(&win);
    // MPI_Type_free(&sendType.newtype);
    // MPI_Type_free(&recvType.newtype);
	// free (dataBuf);
	// free (dataBufRef);
	// MPI_Free_mem (dataBufShadow);
	MPI_Finalize ();
	
	return 0;
}
