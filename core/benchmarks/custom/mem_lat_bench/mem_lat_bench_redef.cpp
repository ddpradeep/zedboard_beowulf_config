#define data_tt       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include "helper.h"

#define STRIDED 		10
#define RANDOM 			20

#define MAX_DATA_SIZE	64*1024*1024/sizeof(data_tt)

int main(int argc, char *argv[])
{
	double *time_taken, *subtract_time;

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	if (argc!=5)
	{
		fprintf(stderr, "Usage: \t%s "
			"\n\t\t<Type 10-Strided(Ind), 20-Random(Ind)>"
			"\n\t\t<data accesses in Bytes> "
			"\n\t\t<repetitions per timed measurement>\n"
			"\n\t\t<repeat count>\n", 
			argv[0]);
		exit(1);
	}

	// Get Data size and Repeat Count from input arguments
	int bench_type 						= atoi(argv[1]);
	unsigned long num_data_access 		= atoi(argv[2])/sizeof(data_tt);
	unsigned long data_access_repeat 	= atoi(argv[3]);
	unsigned long repeat_count 			= atoi(argv[4]);

	/***********************************************************************************************/
	/*                                        Initialize
	/***********************************************************************************************/

	// Allocate and initialize send buffer of size [ARRAY_SIZE] with sequential values [0, ARRAY_SIZE-1]
	data_tt *dataBuf;
	dataBuf = (data_tt*) malloc (sizeof(data_tt)*MAX_DATA_SIZE);
	for (int i=0; i<MAX_DATA_SIZE; i++)
		dataBuf[i] = i;

	// Placeholder for computations to prevent O3 optimization
	data_tt not_used_val = 0;

	// Allocate time measurement arrays
	time_taken 		= (double *) malloc(sizeof(double)*repeat_count);
	subtract_time	= (double *) malloc(sizeof(double)*repeat_count);

	// 1. Populate address array
	uint32_t *address_pool;
	uint32_t *address_start_ptr;
	address_pool 		= (uint32_t*) malloc (sizeof(uint32_t)*MAX_DATA_SIZE);
	address_start_ptr 	= (uint32_t*) malloc (sizeof(uint32_t)*data_access_repeat);

	get_random_address (address_pool, 0, MAX_DATA_SIZE-num_data_access);
	memcpy (address_start_ptr, address_pool, data_access_repeat*sizeof(uint32_t));

	switch (bench_type)
	{
		case STRIDED:
			memset_seq(address_pool, MAX_DATA_SIZE);
			break;
		case RANDOM:
			get_random_address (address_pool, 0, MAX_DATA_SIZE);
			break;
		default:
			abort();
	}

	// 2. Populate address_ptr array
	TIME_DATA_TYPE timer;
	for (int loopID=0; loopID<repeat_count; loopID++)
	{
		/***********************************************************************************************/
		/*                                  	Memory Benchmark
		/***********************************************************************************************/
		// Flush Cache
		not_used_val += flush_cache (2);

		// 2. Get start time
		resetTime(&timer);

		// 3. Perform accumulation
		for (uint32_t i=0; i<data_access_repeat; i++)
		{
			uint32_t start_addr = address_start_ptr[i];
			for (uint32_t j=start_addr; j<(start_addr+num_data_access); j++)
			{
				not_used_val += dataBuf[address_pool[j]];
			}
		}

		// 4. Calculate time taken
		time_taken [loopID]  = getTime(&timer);

		not_used_val += flush_cache (2);

		// 5. Subtract overhead time
		resetTime(&timer);
		for (uint32_t i=0; i<data_access_repeat; i++)
		{
			uint32_t start_addr = address_start_ptr[i];
			for (uint32_t j=start_addr; j<(start_addr+num_data_access); j++)
			{
				not_used_val += address_pool[j];
			}
		}
		subtract_time [loopID]	= getTime(&timer);
		time_taken [loopID]  	-= subtract_time [loopID];
		time_taken [loopID]		/= (double)data_access_repeat;
	}

	/***********************************************************************************************/
	/*                                    Analyze/Print Results
	/***********************************************************************************************/

	// Sort the timing results
	sort (time_taken, repeat_count);
	sort (subtract_time, repeat_count);

	// Ignore 0 second results
	int readwrite_median_index = 0;
	for (int i=repeat_count/2; i>=0; i--)
		if (time_taken[i]!=0)
		{
		  readwrite_median_index = i;
		  break;
		}
	// printf ("%d ", UNIT_SIZE);
	
	printf ("mem-lat-%d, ignore{%d}, %lu Bytes, %lux, Overhead: %fms, %f ms, %f MB/s\n", 
		bench_type,
		not_used_val,
		num_data_access*sizeof(data_tt),
		data_access_repeat,
		subtract_time[readwrite_median_index]*1000.0,
		time_taken[readwrite_median_index]*1000.0,
		(float)(sizeof(data_tt)*num_data_access)/time_taken[readwrite_median_index]/(1024*1024.0)
	);
	return 0;
}
