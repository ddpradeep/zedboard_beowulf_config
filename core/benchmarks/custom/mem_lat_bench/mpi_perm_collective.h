#include "helper-utils.h"
#include "mpi.h"
#include <thread>

#define MASTER	0
#define SLAVE	11

typedef struct 
{
	int count;
	int *array_of_blocklengths;
	int *array_of_displacements;
	MPI_Datatype oldtype;
	MPI_Datatype newtype;
}mpi_indexed_info;

template<typename BUFFER>
int build_mpi_datatype_send (BUFFER *data, uint32_t size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(i);
			uint32_t blocklength = 1;
			i++;
			while ( (i<size) && is_within_range(data[i], to_node*size, (to_node+1)*size) ) // && data[i]==(old_address+1) )
			{
				blocklength++;

				old_address = data[i];
				i++;
			}
			blocklengths.push_back(blocklength);
			count++;
		}
		else
			i++;
	}

	// 2. Copy them to the required format
	datatype_info->array_of_blocklengths	= new int [count];
	datatype_info->array_of_displacements 	= new int [count];

	memcpy (datatype_info->array_of_blocklengths, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->array_of_displacements, &displacements[0], count*sizeof(uint32_t));
	datatype_info->count = count;
	datatype_info->oldtype = MPI_DATA_TYPE;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->array_of_blocklengths,
		datatype_info->array_of_displacements,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);

	// delete [] datatype_info->array_of_blocklengths;
	// delete [] datatype_info->array_of_displacements;

	// print (datatype_info->array_of_blocklengths, datatype_info->count, " ");
	// print (datatype_info->array_of_displacements, datatype_info->count, " ");

	return getSum (datatype_info->array_of_blocklengths, datatype_info->count);

	// for (int i=0; i<count; i++)
	// {
	// 	for (int j=0; j<blocklengths.at(i); j++)
	// 		printf ("%d, ", displacements.at(i)+j);
	// 	printf ("\n");
	// }

	// MPI_Type_indexed (count, blocklens[], offsets[], old_type,*newtype)
}

template<typename BUFFER>
int build_mpi_datatype_recv (BUFFER *data, uint32_t size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(data[i]);
			uint32_t blocklength = 1;
			i++;
			while ( (i<size) && is_within_range(data[i], to_node*size, (to_node+1)*size) && data[i]==(old_address+1) )
			{
				blocklength++;

				old_address = data[i];
				i++;
			}
			blocklengths.push_back(blocklength);
			count++;
		}
		else
			i++;
	}

	// 2. Copy them to the required format
	datatype_info->array_of_blocklengths	= new int [count];
	datatype_info->array_of_displacements 	= new int [count];

	memcpy (datatype_info->array_of_blocklengths, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->array_of_displacements, &displacements[0], count*sizeof(uint32_t));

	for (int i=0; i<count; i++)
		datatype_info->array_of_displacements[i] -= (to_node*size);

	datatype_info->count = count;
	datatype_info->oldtype = MPI_DATA_TYPE;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->array_of_blocklengths,
		datatype_info->array_of_displacements,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);

	// delete [] datatype_info->array_of_blocklengths;
	// delete [] datatype_info->array_of_displacements;

	// print (datatype_info->array_of_blocklengths, datatype_info->count, " ");
	// print (datatype_info->array_of_displacements, datatype_info->count, " ");

	return getSum (datatype_info->array_of_blocklengths, datatype_info->count);

	// for (int i=0; i<count; i++)
	// {
	// 	for (int j=0; j<blocklengths.at(i); j++)
	// 		printf ("%d, ", displacements.at(i)+j);
	// 	printf ("\n");
	// }

	// MPI_Type_indexed (count, blocklens[], offsets[], old_type,*newtype)
}

template<typename BUFFER>
int build_mpi_datatype_send_uni_blklen (BUFFER *data, uint32_t size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(i);
			uint32_t blocklength = 1;
			blocklengths.push_back(blocklength);
			count++;
		}
		
		i++;
	}

	// 2. Copy them to the required format
	datatype_info->array_of_blocklengths	= new int [count];
	datatype_info->array_of_displacements 	= new int [count];

	memcpy (datatype_info->array_of_blocklengths, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->array_of_displacements, &displacements[0], count*sizeof(uint32_t));
	datatype_info->count = count;
	datatype_info->oldtype = MPI_DATA_TYPE;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->array_of_blocklengths,
		datatype_info->array_of_displacements,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);
	return getSum (datatype_info->array_of_blocklengths, datatype_info->count);
}

template<typename BUFFER>
int build_mpi_datatype_recv_uni_blklen (BUFFER *data, uint32_t size, int to_node, mpi_indexed_info *datatype_info)
{
	// 1. Identify the required data locations efficiently
	vector<BUFFER> blocklengths;
	vector<BUFFER> displacements;
	int count = 0;
	uint32_t i = 0;
	while (i<size)
	{
		if ( is_within_range (data[i], to_node*size, (to_node+1)*size) )
		{
			uint32_t old_address = data[i];

			displacements.push_back(data[i]);
			uint32_t blocklength = 1;
			blocklengths.push_back(blocklength);
			count++;
		}
		
		i++;
	}

	// 2. Copy them to the required format
	datatype_info->array_of_blocklengths	= new int [count];
	datatype_info->array_of_displacements 	= new int [count];

	memcpy (datatype_info->array_of_blocklengths, &blocklengths[0], count*sizeof(uint32_t));
	memcpy (datatype_info->array_of_displacements, &displacements[0], count*sizeof(uint32_t));

	for (int i=0; i<count; i++)
		datatype_info->array_of_displacements[i] -= (to_node*size);

	datatype_info->count = count;
	datatype_info->oldtype = MPI_DATA_TYPE;

	MPI_Type_indexed (
		datatype_info->count,
		datatype_info->array_of_blocklengths,
		datatype_info->array_of_displacements,
		datatype_info->oldtype,
		&datatype_info->newtype);

	MPI_Type_commit(&datatype_info->newtype);
	return getSum (datatype_info->array_of_blocklengths, datatype_info->count);
}