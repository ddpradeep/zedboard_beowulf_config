#!/bin/bash

DATA_SIZE_START=4			# 4
DATA_SIZE_END=64*1024*1024	# 64MB

STRIDE_SIZE_START=1 		# 1Byte
STRIDE_SIZE_END=1		# 1Byte

OPS_SIZE_START=1 		# 1 Ops
OPS_SIZE_END_FACTOR=1 		# Datasize Ceiling * Factor


OPS_CEILING=64*1024*1024

OUTPUT_FILE="new_results.txt"

rm -rf $OUTPUT_FILE

for (( size=DATA_SIZE_START; size<=DATA_SIZE_END; size*=2))					# Varying Datasize
do
	repeat=$((16000000/size>1?16000000/size:1))

    OUTPUT=$(./mem_lat_bench.o 10 $size $repeat 24) 
    echo $OUTPUT >> $OUTPUT_FILE
    echo $OUTPUT

    OUTPUT=$(./mem_lat_bench.o 20 $size $repeat 24) 
    echo $OUTPUT >> $OUTPUT_FILE
    echo $OUTPUT
done