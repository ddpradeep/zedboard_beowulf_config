#!/bin/bash

# Default Libraries
if [ -z "$MPI" ]
then
	MPI=mpich-3.1
fi
if [ -z "$BLAS" ]
then
	BLAS=openblas
fi

##### MPI LIBRARY ######
PATH_MPI=/opt/$MPI
##### BLAS LIBRARY #####
PATH_BLAS=/opt/$BLAS
########################

export PATH=$PATH:$PATH_MPI/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_MPI/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_BLAS/lib

if [ $1 == "perm" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./mem_lat_perm.o $3 $4 $5
	else
		mpirun -machinefile mpich.hf -np $2 ./mem_lat_perm.o $3 $4 $5
	fi
fi