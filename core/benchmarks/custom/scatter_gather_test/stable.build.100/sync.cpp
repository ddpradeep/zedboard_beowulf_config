/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
// #define DEBUG
// #define INFO
#define WARNING	
// #define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"
#include <future>
#include <chrono>

using namespace std;

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(PRIMARY_CORE);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE *global_data;					// vector global to all node [scatter/gather src/dst]
	DATA_TYPE *local_data;					// vector local to each node
	unsigned long BLOCK_SIZE;						// global_data vector length after multiplication
	unsigned long VECTOR_SIZE;
	unsigned long TRANSFER_COUNT;					// number of sequential transfers of size [BLOCK_SIZE]
	unsigned long REPEAT_COUNT;						// number of times to repeat the operation
	int WORK_DURING_TRANSFER;						// flag to decide whether to work while performing remote transfers
	unsigned long WORK_AMOUNT;						// number of add operations

	float *gatherTime, *scatterTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	// MPI_Init (&argc, &argv);						// initialization of MPI environment

	int provided;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank
	
	if (provided != MPI_THREAD_MULTIPLE)
	{
	    printw("[%d][WARN]\tThis MPI implementation does not support multiple threads\n", rank);
	    // MPI_Abort(MPI_COMM_WORLD, 1);
	}

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	printi ("Process %d running on %s\n", rank, processor_name);
	/***********************************************************************************************/


	/***********************************************************************************************/
	/*                                       Initialize
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: <TRANSFER_BLOCK_SIZE> <REPEAT_COUNT> <WORK_DURING_TRANSFER[N/A]>\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	BLOCK_SIZE 			= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);
	if (!(REPEAT_COUNT%2)) REPEAT_COUNT += 1;
	WORK_DURING_TRANSFER= atoi(argv[3]);
	VECTOR_SIZE 		= BLOCK_SIZE * total_proc;

	// 2. Allocate mem for vector and time holders
	gatherTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    scatterTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

    if (rank == MASTER) 
    {
    	// global_data 	= (DATA_TYPE*) malloc (VECTOR_SIZE * sizeof(DATA_TYPE));
    	MPI_Alloc_mem(VECTOR_SIZE * sizeof(DATA_TYPE), MPI_INFO_NULL, &global_data);
    	memset_seq (global_data, VECTOR_SIZE);
    }
    MPI_Alloc_mem(BLOCK_SIZE * sizeof(DATA_TYPE), MPI_INFO_NULL, &local_data);
    // local_data 		= (DATA_TYPE*) malloc (BLOCK_SIZE * sizeof(DATA_TYPE));
    memset_val(local_data, BLOCK_SIZE, 0);

    if (rank == MASTER)
	{
		printb ("Block Size:\t%lu (%.2fKB)\t\tRepeat Count: %lu\n", BLOCK_SIZE, (float)(sizeof(DATA_TYPE)* BLOCK_SIZE)/(1024.0), REPEAT_COUNT);
		printc ("sync-sg, %lu, %.2fKB, %d, ", BLOCK_SIZE*sizeof(DATA_TYPE), (float)(sizeof(DATA_TYPE) * BLOCK_SIZE)/(1024.0), WORK_DURING_TRANSFER);
	}
    // global_data = (DATA_TYPE*) malloc (BLOCK_SIZE * sizeof(DATA_TYPE));

    /***********************************************************************************************/

    MPI_Barrier (MPI_COMM_WORLD);

    /***********************************************************************************************/
    /*                           				Scatter
    /***********************************************************************************************/
	TIME_DATA_TYPE time1;

	for (int i=0; i<REPEAT_COUNT; i++)
    {
    	memset_val(local_data, BLOCK_SIZE, 0);

    	MPI_Barrier (MPI_COMM_WORLD);

		resetTime(&time1);
		MPI_Scatter(global_data, BLOCK_SIZE, MPI_DATA_TYPE, local_data, BLOCK_SIZE, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
		scatterTime[i] = getTime(&time1);

		if (verify_seq(local_data, BLOCK_SIZE, rank*BLOCK_SIZE))
			printd ("%d) Passed\n", rank);
		else
			printf ("%d) Failed\n", rank);

		printd ("Process%d\t-\t%.2f\n", rank, 1000.0 * scatterTime[i]);
	}
    /***********************************************************************************************/

    /***********************************************************************************************/
    /*                           				Gather
    /***********************************************************************************************/

	for (int i=0; i<REPEAT_COUNT; i++)
    {
    	if (rank == MASTER)
    		memset_val(global_data, VECTOR_SIZE, 0);

    	MPI_Barrier (MPI_COMM_WORLD);

		resetTime(&time1);
		MPI_Gather(local_data, BLOCK_SIZE, MPI_DATA_TYPE, global_data, BLOCK_SIZE, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
		gatherTime[i] = getTime(&time1);

		printd ("Process%d\t-\t%.2f\n", rank, 1000.0 * gatherTime[i]);

		if (rank == MASTER)
			if (verify_seq(global_data, VECTOR_SIZE, 0))
				printd ("%d) Passed\n", rank);
			else
				printf ("%d) Failed\n", rank);
	}
    /***********************************************************************************************/

    MPI_Barrier(MPI_COMM_WORLD);

    /***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

	if (rank == MASTER)
    {
    	// 1. Initialize buffers for receving time data from all processes
    	float *gatherTime_buffer = (float *) malloc(sizeof(float)*REPEAT_COUNT * total_proc);
    	float *scatterTime_buffer = (float *) malloc(sizeof(float)*REPEAT_COUNT * total_proc);

    	// 2. Receive time data from all processes
    	for (int i=0; i<REPEAT_COUNT; i++)
    	{
    		scatterTime_buffer[i] 	= scatterTime[i];
    		gatherTime_buffer[i] 	= gatherTime[i];
    	}

    	for (int i=1; i<total_proc; i++)
    	{
    		MPI_Recv (scatterTime_buffer+i*REPEAT_COUNT, REPEAT_COUNT, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    		MPI_Recv (gatherTime_buffer+i*REPEAT_COUNT, REPEAT_COUNT, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    	}

	    // 2. Sort the recorded timings
	    sort_multiple (scatterTime, scatterTime_buffer, total_proc, REPEAT_COUNT, SORT_MAX, 1);
	    sort_multiple (gatherTime, gatherTime_buffer, total_proc, REPEAT_COUNT, SORT_MAX, 0);

	    // 3. Get avg of each time
	    float scatterTimeAvg	= scatterTime[REPEAT_COUNT/2];
	    float gatherTimeAvg 	= gatherTime[REPEAT_COUNT/2];

	    float scatterSpeedAvg	= (float)(sizeof(DATA_TYPE)*BLOCK_SIZE*(total_proc-1))/(scatterTimeAvg*1048576.0);
	    float gatherSpeedAvg	= (float)(sizeof(DATA_TYPE)*BLOCK_SIZE*(total_proc-1))/(gatherTimeAvg*1048576.0);

		printb ("Scat Speed: %.2fMB/s (%f ms)\n", scatterSpeedAvg, scatterTimeAvg*1000.0);
		printb ("Gath Speed: %.2fMB/s (%f ms)\n", gatherSpeedAvg, gatherTimeAvg*1000.0);
		
		printc ("%f ms, %.2f MB/s, %f ms, %.2f MB/s\n", scatterTimeAvg*1000.0, scatterSpeedAvg, gatherTimeAvg*1000.0, gatherSpeedAvg);
	}
	else
	{
		// printf ("Process %d\n", rank);
		MPI_Send (scatterTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
		MPI_Send (gatherTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	}

    if (rank == MASTER)
    	free (global_data);
    free (local_data);

    MPI_Finalize ();
    return 0;
}