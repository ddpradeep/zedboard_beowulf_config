#ifndef HELPER_UTIL
#define HELPER_UTIL

#include "sys/time.h"
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdarg.h>
#include <cstring>
#include <mutex>

// #define TIME_DATA_TYPE struct timeval
#define TIME_DATA_TYPE struct timespec

#define MASTER        0
#define ADMIN_TAG       101
#define DATA_TAG      102
#define PRIMARY_CORE    0
#define SECONDARY_CORE    1
#define SEND_RECV_DELAY   150000    // us delay max (assumed)
#define NOT_ASSIGNED    -1
#define ASSIGNED      2
#define PING_DELAY        200       // us delay avg (measured)

#define SORT_MAX      1
#define SORT_MIN      2
#define SORT_AVG      3

std::mutex printMutex;

void inline resetTime(TIME_DATA_TYPE *timer);
float inline getTime(TIME_DATA_TYPE *time1);
void sort(float values[], unsigned long size);
void sort(float values1[], float values2[], unsigned long size);

// void inline resetTime(TIME_DATA_TYPE *timer)
// {
//     gettimeofday(timer, NULL);
// }

// float inline getTime(TIME_DATA_TYPE *time1)
// {
//     TIME_DATA_TYPE time2;
//     gettimeofday(&time2, NULL);
//     return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
// }

void inline resetTime(TIME_DATA_TYPE *timer)
{
  // Calculate time taken by a request
  clock_gettime(CLOCK_REALTIME, timer);
}

float inline getTime(TIME_DATA_TYPE *time1)
{
    TIME_DATA_TYPE time2;
    clock_gettime(CLOCK_REALTIME, &time2);
    return (float)(( time2.tv_sec - time1->tv_sec ) + ( time2.tv_nsec - time1->tv_nsec ) / 1E9);
}

int stick_this_thread_to_core(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores)
      return EINVAL;

   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);

   pthread_t current_thread = pthread_self();    
   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

static struct termios old, new_;

/* Initialize new_ terminal i/o settings */
void initTermios(int echo) 
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  new_ = old; /* make new_ settings same as old settings */
  new_.c_lflag &= ~ICANON; /* disable buffered i/o */
  new_.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new_); /* use these new_ terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return getch_(1);
}
 
void printd(const char* format, ...)
{
  #ifdef DEBUG
    char *new_format ;
    char const *prefix = "[DBG] \t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printi(const char* format, ...)
{
  #ifdef INFO
    char *new_format ;
    char const *prefix = "[INFO]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printw(const char* format, ...)
{
  #ifdef WARNING
    char *new_format ;
    char const *prefix = "[WARN]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printb(const char* format, ...)
{
  #ifdef BENCHMARK
    char *new_format ;
    char const *prefix = "[RSLT]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printc(const char* format, ...)
{
  #ifdef BATCH_MODE
    char *new_format ;
    char const *prefix = "";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void memset_seq (DATA_TYPE *array, long int size)
{
  for (unsigned long i=0; i<size; i++)
    array[i] = (DATA_TYPE) i;       // allocate ASSIGNED value
}

void memset_val (DATA_TYPE *array, long int size, DATA_TYPE value)
{
  for (unsigned long i=0; i<size; i++)
    array[i] = value;               // allocate ASSIGNED value
}

bool verify_seq (DATA_TYPE *array, long int size, long int offset)
{
  for (unsigned long i=0; i<size; i++)
    if (array[i] != (DATA_TYPE) (offset+i))
    {
      printd ("[%d] = %d != %d\n", i, (DATA_TYPE)array[i], (DATA_TYPE)(offset+i));
      return false;
    }
  return true;
}

void sort (float values[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size; j++)
            if (values[j]<values[j+1])
            {
                float temp = values[j];
                values[j] = values[j+1];
                values[j+1] = temp;
            }
}

void sort (float values1[], float values2[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size-1; j++)
            if ((values1[j]+values2[j]) < (values1[j+1]+values2[j+1]))
            {
                float temp = values1[j];
                values1[j] = values1[j+1];
                values1[j+1] = temp;

                temp = values2[j];
                values2[j] = values2[j+1];
                values2[j+1] = temp;
            }

 //    printf ("\n");
  // for (unsigned long i = 0; i<size; i++)
  //  printf ("%.3f\t%.3f\t%.3f\n", values1[i]*1000.0, values2[i]*1000.0, (values1[i]+values2[i])*1000.0);
  // printf ("\n");
}

void sort_multiple (float output[], float input[], long number_of_ranges, long range_size, short int sort_type, long starting_range)
{
  printi ("Input Arrays:\n");
  for (long i=0; i<range_size; i++)
  {
    for (long j=0; j<number_of_ranges; j++)
      printi ("%.2f\t", 1000.0 * input[j*range_size + i]);
    printi ("\n");
  }
  // 1. Get the maximum timing from a range of values
  for (long i=0; i<range_size; i++)
  {
    float local;

    switch (sort_type)
    {
      case SORT_MAX:
        local = 0;
        for (long j=starting_range; j<number_of_ranges; j++)
          if (input[j*range_size + i] > local)
            local = input[j*range_size + i];
        break;
      case SORT_MIN:
        local = 10000000000.0;
        for (long j=starting_range; j<number_of_ranges; j++)
          if (input[j*range_size + i] < local)
            local = input[j*range_size + i];
        break;
      case SORT_AVG:
        local = 0;
        for (long j=starting_range; j<number_of_ranges; j++)
          local += input[j*range_size + i];
        local /= (number_of_ranges - starting_range);
        break;
    }
    

    output[i] = local;
  }

  printi("\nMax Array:\n");
  for (long i=0; i<range_size; i++)
  {
    printi ("%.2f\n", 1000.0 * output[i]);
  }

  // 2. Sort the output array
  sort (output, range_size);

  // printfi("\nOutput Array:\n");
  // for (long i=0; i<range_size; i++)
  // {
  //   printi ("%.2f\n", 1000.0 * output[i]);
  // }
}

void print (DATA_TYPE *array, long size)
{
  for (long i=0; i<size; i++)
    printf ("%d\n", (int)array[i]);
}

#endif