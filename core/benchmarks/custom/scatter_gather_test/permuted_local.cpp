/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
// #define DEBUG
// #define INFO
#define WARNING	
// #define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"
#include "mpi_perm_collective.h"
#include <future>
#include <chrono>
#include <unistd.h>


using namespace std;

int main (int argc, char *argv[]) 
{
	// stick_this_thread_to_core(PRIMARY_CORE);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE 	*data;								// vector global to all node [scatter/gather src/dst]
	DATA_TYPE 	*shadow_data;						// vector local to each node

	uint32_t	*dest_address;

	uint32_t PARTITION_SIZE;					// global_data vector length after multiplication
	uint32_t VECTOR_SIZE;
	uint8_t RMP_RATIO;					// number of sequential transfers of size [PARTITION_SIZE]
	uint32_t REPEAT_COUNT;						// number of times to repeat the operation
	float achieved_rmp_ratio;

	float *gatherTime, *scatterTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win *win;

	int provided;

	// MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank
	
	if (provided != MPI_THREAD_MULTIPLE)
	    printd("[%d]\tThis MPI implementation does not support multiple threads\n", rank);

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	printi ("Process %d running on %s\n", rank, processor_name);
	/***********************************************************************************************/

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: <PARTITION SIZE> <REPEAT COUNT> <%% REMOTE PUTS>\nMemory Requirement >3xPARTITION SIZE\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	PARTITION_SIZE 		= atoi(argv[1]);
	if (PARTITION_SIZE%2 == 1) PARTITION_SIZE += 1;
	REPEAT_COUNT 		= atoi(argv[2]);
	RMP_RATIO 			= atoi(argv[3]);
	VECTOR_SIZE 		= PARTITION_SIZE*total_proc;
	dest_address 		= new uint32_t [PARTITION_SIZE];

	gatherTime 	= (float *) malloc(sizeof(float)*REPEAT_COUNT);
    scatterTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

	/***********************************************************************************************/
	/*                        		Generate Permutated Address Space
	/***********************************************************************************************/

	if (rank == MASTER)
	{
		uint32_t *global_dest_address = new uint32_t [VECTOR_SIZE];
		achieved_rmp_ratio = get_random_address (global_dest_address, total_proc, PARTITION_SIZE, RMP_RATIO);
		// print (global_dest_address, VECTOR_SIZE, " ");

		MPI_Scatter(global_dest_address, PARTITION_SIZE, MPI_UINT32_T, dest_address, PARTITION_SIZE, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);

		printb ("Partition Size:\t%lu (%.2fKB)\t\tRepeat Count: %lu\tRMP: %.2%f%%\n", PARTITION_SIZE, (float)(sizeof(DATA_TYPE)* PARTITION_SIZE)/(1024.0), REPEAT_COUNT, achieved_rmp_ratio);
		printc ("perm-sg, %d nodes, %lu, %.2fKB, %.2f%%, ", total_proc, PARTITION_SIZE*sizeof(DATA_TYPE), (float)(sizeof(DATA_TYPE) * PARTITION_SIZE)/(1024.0), achieved_rmp_ratio);

		delete [] global_dest_address;
	}
	else
	{
		MPI_Scatter(NULL, PARTITION_SIZE, MPI_UINT32_T, dest_address, PARTITION_SIZE, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);
	}

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == MASTER)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }
	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 1)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 2)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************************************************************/
	/*                                       	Initialize
	/***********************************************************************************************/

	// data 				= new DATA_TYPE [PARTITION_SIZE];
	// shadow_data 		= new DATA_TYPE [PARTITION_SIZE];

	win = new MPI_Win [total_proc];

	MPI_Alloc_mem(PARTITION_SIZE*sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
	MPI_Alloc_mem(PARTITION_SIZE*sizeof(DATA_TYPE), MPI_INFO_NULL, &shadow_data);

	memset_seq(data, PARTITION_SIZE, rank*PARTITION_SIZE);
	memset_val(shadow_data, PARTITION_SIZE, 0);

	/***********************************************************************************************/
	/*                        					Scatter
	/***********************************************************************************************/

	mpi_indexed_info		*sendType;
	mpi_indexed_info		*recvType;

	sendType	= new mpi_indexed_info [total_proc];
	recvType	= new mpi_indexed_info [total_proc];

	// 1. Create the windows for remote and local access
	for (int i=0; i<total_proc; i++)
	{
		if (rank == i)
		{
			MPI_Win_create(shadow_data, PARTITION_SIZE*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
    	}
    	else
		{
			MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
		}
	}

	TIME_DATA_TYPE time1;

	// 2. Build the sending and receiving data type
	for (int i=0; i<total_proc; i++)
	{
		// if (rank == MASTER)
		// {
		// 	printf ("\n");
		// 	print (dest_address, PARTITION_SIZE, " ");
		// }

		int send_count = build_mpi_datatype_send (dest_address, PARTITION_SIZE, i, &sendType[i]);
		int recv_count = build_mpi_datatype_recv (dest_address, PARTITION_SIZE, i, &recvType[i]);

		// if (send_count != recv_count)
		if (send_count != recv_count)
		{
			printf ("[ERROR] Count Mismatch Error -> From [%d] to [%d]: Send Count (%d) != Recv Count (%d)\n", rank, i, send_count, recv_count);
		}
	}

	MPI_Barrier (MPI_COMM_WORLD);

	for (int i=0; i<REPEAT_COUNT; i++)
    {

    	MPI_Barrier (MPI_COMM_WORLD);

		resetTime(&time1);

		// 3. Lock the remote windows
		MPI_Win_lock(MPI_LOCK_SHARED,rank,0,win[rank]);

		// 4. Perform Scatter
		MPI_Put(data, 1, sendType[rank].newtype, rank, 0, 1, recvType[rank].newtype, win[rank]);

		// 5. Unlock the remote windows
		MPI_Win_unlock(rank, win[rank]);

		// for (int j=0; j<total_proc; j++)
		// {
		// 	int k = j; //(j+rank) % total_proc;

		// 	MPI_Win_lock(MPI_LOCK_SHARED,k,0,win[k]);
		// 	MPI_Put(data, 1, sendType[k].newtype, k, 0, 1, recvType[k].newtype, win[k]);
		// 	MPI_Win_unlock(k, win[k]);
		// }

		scatterTime[i] = getTime(&time1);

		// printf ("%d = %.2f ms\n", rank, scatterTime[0]*1000.0);
	}

	// printf ("%d) Done\n", rank);
	
	// 6. Free the windows
	for (int i=0; i<total_proc; i++)
    {
	    MPI_Win_free(&win[i]);
	    MPI_Type_free(&sendType[i].newtype);
	    MPI_Type_free(&recvType[i].newtype);
	}

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == MASTER)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 1)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 2)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************************************************************/
    /*                                     Print Results
    /***********************************************************************************************/

	if (rank == MASTER)
    {
    	// 1. Initialize buffers for receving time data from all processes
    	float *gatherTime_buffer 	= (float *) malloc(sizeof(float)*REPEAT_COUNT * total_proc);
    	float *scatterTime_buffer 	= (float *) malloc(sizeof(float)*REPEAT_COUNT * total_proc);

    	// 2. Receive time data from all processes
    	for (int i=0; i<REPEAT_COUNT; i++)
    	{
    		scatterTime_buffer[i] 	= scatterTime[i];
    		gatherTime_buffer[i] 	= gatherTime[i];
    	}

    	for (int i=1; i<total_proc; i++)
    	{
    		MPI_Recv (scatterTime_buffer+i*REPEAT_COUNT, REPEAT_COUNT, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    		MPI_Recv (gatherTime_buffer+i*REPEAT_COUNT, REPEAT_COUNT, MPI_FLOAT, i, ADMIN_TAG, MPI_COMM_WORLD, &status);
    	}

	    // 2. Sort the recorded timings
	    sort_multiple (scatterTime, scatterTime_buffer, total_proc, REPEAT_COUNT, SORT_AVG, 0);
	    sort_multiple (gatherTime, gatherTime_buffer, total_proc, REPEAT_COUNT, SORT_AVG, 0);

	    // 3. Get avg of each time
	    float scatterTimeAvg	= scatterTime[REPEAT_COUNT/2];
	    float gatherTimeAvg 	= gatherTime[REPEAT_COUNT/2] = 0;

	    float scatterSpeedAvg	= (float)(sizeof(DATA_TYPE)*PARTITION_SIZE*(total_proc)*(achieved_rmp_ratio/100.0))/(scatterTimeAvg*1048576.0);
	    // float gatherSpeedAvg	= (float)(sizeof(DATA_TYPE)*PARTITION_SIZE*(total_proc)*(achieved_rmp_ratio/100.0))/(gatherTimeAvg*1048576.0);
	    float gatherSpeedAvg	= 0;

		printb ("Scat Speed: %.2fMB/s (%f ms)\n", scatterSpeedAvg, scatterTimeAvg*1000.0);
		printb ("Gath Speed: %.2fMB/s (%f ms)\n", gatherSpeedAvg, gatherTimeAvg*1000.0);
		
		printc ("%f ms, %.2f MB/s, %f ms, %.2f MB/s\n", scatterTimeAvg*1000.0, scatterSpeedAvg, gatherTimeAvg*1000.0, gatherSpeedAvg);
	}
	else
	{
		MPI_Send (scatterTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
		MPI_Send (gatherTime, REPEAT_COUNT, MPI_FLOAT, MASTER, ADMIN_TAG, MPI_COMM_WORLD);
	}


	/***********************************************************************************************/
	/*                                       	Finalize
	/***********************************************************************************************/
	free (data);
	free (shadow_data);

    MPI_Finalize ();

    return 0;
}