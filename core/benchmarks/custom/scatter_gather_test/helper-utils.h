#ifndef HELPER_UTIL
#define HELPER_UTIL

#include "sys/time.h"
#include <unistd.h>
#include <termios.h>
#include <stdio.h>
#include <stdarg.h>
#include <cstring>
#include <mutex>
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand

using namespace std;

// #define TIME_DATA_TYPE struct timeval
#define TIME_DATA_TYPE struct timespec

#define MASTER        0
#define ADMIN_TAG       101
#define DATA_TAG      102
#define PRIMARY_CORE    0
#define SECONDARY_CORE    1
#define SEND_RECV_DELAY   150000    // us delay max (assumed)
#define NOT_ASSIGNED    -1
#define ASSIGNED      2
#define PING_DELAY        200       // us delay avg (measured)

#define SORT_MAX      1
#define SORT_MIN      2
#define SORT_AVG      3

std::mutex printMutex;

void inline resetTime(TIME_DATA_TYPE *timer);
float inline getTime(TIME_DATA_TYPE *time1);
void sort(float values[], unsigned long size);
void sort(float values1[], float values2[], unsigned long size);

// void inline resetTime(TIME_DATA_TYPE *timer)
// {
//     gettimeofday(timer, NULL);
// }

// float inline getTime(TIME_DATA_TYPE *time1)
// {
//     TIME_DATA_TYPE time2;
//     gettimeofday(&time2, NULL);
//     return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
// }

void inline resetTime(TIME_DATA_TYPE *timer)
{
  // Calculate time taken by a request
  clock_gettime(CLOCK_REALTIME, timer);
}

float inline getTime(TIME_DATA_TYPE *time1)
{
    TIME_DATA_TYPE time2;
    clock_gettime(CLOCK_REALTIME, &time2);
    return (float)(( time2.tv_sec - time1->tv_sec ) + ( time2.tv_nsec - time1->tv_nsec ) / 1E9);
}

int stick_this_thread_to_core(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores)
      return EINVAL;

   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);

   pthread_t current_thread = pthread_self();    
   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

static struct termios old, new_;

/* Initialize new_ terminal i/o settings */
void initTermios(int echo) 
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  new_ = old; /* make new_ settings same as old settings */
  new_.c_lflag &= ~ICANON; /* disable buffered i/o */
  new_.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new_); /* use these new_ terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo) 
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return getch_(1);
}
 
void printd(const char* format, ...)
{
  #ifdef DEBUG
    char *new_format ;
    char const *prefix = "[DBG] \t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printi(const char* format, ...)
{
  #ifdef INFO
    char *new_format ;
    char const *prefix = "[INFO]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printw(const char* format, ...)
{
  #ifdef WARNING
    char *new_format ;
    char const *prefix = "[WARN]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printb(const char* format, ...)
{
  #ifdef BENCHMARK
    char *new_format ;
    char const *prefix = "[RSLT]\t";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void printc(const char* format, ...)
{
  #ifdef BATCH_MODE
    char *new_format ;
    char const *prefix = "";
    new_format = (char *) malloc(strlen(prefix)+strlen(format)+1);
    new_format[0] = '\0';
    strcat(new_format,prefix);
    strcat(new_format,format);

    va_list args;
    va_start(args, format);
    printMutex.lock();
    vprintf(new_format, args);
    printMutex.unlock();
    va_end(args);
  #endif
}

void memset_seq (DATA_TYPE *array, long int size)
{
  for (unsigned long i=0; i<size; i++)
    array[i] = (DATA_TYPE) i;       // allocate ASSIGNED value
}

void memset_seq (DATA_TYPE *array, long int size, uint32_t offset)
{
  for (unsigned long i=0; i<size; i++)
    array[i] = (DATA_TYPE) (offset+i);       // allocate ASSIGNED value
}

void memset_val (DATA_TYPE *array, long int size, DATA_TYPE value)
{
  for (unsigned long i=0; i<size; i++)
    array[i] = value;               // allocate ASSIGNED value
}

bool verify_seq (DATA_TYPE *array, long int size, long int offset)
{
  for (unsigned long i=0; i<size; i++)
    if (array[i] != (DATA_TYPE) (offset+i))
    {
      printd ("[%d] = %d != %d\n", i, (DATA_TYPE)array[i], (DATA_TYPE)(offset+i));
      return false;
    }
  return true;
}

void sort (float values[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size; j++)
            if (values[j]<values[j+1])
            {
                float temp = values[j];
                values[j] = values[j+1];
                values[j+1] = temp;
            }
}

void sort (float values1[], float values2[], unsigned long size)
{
    for (unsigned long i=1; i<size; i++)
        for (unsigned long j=0; j<size-1; j++)
            if ((values1[j]+values2[j]) < (values1[j+1]+values2[j+1]))
            {
                float temp = values1[j];
                values1[j] = values1[j+1];
                values1[j+1] = temp;

                temp = values2[j];
                values2[j] = values2[j+1];
                values2[j+1] = temp;
            }

 //    printf ("\n");
  // for (unsigned long i = 0; i<size; i++)
  //  printf ("%.3f\t%.3f\t%.3f\n", values1[i]*1000.0, values2[i]*1000.0, (values1[i]+values2[i])*1000.0);
  // printf ("\n");
}

void sort_multiple (float output[], float input[], long number_of_ranges, long range_size, short int sort_type, long starting_range)
{
  printi ("Input Arrays:\n");
  for (long i=0; i<range_size; i++)
  {
    for (long j=0; j<number_of_ranges; j++)
      printi ("%.2f\t", 1000.0 * input[j*range_size + i]);
    printi ("\n");
  }
  // 1. Get the maximum timing from a range of values
  for (long i=0; i<range_size; i++)
  {
    float local;

    switch (sort_type)
    {
      case SORT_MAX:
        local = 0;
        for (long j=starting_range; j<number_of_ranges; j++)
          if (input[j*range_size + i] > local)
            local = input[j*range_size + i];
        break;
      case SORT_MIN:
        local = 10000000000.0;
        for (long j=starting_range; j<number_of_ranges; j++)
          if (input[j*range_size + i] < local)
            local = input[j*range_size + i];
        break;
      case SORT_AVG:
        local = 0;
        for (long j=starting_range; j<number_of_ranges; j++)
          local += input[j*range_size + i];
        local /= (number_of_ranges - starting_range);
        break;
    }
    

    output[i] = local;
  }

  printi("\nMax Array:\n");
  for (long i=0; i<range_size; i++)
  {
    printi ("%.2f\n", 1000.0 * output[i]);
  }

  // 2. Sort the output array
  sort (output, range_size);

  // printfi("\nOutput Array:\n");
  // for (long i=0; i<range_size; i++)
  // {
  //   printi ("%.2f\n", 1000.0 * output[i]);
  // }
}

template<typename TYPE>
void print (TYPE *array, long size)
{
  for (long i=0; i<size; i++)
    printf ("%d\n", (int)array[i]);
}

template<typename TYPE>
void print (TYPE *array, long size, const char *spacing)
{
  for (long i=0; i<size; i++)
    printf ("%d%s", (int)array[i], spacing);
  printf ("\n");
}

uint32_t random (uint32_t min, uint32_t max)
{
    uint32_t n = max - min + 1;
    uint32_t remainder = RAND_MAX % n;
    uint32_t x;
    do{
        x = uint32_t();
    }while (x >= RAND_MAX - remainder);
    return min + x % n;
}

template<typename BUFFER, typename TYPE>
void new_rand_perm (BUFFER *buffer, TYPE min_value, TYPE max_value)
{
  srand (unsigned(time(0)));
  vector<BUFFER> tempBuffer;

  for (int i=min_value; i<=max_value; ++i) 
    tempBuffer.push_back(i);

  random_shuffle (tempBuffer.begin(), tempBuffer.end());

  BUFFER *bufferArray = &tempBuffer[0];

  memcpy (buffer, bufferArray, (max_value-min_value+1)*sizeof(BUFFER));

}

template<typename BUFFER, typename TYPE>
void shuffle (BUFFER *input, TYPE count)
{
  srand (unsigned(time(0)));
  vector<BUFFER> inputVector;

  for (int i=0; i<count; ++i) 
    inputVector.push_back(input[i]);

  random_shuffle (inputVector.begin(), inputVector.end());

  BUFFER *vectorArray = &inputVector[0];

  memcpy (input, vectorArray, (count)*sizeof(BUFFER));
}

template<typename TYPE>
bool is_within_range (TYPE address, TYPE min, TYPE max)
{
  if ( (address >= min) && (address < max ) )
    return true;
  return false;
}

template<typename BUFFER, typename TYPE>
void new_sequence(BUFFER dest_address, TYPE min_value, TYPE max_value)
{
  for (int i=0; i<(max_value-min_value+1); ++i) 
    dest_address[i] = min_value+i;
}

template<typename TYPE>
bool read_array_from_file (TYPE *buffer, int count)
{
  FILE * file = fopen("last_perm.bin", "rb");

  int bytes_read = fread(buffer, sizeof(TYPE), count, file);

  if (bytes_read != count)
    printf ("[ERROR] Error reading permutation address from file!\n");

  fclose(file);
}

template<typename TYPE>
bool write_array_to_file (TYPE *buffer, int count)
{
  FILE * file = fopen("last_perm.bin", "wb");

  int bytes_written = fwrite(buffer, sizeof(TYPE), count, file);

  if (bytes_written != count)
    printf ("[ERROR] Error writing permutation address from file!\n");

  fclose(file);
}

template<typename BUFFER, typename TYPE>
float get_random_address (BUFFER *buffer, int total_procs, TYPE partition_size, uint8_t rmp_ratio)
{
  // 1. Generate sequential address for all nodes
  new_sequence(buffer, (uint32_t)0, total_procs*partition_size);

  // 2. Shuffle all addresses ensuring no cross node references
  for (int i=0; i<total_procs; i++)                               // for every node
    shuffle (buffer+i*partition_size, partition_size);            //  ENABLE This!

  // 3. Retain only the first [ 1-[rmp_ratio] ]% local addresses for each node, store the remaining addresses in an "exposed" buffer
  uint32_t exposed_address_count = (uint32_t)((float)(partition_size/100.0)*(float)rmp_ratio);

  vector<BUFFER> *local_address   = new vector<BUFFER> [total_procs];
  vector<BUFFER> *exposed_address = new vector<BUFFER> [total_procs];

  for (int i=0; i<total_procs; i++)                               // for every node
  {
    for (int j=i*partition_size; j<(i*partition_size+partition_size-exposed_address_count); j++)
      local_address[i].push_back(buffer[j]);

    for (int j=(i*partition_size+partition_size-exposed_address_count); j<(i+1)*partition_size; j++)
      exposed_address[i].push_back(buffer[j]);
  }

  // 4. Fill up the remaining address slots of each node with randomly choosen remote addresses (from others' exposed_addresses)
  vector<vector<BUFFER>*> avail_remote_nodes;

  for (int j=0; j<total_procs; j++)                               // for every node
  {
    printd ("%d)\t%d + %d\t= %d\t: ",
      (int)j,
      (int)local_address[j].size(), 
      (int)exposed_address[j].size(), 
      (int)(local_address[j].size() + exposed_address[j].size()));
    for (unsigned k=0; k<local_address->size(); k++)
      printd ("%d ", local_address[j].at(k));
    printd (" | ");
    for (unsigned k=0; k<exposed_address->size(); k++)
      printd ("%d ", exposed_address[j].at(k));
    printd ("\n");
  }

  for (int i=0; i<total_procs; i++)                               // for every node
  {
    printd ("For node %d:\n", i);
    // i. Update available remote nodes for picking
    for (int j=0; j<total_procs; j++)
    {
      if (j==i)
        continue;   // dont add local node to this list!

      if (exposed_address[j].size() > 0)
        avail_remote_nodes.push_back(&exposed_address[j]);
    }

    for (int j=0; j<avail_remote_nodes.size(); j++)
    {
      printd ("avail[%d/%d]->size %d\t: ", j, avail_remote_nodes.size(), avail_remote_nodes.at(j)->size());
      for (int k=0; k<avail_remote_nodes.at(j)->size(); k++)
        printd ("%d ", avail_remote_nodes.at(j)->at(k));
      printd ("\n");
    }

    // ii. Fill up the local nodes target address space to match partition size
    TYPE remAddress;

    while (local_address[i].size() < partition_size)
    {
      printd (" - %d/%d - ", local_address[i].size(), partition_size);
      // if there are remote addresses available
      if (avail_remote_nodes.size() > 0)
      {
        // shuffle the available nodes
        random_shuffle (avail_remote_nodes.begin(), avail_remote_nodes.end()); // ENABLE This!

        // pop a address from the last remote node in the shuffled list
        remAddress = avail_remote_nodes.back()->back();
        avail_remote_nodes.back()->pop_back();

        // if that node has no more addresses left, pop that from this list
        if (avail_remote_nodes.back()->size() <= 0)
          avail_remote_nodes.pop_back();
      }
      // else fill up with local addresses
      else if (exposed_address[i].size() > 0)
      {
        remAddress = exposed_address[i].back();
        exposed_address[i].pop_back();
      }
      // if no local unsed addresses too, something's wrong! Abort!
      else
      {
        printd ("Critical error in generating purmuted address space!\n");
        while (1);
        remAddress = i*partition_size;
      }
      local_address[i].push_back(remAddress);
      printd ("Used %d\n", remAddress);
    }

    // iii. Empty the available remote nodes list
    avail_remote_nodes.clear();

    printd ("Output - Node %d (%d): ", i, local_address[i].size());
    for (unsigned k=0; k<local_address->size(); k++)
      printd ("%d ", local_address[i].at(k));
    printd ("\n");
    // pop a random address from that node [check if that node needs to be popped as well]
    // no other node available?
      // pop a random address from self's exposed address
    // self is also empty?
      // throw error
  }

  printd ("Trying to free\n");
  for (int i=0; i<total_procs; i++)
  {
    random_shuffle (local_address[i].begin(), local_address[i].end()); // ENABLE This!
    // reverse(local_address[i].begin(), local_address[i].end());  // DISABLE This!
    memcpy(buffer+i*partition_size, &local_address[i][0], sizeof(TYPE)*partition_size);
    local_address[i].clear();
  }
  printd ("Cleared\n");
  delete [] local_address;
  printd ("Freed local\n");
  delete [] exposed_address;
  printd ("Freed exposed region\n");

  TYPE sum_local_counts = 0;
  for (int i=0; i<total_procs; i++)
  {
    for (int j=0; j<partition_size; j++)
      if (is_within_range(*(buffer+i*partition_size+j), i*partition_size, (i+1)*partition_size))
        sum_local_counts++;
  }

  // // Sanity Check 1: Within address space
  // for (int i=0; i<total_procs; i++)
  // {
  //   for (int j=0; j<partition_size; j++)
  //     if (!is_within_range(*(buffer+i*partition_size+j), (TYPE)0, (total_procs)*partition_size))
  //       printf ("Address surpasses bounds!\n");
  // }
  // // Sanity Check 2: Within address space

  // BUFFER *temp_buf  = new BUFFER [total_procs*partition_size];
  // memcpy (temp_buf, buffer, total_procs*partition_size*sizeof(BUFFER));

  // std::sort(temp_buf, temp_buf+total_procs*partition_size);

  // for (int i=0; i<(total_procs*partition_size-1); i++)
  // {
  //   if (temp_buf[i] == temp_buf[i+1])
  //     printf ("Duplicate address found!\n");
  // }

  // delete [] temp_buf;

  // print (temp_buf, total_procs*partition_size, " ");

  // printf ("Sanity Pass\n");

  write_array_to_file (buffer, total_procs*partition_size);
  return (1.0-(float)sum_local_counts/(float)(partition_size*total_procs))*100.0;
  // return (uint32_t)((float)(partition_size/100.0)*(float)rmp_ratio)/(float)partition_size*100.0;
}

template<typename BUFFER, typename TYPE>
float get_random_address_from_file (BUFFER *buffer, int total_procs, TYPE partition_size, uint8_t rmp_ratio)
{ 

  read_array_from_file (buffer, total_procs*partition_size);

  TYPE sum_local_counts = 0;
  for (int i=0; i<total_procs; i++)
  {
    for (int j=0; j<partition_size; j++)
      if (is_within_range(*(buffer+i*partition_size+j), i*partition_size, (i+1)*partition_size))
        sum_local_counts++;
  }

  // Sanity Check 1: Within address space
  for (int i=0; i<total_procs; i++)
  {
    for (int j=0; j<partition_size; j++)
      if (!is_within_range(*(buffer+i*partition_size+j), (TYPE)0, (total_procs)*partition_size))
        printf ("Address surpasses bounds!\n");
  }
  // Sanity Check 2: Within address space

  BUFFER *temp_buf  = new BUFFER [total_procs*partition_size];
  memcpy (temp_buf, buffer, total_procs*partition_size*sizeof(BUFFER));

  std::sort(temp_buf, temp_buf+total_procs*partition_size);

  for (int i=0; i<(total_procs*partition_size-1); i++)
  {
    if (temp_buf[i] == temp_buf[i+1])
      printf ("Duplicate address found!\n");
  }

  delete [] temp_buf;

  // print (temp_buf, total_procs*partition_size, " ");

  // printf ("Sanity Pass\n");

  return (1.0-(float)sum_local_counts/(float)(partition_size*total_procs))*100.0;
  // return (uint32_t)((float)(partition_size/100.0)*(float)rmp_ratio)/(float)partition_size*100.0;
}


template<typename TYPE>
TYPE getSum (TYPE *buffer, int count)
{
  TYPE sum = 0;

  for (int i=0; i<count; i++)
    sum += buffer[i];

  return sum;
}

#endif