#include "mpi_collective.h"
#include <thread>

#define MASTER	0
#define SLAVE	11

class collective_v2
{
	long vector_size;
	long sub_vector_size;
	int type;
	DATA_TYPE *local_data;

	std::thread *threads;

	collective *rma_host, *rma_leech;

	int rank;
	int total_proc;

public:

	collective_v2 (short int type, long vector_size, long sub_vector_size);
	~collective_v2();

	void scatter	(DATA_TYPE *dest, long block_size, int target_rank);
	void gather		(DATA_TYPE *src,  long block_size, int target_rank);

	DATA_TYPE *get_vector_ptr();
	DATA_TYPE *get_sub_vector_ptr();
};

collective_v2::collective_v2(short int type, long vector_size, long sub_vector_size)
{
	this->vector_size = vector_size;
	this->sub_vector_size = sub_vector_size;
	this->type = type;

	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count

	switch (type)
	{
		case MASTER:
			// 1. Vector host
			rma_host 	= new collective(HOSTING, vector_size);

	    	//  2. Leech window
	    	rma_leech	= new collective(LEECHING, sub_vector_size);

	    	// 3. Local sub-vector
	    	MPI_Alloc_mem(sub_vector_size * sizeof(DATA_TYPE), MPI_INFO_NULL, &local_data);

			threads = new std::thread[total_proc];

	    	break;

	    case SLAVE:
	    	// 1. Leech window
	    	rma_leech 	= new collective(LEECHING, vector_size);

	    	// 2. Sub-vector host
	    	rma_host 	= new collective(HOSTING, sub_vector_size);
	    	local_data  = rma_host->get_data_ptr();
	    	break;
	}
}

collective_v2::~collective_v2()
{
	;
}

DATA_TYPE *collective_v2::get_vector_ptr()
{
	switch (type)
	{
		case MASTER:
			return rma_host->get_data_ptr();
			break;
		case SLAVE:
			return 0;
			break;
	}
}

DATA_TYPE *collective_v2::get_sub_vector_ptr()
{
	switch (type)
	{
		case MASTER:
			return local_data;
			break;
		case SLAVE:
			return local_data;
			break;
	}
}

void collective_v2::scatter(DATA_TYPE *src, long block_size, int target_rank)
{
	// Psedo-code
	//
	//        |------> SLAVE 0
	// MASTER |------> SLAVE 1
	//        |------> SLAVE 2
	//

	// threads[i] = new std::thread(&collective::blocking_put, rma_leech, src+i*block_size, block_size, i, 0);
	// for (int i=1; i<total_proc; i++)
	// 	threads[i].join();

	switch (type)
	{
		case MASTER:
			// printf ("here\n");
			// rma_host->put(dest, block_size, rank*block_size);
			memcpy(local_data, src, block_size*sizeof(DATA_TYPE));
			// rma_host->blocking_put(src, block_size, 0, 0);
			for (int i=1; i<total_proc; i++)
			{
				threads[i] = std::thread(&collective::blocking_put, rma_leech, src+i*block_size, block_size, i, 0);
				// rma_leech->blocking_put(src+i*block_size, block_size, i, 0);
			}
			for (int i=1; i<total_proc; i++)
				threads[i].join();

			break;
		case SLAVE:
			break;
	}

	// switch (type)
	// {
	// 	case MASTER:
	// 		rma_host->scatter(dest, block_size, target_rank);
	// 		break;
	// 	case SLAVE:
	// 		rma_leech->scatter(dest, block_size, target_rank);
	// 		break;
	// }
}

void collective_v2::gather(DATA_TYPE *src, long block_size, int target_rank)
{
	switch (type)
	{
		case MASTER:
			rma_host->gather(src, block_size, target_rank);
			break;
		case SLAVE:
			rma_leech->gather(src, block_size, target_rank);
			break;
	}
}