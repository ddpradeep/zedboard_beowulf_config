/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 
#define DATA_TYPE 		int
#define MPI_DATA_TYPE	MPI_INT
// #define DEBUG
// #define INFO
#define WARNING	
// #define BENCHMARK
#define BATCH_MODE

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <thread>
#include "helper-utils.h"
#include "mpi_perm_collective.h"
#include <future>
#include <chrono>
#include <unistd.h>


using namespace std;

int main (int argc, char *argv[]) 
{
	stick_this_thread_to_core(PRIMARY_CORE);
	int total_proc;									// total number of processes	
	int rank;										// rank of process
	DATA_TYPE 	*data;								// vector global to all node [scatter/gather src/dst]
	DATA_TYPE 	*shadow_data;						// vector local to each node

	uint32_t	*dest_address;

	uint32_t PARTITION_SIZE;					// global_data vector length after multiplication
	uint32_t VECTOR_SIZE;
	uint8_t RMP_RATIO;					// number of sequential transfers of size [PARTITION_SIZE]
	uint32_t REPEAT_COUNT;						// number of times to repeat the operation

	float *gatherTime, *scatterTime;							// placeholders to store timing measurements

	/***********************************************************************************************/
	/*                                   MPI Initialization
	/***********************************************************************************************/
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win *win;

	int provided;

	// MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	MPI_Init (&argc, &argv);						// initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);			// get current process rank
	
	if (provided != MPI_THREAD_MULTIPLE)
	    printd("[%d]\tThis MPI implementation does not support multiple threads\n", rank);

	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);
	printi ("Process %d running on %s\n", rank, processor_name);
	/***********************************************************************************************/

	/***********************************************************************************************/
	/*                                       Parse Input Arguments
	/***********************************************************************************************/

	// 1. Get the parameters from input
	if (argc != 4)
	{
		printf ("%d \n", argc);
		printf ("Usage: <PARTITION SIZE> <REPEAT COUNT> <%% REMOTE PUTS>\nMemory Requirement >3xPARTITION SIZE\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	PARTITION_SIZE 		= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);
	RMP_RATIO 			= atoi(argv[3]);
	VECTOR_SIZE 		= PARTITION_SIZE*total_proc;
	dest_address 		= new uint32_t [PARTITION_SIZE];

	gatherTime 	= (float *) malloc(sizeof(float)*REPEAT_COUNT);
    scatterTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

	/***********************************************************************************************/
	/*                        		Generate Permutated Address Space
	/***********************************************************************************************/

	if (rank == MASTER)
	{
		uint32_t *global_dest_address = new uint32_t [VECTOR_SIZE];
		float achieved_rmp_ratio = get_random_address (global_dest_address, total_proc, PARTITION_SIZE, RMP_RATIO);
		// printf (global_dest_address, VECTOR_SIZE, " ");

		MPI_Scatter(global_dest_address, PARTITION_SIZE, MPI_UINT32_T, dest_address, PARTITION_SIZE, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);

		printb ("Partition Size:\t%lu (%.2fKB)\t\tRepeat Count: %lu\tRMP: %.2%f%%\n", PARTITION_SIZE, (float)(sizeof(DATA_TYPE)* PARTITION_SIZE)/(1024.0), REPEAT_COUNT, achieved_rmp_ratio);
		printc ("perm-sg, %lu, %.2fKB, %.2f%%, ", PARTITION_SIZE*sizeof(DATA_TYPE), (float)(sizeof(DATA_TYPE) * PARTITION_SIZE)/(1024.0), achieved_rmp_ratio);

		delete [] global_dest_address;
	}
	else
	{
		MPI_Scatter(NULL, PARTITION_SIZE, MPI_UINT32_T, dest_address, PARTITION_SIZE, MPI_UINT32_T, MASTER, MPI_COMM_WORLD);
	}

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == MASTER)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }
	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 1)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 2)
	// {
	// 	print (dest_address, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	MPI_Barrier(MPI_COMM_WORLD);

	/***********************************************************************************************/
	/*                                       	Initialize
	/***********************************************************************************************/

	// data 				= new DATA_TYPE [PARTITION_SIZE];
	// shadow_data 		= new DATA_TYPE [PARTITION_SIZE];

	win = new MPI_Win [total_proc];

	MPI_Alloc_mem(PARTITION_SIZE*sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
	MPI_Alloc_mem(PARTITION_SIZE*sizeof(DATA_TYPE), MPI_INFO_NULL, &shadow_data);

	memset_seq(data, PARTITION_SIZE, rank*PARTITION_SIZE);
	memset_val(shadow_data, PARTITION_SIZE, 0);

	// if (rank == MASTER)
	// {
	// 	MPI_Win_create(shadow_data, PARTITION_SIZE*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win_host);
	// 	MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win_leech);
	// }
	// else
	// {
	// 	MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win_leech);
	// 	MPI_Win_create(shadow_data, PARTITION_SIZE*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win_host);
	// }

	MPI_Barrier (MPI_COMM_WORLD);

	/***********************************************************************************************/
	/*                        		Create MPI Custom Data Type
	/***********************************************************************************************/

	mpi_indexed_info		*sendType;
	mpi_indexed_info		*recvType;

	sendType	= new mpi_indexed_info [total_proc];
	recvType	= new mpi_indexed_info [total_proc];

	for (int i=0; i<total_proc; i++)
	{
		if (rank == i)
		{
			MPI_Win_create(shadow_data, PARTITION_SIZE*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
    	}
    	else
		{
			MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win[i]);
		}
	}

	TIME_DATA_TYPE time1;
	resetTime(&time1);

	for (int k=0; k<total_proc; k++)
		if (true)
		{
			// MPI_Win_lock(MPI_LOCK_SHARED,k,MPI_MODE_NOCHECK,winA[k]);
			// MPI_Win_lock(MPI_LOCK_SHARED,k,MPI_MODE_NOCHECK,winB[k]);
    		MPI_Win_lock(MPI_LOCK_SHARED,k,MPI_MODE_NOCHECK,win[k]);
		}

	// int to = 0;
	// switch (rank)
	// {
	// 	case 0:
	// 		to = 1;
	// 		// MPI_Win_lock(MPI_LOCK_SHARED, to, 0, win[to]);
	// 		MPI_Put(data, 1, MPI_DATA_TYPE, to, 0, 1, MPI_DATA_TYPE, win[to]);
	// 		// MPI_Win_unlock(to, win[to]);
	// 		break;
	// 	case 1:
	// 		break;
	// 	case 2:
	// 		to = 1;
	// 		// MPI_Win_lock(MPI_LOCK_SHARED, to, 0, win[to]);
	// 		MPI_Put(data, 1, MPI_DATA_TYPE, to, 0, 1, MPI_DATA_TYPE, win[to]);
	// 		// MPI_Win_unlock(to, win[to]);
	// 		break;
	// }

	// if (rank%2 == 0)
		// printf ("Node %d: ", rank);
		// print (dest_address, PARTITION_SIZE, " ");
		for (int j=0; j<total_proc; j++)
		{
			int i = (rank+j) % total_proc;
			printi ("[SEND] %d ->  %d\t: \n", rank, i);
			build_mpi_datatype_send (dest_address, PARTITION_SIZE, i, sendType);
			// print (sendType->array_of_blocklengths, sendType->count, " ");
			// print (sendType->array_of_displacements, sendType->count, " ");

			// printf ("[RECV] %d ->  %d\t: \n", rank, i);
			build_mpi_datatype_recv (dest_address, PARTITION_SIZE, i, recvType);	
			// print (recvType->array_of_blocklengths, recvType->count, " ");
			// print (recvType->array_of_displacements, recvType->count, " ");

			// if (i==rank)
			// {
			// 	// MPI_Win_lock(MPI_LOCK_EXCLUSIVE, i, 0, win_host);
			// 	// // MPI_Put(data, 1, sendType->newtype, i, 0, 1, recvType->newtype, win_host);
			// 	// MPI_Put(data, 1, MPI_DATA_TYPE, i, 0, 1, MPI_DATA_TYPE, win_host);
			// 	// MPI_Win_unlock(i, win_host);
			// }
			// else
			// {
			// 	MPI_Win_lock(MPI_LOCK_EXCLUSIVE, i, 0, win_leech);
				// MPI_Put(data, 1, sendType->newtype, i, 0, 1, recvType->newtype, win_leech);
			// 	MPI_Put(data, 1, MPI_DATA_TYPE, i, 0, 1, MPI_DATA_TYPE, win_leech);
			// 	MPI_Win_unlock(i, win_leech);
			// }
			MPI_Put(data, 1, sendType->newtype, i, 0, 1, recvType->newtype, win[i]);
			// MPI_Put(data, 1, MPI_DATA_TYPE, i, 0, 1, MPI_DATA_TYPE, win[i]);

			printi ("[~SENT] %d ->  %d\t: \n", rank, i);
		}

	for (int k=0; k<total_proc; k++)
		if (true)
		{
			// MPI_Win_unlock(k, winA[k]);
			// MPI_Win_unlock(k, winB[k]);
			MPI_Win_unlock(k, win[k]);
		}

	scatterTime[0] = getTime(&time1);

	printf ("%.2f ms\n", scatterTime[0]*1000.0);
	// for every node
	// get sender custom data type

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == MASTER)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 1)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// MPI_Barrier(MPI_COMM_WORLD);

	// if (rank == 2)
	// {
	// 	print (shadow_data, PARTITION_SIZE, " ");
	// 	usleep(100000);
	// }

	// printf ("%d-Done\n", rank);

	for (int i=0; i<total_proc; i++)
    {
	    // MPI_Win_free(&winA[i]);
	    // MPI_Win_free(&winB[i]);
	    MPI_Win_free(&win[i]);
	}
	// if (rank == MASTER)
	// {
	// 	MPI_Win_free (&win_host);
	// 	MPI_Win_free (&win_leech);
	// }
	// else
	// {
	// 	MPI_Win_free (&win_leech);
	// 	MPI_Win_free (&win_host);
	// }


    MPI_Finalize ();
    return 0;
}