#!/bin/bash

# Default Libraries
if [ -z "$MPI" ]
then
	MPI=openmpi-1.8
fi
if [ -z "$BLAS" ]
then
	BLAS=openblas
fi
# HOSTFILE=mpich.hf

##### MPI LIBRARY ######
PATH_MPI=/opt/$MPI
##### BLAS LIBRARY #####
PATH_BLAS=/opt/$BLAS
########################

export PATH=$PATH:$PATH_MPI/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_MPI/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_BLAS/lib

if [ $1 == "sync" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./sync.o $3 $4 $5
	else
		mpirun -machinefile mpich.hf -np $2 ./sync.o $3 $4 $5
	fi
elif [ $1 == "one-sided" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./one-sided.o $3 $4 $5
	else
		mpirun -machinefile mpich.hf -np $2 ./one-sided.o $3 $4 $5
	fi
elif [ $1 == "one-sided-v2" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./one-sided_v2.o $3 $4 $5
	else
		mpirun -machinefile mpich.hf -np $2 ./one-sided_v2.o $3 $4 $5
	fi
elif [ $1 == "perm" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi.hf -np $2 -prefix /opt/$MPI ./perm.o $3 $4 $5
	else
		mpirun -machinefile mpich.hf -np $2 ./perm.o $3 $4 $5
	fi
elif [ $1 == "perm_local" ]
then
	if [[ $MPI == *openmpi* ]]
	then
		mpirun -machinefile ompi_2nodes.hf -np $2 -prefix /opt/$MPI ./perm_local.o $3 $4 $5
	else
		mpirun -machinefile mpich_2nodes.hf -np $2 ./perm_local.o $3 $4 $5
	fi
fi
# /opt/mpich-3.1/bin/mpirun -machinefile 1mpich.hf -np 2 hostname
# mpirun -machinefile 1ompi.hf -np 2 -prefix /opt/openmpi-1.8 hostname
