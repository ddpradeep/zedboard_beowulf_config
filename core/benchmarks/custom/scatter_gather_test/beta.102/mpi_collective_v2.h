#include "mpi_collective.h"

#define MASTER	0
#define SLAVE	11

class collective_v2
{
	long vector_size;
	long sub_vector_size;
	int type;
	DATA_TYPE *local_data;

	collective *rma_host, *rma_leech;

public:

	collective_v2 (short int type, long size);
	~collective_v2();

	void scatter	(DATA_TYPE *dest, long block_size, int target_rank);
	void gather		(DATA_TYPE *src,  long block_size, int target_rank);

	DATA_TYPE *get_vector_ptr();
	DATA_TYPE *get_sub_vector_ptr();
};

collective_v2::collective_v2(short int type, long vector_size, long sub_vector_size)
{
	this->vector_size = vector_size;
	this->sub_vector_size = sub_vector_size;
	this->type = type;

	switch (type)
	{
		case MASTER:
			rma_host 	= new collective(HOSTING, VECTOR_SIZE);
	    	global_data = rma_host->get_data_ptr();
	    	memset_seq (global_data, VECTOR_SIZE);

	    	//  2. Leech window
	    	rma_leech	= new collective(LEECHING, BLOCK_SIZE);

	    	// 3. Local sub-vector
	    	MPI_Alloc_mem(BLOCK_SIZE * sizeof(DATA_TYPE), MPI_INFO_NULL, &local_data);
	    	break;
	    case SLAVE:
	    	// 1. Leech window
	    	rma_leech 	= new collective(LEECHING, VECTOR_SIZE);

	    	// 2. Sub-vector host
	    	rma_host 	= new collective(HOSTING, BLOCK_SIZE);
	    	local_data  = rma_host->get_data_ptr();
	}
}

DATA_TYPE *collective_v2::get_vector_ptr()
{
	switch (type)
	{
		case MASTER:
			return rma_host->get_data_ptr();
			break;
		case SLAVE:
			return 0;
			break;
	}
}

DATA_TYPE *collective_v2::get_sub_vector_ptr()
{
	switch (type)
	{
		case MASTER:
			return local_data;
			break;
		case SLAVE:
			return local_data;
			break;
	}
}

void collective_v2::scatter(DATA_TYPE *dest, long block_size, int target_rank)
{
	switch (type)
	{
		case MASTER:
			rma_host->scatter(dest, block_size, target_rank);
			break;
		case SLAVE:
			rma_leech->scatter(dest, block_size, target_rank);
			break;
	}
}

void collective_v2::gather(DATA_TYPE *src, long block_size, int target_rank)
{
	switch (type)
	{
		case MASTER:
			rma_host->scatter(src, block_size, target_rank);
			break;
		case SLAVE:
			rma_leech->scatter(src, block_size, target_rank);
			break;
	}
}