#include "mpi.h"
#include "helper-utils.h"

#define HOSTING 		1
#define LEECHING 		2


class collective 
{
	DATA_TYPE 	*data;
	MPI_Win 	win;
	short int 	type;
	long 		data_size;
	int rank;
	int target_rank;

	void create_window();
	void open_window();
public:

	collective (short int type, long size);
	~collective();
	
	void lock(int target_rank);
	void unlock();
	void put(DATA_TYPE *origin, long size, long target_disp);
	void get(DATA_TYPE *dest, long size, long target_disp);

	void scatter(DATA_TYPE *dest, long block_size, int target_rank);
	void gather	(DATA_TYPE *src,  long block_size, int target_rank);

	DATA_TYPE *get_data_ptr();
};

collective::collective(short int type, long size)
{
	this->type 		= type;
	this->data_size = size;
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	create_window();
	open_window();
	// printf ("Opened window\n");
}

collective::~collective()
{
	switch (type)
	{
		case HOSTING:
			free (data);
			MPI_Win_free(&win);
			break;
		case LEECHING:
			MPI_Win_free(&win);
			break;
	}
}

DATA_TYPE *collective::get_data_ptr()
{
	return data;
}

void collective::create_window()
{
	switch (type)
	{
		case HOSTING:
			MPI_Alloc_mem(data_size * sizeof(DATA_TYPE), MPI_INFO_NULL, &data);
			break;
		case LEECHING:
			break;
	}
}

void collective::open_window()
{
	switch (type)
	{
		case HOSTING:
			MPI_Win_create(data, data_size*sizeof(DATA_TYPE), sizeof(DATA_TYPE), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
			break;
		case LEECHING:
			MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win);
			break;
	}
}

void collective::lock(int target_rank)
{
	this->target_rank = target_rank;
	switch (type)
	{
		case HOSTING:
			break;
		case LEECHING:
			MPI_Win_lock(MPI_LOCK_SHARED, target_rank, MPI_MODE_NOCHECK, win);
			break;
	}
}

void collective::unlock()
{
	switch (type)
	{
		case HOSTING:
			break;
		case LEECHING:
			MPI_Win_unlock(target_rank, win);
			break;
	}
}

void collective::put(DATA_TYPE *origin, long size, long target_disp)
{
	switch (type)
	{
		case HOSTING:
			if (target_rank == rank)
			{
				memcpy(data+target_disp, origin, size*sizeof(DATA_TYPE));
			}
			break;
		case LEECHING:
			MPI_Put(origin, size, MPI_DATA_TYPE, target_rank, target_disp, size, MPI_DATA_TYPE, win);
			break;
	}
}

void collective::get(DATA_TYPE *dest, long size, long target_disp)
{
	switch (type)
	{
		case HOSTING:
			if (target_rank == rank)
			{
				memcpy(dest, data+target_disp, size*sizeof(DATA_TYPE));
			}
			break;
		case LEECHING:
			MPI_Get(dest, size, MPI_DATA_TYPE, target_rank, target_disp, size, MPI_DATA_TYPE, win);
			break;
	}
}

void collective::scatter(DATA_TYPE *dest, long block_size, int target_rank)
{
	// MPI_Scatter(data, BLOCK_SIZE, MPI_DATA_TYPE, local_data, BLOCK_SIZE, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	lock(target_rank);
	get(dest, block_size, rank*block_size);
	unlock();
}

void collective::gather(DATA_TYPE *src, long block_size, int target_rank)
{
	// MPI_Scatter(data, BLOCK_SIZE, MPI_DATA_TYPE, local_data, BLOCK_SIZE, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	lock(target_rank);
	put(src, block_size, rank*block_size);
	unlock();
}