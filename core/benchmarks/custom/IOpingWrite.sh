#!/bin/bash

DATA_SIZE_END=128
OUTPUT_FILE="result/ioping_write.txt"

WRITE_PATH=$1

rm -rf $OUTPUT_FILE

for (( size=1; size<1024; ))
do
        echo -n "$size B, " >> $OUTPUT_FILE
	ioping -W -WWW -q $WRITE_PATH -s $((size))b -c 8 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE 
	(( size *= 2 ))
done

for (( size=1; size<1024; ))
do
	echo -n "$size KB, " >> $OUTPUT_FILE
	ioping -W -WWW -q $WRITE_PATH -s $((size))k -c 8 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE
	(( size *= 2 ))
done

for (( size=1; size<=DATA_SIZE_END; ))
do
    echo -n "$size MB, " >> $OUTPUT_FILE
    ioping -W -WWW -q $WRITE_PATH -s $((size))m -c 4 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE
    (( size *= 2 ))
done

echo >> $OUTPUT_FILE
