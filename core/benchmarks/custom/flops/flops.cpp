#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#define SIZE 100000000L

int main(void){
	std::clock_t start;
	double duration, offset;
	float y = 3.14;
	float x = 3.2;
	float z = 5.21;
	float w = 9.8;

	start=std::clock();

	for(long i=0; i <= SIZE; i++)
	{
		y = x * 2 * (y + z * w);
	}
	
	duration = ( std::clock() - start)/ (double) CLOCKS_PER_SEC;
	
	start=std::clock();

	for(long i=0; i <= SIZE; i++)
        {
        }

	offset = ( std::clock() - start)/ (double) CLOCKS_PER_SEC;

	float flops = ( (float)(SIZE * 4) / duration)/1000.0;
	duration -= offset;
	printf("Time taken: %f (%f) %f\n", duration, offset, y);
	printf("Flops: %f", flops);
	return 0;
}
