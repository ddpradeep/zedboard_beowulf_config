#!/bin/bash
	
	#VECT_SIZE=(1000 10000 100000 500000 1000000 5000000 10000000 20000000 100000000)

SAMPLE_SIZE=100
START_VECTOR_SIZE=1
END_VECTOR_SIZE=10000
START_NODE_SIZE=2
END_NODE_SIZE=26

OUTPUT_FILE="results/one_sided_latency.txt"

rm -rf $OUTPUT_FILE

# Create the log files
if [ ! -f $OUTPUT_FILE ]; then
	touch $OUTPUT_FILE
fi

echo "Starting Single Core Benchmark..."
for ((NO_OF_NODES=$START_NODE_SIZE; NO_OF_NODES<=END_NODE_SIZE; NO_OF_NODES++))
do
	for ((VECTOR_SIZE=$START_VECTOR_SIZE; VECTOR_SIZE<1024; ))
	do
		./run SingleOneToManyAsyncNonBlockingTransfers $NO_OF_NODES 1 $VECTOR_SIZE 1024 >> $OUTPUT_FILE
		VECTOR_SIZE=$(( VECTOR_SIZE = VECTOR_SIZE*2 ))
	done

	for ((VECTOR_SIZE=1024; VECTOR_SIZE<=1024*1024; ))
	do
		./run SingleOneToManyAsyncNonBlockingTransfers $NO_OF_NODES 1 $VECTOR_SIZE 96 >> $OUTPUT_FILE
		VECTOR_SIZE=$(( VECTOR_SIZE = VECTOR_SIZE*2 ))
	done
	
done
printf "\rDone    \n"
