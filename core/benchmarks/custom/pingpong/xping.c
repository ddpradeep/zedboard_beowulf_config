/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define INFO 		0

#define MASTER 0
#define NOT_ASSIGNED -1

typedef unsigned char byte;

void allread  (int fd, byte *buf, int len);
void allwrite (int fd, byte *buf, int len);

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	byte *Data;										// vector Data
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	long long int i, j, k;	// vector size
	unsigned long VECTOR_SIZE, REPEAT_COUNT;						// elements per process	
	int fd;
	int fd1;
	MPI_Status status;

	/* Start Profiling */
	struct timeval time1, time2, time3, time4;
	float *s2mTime, *m2sTime, *fwTime, *frTime;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank

	/***************************************************************************/
	/* Initialize */
	/***************************************************************************/

	// Get data from args
	VECTOR_SIZE 		= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);
	fd = open(argv[3], O_WRONLY);
	fd1 = open(argv[4], O_RDONLY);
	if (fd < 0 || fd1 < 0) 
	{
	    perror("Failed to open devfile");
	    exit(1);
	 }

	// Do initial printfs
	if (rank == MASTER)
	{
		#if INFO==1
			printf ("Vector Size:\t%lu (%.2fKB)\tRepeat Count: %lu\n", VECTOR_SIZE, (float)(sizeof(byte)* VECTOR_SIZE)/(1024.0), REPEAT_COUNT);
		#else
			printf ("%lu, %.2fKB, ", VECTOR_SIZE, (float)(sizeof(byte) * VECTOR_SIZE)/(1024.0));
		#endif
	}
	MPI_Alloc_mem(VECTOR_SIZE * sizeof(byte), MPI_INFO_NULL, &Data);

	for(i=0;i<VECTOR_SIZE;i++)
	{
		Data[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
	}

	s2mTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	m2sTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	fwTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	frTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

	if (rank == MASTER) 
	{
		for(i=0;i<VECTOR_SIZE;i++)
		{
			Data[i] = i;		// Allocate NOT_ASSIGNED value
		}
	}
    /***************************************************************************/

	/***************************************************************************/
	/*  I) Master: Get From PL */
	/***************************************************************************/
	if (rank == MASTER)
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			for (j=0; j<VECTOR_SIZE/4; j++)
				allwrite(fd, Data+j*4, 4);
			// allwrite(fd, Data, VECTOR_SIZE);
			resetTime(&time1);
			for (j=0; j<VECTOR_SIZE/4; j++)
				allread(fd1, Data+j*4, 4);
			// allread(fd1, Data, VECTOR_SIZE);
			frTime [i] = getTime(&time1);							// Get time spent on local and RMA processing
			// for (j=0; j<VECTOR_SIZE; j++)
			// 	printf("Read [%lld]: %d\n", j, Data[j]);
		}
	}

	/***************************************************************************/
	/*  II) Master: Send to Slave */
	/***************************************************************************/
	if (rank == MASTER)
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			MPI_Barrier (MPI_COMM_WORLD);
			resetTime(&time2);
			MPI_Send (Data, VECTOR_SIZE, MPI_UNSIGNED_CHAR, 1, 1, MPI_COMM_WORLD);
			m2sTime [i] = getTime(&time2);							// Get time spent on local and RMA processing
		}
		// for (j=0; j<VECTOR_SIZE; j++)
		// 	printf("%d)Sent [%lld]: %d\n", rank, j, Data[j]);
	}
	else
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			MPI_Barrier (MPI_COMM_WORLD);
			MPI_Recv (Data, VECTOR_SIZE, MPI_UNSIGNED_CHAR, MASTER, 1, MPI_COMM_WORLD, &status);
		}
		// for (j=0; j<VECTOR_SIZE; j++)
		// 	printf("%d)Recv [%lld]: %d\n", rank, j, Data[j]);
	}

	/***************************************************************************/
	/*  III)	Slave: Send to PL 	*/
	/*	IV)		Slave: Recv from PL */
	/***************************************************************************/
	if (rank == 1)
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			resetTime(&time1);
			for (j=0; j<VECTOR_SIZE/4; j++)
				allwrite(fd, Data+j*4, 4);
			fwTime [i] = getTime(&time1);							// Get time spent on local and RMA processing
			resetTime(&time1);
			for (j=0; j<VECTOR_SIZE/4; j++)
				allread(fd1, Data+j*4, 4);
			frTime [i] = getTime(&time1);							// Get time spent on local and RMA processing
			// for (j=0; j<VECTOR_SIZE; j++)
			// 	printf("Read [%lld]: %d\n", j, Data[j]);
		}
	}

	/***************************************************************************/
	/*   V) Slave: Send to Master */
	/***************************************************************************/
	if (rank != MASTER)
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			MPI_Barrier (MPI_COMM_WORLD);
			resetTime(&time2);
			MPI_Send (Data, VECTOR_SIZE, MPI_UNSIGNED_CHAR, MASTER, rank, MPI_COMM_WORLD);
			s2mTime [i] = getTime(&time2);							// Get time spent on local and RMA processing
		}
	}
	else
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			MPI_Barrier (MPI_COMM_WORLD);
			MPI_Recv (Data, VECTOR_SIZE, MPI_UNSIGNED_CHAR, 1, 1, MPI_COMM_WORLD, &status);
		}
	}

	/***************************************************************************/
	/*   VI) Master: Send to PL */
	/***************************************************************************/
	if (rank == MASTER)
	{
		for (i=0; i<REPEAT_COUNT; i++)
		{
			resetTime(&time1);
			for (j=0; j<VECTOR_SIZE/4; j++)
				allwrite(fd, Data+j*4, 4);
			fwTime [i] = getTime(&time1);							// Get time spent on local and RMA processing
		}
	}

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Process Timing Behavior */
	/***************************************************************************/
	if (rank == MASTER)
	{
		float _1_pl2m, _2_m2s, _3_s2pl, _4_pl2s, _5_s2m, _6_m2pl, _total_time;

		// 1) PL to MASTER
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1; j++)
				if (frTime[j]<frTime[j+1])
				{
					float temp = frTime[j];
					frTime[j] = frTime[j+1];
					frTime[j+1] = temp;
				}
		_1_pl2m = frTime [REPEAT_COUNT/2];

		// 2) MASTER to SLAVE
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1; j++)
				if (m2sTime[j]<m2sTime[j+1])
				{
					float temp = m2sTime[j];
					m2sTime[j] = m2sTime[j+1];
					m2sTime[j+1] = temp;
				}
		_2_m2s = m2sTime [REPEAT_COUNT/2];

		// 3) SLAVE to PL
		MPI_Recv (&_3_s2pl, 1, MPI_FLOAT, 1, 1, MPI_COMM_WORLD, &status);

		// 4) PL to SLAVE
		MPI_Recv (&_4_pl2s, 1, MPI_FLOAT, 1, 2, MPI_COMM_WORLD, &status);

		// 5) SLAVE to MASTER
		MPI_Recv (&_5_s2m, 1, MPI_FLOAT, 1, 3, MPI_COMM_WORLD, &status);

		// 6) MASTER to PL
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1; j++)
				if (fwTime[j]<fwTime[j+1])
				{
					float temp = fwTime[j];
					fwTime[j] = fwTime[j+1];
					fwTime[j+1] = temp;
				}
		_6_m2pl = fwTime [REPEAT_COUNT/2];

		// Offset Overhead
		resetTime(&time2);
		resetTime(&time1);
		getTime(&time1);
		float overhead = getTime(&time2);

		// Round Trip Time
		_total_time = _1_pl2m + _2_m2s + _3_s2pl + _4_pl2s + _5_s2m + _6_m2pl;

		#if INFO == 1
			printf (" PL2M:\t\t\t%f ms\n M2S:\t\t\t%f ms\n S2PL:\t\t\t%f ms\n PL2S:\t\t\t%f ms\n S2M:\t\t\t%f ms\n M2PL:\t\t\t%f ms\n Round Trip Total:\t%f ms\n", _1_pl2m*1000.0, _2_m2s*1000.0, _3_s2pl*1000.0, _4_pl2s*1000.0,_5_s2m*1000.0, _6_m2pl*1000.0, _total_time*1000.0);
		#else
			printf ("%f ms, %f ms, %f ms, %f ms, %f ms, %f ms, %f ms\n", _1_pl2m*1000.0, _2_m2s*1000.0, _3_s2pl*1000.0, _4_pl2s*1000.0,_5_s2m*1000.0, _6_m2pl*1000.0, _total_time*1000.0);
		#endif
	}
	else
	{
		// 3) SLAVE to PL
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (fwTime[j]<fwTime[j+1])
				{
					float temp = fwTime[j];
					fwTime[j] = fwTime[j+1];
					fwTime[j+1] = temp;
				}

		// 4) PL to SLAVE
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (frTime[j]<frTime[j+1])
				{
					float temp = frTime[j];
					frTime[j] = frTime[j+1];
					frTime[j+1] = temp;
				}

		// 5) SLAVE to MASTER
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (s2mTime[j]<s2mTime[j+1])
				{
					float temp = s2mTime[j];
					s2mTime[j] = s2mTime[j+1];
					s2mTime[j+1] = temp;
				}

		MPI_Send (&fwTime[REPEAT_COUNT*1/2], 1, MPI_FLOAT, MASTER, 1, MPI_COMM_WORLD);
		MPI_Send (&frTime[REPEAT_COUNT*1/2], 1, MPI_FLOAT, MASTER, 2, MPI_COMM_WORLD);
		MPI_Send (&s2mTime[REPEAT_COUNT*1/2], 1, MPI_FLOAT, MASTER, 3, MPI_COMM_WORLD);
	}
    /***************************************************************************/

	free (Data);

    MPI_Finalize ();

	return 0;
}

void allwrite(int fd, byte *buf, int len) {
  int sent = 0;
  int rc;

  while (sent < len) {
    rc = write(fd, buf + sent, sizeof(byte) * (len - sent));
	
    if ((rc < 0) && (errno == EINTR))
      continue;

    if (rc < len*sizeof(byte)) {
      perror("allwrite() failed to write");
      exit(1);
    }
	
    if (rc == 0) {
      fprintf(stderr, "Reached write EOF (?!)\n");
      exit(1);
    }
 
    sent += rc;
  }
}


void allread(int fd, byte *buf, int len) {
  int received = 0;
  int rc;

  while (received < len) {
    rc = read(fd, buf + received, sizeof (byte) * (len - received));
	
    if ((rc < 0) && (errno == EINTR))
      continue;

    if (rc < len*sizeof(byte)) {
      perror("allread() failed to read");
      exit(1);
    }
	
    if (rc == 0) {
      fprintf(stderr, "Reached read EOF (?!)\n");
      exit(1);
    }
 
    received += rc;
  }
} 
