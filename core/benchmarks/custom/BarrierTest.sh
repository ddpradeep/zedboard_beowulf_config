#!/bin/bash

NODE_END=26
OUTPUT_FILE="results/barrier_overhead.txt"

rm -rf $OUTPUT_FILE

for (( size=1; size<=NODE_END; size++))
do
	./run barrier $size 1 1000 >> $OUTPUT_FILE
done

exit