/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep								     */
/*****************************************************/ 


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO 		0
#define MIX_REMOTE_LOCAL_ACCESS	0

#define MASTER 0
#define NOT_ASSIGNED -1
#define ASSIGNED 10

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

void inline busyWait (float wait)
{
	struct timeval time1;
	resetTime(&time1);
	
	while (getTime(&time1) < wait)
	{
	  ;
	}


}

int main (int argc, char *argv[]) 
{
	int * Data;										// vector Data
	
	int *randLoc;
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	unsigned long size;
	long long int i, j, k;	// vector size
	unsigned long VECTOR_SIZE, REPEAT_COUNT;						// elements per process	
	
	MPI_Status status;
	MPI_Group comm_group, group;
    MPI_Win win;

	/* Start Profiling */
	struct timeval time1, time2, time3, time4;
	float *gTime, *pTime, *gtTime, *ptTime;
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank
	
    //MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
	int minArrayPtr, maxArrayPtr;

	/***************************************************************************/
	/* Initialize */
	/***************************************************************************/

	VECTOR_SIZE 		= atoi(argv[1]);
	REPEAT_COUNT 		= atoi(argv[2]);

	if (rank == MASTER)
	{
		#if INFO==1
			printf ("Vector Size:\t%lu (%.2fKB)\tRepeat Count: %lu\n", VECTOR_SIZE, (float)(sizeof(int)* VECTOR_SIZE)/(1024.0), REPEAT_COUNT);
		#else
			printf ("%d %lu %.2f KB ", total_proc, VECTOR_SIZE, (float)(sizeof(int) * VECTOR_SIZE)/(1024.0));
		#endif
	}
	size = total_proc * VECTOR_SIZE;
	MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &Data);
	MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

	for(i=0;i<total_proc * VECTOR_SIZE;i++)
	{
		Data[i] = NOT_ASSIGNED;		// Allocate NOT_ASSIGNED value
	}

	minArrayPtr = rank*VECTOR_SIZE;							// The min index of local memory
	maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;				// The max index of local memory

	gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

	gtTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
	ptTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

	if (rank == MASTER) 
	{
		for(i=0;i<total_proc * VECTOR_SIZE;i++)
		{
			Data[i] = ASSIGNED;		// Allocate NOT_ASSIGNED value
		}

		MPI_Win_create(Data, size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    }
    else
	{
		MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win); 
    }
    /***************************************************************************/

	/***************************************************************************/
	/* Get Test */
	/***************************************************************************/
	for (i=0; i<REPEAT_COUNT; i++)
	{
		MPI_Barrier (MPI_COMM_WORLD);

	    if (rank!=MASTER)
	    {
	        resetTime(&time1);

	        MPI_Win_lock(MPI_LOCK_SHARED,MASTER,0,win);
			MPI_Get(Data+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, win);

			gTime [i] = getTime(&time1);							// Get time spent on local and RMA processing

			resetTime(&time2);
			MPI_Win_unlock(MASTER,win);
			gtTime [i] = getTime(&time2);

			for (j=minArrayPtr; j<=minArrayPtr; j++)
				if (Data[j]!=ASSIGNED)
					printf ("GET(%llu): Data Transfer Error!\n\n", i);
		}
	}
	/***************************************************************************/

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Put Test */
	/***************************************************************************/
	for (i=0; i<REPEAT_COUNT; i++)
	{
		MPI_Barrier (MPI_COMM_WORLD);

		if (rank != MASTER)
		{
			resetTime(&time2);

			MPI_Win_lock(MPI_LOCK_SHARED,MASTER,MPI_MODE_NOCHECK,win);
	        MPI_Put(Data+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, minArrayPtr, VECTOR_SIZE, MPI_INT, win);

			pTime [i] = getTime(&time2);							// Get time spent on local and RMA processing

			resetTime(&time1);
			MPI_Win_unlock(MASTER,win);
			ptTime [i] = getTime(&time1);
		}
	}
	/***************************************************************************/

	MPI_Barrier (MPI_COMM_WORLD);

	/***************************************************************************/
	/* Print Results */
	/***************************************************************************/
	if (rank == MASTER)
	{
		float getTimeMax = 0;
		float putTimeMax = 0;
		float getTransferTimeMax = 0;
		float putTransferTimeMax = 0;

		float getTotalTime;
		float putTotalTime;
		for (i=1; i<total_proc; i++)
		{
			MPI_Recv (&gTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
			MPI_Recv (&gtTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
			MPI_Recv (&pTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
			MPI_Recv (&ptTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
			// if (gTime[0]>getTimeMax)
			// 	getTimeMax = gTime[0];
			// if (pTime[0]>putTimeMax)
			// 	putTimeMax = pTime[0];
			getTimeMax += gTime[0];
			putTimeMax += pTime[0];
			getTransferTimeMax += gtTime[0];
			putTransferTimeMax += ptTime[0];

			getTotalTime += (gTime[0] + gtTime[0]);
			putTotalTime += (pTime[0] + ptTime[0]);
		}

		// Average the timings
		getTimeMax /= (float)(total_proc-1);
		putTimeMax /= (float)(total_proc-1);

		getTransferTimeMax /= (float)(total_proc-1);
		putTransferTimeMax /= (float)(total_proc-1);

		// Multiply for bandwidth calculation
		
		#if INFO == 1
			printf ("Get Speed: %.2fMB/s (%f ms, %f ms)\t Put Speed: %.2fMB/s (%f ms, %f ms)\n", 
				(float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/((getTotalTime)*1048576.0), 
				getTimeMax*1000.0, 
				getTransferTimeMax*1000.0,
				(float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/((putTotalTime)*1048576.0), 
				putTimeMax*1000.0, 
				putTransferTimeMax*1000.0);
		#else
			printf ("%.2f MB/s %f ms %f ms %.2f MB/s %f ms %f ms\n", 
				(float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/((getTotalTime)*1048576.0), 
				getTimeMax*1000.0, 
				getTransferTimeMax*1000.0,
				(float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/((putTotalTime)*1048576.0), 
				putTimeMax*1000.0, 
				putTransferTimeMax*1000.0);
		#endif
	}
	else
	{
		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (gTime[j]<gTime[j+1])
				{
					float temp = gTime[j];
					gTime[j] = gTime[j+1];
					gTime[j+1] = temp;
				}

		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (pTime[j]<pTime[j+1])
				{
					float temp = pTime[j];
					pTime[j] = pTime[j+1];
					pTime[j+1] = temp;
				}

		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (gtTime[j]<gtTime[j+1])
				{
					float temp = gtTime[j];
					gtTime[j] = gtTime[j+1];
					gtTime[j+1] = temp;
				}

		for (i=1; i<REPEAT_COUNT; i++)
			for (j=0; j<REPEAT_COUNT-1-i; j++)
				if (ptTime[j]<ptTime[j+1])
				{
					float temp = ptTime[j];
					ptTime[j] = ptTime[j+1];
					ptTime[j+1] = temp;
				}

		  // Ignore 0 second results
		for (i=REPEAT_COUNT/2; i>=0; i--)
			if (gTime[i]!=0)
			{
				gTime[REPEAT_COUNT/2] = gTime [i];
				break;
			}

		for (i=REPEAT_COUNT/2; i>=0; i--)
			if (gtTime[i]!=0)
			{
				gtTime[REPEAT_COUNT/2] = gtTime [i];
				break;
			}

		for (i=REPEAT_COUNT/2; i>=0; i--)
			if (pTime[i]!=0)
			{
				pTime[REPEAT_COUNT/2] = pTime [i];
				break;
			}

		for (i=REPEAT_COUNT/2; i>=0; i--)
			if (ptTime[i]!=0)
			{
				ptTime[REPEAT_COUNT/2] = ptTime [i];
				break;
			}

		MPI_Send (&gTime[REPEAT_COUNT/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		MPI_Send (&gtTime[REPEAT_COUNT/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		MPI_Send (&pTime[REPEAT_COUNT/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
		MPI_Send (&ptTime[REPEAT_COUNT/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
	}
    /***************************************************************************/

	free (Data);

    MPI_Group_free(&comm_group);
    MPI_Win_free(&win);
    MPI_Finalize ();

	return 0;
}
