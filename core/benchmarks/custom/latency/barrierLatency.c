 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO        0

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

#define MASTER 0

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
    
    int total_proc;                                 // total number of processes    
    int rank;                                       // rank of each process
    int repeatCount = 1;
    int loopCounter=repeatCount;                    // Repeats for averaging
    long long int i, j; // vector size
    unsigned long REPEAT_COUNT;                      // elements per process 
    
    MPI_Status status;

    /* Start Profiling */
    struct timeval timeT, timeC, timeS, timeR;
    float *barrierTime;

    REPEAT_COUNT          = atoi(argv[1]);

    barrierTime = (float *) malloc (sizeof(float)*REPEAT_COUNT);
 
    MPI_Init (&argc, &argv);                        // Initialization of MPI environment
    MPI_Comm_size (MPI_COMM_WORLD, &total_proc);    // Get total processes count
    MPI_Comm_rank (MPI_COMM_WORLD,&rank);           // Get current process rank
    
    int * ap;
    int * bp;
    int * cp;

    for (i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier(MPI_COMM_WORLD);

        resetTime(&timeS);
        MPI_Barrier(MPI_COMM_WORLD);
        barrierTime [i] = getTime(&timeS);
    }

    if (rank == MASTER) // For master process
    {
        for (i=1; i<REPEAT_COUNT; i++)
            for (j=0; j<REPEAT_COUNT; j++)
                if (barrierTime[j]<barrierTime[j+1])
                {
                    float temp = barrierTime[j];
                    barrierTime[j] = barrierTime[j+1];
                    barrierTime[j+1] = temp;
                }

        // Find average instead of median
        float temp_sum=0;
        int temp_count=0;
        for (i=REPEAT_COUNT/4; i<REPEAT_COUNT*0.75; i++)
        {
            temp_sum += barrierTime[i];
            temp_count ++;
        }

        barrierTime [REPEAT_COUNT/2] = temp_sum/(float)temp_count;

        for (i=1; i<REPEAT_COUNT; i++)
            ; // printf ("%f\n", barrierTime[i]);
        #if INFO==1
            printf ("Computation (P0)\t\t%.4f ms [%.2f, %.2f]\n",   (float)barrierTime*1000.0, 0.0, 0.0);
            printf ("\nCommunication \t\t\t%.2f ms [%.2f, %.2f]\n\n",   (float)(timeAcc - barrierTime)*1000.0, 0.0, 0.0);
            printf("Time time taken:\t%f s\n", timeAcc/(float)repeatCount);
        #else
            printf ("%d, %f, %f, %f\n", total_proc, barrierTime[REPEAT_COUNT/2]*1000.0, barrierTime[0]*1000.0, barrierTime[REPEAT_COUNT-1]*1000.0);
        #endif
    }
    
    MPI_Finalize ();
    //printf ("Process %d Finished\n", rank);

    return 0;
}
