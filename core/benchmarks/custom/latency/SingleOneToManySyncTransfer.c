/*****************************************************/
/* MPI Network Latency and Computation Analysis Tool */
/* Author: Pradeep                                   */
/*****************************************************/ 


#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define INFO        1
#define MIX_REMOTE_LOCAL_ACCESS 0

#define MASTER 0
#define NOT_ASSIGNED -1

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
    int * Data;                                     // vector Data
    
    int *randLoc;
    
    int total_proc;                                 // total number of processes    
    int rank;                                       // rank of each process
    unsigned long size;
    long long int i, j, k;  // vector size
    unsigned long VECTOR_SIZE, REPEAT_COUNT;                        // elements per process 
    
    MPI_Status status;
    MPI_Group comm_group, group;

    /* Start Profiling */
    struct timeval time1, time2, time3, time4;
    float *gTime, *pTime;
 
    MPI_Init (&argc, &argv);                        // Initialization of MPI environment
    MPI_Comm_size (MPI_COMM_WORLD, &total_proc);    // Get total processes count
    MPI_Comm_rank (MPI_COMM_WORLD,&rank);           // Get current process rank
    
    //MPI_Group_incl(comm_group, 1, &rankInGroup, &group);
    int minArrayPtr, maxArrayPtr;

    /***************************************************************************/
    /* Initialize */
    /***************************************************************************/

    VECTOR_SIZE         = atoi(argv[1]);
    REPEAT_COUNT        = atoi(argv[2]);

    if (rank == MASTER)
    {
        #if INFO==1
            printf ("Vector Size:\t%lu (%.2fKB)\tRepeat Count: %lu\n", VECTOR_SIZE, (float)(sizeof(int)* VECTOR_SIZE)/(1024.0), REPEAT_COUNT);
        #else
            printf ("%lu, %.2fKB, %d, %f, ", VECTOR_SIZE, (float)(sizeof(int) * VECTOR_SIZE)/(1024.0), 0, 0.0);
        #endif
    }
    size = total_proc * VECTOR_SIZE;
    MPI_Alloc_mem(size * sizeof(int), MPI_INFO_NULL, &Data);
    MPI_Comm_group(MPI_COMM_WORLD, &comm_group);

    for(i=0;i<total_proc * VECTOR_SIZE;i++)
    {
        Data[i] = NOT_ASSIGNED;     // Allocate NOT_ASSIGNED value
    }

    minArrayPtr = rank*VECTOR_SIZE;                         // The min index of local memory
    maxArrayPtr = minArrayPtr+VECTOR_SIZE-1;                // The max index of local memory

    gTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);
    pTime = (float *) malloc(sizeof(float)*REPEAT_COUNT);

    if (rank == MASTER) 
    {
        for(i=0;i<total_proc * VECTOR_SIZE;i++)
        {
            Data[i] = i;        // Allocate NOT_ASSIGNED value
        }
    }

    /***************************************************************************/

    /***************************************************************************/
    /* Receive Test */
    /***************************************************************************/
    for (i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier (MPI_COMM_WORLD);

        if (rank!=MASTER)
        {
            resetTime(&time1);
            MPI_Recv (Data+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);                             
            gTime [i] = getTime(&time1);
            
            for (j=minArrayPtr; j<maxArrayPtr; j++)
                if (Data[j]!=j)
                    printf ("GET: Data Transfer Error! Expected %llu, received %d\n\n", j, Data[j]);
        }
        else
        {
            resetTime(&time1);
        	for (k=1; k<total_proc; k++)
        		MPI_Send (Data+k*VECTOR_SIZE, VECTOR_SIZE, MPI_INT, k, k, MPI_COMM_WORLD);
            pTime [i] = getTime(&time1);
        }
    }
    /***************************************************************************/

    MPI_Barrier (MPI_COMM_WORLD);

    /***************************************************************************/
    /* Put Test */
    /***************************************************************************/
    for (i=0; i<REPEAT_COUNT; i++)
    {
        MPI_Barrier (MPI_COMM_WORLD);

        if (rank != MASTER)
        {
            resetTime(&time2);
            MPI_Send (Data+minArrayPtr, VECTOR_SIZE, MPI_INT, MASTER, rank, MPI_COMM_WORLD);
            pTime [i] = getTime(&time2);
        }
        else
        {
            resetTime(&time2);
        	for (j=1; j<total_proc; j++)
        		MPI_Recv (Data+minArrayPtr, VECTOR_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            gTime [i] = getTime(&time2);
        }
    }
    /***************************************************************************/

    MPI_Barrier (MPI_COMM_WORLD);

    /***************************************************************************/
    /* Print Results */
    /***************************************************************************/
    if (rank == MASTER)
    {
        for (i=1; i<REPEAT_COUNT; i++)
            for (j=0; j<REPEAT_COUNT; j++)
                if (gTime[j]<gTime[j+1])
                {
                    float temp = gTime[j];
                    gTime[j] = gTime[j+1];
                    gTime[j+1] = temp;
                }

       for (i=1; i<REPEAT_COUNT; i++)
            for (j=0; j<REPEAT_COUNT; j++)
                if (pTime[j]<pTime[j+1])
                {
                    float temp = pTime[j];
                    pTime[j] = pTime[j+1];
                    pTime[j+1] = temp;
                }

        float getTimeMax = gTime[REPEAT_COUNT*1/2];
        float putTimeMax = pTime[REPEAT_COUNT*1/2];
        for (i=1; i<total_proc; i++)
        {
            MPI_Recv (&gTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
            MPI_Recv (&pTime[0], 1, MPI_FLOAT, i, i, MPI_COMM_WORLD, &status);
            // if (gTime[0]>getTimeMax)
            //     getTimeMax = gTime[0];
            // if (pTime[0]>putTimeMax)
            //     putTimeMax = pTime[0];
            // getTimeMax += gTime[0];
            // putTimeMax += pTime[0];
		}

        // getTimeMax /= (total_proc-1);
        // putTimeMax /= (total_proc-1);
		// Average the timings
		// getTimeMax /= (float)(total_proc-1);
		// putTimeMax /= (float)(total_proc-1);

		// // Multiply for bandwidth calculation
		// getTimeMax /= (float)(total_proc-1);
		// putTimeMax /= (float)(total_proc-1);

        #if INFO == 1
            printf ("Throughtput: %.2fMB/s (%f ms)\n", (float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/(getTimeMax*1048576.0), getTimeMax*1000.0);
        #else
            printf ("%.2fMB/s, %f\n", (float)(sizeof(int)*VECTOR_SIZE*(total_proc-1))/(getTimeMax*1048576.0), getTimeMax*1000.0);
        #endif
    }
    else
    {
        for (i=1; i<REPEAT_COUNT; i++)
            for (j=0; j<REPEAT_COUNT; j++)
                if (gTime[j]<gTime[j+1])
                {
                    float temp = gTime[j];
                    gTime[j] = gTime[j+1];
                    gTime[j+1] = temp;
                }

       for (i=1; i<REPEAT_COUNT; i++)
            for (j=0; j<REPEAT_COUNT; j++)
                if (pTime[j]<pTime[j+1])
                {
                    float temp = pTime[j];
                    pTime[j] = pTime[j+1];
                    pTime[j+1] = temp;
                }
        MPI_Send (&gTime[REPEAT_COUNT*1/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
        MPI_Send (&pTime[REPEAT_COUNT*1/2], 1, MPI_FLOAT, MASTER, rank, MPI_COMM_WORLD);
    }
    /***************************************************************************/

    free (Data);

    MPI_Group_free(&comm_group);
    MPI_Finalize ();

    return 0;
}
