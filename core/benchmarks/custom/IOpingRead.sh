#!/bin/bash

DATA_SIZE_END=128
OUTPUT_FILE="result/ioping_read.txt"

READ_PATH=$1

rm -rf $OUTPUT_FILE

for (( size=1; size<1024; ))
do
    echo -n "$size B, " >> $OUTPUT_FILE
	ioping -R $READ_PATH -s $((size))b -c 1024 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE 
	(( size *= 2 ))
done

for (( size=1; size<1024; ))
do
	echo -n "$size KB, " >> $OUTPUT_FILE
	ioping -R $READ_PATH -s $((size))k -c 256 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE
	(( size *= 2 ))
done

for (( size=1; size<=DATA_SIZE_END; ))
do
    echo -n "$size MB, " >> $OUTPUT_FILE
    ioping -R $READ_PATH -s $((size))m -c 8 | grep -E 'ms | us | s ' | awk -F' /' '{print $1 $2 $3 $4}' | cut -d "=" -f2 >> $OUTPUT_FILE
    (( size *= 2 ))
done

echo >> $OUTPUT_FILE