#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include "helper-utils.h"

void config_console();

int main(int argc, char *argv[])
{

  int fd_write, fd_read;
  long rc;

  long DATASIZE, REPEAT_COUNT;
  TIME_DATA_TYPE start_time, end_time;
  float *time_to_read, *time_to_write;  

  int loopID;
  long i, j;

  /***********************************************************************************************/
  /*                                       Parse Input Arguments
  /***********************************************************************************************/

  if (argc!=5)
  {
    fprintf(stderr, "Usage: %s <devfile to write> <devfile to read> <data size in bytes> <repeat count>\n", argv[0]);
    exit(1);
  }

  // Get Data size and Repeat Count from input arguments
  DATASIZE      = atoi (argv[3])/sizeof(DATA_TYPE);
  REPEAT_COUNT  = atoi (argv[4]);

  // Configure standard input not to wait for CR
  config_console();

  // Open device for writing
  fd_write = open(argv[1], O_WRONLY);
  if (fd_write < 0)
  {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a read-only file?)\n", argv[1]);

    perror("Failed to open devfile");
    exit(1);
  }

  // Open device for reading
  fd_read = open(argv[2], O_RDONLY);
  if (fd_read < 0) {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a write-only file?)\n", argv[2]);

    perror("Failed to open devfile");
    exit(1);
  }

  /***********************************************************************************************/
  /*                                        Initialize
  /***********************************************************************************************/

  // Allocate and initialize send buffer of size [DATASIZE] with sequential values [0, DATASIZE-1]
  DATA_TYPE *sendBuf;
  sendBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);
  for (i=0; i<DATASIZE; i++)
    sendBuf[i] = i;

  // Allocate receive buffer of size [DATASIZE/2]
  DATA_TYPE *recvBuf;
  recvBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);

  // Allocate time measurement arrays
  time_to_read  = (float *) malloc(sizeof(float)*REPEAT_COUNT);
  time_to_write = (float *) malloc(sizeof(float)*REPEAT_COUNT);

  for (loopID=0; loopID<REPEAT_COUNT; loopID++)
  {

    // Generate a new sequence
    for (i=0; i<DATASIZE; i++)
      sendBuf[i] += 1;

    memset (recvBuf, 0, DATASIZE);

    /***********************************************************************************************/
    /*                                      Write Benchmark
    /***********************************************************************************************/

    // 1. Get Start Time
    resetTime(&start_time);

    // 2. Write data to stream
    rc = write(fd_write, sendBuf, DATASIZE*sizeof(DATA_TYPE));
    int rc2 = write(fd_write, NULL, 0);

    // 3. Calculate Time Taken
    time_to_write[loopID] = getTime(&start_time);

    debug ("Write time\t: %f\n", time_to_write[loopID]);

    // 4. Verify written count
    if (rc!=DATASIZE*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Write operation is incomplete!");
      exit(1);
    }

    // 5. Pause for 10ms before proceeding
    // usleep (10000);

    /***********************************************************************************************/
    /*                                      Read Benchmark
    /***********************************************************************************************/

    // 1. Get Start Time
    resetTime(&start_time);

    // 2. Read data from stream
    rc = read (fd_read, recvBuf, DATASIZE/2*sizeof(DATA_TYPE));
    
    // 3. Calculate Time Taken
    time_to_read [loopID] = getTime(&start_time);

    debug ("Read time\t: %f\n", time_to_read[loopID]);

    // 4. Verify read count
    if (rc!=DATASIZE/2*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Read operation is incomplete! Only read %lu bytes\n", rc);
      exit(1);
    }

    // 5. Verify that received values are indeed sum of consequtive numbers
    for (i=0; i<DATASIZE/2; i++)
    {
      if ((sendBuf[2*i]+sendBuf[2*i+1])!=recvBuf[i])
        printf ("Stream[%lu]\tExpected %d, Received %d\n", i, sendBuf[2*i]+sendBuf[2*i+1], recvBuf[i]);
    }

    // print (sendBuf, DATASIZE, " ");
    // print (recvBuf, DATASIZE, " ");
  }


  /***********************************************************************************************/
  /*                                    Analyze/Print Results
  /***********************************************************************************************/

  // Sort the timing results
  sort (time_to_write, REPEAT_COUNT);
  sort (time_to_read, REPEAT_COUNT);

  // Ignore 0 second results and get the median index
  int write_median_index = 0;
  for (i=REPEAT_COUNT/2; i>=0; i--)
    if (time_to_write[i]!=0)
    {
      write_median_index = i;
      break;
    }

  int read_median_index = 0;
  for (i=REPEAT_COUNT/2; i>=0; i--)
    if (time_to_read[i]!=0)
    {
      read_median_index = i;
      break;
    }

  printf ("%lu Bytes, Write %.3f ms %.3f MB/s, Read %.3f ms %.3f MB/s\n",
    DATASIZE*sizeof(DATA_TYPE),
    time_to_write[write_median_index]*1000.0,
    ((float)(DATASIZE*sizeof(DATA_TYPE))/(time_to_write[write_median_index]))/1048576.0,
    time_to_read[read_median_index]*1000.0,
    ((float)(DATASIZE/2*sizeof(DATA_TYPE))/(time_to_read[read_median_index]))/1048576.0);

  return 0;
}

/* config_console() does some good-old-UNIX vodoo standard input, so that
   read() won't wait for a carriage-return to get data. It also catches
   CTRL-C and other nasty stuff so it can return the terminal interface to
   what is was start_time. In short, a lot of mumbo-jumbo, with nothing relevant
   to Xillybus.
 */

void config_console() {
  struct termio console_attributes;

  if (ioctl(0, TCGETA, &console_attributes) != -1) {
    // If we got here, we're reading from console

    console_attributes.c_lflag &= ~ICANON; // Turn off canonical mode
    console_attributes.c_cc[VMIN] = 1; // One character at least
    console_attributes.c_cc[VTIME] = 0; // No timeouts

    if (ioctl(0, TCSETAF, &console_attributes) == -1)
      fprintf(stderr, "Warning: Failed to set console to char-by-char\n");
  }
}
