#!/bin/bash

DATA_SIZE_START=1
DATA_SIZE_END=128
OUTPUT_FILE="result/dd_benchmark.txt"

sudo mount /dev/sdd1 extHDD

rm -rf $OUTPUT_FILE

for (( size=1; size<=1024; ))
do
        echo -n "$size B, " >> $OUTPUT_FILE
        dd bs=$((size))B count=1024 if=/dev/zero of=extHDD/test conv=fdatasync 2>&1 | awk '/copied/ {print $8 " "  $9}' >> $OUTPUT_FILE
        (( size *= 2 ))
done

for (( size=1; size<=1024; ))
do
	echo -n "$size KB, " >> $OUTPUT_FILE
	dd bs=$((size))K count=128 if=/dev/zero of=extHDD/test conv=fdatasync 2>&1 | awk '/copied/ {print $8 " "  $9}' >> $OUTPUT_FILE
	(( size *= 2 ))
done

for (( size=1; size<=DATA_SIZE_END; ))
do
        echo -n "$size MB, " >> $OUTPUT_FILE
        dd bs=$((size))M count=8 if=/dev/zero of=extHDD/test conv=fdatasync 2>&1 | awk '/copied/ {print $8 " "  $9}' >> $OUTPUT_FILE
        (( size *= 2 ))
done

echo >> $OUTPUT_FILE
