#!/bin/bash
START_VECTOR_SIZE=1
END_VECTOR_SIZE=134217728

MPI_OUTPUT="result/round_trip_test.txt"

rm -rf $MPI_OUTPUT

# Create the log files
if [ ! -f $MPI_OUTPUT ]; then
	touch $MPI_OUTPUT
fi

echo "Starting Round Trip Benchmark..."
for ((VECTOR_SIZE=$START_VECTOR_SIZE; VECTOR_SIZE<=$END_VECTOR_SIZE; ))
do
	if (($VECTOR_SIZE <= 1024)) ; then
		REPEAT_COUNT=1000
		VECTOR_INCREMENT=10
	elif (($VECTOR_SIZE <= 1048576)) ; then
		REPEAT_COUNT=100
		VECTOR_INCREMENT=10240
	else
		REPEAT_COUNT=8
		VECTOR_INCREMENT=10485760
	fi

	./run pingpong $VECTOR_SIZE $REPEAT_COUNT >> $MPI_OUTPUT

	printf "%d %d\n" $VECTOR_SIZE $REPEAT_COUNT

	# printf "\r[$((($VECTOR_SIZE-$VECTOR_INCREMENT)*100/$END_VECTOR_SIZE))%%]"
	VECTOR_SIZE=$(($VECTOR_SIZE+$VECTOR_INCREMENT))
done
printf "\rDone    \n"