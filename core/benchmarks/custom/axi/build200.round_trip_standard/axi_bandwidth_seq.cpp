#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include "helper-utils.h"

void config_console();

int main(int argc, char *argv[])
{

  int fd_write, fd_read;
  long rc;

  long DATASIZE, REPEAT_COUNT;
  TIME_DATA_TYPE start_time_write, start_time_read, end_time_write, end_time_read;
  double *time_to_readwrite;

  int loopID;
  long i, j;

  /***********************************************************************************************/
  /*                                       Parse Input Arguments
  /***********************************************************************************************/

  if (argc!=5)
  {
    fprintf(stderr, "Usage: %s <devfile to write> <devfile to read> <data size in bytes> <repeat count>\n", argv[0]);
    exit(1);
  }

  // Get Data size and Repeat Count from input arguments
  DATASIZE      = atoi (argv[3])/sizeof(DATA_TYPE);
  REPEAT_COUNT  = atoi (argv[4]);

  // Configure standard input not to wait for CR
  config_console();

  // Open device for writing
  fd_write = open(argv[1], O_WRONLY);
  if (fd_write < 0)
  {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a read-only file?)\n", argv[1]);

    perror("Failed to open devfile");
    exit(1);
  }

  // Open device for reading
  fd_read = open(argv[2], O_RDONLY);
  if (fd_read < 0) {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a write-only file?)\n", argv[2]);

    perror("Failed to open devfile");
    exit(1);
  }

  /***********************************************************************************************/
  /*                                        Initialize
  /***********************************************************************************************/

  // Allocate and initialize send buffer of size [DATASIZE] with sequential values [0, DATASIZE-1]
  DATA_TYPE *sendBuf;
  sendBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);
  for (i=0; i<DATASIZE; i++)
    sendBuf[i] = i;

  // Allocate receive buffer of size [DATASIZE]
  DATA_TYPE *recvBuf;
  recvBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);

  // Allocate time measurement arrays
  time_to_readwrite = (double *) malloc(sizeof(double)*REPEAT_COUNT);

  for (loopID=0; loopID<REPEAT_COUNT; loopID++)
  {

  	// Generate a new write sequence and clear input buffer
    for (i=0; i<DATASIZE; i++)
      sendBuf[i] += 1;
    memset (recvBuf, 0, DATASIZE);

    /***********************************************************************************************/
    /*                                  Write -> Read Benchmark
    /***********************************************************************************************/

    // 1. Get start time
    resetTime(&start_time_write);
    int rc_w, rc_r;

    int BLOCK_COUNT = 100;
    for (int i=0; i<BLOCK_COUNT; i++)
    {
      // 2. Write data to stream
      rc_w  = write(fd_write, sendBuf, DATASIZE*sizeof(DATA_TYPE));
      int trash = write(fd_write, NULL, 0);

      // 5. Read data from stream
      rc_r = read (fd_read, recvBuf, DATASIZE*sizeof(DATA_TYPE));
    }

    // 6. Get end time (read)
    resetTime(&end_time_read);
    
    // 7. Calculate read, write, read+write timings
    time_to_readwrite [loopID]  = getTime(&start_time_write, &end_time_read)/BLOCK_COUNT;

    // 8. Verify transfer was complate
    if (rc_w!=DATASIZE*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Write operation is incomplete!");
      exit(1);
    }
    if (rc_r!=DATASIZE*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Read operation is incomplete!");
      exit(1);
    }

    // 9. Verify integrity of transfer
    for (i=0; i<DATASIZE; i++)
    {
      if (sendBuf[i]!=recvBuf[i])
      {
        fprintf(stderr, "Stream[%lu]\tExpected %d, Received %d\n", i, sendBuf[i], recvBuf[i]);
      }
    }
  }

  /***********************************************************************************************/
  /*                                    Analyze/Print Results
  /***********************************************************************************************/

  // Sort the timing results
  sort (time_to_readwrite, REPEAT_COUNT);

  // Ignore 0 second results
  int readwrite_median_index = 0;
  for (i=REPEAT_COUNT/2; i>=0; i--)
    if (time_to_readwrite[i]!=0)
    {
      readwrite_median_index = i;
      break;
    }

  // |              |           |           |     Actual Round Trip      |    
  // | Size (Bytes) | Size (KB) | Size (MB) | Latency, Throughput (MB/s) |
  printf ("bw-seq, %d, %d, %d, %.3f, %.3f\n", 
    (int)DATASIZE*sizeof(DATA_TYPE), ftoi(DATASIZE*sizeof(DATA_TYPE)/1024.0), ftoi(DATASIZE*sizeof(DATA_TYPE)/(1024*1024.0)),
    time_to_readwrite[readwrite_median_index]*1000.0,
    ((double)(2*DATASIZE*sizeof(DATA_TYPE))/(time_to_readwrite[readwrite_median_index]))/1048576.0
    );
  return 0;
}

/* config_console() does some good-old-UNIX vodoo standard input, so that
   read() won't wait for a carriage-return to get data. It also catches
   CTRL-C and other nasty stuff so it can return the terminal interface to
   what is was start_time. In short, a lot of mumbo-jumbo, with nothing relevant
   to Xillybus.
 */

void config_console() {
  struct termio console_attributes;

  if (ioctl(0, TCGETA, &console_attributes) != -1) {
    // If we got here, we're reading from console

    console_attributes.c_lflag &= ~ICANON; // Turn off canonical mode
    console_attributes.c_cc[VMIN] = 1; // One character at least
    console_attributes.c_cc[VTIME] = 0; // No timeouts

    if (ioctl(0, TCSETAF, &console_attributes) == -1)
      fprintf(stderr, "Warning: Failed to set console to char-by-char\n");
  }
}
