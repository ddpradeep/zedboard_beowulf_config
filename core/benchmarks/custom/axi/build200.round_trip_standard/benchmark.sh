#!/bin/bash

START_DATASIZE=4
END_DATASIZE=16777216 #1048576
SEGMENTS=20

OUTPUT_BANDWIDTH="bandwidth_seq.txt"
OUTPUT_TBANDWIDTH="bandwidth_mt.txt"

if [ $1 == "seq" ]
then
	# Remove Old Results
	rm -rf $OUTPUT_BANDWIDTH

	START_DATASIZES=(START_DATASIZE 1024 1024*1024)
	END_DATASIZES=(1024 1024*1024 END_DATASIZE)
	REPEAT_COUNTS=(2000 200 20)

	for ((i=0;i<${#START_DATASIZES[@]};++i))
	do
		for (( DATASIZE=${START_DATASIZES[i]}; DATASIZE < ${END_DATASIZES[i]}; ))
		do
			echo "./acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]}"
			./axi_bandwidth_seq.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_BANDWIDTH
			
			NEW_DATASIZE=$(( DATASIZE + (${END_DATASIZES[i]}-${START_DATASIZES[i]})/SEGMENTS ))
			if (( NEW_DATASIZE == DATASIZE )); then
				break
			fi
			DATASIZE=$NEW_DATASIZE
		done
	done
elif [ $1 == "mt" ]
then
	# Remove Old Results
	rm -rf $OUTPUT_TBANDWIDTH

	START_DATASIZES=(START_DATASIZE 1024 1024*1024)
	END_DATASIZES=(1024 1024*1024 END_DATASIZE)
	REPEAT_COUNTS=(2000 200 20)

	for ((i=0;i<${#START_DATASIZES[@]};++i))
	do
		for (( DATASIZE=${START_DATASIZES[i]}; DATASIZE < ${END_DATASIZES[i]}; ))
		do
			echo "./threaded_acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]}"
			./axi_bandwidth_mt.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_TBANDWIDTH
			
			NEW_DATASIZE=$(( DATASIZE + (${END_DATASIZES[i]}-${START_DATASIZES[i]})/SEGMENTS ))
			if (( NEW_DATASIZE == DATASIZE )); then
				break
			fi
			DATASIZE=$NEW_DATASIZE
		done
	done
fi