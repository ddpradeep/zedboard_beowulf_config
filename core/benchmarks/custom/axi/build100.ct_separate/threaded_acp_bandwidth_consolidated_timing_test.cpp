#define DATA_TYPE       int
#define MPI_DATA_TYPE   MPI_INT

// #define DEBUG // To enable debug outputs, comment this line to disable

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include <signal.h>
#include <time.h>
#include <thread>
#include "helper-utils.h"

void config_console ();
void sendToPL       (int fd_write, DATA_TYPE *sendBuf, long DATASIZE, float *time_to_write, int loopID, TIME_DATA_TYPE *start_time, TIME_DATA_TYPE *end_time);
void recvFromPL     (int fd_read, DATA_TYPE *recvBuf, long DATASIZE, float *time_to_read, int loopID, TIME_DATA_TYPE *start_time, TIME_DATA_TYPE *end_time);

int main(int argc, char *argv[])
{
  stick_this_thread_to_core(1);
  int fd_write, fd_read;
  long rc;

  long DATASIZE, REPEAT_COUNT;
  TIME_DATA_TYPE start_time, end_time;
  float *time_to_read, *time_to_write, *time_to_readwrite;

  int loopID;
  long i, j;

  /***********************************************************************************************/
  /*                                       Parse Input Arguments
  /***********************************************************************************************/

  if (argc!=5)
  {
    fprintf(stderr, "Usage: %s <devfile to write> <devfile to read> <data size in bytes> <repeat count>\n", argv[0]);
    exit(1);
  }

  // Get Data size and Repeat Count from input arguments
  DATASIZE      = atoi (argv[3])/sizeof(DATA_TYPE);
  REPEAT_COUNT  = atoi (argv[4]);

  // Configure standard input not to wait for CR
  config_console();

  // Open device for writing
  fd_write = open(argv[1], O_WRONLY);
  if (fd_write < 0)
  {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a read-only file?)\n", argv[1]);

    perror("Failed to open devfile");
    exit(1);
  }

  // Open device for reading
  fd_read = open(argv[2], O_RDONLY);
  if (fd_read < 0) {
    if (errno == ENODEV)
      fprintf(stderr, "(Maybe %s a write-only file?)\n", argv[2]);

    perror("Failed to open devfile");
    exit(1);
  }

  /***********************************************************************************************/
  /*                                        Initialize
  /***********************************************************************************************/

  // Allocate and initialize send buffer of size [DATASIZE] with sequential values [0, DATASIZE-1]
  DATA_TYPE *sendBuf;
  sendBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);
  for (i=0; i<DATASIZE; i++)
    sendBuf[i] = i;

  // Allocate receive buffer of size [DATASIZE]
  DATA_TYPE *recvBuf;
  recvBuf = (DATA_TYPE*) malloc (sizeof(DATA_TYPE)*DATASIZE);

  // Allocate time measurement arrays
  time_to_read  = (float *) malloc(sizeof(float)*REPEAT_COUNT);
  time_to_write = (float *) malloc(sizeof(float)*REPEAT_COUNT);
  time_to_readwrite = (float *) malloc(sizeof(float)*REPEAT_COUNT);

  for (loopID=0; loopID<REPEAT_COUNT; loopID++)
  {

  	// Generate a new sequence
    for (i=0; i<DATASIZE; i++)
      sendBuf[i] += 1;

    memset (recvBuf, 0, DATASIZE);

    TIME_DATA_TYPE start_time_write,  end_time_write;
    TIME_DATA_TYPE start_time_read,   end_time_read;

    /***********************************************************************************************/
    /*                                      Write Benchmark
    /***********************************************************************************************/

    thread sendToPLThread(sendToPL, fd_write, sendBuf, DATASIZE, time_to_write, loopID, &start_time_write, &end_time_write);

    // 5. Pause for 10ms before proceeding
    // usleep (10000);

    /***********************************************************************************************/
    /*                                      Read Benchmark
    /***********************************************************************************************/

    thread recvFromPLThread(recvFromPL, fd_read, recvBuf, DATASIZE, time_to_read, loopID, &start_time_read, &end_time_read);

    sendToPLThread.join();
    recvFromPLThread.join();

    // Set start time
    // if (compTime(&start_time_write, &start_time_read) == FIRST_TIME_IS_OLDER)
    // {
    //   start_time = start_time_write;
    // }
    // else
    //   start_time = start_time_read;

    start_time = start_time_write;

    // Set end time
    if (compTime(&end_time_write, &end_time_read) == FIRST_TIME_IS_NEWER)
    {
      end_time    = end_time_write;
    }
    else
      end_time    = end_time_read;

    end_time = end_time_read;

    // Calculate time
    time_to_readwrite[loopID] = getTime(&start_time, &end_time);

    // 5. Verify that received values are indeed what was sent
    // for (i=0; i<DATASIZE; i++)
    // {
    //   if (sendBuf[i]!=recvBuf[i])
    //     printf ("Stream[%lu]\tExpected %d, Received %d\n", i, sendBuf[i], recvBuf[i]);
    // }
  }

  /***********************************************************************************************/
  /*                                    Analyze/Print Results
  /***********************************************************************************************/

  // Sort the timing results
  // sort (time_to_write, REPEAT_COUNT);
  // sort (time_to_read, REPEAT_COUNT);
  sort (time_to_readwrite, REPEAT_COUNT);

  // Ignore 0 second results
  // int write_median_index = 0;
  // for (i=REPEAT_COUNT/2; i>=0; i--)
  //   if (time_to_write[i]!=0)
  //   {
  //     write_median_index = i;
  //     break;
  //   }
  // int read_median_index = 0;
  // for (i=REPEAT_COUNT/2; i>=0; i--)
  //   if (time_to_read[i]!=0)
  //   {
  //     read_median_index = i;
  //     break;
  //   }

  int write_median_index = 0;
  for (i=REPEAT_COUNT/2; i>=0; i--)
    if (time_to_readwrite[i]!=0)
    {
      write_median_index = i;
      break;
    }

  printf ("Tband_ct, %lu Bytes, Read+Write %.3f ms %.3f MB/s\n",
    DATASIZE*sizeof(DATA_TYPE),
    time_to_readwrite[write_median_index]*1000.0,
    ((float)(DATASIZE*sizeof(DATA_TYPE))/(time_to_readwrite[write_median_index]))/1048576.0);

  // #ifdef SCRIPT_READY
  //     printf ("%lu, %f, %f\n", DATASIZE, ((float)DATASIZE/(time_to_write[write_median_index]/1000.0))/1048576.0, ((float)DATASIZE/(time_to_read[read_median_index]/1000.0))/1048576.0);
  // #else
  //   printf ("\n\nTime Taken to Write %lu Bytes:\t%.5f ms\t[%.2f, %.2f]\n", DATASIZE, time_to_write[write_median_index], time_to_write[REPEAT_COUNT-1], time_to_write[0]);
  //   printf ("Acieved Bandwidth:\t\t\t%.2f MB/s\t[%.2f, %.2f]\n", 
  //     ((float)DATASIZE/(time_to_write[write_median_index]/1000.0))/1048576.0, 
  //     ((float)DATASIZE/(time_to_write[0]/1000.0))/1048576.0,
  //     ((float)DATASIZE/(time_to_write[REPEAT_COUNT-1]/1000.0))/1048576.0);

  //   printf ("\nTime Taken to Read %lu Bytes:\t%.5f ms\t[%.2f, %.2f]\n", DATASIZE, time_to_read[read_median_index], time_to_read[REPEAT_COUNT-1], time_to_read[0]);
  //   printf ("Acieved Bandwidth:\t\t\t%.2f MB/s\t[%.2f, %f]\n\n\n",
  //     ((float)DATASIZE/(time_to_read[read_median_index]/1000.0))/1048576.0, 
  //     ((float)DATASIZE/(time_to_read[0]/1000.0))/1048576.0,
  //     ((float)DATASIZE/(time_to_read[REPEAT_COUNT-1]/1000.0))/1048576.0);
  // #endif

  return 0;
}

void sendToPL(int fd_write, DATA_TYPE *sendBuf, long DATASIZE, float *time_to_write, int loopID, TIME_DATA_TYPE *start_time, TIME_DATA_TYPE *end_time)
{
    stick_this_thread_to_core(0);

    // barrier(1, 2);

    // printf ("Hi from Thread %d\n", 1);

    // return;

    /***********************************************************************************************/
    /*                                      Write Benchmark
    /***********************************************************************************************/

    // 1. Get Start Time
    // resetTime(&start_time);
    clock_gettime(CLOCK_REALTIME, start_time);

    // 2. Write data to stream
    int rc = write(fd_write, sendBuf, DATASIZE*sizeof(DATA_TYPE));
    int rc2 = write(fd_write, NULL, 0);

    // 3. Calculate Time Taken
    // time_to_write[loopID] = getTime(&start_time);
    clock_gettime(CLOCK_REALTIME, end_time);

    // 4. Verify written count
    if (rc!=DATASIZE*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Write operation is incomplete!");
      exit(1);
    }

    // free_barrier (1, 2);
}


void recvFromPL(int fd_read, DATA_TYPE *recvBuf, long DATASIZE, float *time_to_read, int loopID, TIME_DATA_TYPE *start_time, TIME_DATA_TYPE *end_time)
{
    stick_this_thread_to_core(0);

    // usleep(100);
    // barrier(2, 2);

    // printf ("Hi from Thread %d\n", 2);

    // return;

    /***********************************************************************************************/
    /*                                      Read Benchmark
    /***********************************************************************************************/

    // 1. Get Start Time
    // resetTime(&start_time);
    clock_gettime(CLOCK_REALTIME, start_time);

    // 2. Read data from stream
    int rc = read (fd_read, recvBuf, DATASIZE*sizeof(DATA_TYPE));

    // 3. Calculate Time Taken
    // time_to_write[loopID] = getTime(&start_time);
    clock_gettime(CLOCK_REALTIME, end_time);

    debug ("Read time\t: %f\n", time_to_read[loopID]);

    // 4. Verify read count
    if (rc!=DATASIZE*sizeof(DATA_TYPE))
    {
      fprintf(stderr, "[ERROR] Read operation is incomplete!");
      exit(1);
    }

    // free_barrier (2, 2);
}

/*
   Plain write() may not write all bytes requested in the buffer, so
   allwrite() loops until all data was indeed written, or exits in
   case of failure, except for EINTR. The way the EINTR condition is
   handled is the standard way of making sure the process can be suspended
   with CTRL-Z and then continue running properly.

   The function has no return value, because it always succeeds (or exits
   instead of returning).

   The function doesn't expect to reach EOF either.
*/

void allwrite(int fd, unsigned char *sendBuf, int len) {
  int sent = 0;
  int rc;

  while (sent < len) {
    rc = write(fd, sendBuf + sent, len - sent);

    if ((rc < 0) && (errno == EINTR))
      continue;

    if (rc < 0) {
      perror("allwrite() failed to write");
      exit(1);
    }

    if (rc == 0) {
      fprintf(stderr, "Reached write EOF (?!)\n");
      exit(1);
    }

    sent += rc;
  }
}

/* config_console() does some good-old-UNIX vodoo standard input, so that
   read() won't wait for a carriage-return to get data. It also catches
   CTRL-C and other nasty stuff so it can return the terminal interface to
   what is was start_time. In short, a lot of mumbo-jumbo, with nothing relevant
   to Xillybus.
 */

void config_console() {
  struct termio console_attributes;

  if (ioctl(0, TCGETA, &console_attributes) != -1) {
    // If we got here, we're reading from console

    console_attributes.c_lflag &= ~ICANON; // Turn off canonical mode
    console_attributes.c_cc[VMIN] = 1; // One character at least
    console_attributes.c_cc[VTIME] = 0; // No timeouts

    if (ioctl(0, TCSETAF, &console_attributes) == -1)
      fprintf(stderr, "Warning: Failed to set console to char-by-char\n");
  }
}
