#!/bin/bash

START_DATASIZE=4
END_DATASIZE=16777216 #1048576
SEGMENTS=20

OUTPUT_BANDWIDTH="bandwidth_benchmark.txt"
OUTPUT_TBANDWIDTH="threaded_bandwidth_benchmark.txt"
OUTPUT_BANDWIDTH_CT="bandwidth_ct_benchmark.txt"
OUTPUT_TBANDWIDTH_CT="threaded_bandwidth_ct_benchmark.txt"

if [ $1 == "single_thread" ]
then
	# Remove Old Results
	rm -rf $OUTPUT_BANDWIDTH $OUTPUT_BANDWIDTH_CT

	START_DATASIZES=(START_DATASIZE 1024 1024*1024)
	END_DATASIZES=(1024 1024*1024 END_DATASIZE)
	REPEAT_COUNTS=(10000 1000 100)

	for ((i=0;i<${#START_DATASIZES[@]};++i))
	do
		for (( DATASIZE=${START_DATASIZES[i]}; DATASIZE < ${END_DATASIZES[i]}; ))
		do
			echo "./acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]}"
			./acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_BANDWIDTH
			./acp_bandwidth_ct_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_BANDWIDTH_CT
			
			NEW_DATASIZE=$(( DATASIZE + (${END_DATASIZES[i]}-${START_DATASIZES[i]})/SEGMENTS ))
			if (( NEW_DATASIZE == DATASIZE )); then
				break
			fi
			DATASIZE=$NEW_DATASIZE
		done
	done
elif [ $1 == "multi_thread" ]
then
	# Remove Old Results
	rm -rf $OUTPUT_TBANDWIDTH $OUTPUT_TBANDWIDTH_CT

	START_DATASIZES=(START_DATASIZE 1024 1024*1024)
	END_DATASIZES=(1024 1024*1024 END_DATASIZE)
	REPEAT_COUNTS=(10000 1000 100)

	for ((i=0;i<${#START_DATASIZES[@]};++i))
	do
		for (( DATASIZE=${START_DATASIZES[i]}; DATASIZE < ${END_DATASIZES[i]}; ))
		do
			echo "./threaded_acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]}"
			./threaded_acp_bandwidth_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_TBANDWIDTH
			./threaded_acp_bandwidth_ct_test.o /dev/xillybus_write_32 /dev/xillybus_read_32 $DATASIZE ${REPEAT_COUNTS[i]} >> $OUTPUT_TBANDWIDTH_CT
			
			NEW_DATASIZE=$(( DATASIZE + (${END_DATASIZES[i]}-${START_DATASIZES[i]})/SEGMENTS ))
			if (( NEW_DATASIZE == DATASIZE )); then
				break
			fi
			DATASIZE=$NEW_DATASIZE
		done
	done
fi