#!/bin/bash

DATA_SIZE_START=4			# 4 bytes
DATA_SIZE_END=32*1024*1024	# 64MB

for bin in axi_bandwidth_seq axi_bandwidth_mt100 axi_bandwidth_mt101 axi_bandwidth_mt110 axi_bandwidth_mt111
do
	for (( size=DATA_SIZE_START; size<=DATA_SIZE_END; size*=2))					# Varying Datasize
	do
		OUTPUT=$(./$bin.o /dev/xillybus_write_32 /dev/xillybus_read_32 $size 24) 
		echo $OUTPUT >> axi_$bin.txt
		echo $OUTPUT
	done
done