# Installation Script #

This is a script to setup a beowulf cluster.

## Introduction ##

This script is used to facilitate the automated configuration of master and client nodes with minimal intervention.
It does the following tasks on Xillinux OS (Tested on Zedboard)

- Install OpenMPI (Includes Swap memory creation)
- Configure static IP and DNS (and also MAC!)
- Configure Master and Client nodes (including NFS and passwordless ssh setup)
- Install GIT and ZSH

## Usage ##

*[for every node]*

1. Clone the repository.
2. Run the script, passing -help for more information (sudo may be required)

	- ./run.sh -help

3. For running the sample MPI code, login as mpiuser, compile and run. Password for mpiuser: mpi

	- su mpiuser
	- mpicc mpiCluster.c -o mpiCluster.o
	- mpirun -np 4 -hostfile hostfile ./mpiCluster.o

## To be added ##
- Autoconfigure hostfile for openMPI
- Clean up the script
