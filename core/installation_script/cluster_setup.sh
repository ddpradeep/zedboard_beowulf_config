apt-get install nfs-client -y

mkdir /imports/mpiuser
sudo useradd -u 999 -m -d /imports/mpiuser/ mpiuser
echo mpiuser:mpi | chpasswd
chown mpiuser /imports/mpiuser/
adduser mpiuser sudo

# add to rc.local
mount -t nfs -o proto=tcp,vers=3,port=2049 duckula.sce.ntu.edu.sg:/home/mpiuser /imports/mpiuser