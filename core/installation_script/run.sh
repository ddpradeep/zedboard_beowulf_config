#!/bin/bash

SYS_UPDATE=false
SWAP_CREATE=false
SWAP_DELETE=false
SWAP_PERM=false
INIT_NETWORK=false
NET_MANUAL_CONFIG=false
CLIENT_NODE_CONFIG=false
SERVER_NODE_CONFIG=false
INSTALL_GIT=false
INSTALL_MPI=false
INSTALL_ZSH=false
DISPLAY_HELP=true
CREATE_GUEST_USER=false
IPERF=false
MBW=false

for var in "$@"
do
	DISPLAY_HELP=false

	if [ $var == "-update" ]
	then
		SYS_UPDATE=true
	elif [ $var == "-swapon" ]
	then
		SWAP_CREATE=true
	elif [ $var == "-swapoff" ]
	then
		SWAP_DELETE=true
	elif [ $var == "-swapperm" ]
	then
		SWAP_CREATE=true
		SWAP_PERM=true
	elif [ $var == "-git" ]
	then
		INSTALL_GIT=true
	elif [ $var == "-zsh" ]
	then
		INSTALL_ZSH=true
	elif [ $var == "-mpi" ]
	then
		#SWAP_CREATE=true
		INSTALL_MPI=true
	elif [ $var == "-netman" ]
	then
		INIT_NETWORK=true
		NET_MANUAL_CONFIG=true
	elif [ $var == "-netauto" ]
	then
		INIT_NETWORK=true
	elif [ $var == "-iperf" ]
	then
		IPERF=true
	elif [ $var == "-adduser" ]
	then
		CREATE_GUEST_USER=true
	elif [ $var == "-mbw" ]
	then
		MBW=true
	elif [ $var == "-clientconfig" ]
	then
		#INIT_NETWORK=true
		CLIENT_NODE_CONFIG=true
		#SYS_UPDATE=true
		#SWAP_CREATE=true
		#INSTALL_MPI=true
	elif [ $var == "-serverconfig" ]
	then
		#INIT_NETWORK=true
		SERVER_NODE_CONFIG=true
		#SYS_UPDATE=true
		#SWAP_CREATE=true
		#INSTALL_MPI=true

	elif [ $var == "-all" ]
	then
		INIT_NETWORK=true
		SYS_UPDATE=true
		SWAP_CREATE=true
		INSTALL_GIT=true
		INSTALL_ZSH=true
		INSTALL_MPI=true
	elif [ $var == "help" ]
	then
		DISPLAY_HELP=true
		break
	else
		printf "\nERROR:Invalid argument: $var"
		printf "\nType -help for more information\n"
		echo
	fi
done

if $DISPLAY_HELP
then
	printf "\nSpecify atleast one argument:\n" 
	printf "\n\t-update\t\tUpdates the system"
	printf "\n\t-swapon\t\tCreates swap file (Reset upon reboot)"
	printf "\n\t-swapperm\tCreates swap file (Persistent)"
	printf "\n\t-git\t\tInstalls Git"
	printf "\n\t-zsh\t\tInstalls ZSH Shell"
	printf "\n\t-mpi\t\tInstalls OpenMPI"
	printf "\n\t-netman\t\tConfigure network by MANUALLY entering ip addresses"
	printf "\n\t-netauto\t\tConfigure network using predefined settings"
	printf "\n\n\t-serverconfig\tInstalls essential packages for setting up a Server node"
	printf "\n\t-clientconfig\tInstalls essential packages for setting up client (Server node must be connected and running)"
	printf "\n\t-all\t\tInstalls all common packages for Server and Client (Not Recommended)\n"
	echo
	exit
fi

#########################
# Configure Network
#########################

# Function to validate IP
isIP ()
{
	if [ `echo $1 | grep -o '\.' | wc -l` -ne 3 ]; then
			return 1
	elif [ `echo $1 | tr '.' ' ' | wc -w` -ne 4 ]; then
			return 1
	else
			for OCTET in `echo $1 | tr '.' ' '`; do
					if ! [[ $OCTET =~ ^[0-9]+$ ]]; then
							return 1
					elif [[ $OCTET -lt 0 || $OCTET -gt 255 ]]; then
							return 1
					fi
			done
	fi

	return 0
}

if $INIT_NETWORK
then

	if $NET_MANUAL_CONFIG
	then
		# Get IP Address
		while :
		do
			printf "Host IP Address: \t"
			read HOST_IP
			if isIP $HOST_IP ; then
				break
			fi
			echo "ERROR: Invalid input"
		done

		# Get Netmask
		while :
		do
			printf "Netmask: \t\t"
			read NETMASK
			if isIP $NETMASK ; then
				break
			fi
			echo "ERROR: Invalid input"
		done

		# Get Gateway
		while :
		do
			printf "Gateway: \t\t"
			read GATEWAY
			if isIP $GATEWAY ; then
				break
			fi
			echo "ERROR: Invalid input"
		done
		NODE_ID=0
	else
		# Auto configure network by node number
		while :
		do
			printf "Node ID [0 - 64]: "
			read NODE_ID
			if [[ $NODE_ID =~ ^[\-0-9]+$ ]] && (( NODE_ID >= 0)) && (( NODE_ID <= 64)); then
				break
			fi
			echo "ERROR: Invalid input"
		done
		HOST_IP="192.168.2.$(( 10 + $NODE_ID ))"
		NETMASK="255.255.255.0"
		GATEWAY="192.168.2.1"
	fi

	# Edit MAC Address
	LINE1="ifconfig eth0 down"
	LINE2="ifconfig eth0 hw ether 00:0a:35:00:01:$NODE_ID"
	LINE3="ifconfig eth0 up"
	LINE4="/etc/init.d/networking restart"

#	$LINE1
#	$LINE2
#	$LINE3

	LINE_COUNT=$(cat /etc/rc.local | wc -l)
	CMD="sed '$((LINE_COUNT))i\\$LINE1\n$LINE2\n$LINE3\n$LINE4\n' /etc/rc.local"
	if ! grep -Fxq "$LINE2" /etc/rc.local
	then
		echo "$(eval $CMD)" > /etc/rc.local
		echo "If this is not the first time you changed the ip address, edit the /etc/rc.local"
		cat /etc/rc.local
	fi

	# Setup hosts file
	echo "127.0.0.1 localhost" > /etc/hosts
#	echo "127.0.1.1 zed$NODE_ID" >> /etc/hosts

	if ! $NET_MANUAL_CONFIG ; then
		for ID in {0..49} ; do
			echo "192.168.2.$(( 10 + $ID )) zed$ID" >> /etc/hosts
		done
	fi

	if ! $NET_MANUAL_CONFIG ; then
		for ID in {0..32} ; do
			echo "192.168.2.$(( 10 + 50 + $ID )) microzed$ID" >> /etc/hosts
		done
	fi

	# Setup hostname file
	if [[ $NODE_ID < 50 ]]
	then
		echo "zed$NODE_ID" > /etc/hostname
	else
		echo "microzed$(( NODE_ID - 50 ))" > /etc/hostname
	fi

	# Configure static ip
	echo "auto lo" > /etc/network/interfaces
	echo "iface lo inet loopback" >> /etc/network/interfaces
	echo "" >> /etc/network/interfaces
	echo "auto eth0" >> /etc/network/interfaces
	echo "iface eth0 inet static" >> /etc/network/interfaces
	echo "address $HOST_IP" >> /etc/network/interfaces
	echo "netmask $NETMASK" >> /etc/network/interfaces
	echo "gateway $GATEWAY" >> /etc/network/interfaces
	echo "dns-nameservers $GATEWAY 8.8.8.8" >> /etc/network/interfaces

	# Restart networking service
#	/etc/init.d/networking restart
	echo "Restart might be required"

	ifconfig
	printf "\n*****  Network configured   *****\n"
fi

#########################
# Server Node Config
#########################

if $SERVER_NODE_CONFIG
then
	apt-get install nfs-server -y

	if id -u mpiuser >/dev/null 2>&1; then
		echo "User already exists"
	else
		useradd -u 999 mpiuser
		echo mpiuser:mpi | chpasswd
		adduser mpiuser sudo
	fi
	
	mkdir /home/mpiuser
	mkdir /home/mpiuser/.ssh
	
	chmod 777 /home/mpiuser
	chmod 777 /home/mpiuser/.ssh
	
	chown -R mpiuser /home/mpiuser 
	
	# For portability, refer http://stackoverflow.com/questions/7815989/need-to-break-ip-address-stored-in-bash-variable-into-octets
	LINE1="/home/mpiuser 192.168.2.0/24(rw,sync,no_root_squash,fsid=0,no_subtree_check)"
	if ! grep -Fxq "$LINE1" /etc/exports
	then
		$LINE1
		echo "$LINE1" > /etc/exports
		service nfs-kernel-server restart
		ufw allow from 192.168.0.0/24 to any
		echo "Configured NFS"
	fi

	echo "Generating  SSH key"
	CMD1="echo -e 'y\n'|ssh-keygen -f /home/mpiuser/.ssh/id_dsa -t dsa -q -N \"\""
	CMD2="cp /home/mpiuser/.ssh/id_dsa.pub /home/mpiuser/.ssh/authorized_keys"
	CMD3="chmod 755 /home/mpiuser/"
	CMD4="chmod 755 /home/mpiuser/.ssh"
	CMD5="chmod 600 /home/mpiuser/.ssh/authorized_keys"
	CMD6="chmod 600 /home/mpiuser/.ssh/id_dsa"
	CMD7="chmod 600 /home/mpiuser/.ssh/id_dsa.pub"
	su -c  "$CMD1" mpiuser
	su -c  "$CMD2" mpiuser
	su -c  "$CMD3" mpiuser
	su -c  "$CMD4" mpiuser
	su -c  "$CMD5" mpiuser
	su -c  "$CMD6" mpiuser
	su -c  "$CMD7" mpiuser

	printf "\n*****   Server node configured   ******\n"
fi

#########################
# Client Node Config
#########################

if $CLIENT_NODE_CONFIG
then
	apt-get install nfs-client -y
	if id -u mpiuser >/dev/null 2>&1; then
		echo "User already exists"
	else
		useradd -u 999 mpiuser
		echo mpiuser:mpi | chpasswd
		adduser mpiuser sudo
		mkdir /home/mpiuser
	fi

	LINE1="mount -t nfs -o proto=tcp,port=2049 zed0:/home/mpiuser /home/mpiuser"
	LINE_COUNT=$(cat /etc/rc.local | wc -l)
	CMD="sed '$((LINE_COUNT))i\\$LINE1\n' /etc/rc.local"
	if ! grep -Fxq "$LINE1" /etc/rc.local
	then
		echo "Adding mount to startup"
		echo "$(eval $CMD)" > /etc/rc.local
		$LINE1
	fi

	printf "\n*****   Client node configured   ******\n"
fi

#########################
# System Update
#########################

if $SYS_UPDATE
then
	apt-get update
	#apt-get upgrade 

	printf "\n*****   System up-to date   ******\n"
fi

#########################
# Activate Swap File
#########################

if $SWAP_CREATE
then
	# Create swap file of size x in GB
	printf "Creating swap file..."
	dd if=/dev/zero of=/media/swapfile.img bs=1024 count=2M
	printf "done\n"

	mkswap /media/swapfile.img

	# Enable swap file on evry boot
	if $SWAP_PERM
	then
		echo "/media/swapfile.img swap swap sw 0 0" >> /etc/fstab
	fi

	swapon /media/swapfile.img

	printf "\n*****  Swapfile configured   *****\n"
fi

#########################
# Deactivate Swap File
#########################

if $SWAP_DELETE
then
	swapoff -a
	rm /media/swapfile.img
	
	 df -h

	printf "\n*****  Swapfile Deleted   *****\n"
fi

#########################
# Install OpenMPI
#########################

if $INSTALL_MPI
then
	# Set destination
	installDIR="/opt/openmpi-1.8"

	# Install dependencies
	apt-get install -y autotools-dev autoconf-dev automake-dev g++ build-essential gfortran
	apt-get autoremove

	# Build using maximum number of physical cores
	n=`cat /proc/cpuinfo | grep "cpu cores" | uniq | awk '{print $NF}'`

	# Grab the OpenMPI 1.8 source file
	wget http://www.open-mpi.org/software/ompi/v1.8/downloads/openmpi-1.8.tar.gz
	tar xzvf openmpi-1.8.tar.gz

	cp -R openmpi-1.8 openmpi-1.8_mt

	echo -n "Do you want to install OpenMPI? [Y/N]"
	read ompi

	if [ $ompi == 'y' ]
	then
		# Build the source and install openMPI Single Threaded
		cd openmpi-1.8
		./configure --prefix=$installDIR

		make -j $n
		make install

		cd ..

		printf "\n*****   OpenMPI installed    *****\n"
	fi

	echo -n "Do you want to install OpenMPI Multi-Threaded? [Y/N]"
	read ompi_mt

	if [ $ompi_mt == 'y' ]
	then
		# Build the source and install openMPI Multi-Threaded
		installDIR="/opt/openmpi-1.8_mt"
		cd openmpi-1.8_mt
		./configure --prefix=$installDIR --enable-mpi-thread-multiple

		make -j $n
		make install

		printf "\n*****   OpenMPI-MT installed    *****\n"
	fi

	# Set the environment path
	ldconfig

	# Remove temporary files
	cd ..
#	rm -r -f openmpi-1.8
	rm -r -f openmpi-1.8.tar.gz

	# Remove Swap File
	if [ $SWAP_PERM == false ]
	then
	swapoff -a
	rm /media/swapfile.img
	fi

	printf "\n*****   Done    *****\n"
fi

#########################
# Install git
#########################

if $INSTALL_GIT
then
	apt-get install -y --force-yes git
fi

#########################
# Zsh Shell
#########################

if $INSTALL_ZSH
then
	sudo apt-get install -y curl vim git zsh
	curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | bash
	sudo chsh -s $(which zsh) $(whoami)

	printf "\n*****     ZSH installed     *****\n"
fi

###############################
# Network Benchmark Installer
###############################

if $IPERF
then
	wget http://iperf.fr/download/iperf_2.0.5/iperf-2.0.5-source.tar.gz
	tar xzvf iperf-2.0.5-source.tar.gz
	cd iperf-2.0.5
	./configure
	make
	make install
	
	cd ..
	rm -r -f iperf-2.0.5
	rm -r -f iperf-2.0.5-source.tar.gz
printf "\n*****     Network Benchmark Installed       *****\n"
fi

###############################
# Memory Benchmark Installer
###############################

if $MBW
then
	wget https://launchpad.net/ubuntu/feisty/+source/lmbench/3.0-a7-1/+files/lmbench_3.0-a7.orig.tar.gz
	tar xzvf lmbench_3.0-a7.orig.tar.gz
	cd lmbench-3.0-a7/
	tar xzvf lmbench_3.0-a7.tgz
	eval 'ls'
	#cd lmbench-3.0-a7
	#./configure
	#make
	#make install
	
	#cd ../..
	#rm -r -f lmbench-3.0-a7
	#rm -r -f lmbench_3.0-a7.orig.tar.gz
printf "\n*****     Memory Benchmark Installed       *****\n"
fi

#########################
# Create Guest User
#########################

if $CREATE_GUEST_USER
then
	printf "Guest Username: "
	read GUEST
	if id -u $GUEST >/dev/null 2>&1; then
		echo "User already exists"
	else
		useradd -u 998 $GUEST
		echo $GUEST:ce | chpasswd
		adduser $GUEST sudo
	fi
	
	mkdir /home/$GUEST
	mkdir /home/$GUEST/.ssh
	
	chmod 777 /home/$GUEST
	chmod 777 /home/$GUEST/.ssh
	
	chown -R $GUEST /home/$GUEST

	echo "Generating  SSH key"
	CMD1="echo -e 'y\n'|ssh-keygen -f /home/$GUEST/.ssh/id_dsa -t dsa -q -N \"\""
	CMD2="cp /home/$GUEST/.ssh/id_dsa.pub /home/$GUEST/.ssh/authorized_keys"
	CMD3="chmod 755 /home/$GUEST/"
	CMD4="chmod 755 /home/$GUEST/.ssh"
	CMD5="chmod 600 /home/$GUEST/.ssh/authorized_keys"
	CMD6="chmod 600 /home/$GUEST/.ssh/id_dsa"
	CMD7="chmod 600 /home/$GUEST/.ssh/id_dsa.pub"
	su -c  "$CMD1" $GUEST
	su -c  "$CMD2" $GUEST
	su -c  "$CMD3" $GUEST
	su -c  "$CMD4" $GUEST
	su -c  "$CMD5" $GUEST
	su -c  "$CMD6" $GUEST
	su -c  "$CMD7" $GUEST

	printf "\n*****   Guest User Added   ******\n"
fi
