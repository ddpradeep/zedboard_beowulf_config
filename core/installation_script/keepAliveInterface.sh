#!/bin/bash
# ping checker tool

FAILS=0
SERVER="192.168.2.1"
SLEEP=1

while true; do
    ping -w1 -c1 $SERVER >/dev/null 2>&1
    if [ $? -ne 0 ] ; then #if ping exits nonzero...
        FAILS=$[FAILS + 1]
    else
        FAILS=0
    fi
    if [ $FAILS -gt 2 ]; then
        FAILS=0
        sudo ifdown eth0
        sudo ifup eth0
	echo "Restarted Ethernet Interface" | wall
    fi
    sleep $SLEEP #check again in SLEEP seconds
done
