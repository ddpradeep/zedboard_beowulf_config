# MPI Test code #

This code is used for comparing distributed processing vs single-core processing.

## Implemented Algorithm ##

*Vector Addition*

C[i] = A[i] + B[i]

## Usage ##

### For Linear Version ###

make linear
./exec linear

### For Parallel Version ###

make mpi
./exec mpi
