/***************************************
Original AUTHOR: ANKIT MAHATO
amahato@iitk.ac.in
IIT KANPUR
***************************************/
 
 
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

#define MASTER 0

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
	int * a;										// vector A
	int * b;										// vector B
	int * c;										// vector A + B
	
	int total_proc;									// total number of processes	
	int rank;										// rank of each process
	int repeatCount = 1;
	int loopCounter=repeatCount;					// Repeats for averaging
	long long int i, n ;	// vector size
	long long int n_per_proc;						// elements per process		
	
	MPI_Status status;

	/* Start Profiling */
	struct timeval timeT;
	float timeAcc = 0;
	resetTime(&timeT);
 
	MPI_Init (&argc, &argv);						// Initialization of MPI environment
	MPI_Comm_size (MPI_COMM_WORLD, &total_proc);	// Get total processes count
	
	MPI_Comm_rank (MPI_COMM_WORLD,&rank);			// Get current process rank

	n = total_proc * PROBLEM_SIZE;

	int * ap;
	int * bp;
	int * cp;

	if (rank == MASTER) // For master process
	{
			printf ("Vector Size:\t%lld (%.2fMB)\n", n, (float)sizeof(int)*n/(1048576.0));

			MPI_Bcast (&repeatCount, 1, MPI_INT, MASTER, MPI_COMM_WORLD);		// Broadcast repeatCount
        	while (loopCounter--)
        	{	
        		a = (int *) malloc(sizeof(int)*n);	// malloc vector A
				b = (int *) malloc(sizeof(int)*n);	// malloc vector B
				c = (int *) malloc(sizeof(int)*n);	// malloc vector C
				
            	MPI_Bcast (&n, 1, MPI_LONG_LONG_INT, MASTER, MPI_COMM_WORLD);			// Broadcast vector size
				
				for(i=0;i<n;i++)					// Random assign vector A
					a[i] = i;
				for(i=0;i<n;i++)					// Random assign vector B
					b[i] = n-i;
				
				#if PRINT_DEBUG == 1
					printf ("A\t: ");
					for(i=0;i<n;i++)
						printf ("%2d ", a[i]);
					printf ("\n");
					printf ("B\t: ");
					for(i=0;i<n;i++)
						printf ("%2d ", b[i]);
					printf ("\n");
				#endif

				n_per_proc = n/total_proc;			// elements per process
				
				if(n%total_proc != 0)				// divide data evenly by the number of processors
				{
						n_per_proc+=1;
						for(i=0;i<(n_per_proc*total_proc - n);i++)
						{
							a[n+i] = 0;
							b[n+i] = 0;
						}
				}
				
				ap = (int *) malloc(sizeof(int)*n_per_proc);
				bp = (int *) malloc(sizeof(int)*n_per_proc);
				cp = (int *) malloc(sizeof(int)*n_per_proc);
				MPI_Bcast (&n_per_proc, 1, MPI_LONG_LONG_INT, MASTER, MPI_COMM_WORLD);	// Broadcast elements per process
				
				MPI_Scatter(a, n_per_proc, MPI_INT, ap, n_per_proc, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector A
				MPI_Scatter(b, n_per_proc, MPI_INT, bp, n_per_proc, MPI_INT, 0, MPI_COMM_WORLD);	// Scatter vector B
				
				// Perform C = A + B for allocated elements
				for(i=0;i<n_per_proc;i++)
					cp[i] = ap[i]+bp[i];
				
				MPI_Gather(cp, n_per_proc, MPI_INT, c, n_per_proc, MPI_INT, MASTER, MPI_COMM_WORLD);// Gather vector C
				
				timeAcc += getTime(&timeT);

				#if PRINT_DEBUG == 1
					printf ("A + B\t: ");
					for(i=0;i<n;i++)
						printf ("%2d ", c[i]);
					printf ("\n\n");
				#endif
				free (a);
				free (b);
				free (c);
				free (ap);
				free (bp);
				free (cp);
			}
    	}
	else			// For slave processes
	{
		MPI_Bcast (&repeatCount, 1, MPI_INT, MASTER, MPI_COMM_WORLD);								// Receive repeatCount
		
		while (loopCounter--)
		{
			MPI_Bcast (&n, 1, MPI_LONG_LONG_INT, MASTER, MPI_COMM_WORLD);							// Receive vector size
			MPI_Bcast (&n_per_proc, 1, MPI_LONG_LONG_INT, MASTER, MPI_COMM_WORLD);					// Receive elements per process
			ap = (int *) malloc(sizeof(int)*n_per_proc);
			bp = (int *) malloc(sizeof(int)*n_per_proc);
			cp = (int *) malloc(sizeof(int)*n_per_proc);
			
			MPI_Scatter(a, n_per_proc, MPI_INT, ap, n_per_proc, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector A
			MPI_Scatter(b, n_per_proc, MPI_INT, bp, n_per_proc, MPI_INT, 0, MPI_COMM_WORLD);		// Receive scattered vector B
			
			// Perform C = A + B for allocated elements
			for(i=0;i<n_per_proc;i++)
			{
				cp[i] = ap[i]+bp[i];
			}
			
			MPI_Gather(cp, n_per_proc, MPI_INT, c, n_per_proc, MPI_INT, MASTER, MPI_COMM_WORLD);	// Initiate gathering of vector C

			free (ap);
			free (bp);
			free (cp);
        }
	}
	//printf ("Process %d Finished\n", rank);
	
	MPI_Finalize();	

	if (rank == MASTER)	printf("Time taken:\t%f s\n", timeAcc/(float)repeatCount);

	return 0;
}
