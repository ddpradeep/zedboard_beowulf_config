#include <stdio.h>
#include <stdlib.h>
#include "sys/time.h"

#define PRINT_DEBUG 0
#define PROBLEM_SIZE 3500000

void inline resetTime(struct timeval *timer)
{
    gettimeofday(timer, NULL);
}

float inline getTime(struct timeval *time1)
{
    struct timeval time2;
    gettimeofday(&time2, NULL);
    return (float)(time2.tv_sec - time1->tv_sec + (float)(time2.tv_usec - time1->tv_usec) / 1000000.0);
}

int main (int argc, char *argv[]) 
{
    int * a;                    // vector A
    int * b;                    // vector B
    int * c;                    // vector A + B

    int repeatCount = 1;        // total number of test cases
    long long int i, n ;        // vector size   

    /* Start Profiling */
    struct timeval time1;
    resetTime(&time1);

    n = 8 * PROBLEM_SIZE;

    int * ap;
    int * bp;
    int * cp;

    printf ("Vector Size:\t%lld (%.2fMB)\n", n, (float)sizeof(int)*n/(1048576.0));
    
    while (repeatCount--)
    {   
        a = (int *) malloc(sizeof(int)*n);  // malloc vector A
        b = (int *) malloc(sizeof(int)*n);  // malloc vector B
        c = (int *) malloc(sizeof(int)*n);  // malloc vector C
        
        for(i=0;i<n;i++)                    // Random assign vector A
            a[i] = i;
        for(i=0;i<n;i++)                    // Random assign vector B
            b[i] = n-i;
        
        #if PRINT_DEBUG == 1
            printf ("A\t: ");
            for(i=0;i<n;i++)
                printf ("%2d ", a[i]);
            printf ("\n");
            printf ("B\t: ");
            for(i=0;i<n;i++)
                printf ("%2d ", b[i]);
            printf ("\n");
        #endif

        // Perform C = A + B for allocated elements
        for(i=0;i<n;i++)
            c[i] = a[i]+b[i];
        
        #if PRINT_DEBUG == 1
            printf ("A + B\t: ");
            for(i=0;i<n;i++)
                printf ("%2d ", c[i]);
            printf ("\n\n");
        #endif
    }

    printf("Time taken:\t%f s\n", getTime(&time1));

    return 0;
}
