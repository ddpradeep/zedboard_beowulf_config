#!/bin/bash

OUTPUT=$(sudo /opt/open-mx/sbin/omx_init status)
if [[ $OUTPUT =~ .*not\ loaded.* ]]
	then
	echo "Off"
else
	echo "On"
fi