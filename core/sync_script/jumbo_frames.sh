#!/bin/bash

abs_path='/home/mpiuser/zedboard_beowulf_config/core/sync_script'

HELP=false
ENABLE=false
DISABLE=false
STATUS=false

case $1 in
    	-h|--help|help)
    		HELP=true
    		;;
    	-e|enable)
    		ENABLE=true
    		;;
    	-d|disable)
    		DISABLE=true
    		;;
    	-s|status)
    		STATUS=true
    		;;
    	*)
		HELP=true
		;;
esac

if $HELP
then
	printf "\nSpecify atleast one argument:\n"
	printf "\n\t-e|enable\t\tEnable jumbo frames"
        printf "\n\t-d|disable\t\tDisable jumbo frames"
        printf "\n\t-s|status\t\tGet status of jumbo frames"
	printf "\n\n"
fi

if $ENABLE
then
	for HOST in {0..3}; do
		printf "Jetson$HOST\t\t: "
		ssh admin@jetson$HOST bash -s -- < $abs_path/enable_jumbo_frame.sh
	done
fi

if $DISABLE
then
	for HOST in {0..3}; do
		printf "Jetson$HOST\t\t: "
		ssh admin@jetson$HOST bash -s -- < $abs_path/disable_jumbo_frame.sh
	done

	for HOST in {0..31}; do
                printf "Zed$HOST\t\t: "
                ssh admin@zed$HOST bash -s -- < $abs_path/disable_jumbo_frame.sh
        done
	for HOST in {33..33}; do
                printf "Zed$HOST\t\t: "
                ssh admin@zed$HOST bash -s -- < $abs_path/disable_jumbo_frame.sh
        done
fi

if $STATUS
then
	for HOST in {0..3}; do
		printf "Jetson$HOST\t\t: "
		ssh admin@jetson$HOST bash -s -- < $abs_path/status_jumbo_frame.sh
	done

	for HOST in {0..31}; do
                printf "Zed$HOST\t\t: "
                ssh admin@zed$HOST bash -s -- < $abs_path/status_jumbo_frame.sh
        done
	for HOST in {33..33}; do
                printf "Zed$HOST\t\t: "
                ssh admin@zed$HOST bash -s -- < $abs_path/status_jumbo_frame.sh
        done
fi
