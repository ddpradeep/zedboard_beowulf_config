#!/bin/bash

DIR=$(pwd)

echo $DIR

if [ -z "$1" ]; then
	echo "exec_all_nodes <bash_script_to_execute>"
	exit
fi

for HOST in {1..31}; do
       	printf "Zed$HOST\t\t: "
	ssh admin@zed$HOST bash -s -- < $1
done

for HOST in 33 ; do
        printf "Zed$HOST\t\t: "
        ssh admin@zed$HOST bash -s -- < $1
done
