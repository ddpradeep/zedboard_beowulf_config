#!/bin/bash


OUTPUT=$(sudo ifconfig eth0 mtu 9000 up && sudo /opt/open-mx/sbin/omx_init start 2>&1)
if [[ $OUTPUT =~ .*File\ exists.* ]]
	then
	echo "On"
elif [[ $OUTPUT =~ .*ERROR* ]]
	then
	echo "$OUTPUT"
elif [[ $OUTPUT =~ .*Started\ as\ pid* ]]
	then
	echo "On"
fi