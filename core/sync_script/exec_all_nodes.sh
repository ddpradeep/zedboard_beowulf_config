#!/bin/bash

DIR=$(pwd)

echo $DIR

if [ -z "$1" ]; then
	echo "exec_all_nodes <bash_script_to_execute>"
	exit
fi

for HOST in {0..33}; do
       	printf "Zed$HOST\t\t: "
	ssh admin@zed$HOST bash -s -- < $DIR/$1
done

