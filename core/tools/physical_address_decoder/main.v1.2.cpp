// #define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "helper.h"
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <assert.h>
#include <fcntl.h>

#define PAGEMAP_ENTRY_SIZE 8		// 8 bytes by default

using namespace std;

int main(int argc, char** argv) 
{
	if (argc != 2)
	{
		cerr<<"Expected: <data size in bytes>\n";
		return -1;
	}

	/*************************************************************************/
	// Parse Inputs
	/*************************************************************************/

	// Get data buffer size from input
	long dataSize = atoi(argv[1])/sizeof(int);
	debug("Size: %d\n", dataSize);

	/*************************************************************************/
	// Allocate Memory
	/*************************************************************************/

	// Dynamically allocate memory from heap
	int *data = new int [dataSize];

	// Use it
	for (int i=0; i<dataSize; i++)
		data[i] = i;

	// Get the pointer address
	int *virtAddr = data;
	printf ("Virtual Address\t\t\t: %p - %p\n", 
		virtAddr, 
		virtAddr+dataSize);

	/*************************************************************************/
	// Print memory mappings for this process (Only for debug)
	/*************************************************************************/

	// Get this process PID
	int pid = getpid();
	
	char baseDir[15];
	sprintf (baseDir, "/proc/%d", pid);

	// Placeholder to build future commands
	stringstream cmd;

	/*************************************************************************/
	// Get Heap Range
	/*************************************************************************/

	// Build command
	cmd.str(string());
	cmd<<"cat "<<baseDir<<"/maps";
	const string cmd_catMaps=cmd.str();
	debug ("Cmd: %s\n", cmd_catMaps.c_str());

	// Run it
	string cmdResult = exec(cmd_catMaps.c_str());	// run command
	debug("output: \n%s\n", cmdResult.c_str());

	/*************************************************************************/
	// Get Physical Address from Pagemap
	/*************************************************************************/

	// Build filename
	cmd.str(string());
	cmd<<baseDir<<"/pagemap";
	const string filename=cmd.str();

	// Open it
	FILE *pagemap = fopen(filename.c_str(), "rb");
	if(!pagemap)
	{
		printf("Error! Cannot open %s\n", filename.c_str());
		abort();
	}

	// Find the virtual Page File Number (PFN)
	uint32_t virtPFN = (uint32_t)(virtAddr)/getpagesize()*8;
	printf ("Virtual PFN\t\t\t: %p\n", (int *)virtPFN);
	if(fseek(pagemap, virtPFN, SEEK_SET)){
		perror("Failed to do fseek!");
		abort();
	}

	// Seek the pagemap file to get the 8-byte mapping structure
	uint64_t pagemap_entry;
	unsigned char c_buf[PAGEMAP_ENTRY_SIZE];
	for(int i=0; i < PAGEMAP_ENTRY_SIZE; i++)
	{
		int c = getc(pagemap);
		if(c==EOF)
		{
			cerr<<"Reached end of the file\n";
			abort();
		}
		c_buf[PAGEMAP_ENTRY_SIZE - i - 1] = c;
	}
	for(int i=0; i < PAGEMAP_ENTRY_SIZE; i++)
		pagemap_entry = (pagemap_entry << 8) + c_buf[i];
	fclose (pagemap);

	// Get physical Page Frame Number (PFN)
	int physPFN = (uint32_t)(pagemap_entry & 0x7FFFFFFFFFFFFF);
	debug("Pagemap Entry: 0x%llx\n", (unsigned long long) pagemap_entry);
	printf("Physical PFN\t\t\t: 0x%x\n", (uint32_t)physPFN);

	// Calculate physical address
	int *physAddr = (int *)(physPFN*getpagesize() + (uint32_t)(virtAddr)%getpagesize());
	printf("Starting Physical Address\t: 0x%x\n", (uint32_t)physAddr);

	/*************************************************************************/
	// Verify physical address
	/*************************************************************************/

	// Open physical memory
	int32_t fd = open("/dev/mem", O_RDONLY);
	if (fd<0)
	{
		cerr<<"Failed to open memory\n";
		abort();
	}

	// Seek to the physical address
	if(lseek(fd, (uint32_t)physAddr, SEEK_SET)<0)
	{
		cerr<<"Failed to seek memory\n";
		abort();
	}

	// Read the entire chunk of data specified by the dataSize
	int *memData = new int [dataSize];
	read(fd, memData, dataSize*sizeof(int));
	close(fd);
	debug("Read array from mem:\n");
	// printArray(memData, dataSize, " ");

	// Verify with the original array
	for (int i=0; i<dataSize; i++)
		if (data[i] != memData[i])
		{
			printf ("[Error] data[%d] expected to be %d, but memory reads %d\n", i, data[i], memData[i]);
			abort();
		}

	cout<<"[Success] Physical address mapping matches for all array elements"<<endl;

	return 0;
}
