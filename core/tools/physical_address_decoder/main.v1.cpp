#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "helper.h"
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <assert.h>

#define PAGEMAP_ENTRY_SIZE 8		// 64 bits by default

using namespace std;

int main(int argc, char** argv) 
{
	if (argc != 2)
	{
		cerr<<"Expected: <data size in bytes>\n";
		return -1;
	}

	/*************************************************************************/
	// Parse Inputs
	/*************************************************************************/

	// Get data buffer size from input
	long dataSize = atoi(argv[1])/sizeof(int);
	debug("Size: %d\n", dataSize);

	/*************************************************************************/
	// Allocate Memory
	/*************************************************************************/

	// Dynamically allocate memory from heap
	// int *data = new int [dataSize];
	int *data = (int *)mmap(0, dataSize*sizeof(int), PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);

	// Use it
	for (int i=0; i<dataSize; i++)
		data[i] = i;

	// Get the pointer address
	int *virtAddr = data;
	printf ("Virtual Address: %p - %p\n", 
		virtAddr, 
		virtAddr+dataSize);

	/*************************************************************************/
	// Get Base Directory to Get Mapping From
	/*************************************************************************/

	// Get this process PID
	int pid = getpid();
	
	char baseDir[15];
	sprintf (baseDir, "/proc/%d", pid);

	// Placeholder to build future commands
	stringstream cmd;

	/*************************************************************************/
	// Get Heap Range
	/*************************************************************************/

	// Build command
	cmd.str(string());
	cmd<<"cat "<<baseDir<<"/maps";
	const string cmd_catMaps=cmd.str();
	debug ("Cmd: %s\n", cmd_catMaps.c_str());

	// Run it
	string cmdResult = exec(cmd_catMaps.c_str());	// run command
	cout<<"output: \n"<<cmdResult;

	/*************************************************************************/
	// Decode Pagemap
	/*************************************************************************/

	// Build filename
	cmd.str(string());
	cmd<<baseDir<<"/pagemap";
	const string filename=cmd.str();

	// Open it
	FILE *pagemap = fopen(filename.c_str(), "rb");
	if(!pagemap)
	{
		printf("Error! Cannot open %s\n", filename.c_str());
		abort();
	}

	// Find the virtual Page File Number (PFN)
	// uint64_t virtPFN = (uint64_t) virtAddr / (uint64_t)getpagesize() * (uint64_t)PAGEMAP_ENTRY_SIZE;
	uint32_t virtPFN = (uint32_t)(virtAddr)/getpagesize()*8;
	debug ("Virtual PFN: %p\n", virtPFN);
	if(fseek(pagemap, virtPFN, SEEK_SET)){
		perror("Failed to do fseek!");
		abort();
	}

	// Seek the pagemap file to get the 8-byte mapping structure
	uint64_t pagemap_entry;
	unsigned char c_buf[PAGEMAP_ENTRY_SIZE];
	for(int i=0; i < PAGEMAP_ENTRY_SIZE; i++)
	{
		int c = getc(pagemap);
		if(c==EOF)
		{
			printf("\nReached end of the file\n");
			abort();
		}
		c_buf[PAGEMAP_ENTRY_SIZE - i - 1] = c;
		// printf("[%d]0x%x ", i, c);
	}
	for(int i=0; i < PAGEMAP_ENTRY_SIZE; i++)
		pagemap_entry = (pagemap_entry << 8) + c_buf[i];
	fclose (pagemap);

	// Get physical Page Frame Number (PFN)
	int physPFN = (uint32_t)(pagemap_entry & 0x7FFFFFFFFFFFFF);
	debug("Result: 0x%llx\n", (unsigned long long) pagemap_entry);
	printf("Physical PFN: 0x%x\n", (uint32_t)physPFN);

	// Calculate physical address
	int *physAddr = (int *)(physPFN*getpagesize() + (uint32_t)(virtAddr)%getpagesize());
	printf("Physical Address: 0x%x\n", (uint32_t)physAddr);

	// getch();
	// int *physAddr = virtAddr;

	return 0;
}
