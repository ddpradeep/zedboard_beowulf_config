#!/usr/bin/Rscript

require(lattice);
require(ggplot2);
require(reshape2);

a<-read.csv('memory_system_heirarchy.csv',header=T)
b<-data.frame(a$X,a$single_core_read_bw_mbps,a$single_core_write_bw_mbps,a$dual_core_read_bw_mbps,a$dual_core_write_bw_mbps)
colnames(b)[1]="Type"
colnames(b)[2]="Read-1core"
colnames(b)[3]="Write-1core"
colnames(b)[4]="Read-2core"
colnames(b)[5]="Write-2core"
c<-melt(b)

pdf(file="bw.pdf", height=3.5, width=5)

p <- ggplot(c,aes(x=c$variable,y=c$value,fill=c$Type))+geom_bar(width=0.75,position=position_dodge(),stat="identity")+geom_text(label=c$value,size=3,position=position_dodge(width=0.75),aes(ymax=16100),vjust=-0.5,hjust=0.3)+xlab("Metric")+ylab("Bandwidth (Mbyte/s)")+ylim(0,17000)

p + theme(legend.position=c(0.1,0.8), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
