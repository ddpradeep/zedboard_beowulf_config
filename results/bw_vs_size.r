#!/usr/bin/Rscript

require(grid);
require(ggplot2);
require(reshape2);

a<-read.csv('consolidated_mpi_vs_acp_vs_dram.csv',header=T)

b<-data.frame(a)

pdf(file="bw_vs_size.pdf", height=3.5, width=5)

data_a <- data.frame(x=b$size_bytes,y=b$acp_bw_mbps)
data_b <- data.frame(x=b$size_bytes,y=b$dram_rand_bw_mbps)
data_c <- data.frame(x=b$size_bytes,y=b$dram_seq_bw_mbps)
data_d <- data.frame(x=b$size_bytes,y=b$mpi_put_bw_mbps)

df <- rbind(data_a,data_b,data_c,data_d)
df$dataset <- c(rep("A", nrow(data_a)), rep("B", nrow(data_b)),rep("C", nrow(data_c)),rep("D", nrow(data_d)))

p <- ggplot(data=df, aes(x=x, y=y,col=dataset,shape=dataset)) + geom_point(size=3) + geom_line(size=1.2) +
	scale_colour_hue(name="", labels = c("ACP","DRAM-random","DRAM-seq","MPI")) +
	scale_shape_manual(name="",labels = c("ACP","DRAM-random","DRAM-seq","MPI"),values=c(15,16,17,18)) +
	xlab("Bytes Transferred") + ylab("Bandwidth (MB/s)") +
	scale_x_log10() + 
	theme_bw();



p +
theme_bw() +
theme(
      legend.position=c(0.17,0.85), 
      legend.title=element_blank(),
      legend.background = element_rect(fill="transparent"),
      legend.key=element_blank(),
      legend.key.width=unit(0.8,"cm"),
      legend.text=element_text(size=10)) +
guides(
       shape=guide_legend(ncol=1)
       ) +
theme(
      plot.background = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank()
      ) +
#draws x and y axis line
theme(axis.line = element_line(color = 'black'));

