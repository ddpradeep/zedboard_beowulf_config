#!/usr/bin/Rscript

require(ggplot2);
require(reshape2);

a<-read.csv('network_x86_mpich_vs_openmpi_asynchronous.csv',header=T)

pdf(file="mpi_arm_vs_x86_bw.pdf", height=3.5, width=5)

p<-ggplot(data=a)+
	geom_line(aes(x=transfer_block_size_bytes,y=x86_mpich_time_taken_ms,color=factor("green",label="MPICH-x86")))+
	geom_point(aes(x=transfer_block_size_bytes,y=x86_mpich_time_taken_ms),shape=1)+
	geom_line(aes(x=transfer_block_size_bytes,y=arm_mpich_time_taken_ms,color=factor("blue",label="MPICH-ARM")))+
	geom_point(aes(x=transfer_block_size_bytes,y=arm_mpich_time_taken_ms),shape=2)+
	scale_x_log10()+scale_y_log10()+xlab('Transfer Size (bytes)')+ylab('Time (ms)');

p + theme(legend.position=c(0.2,0.8), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
