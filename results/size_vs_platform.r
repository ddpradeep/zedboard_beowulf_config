#!/usr/bin/Rscript

require(grid);
require(ggplot2);
require(reshape2);

a0<-read.csv('arm.csv',header=T)
a1<-read.csv('x86.csv',header=T)
a2<-read.csv('benchmark-M.csv',header=T);

c0<-data.frame(threads=a0$threads, x=a0$nodes, y=a0$synapse_time+a0$neuron_time);
c1<-data.frame(threads=a1$threads, x=a1$nodes, y=a1$synapse_time+a1$neuron_time);
c2<-data.frame(x=a2$nodes,y=a2$time,threads=a2$pe);

c0<-subset(c0,c0$threads==1 & c0$x<=8000000);
c1<-subset(c1,c1$threads==1 & c1$x<=8000000);
c2b<-subset(c0,c0$x<=8000000);
#c0<-subset(c0,c0$threads==1);
#c1<-subset(c1,c1$threads==1);
#c2b<-subset(c0,c0$x<=1000000);
c3a<-subset(c2,c2$threads==4);
e<-data.frame(x=c2b$x,y=c3a$y*1e-3+(c2b$y/4),threads=c2b$threads);
c2a<-subset(c2,c2$threads==14);
d<-data.frame(x=c2b$x,y=c2a$y*1e-3+(c2b$y/14),threads=c2b$threads);
c2a<-subset(c2,c2$threads==14);
f<-data.frame(x=c2b$x,y=c2a$y*1e-3+(c2b$y/32),threads=c2b$threads);

df <- rbind(c1,c0,e,d,f);
df$dataset <- c(rep("x86",nrow(c1)),
		rep("01-Zynq node",nrow(c0)),
		rep("04-Zynq nodes",nrow(e)),
		rep("16-Zynq nodes",nrow(d)),
		rep("32-Zynq nodes",nrow(f))
		);

pdf(file="size_vs_platform.pdf", height=3.5, width=5)

p<-ggplot(data=df,aes(x=x,y=y,linetype=dataset,colour=dataset,shape=dataset)) +
geom_line(size=1.1) + geom_point(size=3) +
xlab('Runtime as a function of number of neurons in the network\n (Synapses fixed at 1M)')+ylab('Time (s)') + 
scale_y_log10(limits=c(0.03,100),breaks=c(0.03,0.1,1,10,100),labels=c(0.03,0.1,1,10,100)) + scale_x_log10(breaks=c(100000,200000,400000,800000,1000000,2000000,4000000,8000000));

p + theme_bw() + theme(legend.position=c(0.2,0.8), legend.key.width=unit(1.5,"cm"), legend.key.height=unit(0.5,"cm"), legend.key=element_blank(), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10),axis.text.x = element_text(angle = 90, hjust = 1));
