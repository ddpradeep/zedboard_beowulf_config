#!/usr/bin/Rscript

require(ggplot2);

a<-read.csv('memory_random_access.csv',header=T)
b<-data.frame(a)

pdf(file="mem_time.pdf", height=5.5, width=9)

p<-ggplot(data=b,aes(x=b$no_of_kilo_memory_access,y=(b$time_taken_ms*1e-3)/(b$no_of_kilo_memory_access*1e3),group=b$array_size_kb))+geom_line(aes(color=factor(b$array_size_kb)))+geom_point()+scale_x_log10()+xlab('Number of Memory Accesses (kilo)')+ylab('Time per access (s)')

p + theme(legend.position=c(0.1,0.55), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
