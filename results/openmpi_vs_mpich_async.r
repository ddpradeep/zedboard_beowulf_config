#!/usr/bin/Rscript

require(ggplot2);
require(reshape2);

a<-read.csv('network_mpich_vs_openmpi_asynchronous.csv',header=T)
b<-data.frame(a);

pdf(file="openmpi_vs_mpich_async.pdf", height=3.5, width=5)

data_0 <- data.frame(x=b$transfer_block_size_bytes,y=b$openmpi_time_taken_ms);
data_1 <- data.frame(x=b$transfer_block_size_bytes,y=b$mpich_time_taken_ms);

df <- rbind(data_0,data_1);
df$dataset <- c(rep("OpenMPI",nrow(data_0)), rep("MPICH",nrow(data_1)));

p<-ggplot(data=df,aes(x=x,y=x/(1000*y),linetype=dataset,colour=dataset,shape=dataset))+
	geom_line(size=1.2)+
	scale_colour_hue()+
	scale_y_log10()+ scale_x_log10()+ 
	xlab('Transfer Size (bytes)')+ylab('Bandwidth (MB/s)');

p + theme_bw() + theme(legend.position=c(0.2,0.8), legend.key=element_blank(), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
