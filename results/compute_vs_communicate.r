#!/usr/bin/Rscript

require(grid);
require(ggplot2);
require(reshape2);

a0<-read.csv('arm.csv',header=T)
a2<-read.csv('benchmark-M.csv',header=T);

c0<-data.frame(threads=a0$threads, x=a0$nodes, y=(a0$synapse_time+a0$neuron_time));

c2<-data.frame(x=a2$nodes,y=a2$time*1e-3,threads=a2$pe);

c0<-subset(c0,c0$x==100000);
c2<-subset(c2,c2$x==100000);

r<-data.frame(threads=seq(1,32));
r$y=rep_len(c0$y,32);
r$y=r$y/r$threads
r$x=seq(1,32);

df <- rbind(c2,r);
df$dataset <- c(rep("Communicate",nrow(c2)),rep("Compute",nrow(r)));

pdf(file="compute_vs_communicate.pdf", height=3.5, width=5)

p<-ggplot(data=df,aes(x=threads,y=y,linetype=dataset,colour=dataset,shape=dataset)) +
geom_line(size=1.1) + geom_point(size=3) +
xlab('Runtime as a function of cluster size \n(Neural network with 1M nodes and 1M edges)')+ylab('Time (s)');

p + theme_bw() + theme(legend.position=c(0.8,0.8), legend.key.width=unit(1.5,"cm"), legend.key.height=unit(0.5,"cm"), legend.key=element_blank(), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10),axis.text.x = element_text(angle = 90, hjust = 1));
