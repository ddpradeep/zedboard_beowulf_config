#!/usr/bin/Rscript

require(lattice);
require(ggplot2);
require(reshape2);

a<-read.csv('network_mpi_latencies_arm_vs_x86.csv',header=T)
b<-data.frame(a$mpi_func,a$arm_mpich_latency_ms,a$arm_openmpi_latency_ms,a$x86_mpich_latency_ms,a$x86_openmpi_latency_ms)
colnames(b)[1]="MPI Function"
colnames(b)[2]="MPICH ARM"
colnames(b)[3]="OpenMPI ARM"
colnames(b)[4]="MPICH x86"
colnames(b)[5]="OpenMPI x86"
c<-melt(b)

pdf(file="mpi_arm_vs_x86.pdf", height=3.5, width=8)

p <- ggplot(c,aes(x=c$variable,y=c$value,fill=c$'MPI Function'))+geom_bar(width=0.5,position=position_dodge(),stat="identity")+xlab("MPI Platform+Library")+ylab("Time (ms)")+ylim(0,0.5)

p + theme(legend.position=c(0.2,0.8), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
