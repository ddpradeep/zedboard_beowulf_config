#!/usr/bin/Rscript

require(grid);
require(ggplot2);
require(reshape2);
require(grid);

a<-read.csv('network_mpich_vs_openmpi_asynchronous.csv',header=T)
b<-data.frame(a);

c<-read.csv('network_mpich_vs_openmpi_synchronous.csv',header=T)
d<-data.frame(c);

pdf(file="mpi_put_vs_sendrecv.pdf", height=3.5, width=5)

data_0 <- data.frame(x=b$transfer_block_size_bytes,y=b$openmpi_time_taken_ms/2);
data_1 <- data.frame(x=b$transfer_block_size_bytes,y=b$mpich_time_taken_ms/2);
data_2 <- data.frame(x=d$transfer_block_size_bytes,y=d$mpich_time_taken_ms/2);
df <- rbind(data_0,data_1,data_2);
df$dataset <- c(rep("OpenMPI-Put",nrow(data_0)), rep("MPICH-Put",nrow(data_1)), rep("MPICH-SendRecv",nrow(data_2)));

p<-ggplot(data=df,aes(x=x,y=x/(1000*y),linetype=dataset,colour=dataset,shape=dataset))+
	geom_line(size=1.2)+
	scale_colour_hue()+
	scale_x_log10()+ 
	xlab('Transfer Size (bytes)')+ylab('Bandwidth (MB/s)');
#	scale_y_log10()+ scale_x_log10()+ 

p +
theme_bw() +
theme(
      legend.position=c(0.25,0.85),
      legend.title=element_blank(),
      legend.background = element_rect(fill="white", colour=NA),
      legend.key=element_blank(),
      legend.key.width=unit(1.2,"cm"),
      legend.text=element_text(size=10)) +
guides(
       shape=guide_legend(ncol=1)
       ) +
theme(
      plot.background = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank()
      ) +
#draws x and y axis line
theme(axis.line = element_line(color = 'black'));

