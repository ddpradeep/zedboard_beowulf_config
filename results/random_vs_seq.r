#!/usr/bin/Rscript

require(ggplot2);
require(reshape2);

a0<-read.csv('memory_random_access.csv',header=T)
a1<-read.csv('memory_strided_access.csv',header=T)
b0<-data.frame(a0)
b1<-data.frame(a1)

#array-size=65536
c0a=subset(b0,b0[,1]==65536)
c0b=subset(c0a,c0a[,2]<65536);
c0=subset(c0b,c0b[,2]>=4);
#stride-length=4
b1a=subset(b1,b1[,1]==65536);
b1b=subset(b1a,b1a[,2]==0.004);
b1c=subset(b1b,b1b[,3]>=4);
c1=subset(b1c,b1c[,3]<65536);
c1[,2]=NULL

d<-data.frame(c0$no_of_kilo_memory_access, c0$time_taken_ms, c1$no_of_kilo_memory_access, c1$time_taken_ms);
e<-melt(d);

pdf(file="random_vs_seq.pdf", height=3.5, width=5)

#p<-ggplot(data=e,aes(value, variable)) + geom_line() + geom_point();

p<-ggplot(data=d) + geom_line(aes(x=c0.no_of_kilo_memory_access*1e3,y=c0.time_taken_ms*1e-3/c0.no_of_kilo_memory_access*1e-3,color=factor("blue",label="random")))+ geom_point(aes(x=c0.no_of_kilo_memory_access*1e3,y=c0.time_taken_ms*1e-3/c0.no_of_kilo_memory_access*1e-3)) + geom_line(aes(x=c1.no_of_kilo_memory_access*1e3,y=c1.time_taken_ms*1e-3/c1.no_of_kilo_memory_access*1e-3,color=factor("red",label="sequential")))+geom_point(aes(x=c1.no_of_kilo_memory_access*1e3,y=c1.time_taken_ms*1e-3/c1.no_of_kilo_memory_access*1e-3)) + scale_x_log10() + xlab('Number of Memory Accesses')  + ylab('Time (s)');
p + theme(legend.position=c(0.2,0.8), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
