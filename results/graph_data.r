#!/usr/bin/Rscript

require(grid);
require(ggplot2);
require(reshape2);

a<-read.csv('network_scatter_with_graph_data_masterlog.csv',header=T)
x<-read.csv('network_scatter_graph_data_sendrecv.csv',header=T)

b<-data.frame(a)
b<-subset(b,b$mpi_library=="mpich03.1");
b<-subset(b,b$architecture=="ARM");
b<-subset(b,b$scatter_type=="PARALLEL_ALL_MPI");
b<-subset(b,b$reset_cache_before_scatter=="NO");
b<-subset(b,b$measure_local_scatter_separately=="NO");
b<-subset(b,b$fine_grained_puts=="NO");
b<-subset(b,b$overload_by_completing_remote_scatter_node_by_node=="NO");


b1<-data.frame(a)
b1<-subset(b1,b1$mpi_library=="mpich03.1");
b1<-subset(b1,b1$architecture=="ARM");
b1<-subset(b1,b1$scatter_type=="PARALLEL_LOCAL_DIRECT");
b1<-subset(b1,b1$reset_cache_before_scatter=="NO");
b1<-subset(b1,b1$measure_local_scatter_separately=="NO");
b1<-subset(b1,b1$fine_grained_puts=="NO");
b1<-subset(b1,b1$overload_by_completing_remote_scatter_node_by_node=="NO");


b2<-data.frame(a)
b2<-subset(b2,b2$mpi_library=="mpich03.1");
b2<-subset(b2,b2$architecture=="ARM");
b2<-subset(b2,b2$scatter_type=="SEQUENTIAL_ALL_MPI");
b2<-subset(b2,b2$reset_cache_before_scatter=="NO");
b2<-subset(b2,b2$measure_local_scatter_separately=="NO");
b2<-subset(b2,b2$fine_grained_puts=="NO");
b2<-subset(b2,b2$overload_by_completing_remote_scatter_node_by_node=="NO");

b3<-data.frame(a)
b3<-subset(b3,b3$mpi_library=="mpich03.1");
b3<-subset(b3,b3$architecture=="ARM");
b3<-subset(b3,b3$scatter_type=="SEQUENTIAL_ALL_MPI");
b3<-subset(b3,b3$reset_cache_before_scatter=="NO");
b3<-subset(b3,b3$measure_local_scatter_separately=="NO");
b3<-subset(b3,b3$fine_grained_puts=="NO");
b3<-subset(b3,b3$overload_by_completing_remote_scatter_node_by_node=="YES");

c<-data.frame(a)
c<-subset(c,c$mpi_library=="mpich03.1");
c<-subset(c,c$architecture=="ARM");
c<-subset(c,c$scatter_type=="PARALLEL_LOCAL_DIRECT");
c<-subset(c,c$reset_cache_before_scatter=="NO");
c<-subset(c,c$measure_local_scatter_separately=="YES");
c<-subset(c,c$fine_grained_puts=="NO");
c<-subset(c,c$overload_by_completing_remote_scatter_node_by_node=="NO");

pdf(file="graph_data.pdf", height=3.5, width=5)

data_0 <- data.frame(x=b$pe,y=b$total_time_taken_ms);
data_1 <- data.frame(x=b1$pe,y=b1$total_time_taken_ms);
data_3 <- data.frame(x=b3$pe,y=b3$total_time_taken_ms);
data_4 <- data.frame(x=x$pe,y=x$sendrecv_ms);

df <- rbind(data_4,data_1,data_0,data_3);
df$dataset <- c(rep("Raw+scheduling+local_opt+sendrecv",nrow(data_4)),
		rep("Raw+scheduling+local_opt",nrow(data_1)),
		rep("Raw+scheduling",nrow(data_0)), 
		rep("Raw (Unoptimized)",nrow(data_3)));

p<-ggplot(data=df,aes(x=x,y=y,linetype=dataset,colour=dataset,shape=dataset))+
		geom_line(size=1.2)+
	scale_x_log10(breaks=c(1,2,4,8,16,32),labels=c(1,2,4,8,16,32)) + xlab('Nodes') + ylab('Time (ms)');
	
#geom_point(data=b1,aes(x=pe,y=total_time_taken_ms))+ 

p + theme_bw() + theme(legend.position=c(0.6,0.8), legend.key.width=unit(1,"cm"), legend.key=element_blank(), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));

