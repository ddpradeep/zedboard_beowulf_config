#!/usr/bin/Rscript

m<-read.csv('network_mpi_barrier_latency.csv',header=T)

pdf(file="mpi_barrier.pdf", height=3.5, width=5);

plot(x=m$node_size,y=m$max_latency_ms,ylim=c(0,6),xlab="Nodes",ylab="Latency (ms)",yaxt="n");
axis(2,las=1);
lines(x=m$node_size,y=m$avg_latency_ms,col='grey',yaxt="n");
lines(x=m$node_size,y=m$min_latency_ms,col='grey',yaxt="n");
points(x=m$node_size,y=m$max_latency_ms,ylim=c(0,6),yaxt="n");
polygon(c(m$node_size, rev(m$node_size)), c(m$avg_latency_ms, rev(m$min_latency_ms)), col='grey90', border=NA)
points(x=m$node_size,y=m$max_latency_ms,ylim=c(0,6),yaxt="n");

