#!/usr/bin/Rscript

require(lattice);
require(ggplot2);
require(reshape2);

a<-read.csv('network_mpi_latencies_arm_vs_x86.csv',header=T,row.names=1)
b<-data.frame(ARM=a$arm_mpich_latency_ms,x86=a$x86_mpich_latency_ms)
# transpose!
b <- t(b)
colnames(b) <- row.names(a)
b <- data.frame(b)
colnames(b)[1]="MPI_Send/Recv"
colnames(b)[2]="MPI_Put"
colnames(b)[3]="MPI_Get"
colnames(b)[4]="MPI_Barrier"
c<-melt(as.matrix(b))

pdf(file="mpich_arm_vs_x86-permuted.pdf", height=3.5, width=5)

p <- ggplot(c,aes(x=c$Var2,y=c$value,fill=c$Var1))+geom_bar(width=0.5,position=position_dodge(),stat="identity")+xlab("MPI Function")+ylab("Time (ms)")+ylim(0,0.2)

p +
theme_bw() +
theme(
      legend.position=c(0.9,0.8),
      legend.title=element_blank(),
      legend.background = element_rect(fill="white", colour=NA),
      legend.key=element_blank(),
      legend.text=element_text(size=10)) +
guides(
       shape=guide_legend(ncol=1)
       ) +
theme(
      plot.background = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.border = element_blank()
      ) +
#draws x and y axis line
theme(axis.line = element_line(color = 'black'));

