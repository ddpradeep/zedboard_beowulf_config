#!/usr/bin/Rscript

require(lattice);
require(ggplot2);
require(reshape2);

a<-read.csv('network_mpi_latencies_arm_vs_x86.csv',header=T,row.names=1)
b<-data.frame(a$arm_mpich_latency_ms,a$arm_openmpi_latency_ms,a$x86_mpich_latency_ms,a$x86_openmpi_latency_ms)
# transpose!
b <- t(b)
colnames(b) <- row.names(a)
b <- data.frame(b)
colnames(b)[1]="MPI_Send/Recv"
colnames(b)[2]="MPI_Put"
colnames(b)[3]="MPI_Get"
colnames(b)[4]="MPI_Barrier"
c<-melt(as.matrix(b))

pdf(file="mpi_arm_vs_x86-permuted.pdf", height=3.5, width=8)

p <- ggplot(c,aes(x=c$Var2,y=c$value,fill=c$Var1))+geom_bar(width=0.5,position=position_dodge(),stat="identity")+xlab("MPI Function")+ylab("Time (ms)")+ylim(0,0.5)

p + theme_bw() + theme(legend.position=c(0.2,0.8), legend.title=element_blank(),legend.background = element_blank(),legend.text=element_text(size=10));
