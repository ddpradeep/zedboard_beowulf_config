# 1. IMB

## Build
echo "edit src/make_mpich to point to desired mpi installation"
make -f make_mpich

## Run
/opt/mpich-3.1_mx/bin/mpirun -machinefile mpich.hf -np 2 ./IMB-RMA Bidir_Put

# 2. Myrinet Open-MX

## Build
./configure --with-mtu=1500
make -j2
sudo make install
echo "add /opt/open-mx/lib to ld.so.conf.d"
sudo ldconfig

## Run
sudo /opt/open-mx/sbin/omx_init start

# 3. MPICH-MX

## Build
./configure --prefix=/opt/mpich-3.1_perf_mx --with-device=ch3:nemesis:mx --with-mx=/opt/open-mx --enable-fast=O3,ndebug --disable-error-checking --without-timing --without-mpit-pvars
make -j2
sudo make install

# 4. Boost Graph Library

## Build
./bootstrap.sh
sudo ./b2 --with-graph --with-regex install

# 5. Cross compiling gcc for arm

# read this to install gmp, mpfr & mpc -- http://stackoverflow.com/a/9450422
# extract src contents to gcc-4.8.4
mkdir gcc-build
cd gcc-build
../gcc-4.8.4/configure --host=arm-linux-gnueabihf --with-gmp=/opt/arm --with-mpfr=/opt/arm --with-mpc=/opt/arm --enable-long-long --target=arm-linux-gnueabihf --prefix=/opt/gcc-4.8_arm --enable-languages=c,c++
make -j6
sudo make install