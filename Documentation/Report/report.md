## Beowulf Cluster Team ##

### Objective ###

This project has two primary objectives:

-	To build a Beowulf style computer system tailored towards Graph processing, using a distributed cluster of ZedBoards as compute nodes. 
-	To optimize the system for a high performance per watt/cost and compete in the Graph500 list.  

### Background ###

Given that applications dealing with excessively large graph problems typically exhibit very low spatial and temporal locality, traditional CPU-based systems are bottlenecked by the inadequacy of the memory system to provide a high bandwidth of very sparse, fine-grained data accesses. In contrast, such applications can exploit the high-bandwidth and low-latency capabilities of FPGA Block RAMs to experience an order of magnitude speedup. To facilitate the realization and verification of the former claim, the following key technologies are involved.

#### GraphStep ####

GraphStep is a system architecture design for FPGAs which maximizes the memory performance, thereby overcoming the memory wall hurdle faced with conventional microprocessor based implementations. It employs concurrent system architecture to save graph nodes in small distributed on-chip memories in an interconnected FPGA cluster. By connecting multiple FPGAs in a parallel network, individual on-chip memory modules became distributed memories allowing parallel access, thus improve the memory performance.

#### Beowulf Cluster ####

A Beowulf cluster represents a network of commodity-grade computers, typically running open-source software, which allow parallel computing through the usage of parallel processing libraries such as OpenMPI. The setup consists of a single master node which controls other slave nodes, sharing files with them.

#### MPI ####

Message Passing Interface (MPI) is a communication protocol adopted as the de facto standard for parallel programming on distributed systems. It provides synchronization and communication between processes running on a system. As MPI uses language independent specifications for function calls, it not only offers portability, but also caters speed as MPI implementations for a hardware platform is highly optimized (such as OpenMPI for ARMv7).

#### Graph500 Benchmark ####

To address the disparity between architectural capabilities enabled by the underlying technology and the graph applications, a standard tool for assessing performance is required. The Graph500 benchmark describes a benchmarking suite tailored towards graph processing. The benchmark consists of three kernels, viz.:

- **Concurrent Search**: Parallel Breadth First Search (BFS)
- **Optimization Problem**: Single source shortest path
- **Edge Oriented**: Maximal Independent Set

To avoid the results of the benchmark being treated as merely the theoretical limits of the system, data sets exhibiting real life patterns are used.

### Idea ###

As graph algorithms are highly data intensive, memory bandwidth plays a pivotal role in determining the performance of such systems, as when compared to arithmetic performance. Interconnection of FPGAs in a cluster complements the aforementioned characteristic by not only providing a high and scalable aggregated memory bandwidth, but also sustaining intra-node configurations supporting multiple processing engines, thereby maximizing the memory bandwidth usage.  

The performance of the Beowulf cluster is targeted towards exceeding that of Imperial College London’s
GLADOS machine which has achieved a GRAPH500 benchmark score of 1.72 GTEPS. Calculations show that theoretically, using a minimum of 32 ZedBoards in parallel, the proposed system can match the performance of the GLADOS machine.

### Milestones ###

	Week 1		Reading on related works
	Week 2		Planning
	Week 3		Create a basic Beowulf cluster
	Week 4		Micro-benchmarking communication links
	Week 5		Advanced configuration of the Beowulf cluster
	Week 6		<open>
	Week 7		<open>
	Week 8		<open>

### Deliverable ###

**Hardware**	– A completely setup Beowulf cluster of zedboards ready for integration with deliverables from other teams.

**Software**	– A highly configurable script for automating the setup initialization.

**Report**		– A comprehensive report profiling the characteristics of the system.

### Teamwork ###

#### Roles ####

**Pradeep**		- Setup of System, benchmarking inter-node communication

**Han**		- Design, verification, benchmarking intra-node communication

#### Networking ####

As this project serves as the foundation for construction of the desired Graph Machine System, regular liaising with other teams is required for the smooth integration of various components of the system. Thus cross-team interactions will be performed on a regular basis with other teams, to update our understanding on other components and to resolve conflicts which may arise. 

One such example is the necessity to cooperate with the memory component team so as to profile the AXI bus on the ZedBoards. 
