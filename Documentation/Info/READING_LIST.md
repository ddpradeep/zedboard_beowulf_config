1. GraphStep: A System Architecture for Sparse-Graph Algorithms

http://www.ntu.edu.sg/home/nachiket/publications/graphstep_fccm-2006.pdf
http://ic.ese.upenn.edu/abstracts/graphstep_fccm2006.html

2. Spatial Hardware Implementation for Sparse Graph Algorithms in GraphStep

http://www.ntu.edu.sg/home/nachiket/publications/graphstep_taas-2011.pdf

3. Introducing the Graph 500
https://cug.org/5-publications/proceedings_attendee_lists/CUG10CD/pages/1-program/final_program/CUG10_Proceedings/pages/authors/11-15Wednesday/14C-Murphy-paper.pdf

4. Setting up a Beowulf Cluster Using OpenMPI on Linux
http://lsi.vc.ehu.es/pablogn/docencia/AS1213/Act5%20Paralelismo/BeowulfClusterMPI.pdf
