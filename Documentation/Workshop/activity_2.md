## Programming Task ##

Programming task can be split into the following subtasks:

- Benchmark the latencies and throughputs for MPI communication calls

- Create linear and parallelizable version of compute intensive codes of varying data sizes to compare performance.

## Portion of code to edit ##

All portions of the code has to be edited as it's a project to be created from scratch

## Code Organization ##

Code changes should be organized into separate files, with support for templates whenever possible.

## Verification ##

Compare outputs with pre-generated reference outputs
