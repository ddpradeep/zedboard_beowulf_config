**Project** : Beowulf Cluster Benchmarking and MPI files

## Code Organization ##

At the current stage, the code is self contained inside a single file. Future works are expected to split the functionaility into multiple files, as the complexity grows.

## Make File ##

The Makefile has options to create pralelizable code and linear code.

## Run Requirements ##

A properly setup beowulf cluster is required.

## Failure Modes ##

- Run time failure - due to MPI installation related issues or broken connectivity with other nodes.