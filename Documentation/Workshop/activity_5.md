% GRAPH BEOWULF
% Han Jianglei, Pradeep
% 21 February 2014

##Dependencies

Graph Beowulf team requires  interaction with Graph_memory


##Benchmark

- For characterising AXI-HP interface
- For characterising AXI-ACP interface

# API 

## send/receive a specific data
* `void send_data_to_pl(*src, *dst,	uint size)`	
	- (pointer to data in memory, pointer to data in PL logic, data size to transfer (bytes))

* `void send_data_from_pl(*src, *dst,	uint size)`
	- (pointer to data in PL logic, pointer to data in memory, data size to receive (bytes))


## send/receive random data


* `void send_rand_data_to_pl( uint size)`
	- (data size to transfer (bytes))

* `void send_rand_data_from_pl( uint size)`
	- (data size to receive (bytes))

