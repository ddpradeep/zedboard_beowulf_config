##### MPI LIBRARY ######
PATH_MPI=/opt/openmpi-1.8_mt
##### BLAS LIBRARY #####
PATH_BLAS=/opt/openblas
########################

export PATH=$PATH:$PATH_MPI/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_MPI/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PATH_BLAS/lib

./sample-blas.o
