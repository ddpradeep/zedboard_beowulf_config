Refer to the [link](http://yarvard.sce.ntu.edu.sg/jira/browse/PSOCPRJ-1) to push to yarvard

# Beowulf Cluster #

* You will learn
	- How HPC-like systems can be built cheaply
	- Handle networking/power/data-distribution across parallel nodes in a cluster
* Tasks
	- Setup Beowulf across 4 Zedboards
	- Run OpenMPI applications
	- Interface with the Graph SoC C++ model

## For Pushing to Yarvard ##

git remote add yarvard ssh://git@yarvard.sce.ntu.edu.sg:7999/ce4054p/graph_beowulf.git
git pull yarvard master
git push -u yarvard


